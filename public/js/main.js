$("#btn-like").click(function() {
    let id = $(this).attr('data-id');
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        type: "POST",
        url: "/favorite/store",
        data: {data: id, type: 'like'},
        success: function (response) {
            if (response.status == 'successfully') {
                $("#favoriteModal").modal('show');
                $('#btn-like').addClass('active');
            }else if(response.status == 'unlike') {
                $("#unlikeModal").modal('show');
                $('#btn-like').removeClass('active');
            }

            return false;
        },
        error: function (response) {
            //
        }
    });
});



$("#btn-favorite").click(function() {
    let id = $(this).attr('data-id');
    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        type: "POST",
        url: "/favorite/store",
        data: {data: id},
        success: function (response) {
            console.log(response);
            if (response.status == 'successfully') {
                $("#exampleModal").modal('show');
                $('#btn-favorite').addClass('active');
            }else if(response.status == 'unfavorite') {
                $("#unfavoriteModal").modal('show');
                $('#btn-favorite').removeClass('active');
            }
            return false;
        },
        error: function (response) {
            //
        }
    });
});


$("#btn-modal-email-confirm").click(function() {
    let id = $('input[name="data-id"]').val();
    let email = $("#confirm_send_mail").val();

    var token = $('meta[name="csrf-token"]').attr('content');
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        type: "POST",
        url: "/send-mail-post",
        data: {id, email},
        success: function (response) {
            console.log(response);
            if(response.status == 'success') {
                $("#modalEmailConfirm").modal('hide');
                $("#modalAlertSuccessEamil").modal('show');
            }
            return false;
        },
        error: function (response) {
            //
        }
    });
})