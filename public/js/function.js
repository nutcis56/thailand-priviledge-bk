$(function(){
    $(window).on("scroll", function (t) {
        $(this).scrollTop() > 30 ? $(".header").addClass("header--sticky") : $(".header").removeClass("header--sticky");
    });
});

$(function(){	

    $(".dropdown--navbar").on("show.bs.dropdown", function (t) {
            $(".header__main").addClass("header--dropdown-active"), $(t.relatedTarget).addClass("show");
    }),
    $(".dropdown--navbar").on("hide.bs.dropdown", function (t) {
            $(".header__main").removeClass("header--dropdown-active"), $(t.relatedTarget).removeClass("show");
    });
});

$(function(){	
    var headerTop = document.getElementById('header__top').offsetHeight;
    var headerMain = document.getElementById('header__main').offsetHeight;
    var headerHeight = headerTop + headerMain - 2;
    // $(".header .header--lg .header__main .header__right .navbar--main-desktop .dropdown--navbar .dropdown-menu").css("top", headerHeight);
});

$(function(){
    $('.dropdown--navbar .dropdown-menu .menu__main ul li:has( > ul)').addClass('menu-dropdown-icon');
});

$(function(){
    $(".hamburger").click(function(e) {
            $(this).toggleClass("btn-open");
            $("body").toggleClass("nav-open");
    });

    $(".menu-close").click(function(e){
            $("body").removeClass("nav-open");
            $('.navWrapper__content .menu__main--sm ul li a').parent().find('ul').hide();
            $('.navWrapper__content .menu__main--sm ul li').removeClass('active')
    });
});

$(function(){
    $('.menu__main--sm ul li:has( > ul)').addClass('menu-dropdown-icon');
    //Checks if li has sub (ul) and adds class for toggle icon - just an UI
    var nav_i = $('.menu__main--sm ul li a');
    nav_i.parent().find('ul').hide();
    nav_i.on('click', function(){
            $(this).parent('li').toggleClass('active').siblings().removeClass('active');
            $(this).parent().find('ul').first().fadeToggle(500);
            $(this).parent().siblings().find('ul').fadeOut(400);
    });
});

$(function() {
    AOS.init({
            duration: 1200,
    })
});

$(function(){
    const $tabsToDropdown = $(".tab--dropdown");
    function generateDropdownMarkup(container) {
    const $navWrapper = container.find(".tabs--wrapper");
    const $navPills = container.find(".nav");
    const firstTextLink = $navPills.find("li:first-child a").text();
    const $items = $navPills.find("li");
    const markup = `
        <div class="dropdown d-md-none">
        <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            ${firstTextLink}
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> 
            ${generateDropdownLinksMarkup($items)}
        </div>
        </div>
    `;
    $navWrapper.prepend(markup);
    }

    function generateDropdownLinksMarkup(items) {
    let markup = "";
    items.each(function () {
        const textLink = $(this).find("a").text();
        markup += `<a class="dropdown-item" href="#">${textLink}</a>`;
    });

    return markup;
    }

    function showDropdownHandler(e) {
    // works also
    //const $this = $(this);
    const $this = $(e.target);
    const $dropdownToggle = $this.find(".dropdown-toggle");
    const dropdownToggleText = $dropdownToggle.text().trim();
    const $dropdownMenuLinks = $this.find(".dropdown-menu a");
    const dNoneClass = "d-none";
    $dropdownMenuLinks.each(function () {
        const $this = $(this);
        if ($this.text() == dropdownToggleText) {
        $this.addClass(dNoneClass);
        } else {
        $this.removeClass(dNoneClass);
        }
    });
    }

    function clickHandler(e) {
    e.preventDefault();
    const $this = $(this);
    const index = $this.index();
    const text = $this.text();
    $this.closest(".dropdown").find(".dropdown-toggle").text(`${text}`);
    $this
        .closest($tabsToDropdown)
        .find(`.nav li:eq(${index}) a`)
        .tab("show");
    }

    function shownTabsHandler(e) {
    // works also
    //const $this = $(this);
    const $this = $(e.target);
    const index = $this.parent().index();
    const $parent = $this.closest($tabsToDropdown);
    const $targetDropdownLink = $parent.find(".dropdown-menu a").eq(index);
    const targetDropdownLinkText = $targetDropdownLink.text();
    $parent.find(".dropdown-toggle").text(targetDropdownLinkText);
    }

    $tabsToDropdown.each(function () {
    const $this = $(this);
    const $pills = $this.find('a[data-toggle="pill"]');

    generateDropdownMarkup($this);

    const $dropdown = $this.find(".dropdown");
    const $dropdownLinks = $this.find(".dropdown-menu a");

    $dropdown.on("show.bs.dropdown", showDropdownHandler);
    $dropdownLinks.on("click", clickHandler);
    $pills.on("shown.bs.tab", shownTabsHandler);
    });
});

function a(t, e) {
        for (var i = 0; i < e.length; i++) {
            var n = e[i];
            n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n)
        }
}

var o = function() {
        function t() {
            ! function(t, e) {
                if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function")
            }(this, t)
        }
        var e, i, n;
        return e = t, n = [{
            key: "init",
            value: function() {
                function t() {
                    var t = $(window).innerHeight();
                    $("html").css("--rwd-full-height", "".concat(t, "px"))
                }
                t(), $(window).resize((function() {
                    t()
                }))
            }
        }], (i = null) && a(e.prototype, i), n && a(e, n), t
}();