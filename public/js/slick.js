$(function(){
    $('.slick--hero-banner').slick({
        autoplay: true,
        arrows: false,
        dots: true,
        speed: 1500,
        autoplaySpeed: 3000,
        adaptiveHeight: true,
    });

    $('.slick--hightlight-menu').slick({
        autoplay: false,
        arrows: true,
        dots: false,
        speed: 1500,
        autoplaySpeed: 3000,
        slidesToShow: 3,
        adaptiveHeight: true,
        responsive: [
            {
            breakpoint: 960,
            settings: {
                slidesToShow: 2
            }
            },
            {
            breakpoint: 480,
            settings: {
                arrows: false,
                slidesToShow: 1
            }
            }
        ]
    });

    $('.slick--member-menu').slick({
        autoplay: true,
        arrows: true,
        dots: false,
        speed: 1500,
        autoplaySpeed: 3000,
        slidesToShow: 5,
        adaptiveHeight: true,
        responsive: [
            {
            breakpoint: 960,
                settings: {
                    slidesToShow: 3
                }
            },
            {
            breakpoint: 480,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });

    $('.slick--knowledge').slick({
        autoplay: false,
        arrows: true,
        dots: false,
        speed: 1500,
        autoplaySpeed: 3000,
        slidesToShow: 3,
        adaptiveHeight: true,
        responsive: [
            {
            breakpoint: 960,
            settings: {
                slidesToShow: 2
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
            }
        ]
    });

    $('.slick--news').slick({
        autoplay: false,
        arrows: true,
        dots: false,
        speed: 1500,
        autoplaySpeed: 3000,
        slidesToShow: 3,
        adaptiveHeight: true,
        responsive: [
            {
            breakpoint: 960,
            settings: {
                slidesToShow: 2
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
            }
        ]
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('.slick--news').slick('setPosition');
    });

    $('.slick--course').slick({
        autoplay: true,
        arrows: true,
        dots: false,
        speed: 1500,
        autoplaySpeed: 3000,
        slidesToShow: 3,
        adaptiveHeight: true,
        responsive: [
            {
            breakpoint: 960,
            settings: {
                slidesToShow: 2
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1
            }
            }
        ]
    });
})