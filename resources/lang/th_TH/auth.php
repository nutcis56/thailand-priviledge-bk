<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'ข้อมูลไม่ถูกต้อง',
    'password' => 'กรุณากรอกรหัสผ่าน',
    'throttle' => 'ล็อคอินเกินจำนวนครั้งที่กำหนด กรุณารอ :seconds วินาที',
    'failed_session' => 'บัญชีนี้กำลังถูกใช้งานบนอุปกรณ์อื่น',

];
