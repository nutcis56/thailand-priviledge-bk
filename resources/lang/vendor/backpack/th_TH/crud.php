<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Backpack Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the CRUD interface.
    | You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    // Forms
    'save_action_save_and_new'         => 'บันทึก และเพิ่มใหม่',
    'save_action_save_and_edit'        => 'บันทึก และแก้ไขต่อ',
    'save_action_save_and_back'        => 'บันทึก และย้อนกลับ',
    'save_action_save_and_preview'     => 'บันทึก และดูตัวอย่าง',
    'save_action_changed_notification' => 'Default behaviour after saving has been changed.',

    // Create form
    'add'                 => 'เพิ่ม',
    'back_to_all'         => 'ย้อนกลับ ',
    'cancel'              => 'ยกเลิก',
    'add_a_new'           => 'เพิ่มใหม่ ',

    // Edit form
    'edit'                 => 'แก้ไข',
    'save'                 => 'บันทึก',

    // Translatable models
    'edit_translations' => 'การแปลภาษา',
    'language'          => 'ภาษา',

    // CRUD table view
    'all'                       => 'ทั้งหมด ',
    'in_the_database'           => 'ในฐานข้อมูล',
    'list'                      => 'รายการ',
    'reset'                     => 'รีเซ็ต',
    'actions'                   => 'การกระทำ',
    'preview'                   => 'ตัวอย่าง',
    'delete'                    => 'ลบ',
    'admin'                     => 'แดชบอร์ด',
    'details_row'               => 'This is the details row. Modify as you please.',
    'details_row_loading_error' => 'There was an error loading the details. Please retry.',
    'clone'                     => 'โคลน',
    'clone_success'             => '<strong>Entry cloned</strong><br>A new entry has been added, with the same information as this one.',
    'clone_failure'             => '<strong>Cloning failed</strong><br>The new entry could not be created. Please try again.',

    // Confirmation messages and bubbles
    'delete_confirm'                              => 'ลบรายการนี้ ใช่หรือไม่?',
    'delete_confirmation_title'                   => 'รายการถูกลบแล้ว',
    'delete_confirmation_message'                 => 'รายการถูกลบเรียบร้อยแล้ว',
    'delete_confirmation_not_title'               => 'NOT deleted',
    'delete_confirmation_not_message'             => "There's been an error. Your item might not have been deleted.",
    'delete_confirmation_not_deleted_title'       => 'Not deleted',
    'delete_confirmation_not_deleted_message'     => 'Nothing happened. Your item is safe.',

    // Bulk actions
    'bulk_no_entries_selected_title'   => 'ไม่ได้เลือกรายการ',
    'bulk_no_entries_selected_message' => 'Please select one or more items to perform a bulk action on them.',

    // Bulk delete
    'bulk_delete_are_you_sure'   => 'Are you sure you want to delete these :number entries?',
    'bulk_delete_sucess_title'   => 'Entries deleted',
    'bulk_delete_sucess_message' => ' items have been deleted',
    'bulk_delete_error_title'    => 'Delete failed',
    'bulk_delete_error_message'  => 'One or more items could not be deleted',

    // Bulk clone
    'bulk_clone_are_you_sure'   => 'Are you sure you want to clone these :number entries?',
    'bulk_clone_sucess_title'   => 'Entries cloned',
    'bulk_clone_sucess_message' => ' items have been cloned.',
    'bulk_clone_error_title'    => 'Cloning failed',
    'bulk_clone_error_message'  => 'One or more entries could not be created. Please try again.',

    // Ajax errors
    'ajax_error_title' => 'Error',
    'ajax_error_text'  => 'ไม่สามารถโหลดรายการได้ กรุณารีเฟรชหน้านี้แล้วลองอีกครั้ง',

   // DataTables translation
    'emptyTable'     => 'ไม่มีข้อมูล',
    'info'           => 'รายการที่ _START_ ถึง _END_ จาก _TOTAL_ รายการ',
    'infoEmpty'      => 'ไม่มีข้อมูล',
    'infoFiltered'   => '(filtered from _MAX_ total entries)',
    'infoPostFix'    => '.',
    'thousands'      => ',',
    'lengthMenu'     => '_MENU_ จำนวนรายการต่อหน้า',
    'loadingRecords' => 'กำลังโหลด...',
    'processing'     => 'กำลังประมวลผล...',
    'search'         => 'ค้นหา',
    'zeroRecords'    => 'ไม่พบรายการ',
    'paginate'       => [
        'first'    => 'หน้าแรก',
        'last'     => 'หน้าสุดท้าย',
        'next'     => 'ถัดไป',
        'previous' => 'ก่อนหน้า',
    ],
    'aria' => [
        'sortAscending'  => ': activate to sort column ascending',
        'sortDescending' => ': activate to sort column descending',
    ],
    'export' => [
        'export'            => 'Export',
        'copy'              => 'Copy',
        'excel'             => 'Excel',
        'csv'               => 'CSV',
        'pdf'               => 'PDF',
        'print'             => 'Print',
        'column_visibility' => 'Column visibility',
    ],

    // global crud - errors
    'unauthorized_access' => 'การเข้าถึงโดยไม่ได้รับอนุญาต - คุณไม่มีสิทธิดูหน้านี้',
    'please_fix'          => 'กรุณาแก้ไขข้อผิดพลาด:',

    // global crud - success / error notification bubbles
    'insert_success' => 'เพิ่มรายการใหม่สำเร็จ',
    'update_success' => 'รายการถูกแก้ไขสำเร็จ',
    'delete_success' => 'รายการถูกลบสำเร็จ',

    // CRUD reorder view
    'reorder'                      => 'การเรียงลำดับ',
    'reorder_text'                 => 'ใช้ drag&drop เพื่อเรียงลำดับ',
    'reorder_success_title'        => 'สำเร็จ',
    'reorder_success_message'      => 'การเรียงลำดับ สำเร็จ',
    'reorder_error_title'          => 'Error',
    'reorder_error_message'        => 'การเรียงลำดับ ไม่สำเร็จ',

    // CRUD yes/no
    'yes' => 'ใช่',
    'no'  => 'ไม่ใช่',

    // CRUD filters navbar view
    'filters'        => 'การกรอง',
    'toggle_filters' => 'เลือกการกรอง',
    'remove_filters' => 'ลบการกรอง',
    'apply' => 'นำไปใช้',

    //filters language strings
    'today' => 'วันนี้',
    'yesterday' => 'เมื่อวาน',
    'last_7_days' => '7 วันล่าสุด',
    'last_30_days' => 'ล่าสุด 30 วัน',
    'this_month' => 'เดือนนี้',
    'last_month' => 'เดือนที่แล้ว',
    'custom_range' => 'กำหนดเอง',
    'weekLabel' => 'W',

    // Fields
    'browse_uploads'            => 'เลือกรายการอัพโหลด',
    'select_all'                => 'Select All',
    'select_files'              => 'Select files',
    'select_file'               => 'Select file',
    'clear'                     => 'เคลียร์',
    'page_link'                 => 'Page link',
    'page_link_placeholder'     => 'http://example.com/your-desired-page',
    'internal_link'             => 'Internal link',
    'internal_link_placeholder' => 'Internal slug. Ex: \'admin/page\' (no quotes) for \':url\'',
    'external_link'             => 'External link',
    'choose_file'               => 'Choose file',
    'new_item'                  => 'New Item',
    'select_entry'              => 'Select an entry',
    'select_entries'            => 'Select entries',

    //Table field
    'table_cant_add'    => 'Cannot add new :entity',
    'table_max_reached' => 'Maximum number of :max reached',

    // File manager
    'file_manager' => 'ตัวจัดการไฟล์',

    // InlineCreateOperation
    'related_entry_created_success' => 'Related entry has been created and selected.',
    'related_entry_created_error' => 'Could not create related entry.',

    // returned when no translations found in select inputs
    'empty_translations' => '(ไม่พบ)',
];
