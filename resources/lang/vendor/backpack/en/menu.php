<?php

return [
    'dashboard' => 'Dashboard',

    'knowledge-base' => 'Knowledge Base',
    'blog' => 'Blog',
    'q-and-a' => 'Q&A',
    'knowledge-idea-sharing-blog' => 'Knowledge Idea Sharing Blog',
    'part1' => 'ฝ่าย 1',
    'download' => 'Download',
    'part2' => 'ฝ่าย 2',
    'part3' => 'ฝ่าย 3',
    'your-job-knowledge-blog' => 'Your Job Knowledge Blog',
    'member-relations' => 'Member Relations',
    'sales' => 'Sales',
    'agent' => 'Agent',
    'government-relation' => 'Government Relation',
    'hr' => 'HR',
    'corporate-communication' => 'Corporate Communication',
    'tpc-sharing-gallery' => 'TPC Sharing Gallery',
    'cat1' => 'หมวด 1',
    'cat2' => 'หมวด 2 ',
    'cat3' => 'หมวด 3',
    'tpc-sharing-vdo' => 'TPC Sharing VDO',

    'open-online-course' => 'Open online course',
    'course' => 'Course',
    'quiz-and-test' => 'Quiz&Test',

    'member-service' => 'Member Service',
    'member-contact-center' => 'Member Contact Center',
    'elite-personal-liaison' => 'Elite Personal Liaison',
    'elite-personal-assistant' => 'Elite Personal Assistant',
    'vendor' => 'Vendor',
    'government-relation' => 'Government Relation',

    'news' => 'News',
    'news-release' => 'Release',
    'news-announcement' => 'Announcement',
    'news-calender' => 'Calendar',
    'news-event-and.activity' => 'Event&Activity',

    'about-company' => 'About Company',
    'company-structure' => 'Structure',
    'line-of-work' => 'Line of work',
    'part' => 'Part',
    'department' => 'Department',
    'vision' => 'Vision',
    'mission' => 'Mission',
    'policy' => 'Policy',

    'data-manage' => 'Data Management',
    'category' => 'Category',

    'admin-menu' => 'Admin Menu',
    'admin-member' => 'Admin Member',
    'admin-role' => 'Role Manager',
    'system-setting' => 'System Setting',
    'user-position' => 'User Position',
    'phone_directory' => 'PHONE DIRECTORY',

    'theme-color' => 'Theme Color Setting',

    'core-config-menu' => 'Menu Config',
    'core-config-big-banner' => 'Big Banner Config',
    'file-manager' => 'File Manager',
    'core-config-service-link' => 'Service Link Config',
    'core-config-icon-link' => 'Icon Link Config',
    'post-manager' => 'Post Manager',
    'log-all' => 'All Logs',
    'log-admin' => 'Administrator Logs',

    'force-sync' => 'Force sync',

];
