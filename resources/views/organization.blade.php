@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;
?>

@push("css")
<style>
    .member_hidden {
        display: none !important;
    }
</style>
@endpush

@section("content")

<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        {{-- <div class="search--wrapper">
                            <form action="{{ route('memberIndex') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น" value="{{ $keyword ?? 'คำค้น' }}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category_search">
                                                <option value="" selected="">หมวดหมู่</option>   
                                                @if(count($category) > 0)
                                                    @foreach($category as $key => $value)
                                                        <option value="{{ $value->name }}" {{ ($categorySelecred == $value->name) ? 'selected' : ''  }}>{{$value->name}}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="">                                                
                                                <option value="Tag" selected="">Tag</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div> --}}
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">โครงสร้างองค์กร</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="member--wrapper">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="member__aside">
                                        <h3 class="member__title">
                                            โครงสร้างองค์กร
                                        </h3>
                                        <ul class="member__nav">
                                            @if(count($lists) > 0)
                                                @foreach($lists as $key => $list)
                                                    <li class="member__item {{ $type == "contact_center" ? 'active' : '' }}">
                                                        <a  class="member__link" data-toggle="collapse" href="#multiCollapseExample{{$list->id}}" role="button" aria-expanded="false" aria-controls="multiCollapseExample{{$list->id}}">
                                                            {{ $list->name ?? '' }}
                                                        </a>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="collapse multi-collapse " id="multiCollapseExample{{$list->id}}" >
                                                                    <ul class="ml-5" style="list-style-type:disc">
                                                                        @if(count($list["section"]) > 0)
                                                                            @foreach($list["section"] as $keySection => $section)
                                                                                <li class="mb-2"><a href="{{route('organization', ['id' => $list->parent_id, 'data' => $section->id, 'type' => $list->name])}}">{{ $section->name ?? '' }}</a></li>
                                                                            @endforeach
                                                                        @endif
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-8">

                                    @if(!empty($data))
                                        <div class="member__content">
                                            {{-- <div class="text-left">
                                                <a href="#" class="btn btn--add-primary">
                                                    <span class="text__link">{{ $data->name ?? '' }}</span>
                                                    <span class="icon__link">
                                                       
                                                    </span>
                                                </a>
                                            </div> --}}
                                            <div class="member__title" style="font-size: 2.5rem">
                                                {{ $data->name ?? '' }}
                                            </div>
                                            <div class="question--list">
                                                {!! $data->detail ?? '' !!}
                                            </div>
                                        </div>

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
@push("scripts")
<script>
   
</script>
@endpush