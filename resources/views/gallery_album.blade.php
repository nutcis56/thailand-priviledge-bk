@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>
@push("css")
<style>
    .knowledge_hidden {
        display: none;
    }
    .knowledge_hidden2 {
        display: none;
    }
</style>
@endpush

@section("content")
<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{ route('galleryAlbumIndex') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น" value="{{ (isset($keyword)) ? ($keyword != '') ? $keyword : '' : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-4">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" value="{{ ($keywordTag != '') ? $keywordTag : '' }}" class="form-control search__input" name="searchTag" placeholder="คำค้นแท็ก">
                                        </div>
                                    </div> --}}
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer mb-md-0">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item">คลังความรู้</li>
                                    <li class="breadcrumb-item active"><a href="{{ route('galleryAlbumIndex') }}" class="breadcrumb-link">คลังภาพ</a></li>
                                </ol>
                            </nav>
                        </div>
                        <div class="gallery--wrapper">
                            <div class="tabs--views">
                                <ul class="nav nav-tabs d-flex justify-content-end" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="RealEstateTab" data-toggle="tab" href="#RealEstate" role="tab" aria-controls="RealEstate" aria-selected="true"><span class="icon-list-view"></span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="ConstructionTab" data-toggle="tab" href="#Construction" role="tab" aria-controls="Construction" aria-selected="false"><span class="icon-table-view"></span></a>
                                    </li>
                                </ul>


                                <div class="tab-content" id="operationTab">
                                    <div class="tab-pane fade show active" id="RealEstate" role="tabpanel" aria-labelledby="RealEstateTab">
                                        <div class="news--list">
                                            <div class="card-deck card-deck--cards-1 card-deck--cards-md-2 card-deck--cards-lg-3">

                                                @if(!empty($lists))
                                                    @foreach($lists as $key => $list)
                                                        @php
                                                            $date = date_format($lists[0]->created_at, 'd.m.Y');
                                                        @endphp

                                                        @if($key <= 8)
                                                            <a href="/gallery-list/{{ $list->post_name != "" ? $list->post_name : $list->id }}" class="card card--knowledge">
                                                                <figure class="card__figure">
                                                                    <img src="{{$list->post_cover ?? asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in">
                                                                </figure>
            
                                                                <div class="card__body">
                                                                    <div class="card__type">
                                                                        {{ $list->position->name ?? "" }}
                                                                    </div>
                                                                    <h3 class="card__title text-uppercase">
                                                                        {{$list->post_title ?? ""}}
                                                                    </h3>
                                                                    <ul class="list-group">
                                                                        <li class="list-group-item"><i class="icon-date"></i> {{$date}}</li>
                                                                        <li class="list-group-item"><i class="icon-create"></i> {{ $list->userInfo->name ?? '' }} </li>
                                                                        <li class="list-group-item"><i class="icon-view"></i> {{ $list->visitlog_count ?? 0 }}</li>
                                                                        <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$list->like_count}}</li>
                                                                        @if($list->statusFavorite == 'active')
                                                                            <li class="list-group-item"><i class="fas fa-heart"></i></li>
                                                                        @else 
                                                                            <li class="list-group-item"><i class="far fa-heart"></i></li>
                                                                        @endif
                                                                    </ul>
                                                                </div>
                                                            </a>
                                                        @else 
                                                            <a href="/gallery-list/{{ $list->post_name != "" ? $list->post_name : $list->id }}" class="card card--knowledge knowledge_hidden">
                                                                <figure class="card__figure">
                                                                    <img src="{{$list->post_cover ?? asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in">
                                                                </figure>
            
                                                                <div class="card__body">
                                                                    <div class="card__type">
                                                                        {{ $list->position->name ?? "" }}
                                                                    </div>
                                                                    <h3 class="card__title text-uppercase">
                                                                        {{$list->post_title ?? ""}}
                                                                    </h3>
                                                                    <ul class="list-group">
                                                                        <li class="list-group-item"><i class="icon-date"></i> {{$date}}</li>
                                                                        <li class="list-group-item"><i class="icon-create"></i> {{ $list->userInfo->name ?? '' }} </li>
                                                                        <li class="list-group-item"><i class="icon-view"></i> {{ $list->visitlog_count ?? 0 }}</li>
                                                                        <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$list->like_count}}</li>
                                                                        @if($list->statusFavorite == 'active')
                                                                            <li class="list-group-item"><i class="fas fa-heart"></i></li>
                                                                        @else 
                                                                            <li class="list-group-item"><i class="far fa-heart"></i></li>
                                                                        @endif
                                                                    </ul>
                                                                </div>
                                                            </a>
                                                        @endif
                                                    
                                                    @endforeach

                                                @endif
                                                
                                                
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <a href="#" class="btn btn--loadmore" id="btn_loadmore">
                                                <span class="text__link">โหลดเพิ่มเติม</span>
                                            </a>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" id="Construction" role="tabpanel" aria-labelledby="ConstructionTab">
                                        <div class="content__wrapper p-0">
                                            <div class="table-responsive">
                                                <table class="table table--secondary">
                                                    <thead>
                                                        <tr>
                                                        <th scope="col"><i class="icon-file"></i> Image</th>
                                                        <th scope="col"> Name</th>
                                                        <th scope="col">Modified</th>
                                                        <th scope="col">Modified by</th>
                                                        <th scope="col">File size</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        @if(!empty($lists))
                                                            @foreach($lists as $key => $list)
                                                                @php
                                                                    $date = date_format($lists[0]->created_at, 'd.m.Y');
                                                                @endphp

                                                                @if($key <= 8)
                                                                    <tr>
                                                                        <th scope="row">
                                                                            <a href="/gallery-table/{{ $list->post_name != "" ? $list->post_name : $list->id }}" class="video--cover">
                                                                                <img src="{{$list->post_cover ?? asset('images/no_image.png')}}" alt="" class="img-fluid">
                                                                            </a>
                                                                        </th>
                                                                        <td>
                                                                            <a href="#" class="video--cover">
                                                                                <i class="icon-folder"></i> {{$list->post_title ?? ""}}
                                                                            </a>
                                                                        </td>
                                                                        <td>{{$date ?? ""}}</td>
                                                                        <td>{{ $list->userInfo->name ?? '' }}</td>
                                                                        <td>2 mb</td>
                                                                    </tr>
                                                                @else 
                                                                    <tr class="knowledge_hidden2">
                                                                        <th scope="row">
                                                                            <a href="/gallery-table/{{ $list->post_name != "" ? $list->post_name : $list->id }}" class="video--cover">
                                                                                <img src="{{$list->post_cover ?? asset('images/no_image.png')}}" alt="" class="img-fluid">
                                                                            </a>
                                                                        </th>
                                                                        <td>
                                                                            <a href="#" class="video--cover">
                                                                                <i class="icon-folder"></i> {{$list->post_title ?? ""}}
                                                                            </a>
                                                                        </td>
                                                                        <td>{{$date ?? ""}}</td>
                                                                        <td>{{ $list->userInfo->name ?? '' }}</td>
                                                                        <td>2 mb</td>
                                                                    </tr>
                                                                @endif
                                                                
                                                            @endforeach
                                                        @endif
                                                        
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="text-center">
                                                <a href="#" class="btn btn--loadmore" id="btn_loadmore2">
                                                    <span class="text__link">โหลดเพิ่มเติม</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@push("scripts")
    <script>
        $(document).ready(function(){
            $("#btn_loadmore").click(function(){
                $("a").removeClass("knowledge_hidden");
                $(this).fadeOut();
            });
            $("#btn_loadmore2").click(function(){
                $("tr").removeClass("knowledge_hidden2");
                $(this).fadeOut();
            });
        })
    </script>
@endpush