@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>
@section("content")

@push("css")


<style>
    .knowledge_hidden {
        display: none !important;
    }
</style>
@endpush

@php 

$tags = CoreConfigService::getTag();
@endphp

<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{ route('searchResult') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" value="{{ ($keyword != '') ? $keyword : '' }}" class="form-control search__input" name="search" placeholder="คำค้น">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group mb-3 mb-lg-0">
                                            {{-- <input type="text" value="{{ ($keywordTag != '') ? $keywordTag : '' }}" class="form-control search__input" name="searchTag" placeholder="คำค้นแท็ก"> --}}
                                            <select class="js-example-basic-single form-control" name="searchTag">
                                                <option value="">แท๊ก</option>
                                                @if(count($tags) > 0)
                                                    @foreach($tags as $key => $value)
                                                        <option value="{{ $value->name }}" {{ $value->name == $keywordTag ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">ผลการค้นหา</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="search--result">
                            <div class="search__header">
                                <h1 class="title">ผลการค้นหา : {{ $keyword ?? '' }}</h1>
                                <p class="text">ค้นพบ {{count($lists)}} รายการ จากการค้นหา “{{ $keyword }}”</p>
                            </div>

                            <div class="search__body">

                                @if(count($lists) > 0)
                                    @foreach($lists as $key => $list)
                                        @if($key <= 4)
                                            <div class="card--search">
                                                <a href="{{ route('knowledgeDetail', ['slug' => $list->id]) }}" class="card__title">
                                                    {{ $list->post_title ?? '' }}
                                                </a>
                                                <div class="card__text">
                                                    {{ $list->post_excerpt ?? '' }}
                                                </div>
                                                <div class="card__date">
                                                    <div class="date">
                                                        {{ $list->post_date ?? '' }}
                                                    </div>
                                                    <div class="time">
                                                        {{ $list->post_date ?? '' }}
                                                    </div>
                                                </div>
                                            </div>
                                        @else 
                                            <div class="card--search knowledge_hidden">
                                                <a href="{{ route('knowledgeDetail', ['slug' => $list->id]) }}" class="card__title">
                                                    {{ $list->post_title ?? '' }}
                                                </a>
                                                <div class="card__text">
                                                    {{ $list->post_excerpt ?? '' }}
                                                </div>
                                                <div class="card__date">
                                                    <div class="date">
                                                        {{ $list->post_date ?? '' }}
                                                    </div>
                                                    <div class="time">
                                                        {{ $list->post_date ?? '' }}
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                            
                                @endif
                                

                            </div>

                            <div class="text-center">
                                <a href="#" class="btn btn--loadmore" id="btn_loadmore">
                                    <span class="text__link">โหลดเพิ่มเติม</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@push("scripts")

<script>
    $(document).ready(function(){
        $("#btn_loadmore").click(function(){
            $("div").removeClass("knowledge_hidden");
            $(this).fadeOut();
        });

        // $('.tags').select2();
    })
</script>
@endpush