@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>

@push("css")
<style>
    .knowledge_hidden {
        display: none;
    }
</style>
@endpush

@section("content")


<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{ route('videoAlbumIndex') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น" value="{{ (isset($keyword)) ? ($keyword != '') ? $keyword : '' : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-4">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="">                                                
                                                <option value="Tag" selected="">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer mb-md-0">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item">คลังความรู้</li>
                                    <li class="breadcrumb-item active"><a href="{{ route('videoAlbumIndex') }}" class="breadcrumb-link">คลังวีดีโอ</a></li>
                                    <li class="breadcrumb-item">TCP Video Table</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="gallery--wrapper">
                            <div class="tabs--views">
                                <ul class="nav nav-tabs d-flex justify-content-end" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link" href="/video-list/{{ $lists->post_name != "" ? $lists->post_name : $lists->id }}"><span class="icon-list-view"></span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="icon-table-view"></span></a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="operationTab">
                                    <div class="tab-pane fade show active">
                                        <div class="table-responsive">
                                            <table class="table table--secondary">
                                                <thead>
                                                    <tr>
                                                    <th scope="col"><i class="icon-file"></i> Image</th>
                                                    <th scope="col"> Name</th>
                                                    <th scope="col">Modified</th>
                                                    <th scope="col">Modified by</th>
                                                    <th scope="col">File size</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(!empty($lists))
                                                    
                                                        @php 
                                                            $fileAll = [];
                                                                if ($lists->files && $lists->files != "") {
                                                                    if (!is_array($lists->files)) {
                                                                        $fileAll = json_decode($lists->files);
                                                                    } else {
                                                                        $fileAll = $lists->files;
                                                                    }
                                                                }
                                                            $date = date_format($lists->created_at, 'd.m.Y');
                                                        @endphp
                                                        @if(count($fileAll) > 0)
                                                       
                                                            @foreach($fileAll as $key => $file)
                                                               
                                                                @php
                                                                    $filesize = "";
                                                                    if(!array_key_exists('youtube_link', $file)) {
                                                                        if(file_exists(public_path($file['image']))) {
                                                                            $filesize = filesize(public_path($file['image'])); 
                                                                            $filesize = round($filesize / 1024 / 1024, 1);
                                                                        }
                                                                    }
                                                                   
                                                                @endphp
                                                            @if($key <= 2)
                                                                <tr>
                                                                    <td scope="row">
                                                                        @if(array_key_exists('youtube_link', $file)) 
                                                                            @php
                                                                                $dataSourceYoutube = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"420\" height=\"315\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen style=\"position:inherit; height:200px; width:420px; \"></iframe>",$file['youtube_link']);
                                                                            @endphp
                                                                            <a class="card card--knowledge spotlight" data-media="video" data-src-webm="{{ asset($file['image'] ?? '') }}" data-src-ogg="{{ asset($file['image'] ?? '') }}" data-src-mp4="{{ asset($file['image'] ?? '') }}" data-autoplay="false" data-poster="{{asset('images/gallery/cover-01.jpg')}}" data-control="close">
                                                                               {!! $dataSourceYoutube !!}
                                                                            </a>
                                                                        @else 
                                                                            <a class="card card--knowledge spotlight" data-media="video" data-src-webm="{{ asset($file['image'] ?? '') }}" data-src-ogg="{{ asset($file['image'] ?? '') }}" data-src-mp4="{{ asset($file['image'] ?? '') }}" data-autoplay="false" data-poster="{{asset('images/gallery/cover-01.jpg')}}" data-control="close">
                                                                                {{-- <img width="100%" height="100px"  src="{{$lists->post_cover ?? asset('images/no_image.png')}}" alt="" class="img-fluid"> --}}
                                                                                <video height="240" width="80%" controls style="position: inherit; width:420px; height:200px; ">
                                                                                    <source src="{{asset($file['image'] ?? '')}}" type="video/mp4">
                                                                                </video>
                                                                                <div class="video__icon" style="top:40%; left:30%; position: absolute;">
                                                                                    <span class="icon-video"></span>
                                                                                </div>
                                                                            </a>
                                                                        @endif
                                                                       
                                                                    </td>
                                                                    <td>
                                                                        <a href="#" class="video--cover">
                                                                            <i class="icon-folder"></i> {{ $file['title'] ?? "" }}
                                                                        </a>
                                                                    </td>
                                                                    <td>{{$date ?? ""}}</td>
                                                                    <td>{{ $lists->userInfo->name ?? '' }}</td>
                                                                    <td>{{ $filesize }} mb</td>
                                                                </tr>
                                                            @else 
                                                                <tr class="knowledge_hidden">
                                                                    <th scope="row">
                                                                        @if(array_key_exists('youtube_link', $file)) 
                                                                            @php
                                                                                $dataSourceYoutube = preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"420\" height=\"315\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen style=\"position:inherit; height:200px; width:420px; \"></iframe>",$file['youtube_link']);
                                                                            @endphp
                                                                            <a class="card card--knowledge spotlight" data-media="video" data-src-webm="{{ asset($file['image'] ?? '') }}" data-src-ogg="{{ asset($file['image'] ?? '') }}" data-src-mp4="{{ asset($file['image'] ?? '') }}" data-autoplay="false" data-poster="{{asset('images/gallery/cover-01.jpg')}}" data-control="close">
                                                                               {!! $dataSourceYoutube !!}
                                                                            </a>
                                                                        @else 
                                                                            <a class="card card--knowledge spotlight" data-media="video" data-src-webm="{{ asset($file['image'] ?? '') }}" data-src-ogg="{{ asset($file['image'] ?? '') }}" data-src-mp4="{{ asset($file['image'] ?? '') }}" data-autoplay="false" data-poster="{{asset('images/gallery/cover-01.jpg')}}" data-control="close">
                                                                                {{-- <img width="100%" height="100px"  src="{{$lists->post_cover ?? asset('images/no_image.png')}}" alt="" class="img-fluid"> --}}
                                                                                <video height="240" width="80%" controls style="position: inherit; width:420px; height:200px;  ">
                                                                                    <source src="{{asset($file['image'] ?? '')}}" type="video/mp4">
                                                                                </video>
                                                                                <div class="video__icon" style="top:40%; left:30%; position: absolute;">
                                                                                    <span class="icon-video"></span>
                                                                                </div>
                                                                            </a>
                                                                        @endif
                                                                    </th>
                                                                    <td>
                                                                        <a href="#" class="video--cover">
                                                                            <i class="icon-folder"></i> {{ $file['title'] ?? "" }}
                                                                        </a>
                                                                    </td>
                                                                    <td>{{$date ?? ""}}</td>
                                                                    <td>{{ $lists->userInfo->name ?? '' }}</td>
                                                                    <td>{{ $filesize }} mb</td>
                                                                </tr>
                                                            @endif
                                                                
                                                            @endforeach
                                                        @endif
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-center">
                                            <a href="#" class="btn btn--loadmore" id="btn_loadmore">
                                                <span class="text__link">โหลดเพิ่มเติม</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@push('scripts')

<script src="{{asset('vendor/spotlight/spotlight.bundle.js')}}">

</script>

<script>
    $(document).ready(function(){
        $("#btn_loadmore").click(function(){
            $("a").removeClass("knowledge_hidden");
            $(this).fadeOut();
        })
    })
</script>
@endpush