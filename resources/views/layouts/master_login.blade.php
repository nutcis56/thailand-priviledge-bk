<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="" rel="shortcut icon"/>
  <!-- addition script for global page -->

  <!-- addition style for global page -->
  <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap-datepicker.min.css')}}">

  <link rel="stylesheet" href="{{asset('vendor/jquery-ui/css/jquery-ui.css')}}">
  <link rel="stylesheet" href="{{asset('vendor/jquery-ui/css/theme.css')}}">

  <link rel="stylesheet" type="text/css" href="{{asset('vendor/slick/slick.css')}}"/>
  <!-- Add the new slick-theme.css if you want the default styling -->
  <link rel="stylesheet" type="text/css" href="{{ asset('vendor/slick/slick-theme.css') }}"/>

  <!-- Add Aos Animation default styling -->
  <link rel="stylesheet" type="text/css" href="{{asset('vendor/aos/aos.css')}}"/>

  <link rel="stylesheet" type="text/css" href="{{asset('vendor/lightcase/css/lightcase.css')}}">

  <link rel="stylesheet" href="{{ asset('brown/css/style.css') }}">

  <!-- Add Fontawesome -->
  <link rel="stylesheet" href="{{ asset('vendor/fontawesome/css/all.min.css') }}">
    @stack("css")

    {{-- <link rel="stylesheet" href="{{ asset('css/style_login.css') }}"> --}}
</head>
<body>
   
    @yield("content")


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="{{ asset("vendor/bootstrap/js/bootstrap.min.js") }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap-datepicker.min.js') }}"></script>
    {{-- <script src="{{ asset("vendor/jquery-ui/js/jquery-ui.min.js") }}"></script> --}}
    <script src="{{ asset("vendor/slick/slick.min.js") }}"></script>
    <script src="{{ asset("vendor/aos/aos.js") }}"></script>
    <script src="{{ asset("vendor/parallax/parallax.min.js") }}"></script>
    <script src="{{ asset("js/function.js") }}"></script>
    <script src="{{ asset("js/slick.js") }}"></script>


    {{-- js --}}
    <script type="text/javascript" src="{{ asset("vendor/lightcase/js/lightcase.js") }}"></script>

    {{-- SELECT2 CDN --}}
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $('a[data-rel^=lightcase]').lightcase();
        });
    </script>

<script>
      function loadJS(u){var r=document.getElementsByTagName("script")[ 0 ],s=document.createElement("script");s.src=u;r.parentNode.insertBefore(s,r);}

      if(!window.HTMLPictureElement || !('sizes' in document.createElement('img'))){
          document.createElement('picture');
          loadJS("plugins/respimg/ls.respimg.min.js");
      }
  </script>
<script src="{{ asset("vendor/lazysizes/lazysizes.min.js") }}"></script>
<script src="{{ asset("js/main.js") }}"></script>
    @stack("scripts")
</body>
</html>