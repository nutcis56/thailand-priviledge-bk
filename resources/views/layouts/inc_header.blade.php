<header class="header">
    <div class="header--lg">
        <div class="header__top" id="header__top">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="header__wrap">
                            <a href="#" id="dropdownSearchDialog" class="header__link header__link--search" data-toggle="modal" data-target="#modalSerach"><img src="{{asset("images/icon/search.svg") }}" alt=""> {{ __('tpc.search') }}</a>

                            <div class="wrap-change-language wrap-change-language-pc">
                                <a href="#" class="header__link header__link--lang current-language" style="color: white;">
                                    @if (session()->has('language') && (session()->get('language') == 'th_TH'))
                                        <img src="{{asset("images/icon/flag-th.svg") }}" alt=""> TH
                                    @endif
                                    @if (session()->has('language') && (session()->get('language') == 'en'))
                                    <img src="{{asset("images/icon/flag-en.svg") }}" alt=""> EN
                                    @endif
                                </a>
                                <div class="popup-language" style="display: none;">
                                    <a href="{{ route('change-lang', ['locale' => 'th_TH']) }}" class="header__link header__link--lang" style="color: white;">
                                        <img src="{{asset("images/icon/flag-th.svg") }}" alt=""> ภาษาไทย
                                    </a>
        
                                    <a href="{{ route('change-lang', ['locale' => 'en']) }}" class="header__link header__link--lang" style="color: white;">
                                        <img src="{{asset("images/icon/flag-en.svg") }}" alt=""> English
                                    </a>
                                </div>
                            </div>
                          
                            @php 
                                $userInfo = \Auth::guard('backpack')->user();
                                $section = $userInfo->position->parent_id;
                                $department = CoreConfigService::department($section);
                                
                            @endphp
                            {{-- <a href="#" class="header__link header__link--login">{{__('tpc.login')}}</a> --}}
                            <div class="dropdown--profile-dialog">
                                <a href="#" id="dropdownProfileDialog" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle header__link">{{ __('tpc.menu_myprofile') }}</a>
                                <div aria-labelledby="dropdownProfileDialog" class="dropdown-menu dropdown-menu-right">
                                    <div class="profile--wrap">
                                        <div class="profile__avatar">
                                            <img src="{{asset("images/icon/profile.svg") }}" alt="">
                                        </div>
                                        <div class="profile__text">
                                            <div class="profile__name">
                                                {{ $userInfo->name ?? '' }}
                                            </div>
                                            <div class="profile__position">
                                                {{ (!empty($department)) ? $department->name : '' }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="profile--nav">
                                        <ul>
                                            <li>
                                                <a href="/admin" target="_blank">{{ __('tpc.manage_backend') }}</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('MyknowLedge') }}">{{ __('tpc.index_my_knowledge_sharing') }}</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('wishlistIndex') }}">{{ __('tpc.menu_favorite') }}</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('wishlistLike') }}">{{ __('tpc.menu_like') }}</a>
                                            </li>
                                            <li>
                                                <form method="POST" id="formLogout" action="{{ route('frontend.auth.logout') }}">
                                                    @csrf
                                                </form>
                                                <a href="#" id="btn-logout">{{ __('tpc.menu_logout') }}</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @php 
            $menuOrganizationStruture = CoreConfigService::menuOrganizeStruture();
            $logo = CoreConfigService::homePageTopLogo();
            $topMenu = CoreConfigService::getTopMenu();
        @endphp
        <div class="header__main" id="header__main">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="header__wrap">
                            <div class="header__left">

                                <a href="{{ route('home') }}" title="ไทยแลนด์ พริวิเลจ คาร์ด">

                                    @if(count($logo) > 0)
                                        <img class="header__logo" src="{{asset($logo[0]->image) }}" alt="ไทยแลนด์ พริวิเลจ คาร์ด">
                                    @endif
                                   
                                </a>
                            </div>
                            <div class="header__right">
                                <div class="navbar navbar--main-desktop navbar-expand-xl">
                                    <div id="navbarNav" class="collapse navbar-collapse">
                                        <ul class="navbar-nav">
                                            @if(count($topMenu) > 0)
                                                @foreach($topMenu as $key => $value)

                                                    @if(count($value->subMenu) > 0)
                                                        <li class="nav-item dropdown dropdown--navbar">
                                                            <a href="#" id="ThanachartGroup_{{$value->code}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">{{ $value->name }}</a>
                                                            <div aria-labelledby="ThanachartGroup_{{$value->code}}" class="dropdown-menu">
                                                                <div class="container-fluid">
                                                                    <div class="row justify-content-center">
                                                                        <div class="col-11">
                                                                            <div class="row">
                                                                                <div class="col-3">
                                                                                    <a href="#" class="link-text dropdown__btn">{{ $value->name }}</a>
                                                                                </div>
                                                                                <div class="col-9">
                                                                                    <div class="menu__main">
                                                                                        <ul>
                                                                                            @foreach($value->subMenu as $keySubLv1 => $valueSubLv1)
                                                                                                @if(count($valueSubLv1->subMenu) > 0)
                                                                                                    <li>
                                                                                                        <a href="#" title="{{ $valueSubLv1->name }}">{{ $valueSubLv1->name }}</a>
                                                                                                        
                                                                                                        <ul>
                                                                                                            @foreach($valueSubLv1->subMenu as $keySublv2 => $valueSubLv2)
                                                                                                                <li><a href="{{ $valueSubLv2->link ?? '#' }}" target="{{ $valueSubLv2->target }}" title="{{ $valueSubLv2->name }}">{{ $valueSubLv2->name }}</a></li>
                                                                                                            @endforeach
                                                                                                        </ul>
                                                                                                    </li>
                                                                                                @else 
                                                                                                    <li>
                                                                                                        <a href="{{ $valueSubLv1->link ?? '#' }}" target="{{ $valueSubLv1->target }}" title="{{ $valueSubLv1->name }}">{{ $valueSubLv1->name }}</a>
                                                                                                    </li>
                                                                                                @endif
                                                                                            @endforeach
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @else 
                                                        <li class="nav-item">
                                                            <a href="{{ $value->link ?? '#' }}" target="{{ $value->target }}" title="{{ $value->name }}" class="nav-link"> {{ $value->name }}</a>
                                                        </li>
                                                    @endif
                                                   
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    </div>
    <div class="header--sm">
        <div class="header__top">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-11">
                        <div class="header__wrap">
                            <a href="#" id="dropdownSearchDialog" class="header__link header__link--search" data-toggle="modal" data-target="#modalSerach"><img src="{{asset("images/icon/search.svg") }}" alt=""> {{ __('tpc.search') }}</a>
                            <div class="wrap-change-language wrap-change-language-mobile">
                                <a href="#" class="header__link header__link--lang current-language">
                                    @if (session()->has('language') && (session()->get('language') == 'th_TH'))
                                        <img src="{{asset("images/icon/flag-th.svg") }}" alt=""> TH
                                    @endif
                                    @if (session()->has('language') && (session()->get('language') == 'en'))
                                        <img src="{{asset("images/icon/flag-en.svg") }}" alt=""> EN
                                    @endif
                                </a>
                                <div class="popup-language" style="display: none;">
                                    <a href="{{ route('change-lang', ['locale' => 'th_TH']) }}" class="header__link header__link--lang">
                                        <img src="{{asset("images/icon/flag-th.svg") }}" alt=""> ภาษาไทย
                                    </a>
                                    <a href="{{ route('change-lang', ['locale' => 'en']) }}" class="header__link header__link--lang">
                                        <img src="{{asset("images/icon/flag-en.svg") }}" alt=""> English
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header__main">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-11">
                        <div class="header__wrap">
                            <div class="header__left">
                                <a href="{{route('home')}}" title="ไทยแลนด์ พริวิเลจ คาร์ด">
                                    <img class="header__logo" src="{{asset("images/logo/logo-thailand-elite.svg") }}" alt="ไทยแลนด์ พริวิเลจ คาร์ด">
                                </a>
                            </div>
                            <div class="header__right">
                                <div class="hamburger">
                                    <a href="javascript:void(0)" class="menu--button">
                                        <div class="menu--button_i btn-open">
                                            <div class="line-1"></div>
                                            <div class="line-2"></div>
                                            <div class="line-3"></div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navWrapper">
                <div class="navWrapper__content">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-lg-11">
                                <div class="navWrapper__top">
                                    <a href="#" class="header__link header__link--login">เข้าสู่ระบบ</a>
                                    <div class="dropdown--profile-dialog">
                                        <a href="#" id="dropdownProfileDialog" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle header__link">{{ __('tpc.menu_myprofile') }}</a>
                                        <div aria-labelledby="dropdownProfileDialog" class="dropdown-menu dropdown-menu-right">
                                            <div class="profile--wrap">
                                                <div class="profile__avatar">
                                                    <img src="{{asset("images/icon/profile.svg") }}" alt="">
                                                </div>
                                                <div class="profile__text">
                                                    <div class="profile__name">
                                                        {{ $userInfo->name ?? '' }}
                                                    </div>
                                                    <div class="profile__position">
                                                        {{ (!empty($department)) ? $department->name : '' }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="profile--nav">
                                                <ul>
                                                    <li>
                                                        <a href="/admin" target="_blank">{{ __('tpc.manage_backend') }}</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('MyknowLedge') }}">{{ __('tpc.index_my_knowledge_sharing') }}</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('wishlistIndex') }}">{{ __('tpc.menu_favorite') }}</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ route('wishlistLike') }}">{{ __('tpc.menu_like') }}</a>
                                                    </li>
                                                    <li>
                                                        <form method="POST" id="formLogout" action="{{ route('frontend.auth.logout') }}">
                                                            @csrf
                                                        </form>
                                                        <a href="#" id="btn-logout">log out</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-11">
                                <div class="menu__main--sm">
                                    <ul>
                                        @foreach($topMenu as $key => $value)
                                            @if(count($value->subMenu) > 0)
                                                <li>
                                                    <a href="#" title="ชื่อเมนู">{{ $value->name ?? '' }}</a>
                                                    <ul>
                                                        @foreach($value->subMenu as $key => $valSubLv1)
                                                            @if(count($valSubLv1->subMenu) > 0)
                                                                <li>
                                                                    <a href="#" title="ชื่อเมนู">{{ $valSubLv1->name ?? '' }}</a>
                                                                    <ul>
                                                                        @foreach($valSubLv1->subMenu as $valSubLv2)
                                                                            <li><a href="{{ $valSubLv2->link ?? '#' }}" title="ชื่อเมนู">{{ $valSubLv2->name ?? '' }}</a></li>
                                                                        @endforeach
                                                                    </ul>
                                                                </li>
                                                            @else 
                                                                <li><a href="{{ $valSubLv1->link ?? '#' }}" title="ชื่อเมนู">{{ $valSubLv1->name ?? '' }}</a>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                </li>
                                            @else
                                                <li><a href="{{ $value->link ?? '#' }}" title="ชื่อเมนู">{{ $value->name ?? '' }}</a></li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="menu-close">
                        <span class="icon-close-modal"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<style>
    .wrap-change-language {
        position: relative;
        z-index: 1;
    }
    .wrap-change-language .popup-language {
        position: absolute;
        background: #52331e;
        width: 120px;
        right: 0;
        /* padding: 0.5em; */
        top: 33px;
        right: -15px;
    }
    .wrap-change-language .popup-language a {
        display: block !important;
        padding: 5px 0px !important;   
        margin: 0px !important;
        padding: 8px 10px !important;
    }
    .wrap-change-language .popup-language a:hover {
        background: #ab9357;
    }
    .wrap-change-language .popup-language a:before {
        background-color: unset !important;
    }

    .wrap-change-language-pc .popup-language {
        width: 150px;
    }
</style>

@push('scripts')
    <script>
        var ThaiPLanauge = {
            init: function() {
                let current_language_element = $('.wrap-change-language .current-language')
                let popup_language =$('.wrap-change-language .popup-language')
                current_language_element.click(function(e) {
                    e.preventDefault()
                    popup_language.fadeIn(200);
                    setTimeout(function() {
                        popup_language.fadeOut(200)
                    }, 5000)
                })
            }
        }
        jQuery(document).ready(function() {
            ThaiPLanauge.init()
        })
    </script>
@endpush