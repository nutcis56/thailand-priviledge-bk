@php 
    $footer = CoreConfigService::getFooter();
@endphp
<footer class="footer">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="footer__top">
                    <div class="row justify-content-center justify-content-md-between align-items-center">
                        <div class="col-auto">
                            <a href="../home/" title="NIRVANA">
                                {{ (!empty($footer['footerTitle'])) ? $footer['footerTitle']->name : '' }}
                            </a>
                            <address>
                                {{ (!empty($footer['footerAddress'])) ? $footer['footerAddress']->name : '' }}
                            </address>
                        </div>
                        <div class="col-auto">
                            <div class="wrap-sitemap-menu">
                                <a href="{{ route('frontend.sitemap') }}">
                                    {{ __('tpc.sitemap_menu') }}
                                </a>
                            </div>
                            <div>
                                {{ (!empty($footer['footerCopyRight'])) ? $footer['footerCopyRight']->name : '' }}
                            </div>
                            <div class="wrap-sitemap-menu">
                                <a href="{{ route('frontend.service_api') }}">
                                    {{ __('tpc.service_api') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="modal fade modal-custom" id="modalSerach" tabindex="-1" role="dialog" aria-labelledby="modalSerachTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        
            @php 
                $categoryKnwoledge = KnowledgeCategoryService::knowledgeCategory();
                $sectionService = CoreConfigService::getSection();
                $tags = CoreConfigService::getTag();
            @endphp
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-center" id="exampleModalLongTitle">{{ __('tpc.search_result') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="icon-close-modal" aria-hidden="true"></span>
            </button>
        </div>
        <div class="modal-body">
            <form action="{{ route('searchResult') }}" method="GET" class="form form-srch">
                <div class="row">
                    <div class="col-12">
                        <div class="input-group mb-4 mr-sm-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                <span class="icon-search"></span>
                                </div>
                            </div>
                            <input type="text" class="form-control" id="" name="search" placeholder="{{ __('tpc.search') }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        
                        <select class="form-control select__input mb-4" name="category">                                                
                            <option  value="" selected="">{{ __('tpc.category') }}</option>
                            @if(count($sectionService) > 0)
                                @foreach($sectionService as $key => $value)
                                    <option value="{{ $value->id }}">{{ $value->name ?? '' }}</option>
                                @endforeach
                            @endif
                        </select>
                       
                    </div>
                    <div class="col-sm-6">
                        {{-- <input type="text" class="form-control" id="" name="searchTag" placeholder="คำค้นแท็ก"> --}}
                        
                        <select class="js-example-basic-single form-control select__input mb-4" style="width: 100% !important" name="searchTag">
                            <option value="">{{ __('tpc.choice') }}</option>
                            @if(!empty($tags) && count($tags) > 0)
                                @foreach($tags as $key => $value)
                                    <option value="{{ $value->name }}" >{{ $value->name ?? '' }}</option>
                                @endforeach
                            @endif
                        </select>
                        
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-sm-4">
                        <button type="submit" class="btn btn--secondary w-100 mb-4"><span class="btn__text btn__text--sm">{{ __('tpc.search') }}</span></button>
                    </div>
                </div>
            </form>
        </div>
        </div>
    </div>
</div>

<style>
    .wrap-sitemap-menu {
        text-align: right;
    }
    @media screen and (max-width: 613px) {
        .wrap-sitemap-menu {
            text-align: center;
        }
    }
</style>