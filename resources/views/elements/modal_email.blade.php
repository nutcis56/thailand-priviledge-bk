<section>
    <!-- Modal UNFAVORITE CANCEL -->
    <div class="modal fade" id="modalEmailConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0 none;">
            <h5 class="modal-title" id="exampleModalLabel">Send Mail</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="confirm_send_mail">อีเมล์:</label>
                    <input id="confirm_send_mail" type="email" placeholder="Enter email" name="confirm_send_mail" class="form-control">
                    <input type="hidden" name="data-id" value="{{ $id ?? 0 }}">
                </div>
            </div>
            <div class="modal-footer" style="border-bottom: 0 none; border-top: 0 none;">
                <button type="button" style="width: 20%"  class="btn-primary btn" id="btn-modal-email-confirm">ตกลง</button>
            </div>
        </div>
        </div>
    </div>
</section>

<section>
    <!-- Modal -->
    <div class="modal fade" id="modalAlertSuccessEamil" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0 none;">
            <h5 class="modal-title" id="exampleModalLabel">แจ้งเตือน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            ส่งเมล์เสร็จสิ้น
            </div>
            <div class="modal-footer" style="border-bottom: 0 none; border-top: 0 none;">
                <button type="button" style="width: 20%"  class="btn-primary btn" data-dismiss="modal">ตกลง</button>
            </div>
        </div>
        </div>
    </div>
</section>