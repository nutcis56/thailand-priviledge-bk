<section>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0 none;">
            <h5 class="modal-title" id="exampleModalLabel">แจ้งเตือน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            เพิ่มรายการโปรดเสร็จสิ้น
            </div>
            <div class="modal-footer" style="border-bottom: 0 none; border-top: 0 none;">
                <button type="button" style="width: 20%"  class="btn-primary btn" data-dismiss="modal">ตกลง</button>
            </div>
        </div>
        </div>
    </div>
</section>


<section>
    <!-- Modal -->
    <div class="modal fade" id="favoriteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0 none;">
            <h5 class="modal-title" id="exampleModalLabel">แจ้งเตือน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            เพิ่มรายการถูกใจเสร็จสิ้น
            </div>
            <div class="modal-footer" style="border-bottom: 0 none; border-top: 0 none;">
                <button type="button" style="width: 20%"  class="btn-primary btn" data-dismiss="modal">ตกลง</button>
            </div>
        </div>
        </div>
    </div>
</section>


<section>
    <!-- Modal UNLIKE CANCEL -->
    <div class="modal fade" id="unlikeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0 none;">
            <h5 class="modal-title" id="exampleModalLabel">แจ้งเตือน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
            ยกเลิกรายการถูกใจเสร็จสิ้น
            </div>
            <div class="modal-footer" style="border-bottom: 0 none; border-top: 0 none;">
                <button type="button" style="width: 20%"  class="btn-primary btn" data-dismiss="modal">ตกลง</button>
            </div>
        </div>
        </div>
    </div>
</section>


<section>
    <!-- Modal UNFAVORITE CANCEL -->
    <div class="modal fade" id="unfavoriteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0 none;">
            <h5 class="modal-title" id="exampleModalLabel">แจ้งเตือน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                ยกเลิกรายการโปรดเสร็จสิ้น
            </div>
            <div class="modal-footer" style="border-bottom: 0 none; border-top: 0 none;">
                <button type="button" style="width: 20%"  class="btn-primary btn" data-dismiss="modal">ตกลง</button>
            </div>
        </div>
        </div>
    </div>
</section>


<section>
    <!-- Modal UNFAVORITE CANCEL -->
    <div class="modal fade" id="modalRating" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 0 none;">
            <h5 class="modal-title" id="exampleModalLabel">ให้คะแนน</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>
            <div class="modal-body">
                <div class="rating__star">
                    {{-- <ul class="list-group">
                        <li class="list-group-item ratingLi">
                            
                            <div class="rating__star">
                                คะแนนคอร์ส:
                                <span class="icon-star courseDetailIcon"></span>
                                <span class="icon-star courseDetailIcon"></span>
                                <span class="icon-star courseDetailIcon"></span>
                                <span class="icon-star courseDetailIcon"></span>
                            </div>
                            <div class="rating"> 
                                <input type="radio" name="rating" value="5" id="5"><label class="label_rating" data-value="5" for="5">☆</label> 
                                <input type="radio" name="rating" value="4" id="4"><label class="label_rating" data-value="4" for="4">☆</label> 
                                <input type="radio" name="rating" value="3" id="3"><label class="label_rating" data-value="3" for="3">☆</label> 
                                <input type="radio" name="rating" value="2" id="2"><label class="label_rating" data-value="2" for="2">☆</label> 
                                <input type="radio" name="rating" value="1" id="1"><label class="label_rating" data-value="1" for="1">☆</label>
                            </div>

                        </li>
                    </ul> --}}
                    <div class="rating"> 
                        <input type="radio" name="rating" value="5" id="5"><label class="label_rating" data-value="5" for="5">☆</label> 
                        <input type="radio" name="rating" value="4" id="4"><label class="label_rating" data-value="4" for="4">☆</label> 
                        <input type="radio" name="rating" value="3" id="3"><label class="label_rating" data-value="3" for="3">☆</label> 
                        <input type="radio" name="rating" value="2" id="2"><label class="label_rating" data-value="2" for="2">☆</label> 
                        <input type="radio" name="rating" value="1" id="1"><label class="label_rating" data-value="1" for="1">☆</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="border-bottom: 0 none; border-top: 0 none;">
                <button type="button" style="width: 20%"  class="btn-primary btn" id="btn-modalRating-save">ตกลง</button>
            </div>
        </div>
        </div>
    </div>
</section>