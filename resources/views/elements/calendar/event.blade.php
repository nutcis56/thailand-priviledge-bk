
    <div id="event-list-content">
        <h4>กิจกรรม วันที่ {{$dateEvent}}</h4>
        <!-- <p>ไม่มีอีเวนต์ในหมวดที่เลือกในวันดังกล่าว</p> -->
        @if(count($lists) > 0)
            @foreach($lists as $value)
                @php 
                    $strtotimeStart = strtotime($value->start_date);
                    $startDate = date('Y-m-d', $strtotimeStart);
                    $dateStartFormat = getFormatDateThai($startDate);

                    $strtotimeEnd = strtotime($value->end_date);
                    $endDate = date('Y-m-d', $strtotimeEnd);
                    $dateEndFormat = getFormatDateThai($endDate);

                @endphp
                <div class="item_right_event eventDetailData" data-id="{{$value->id}}">
                    <h4 class="mb-2">วันที่ {{$dateStartFormat}} – {{$dateEndFormat}}</h4>
                    <p>{{$value->post_title}}</p>
                </div>
            @endforeach
        @endif
    </div>
