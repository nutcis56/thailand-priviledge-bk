@if($data == 1)
    <span class="icon-star"></span>
@elseif($data == 2)
    <span class="icon-star"></span>
    <span class="icon-star"></span>
@elseif($data == 3)
    <span class="icon-star"></span>
    <span class="icon-star"></span>
    <span class="icon-star"></span>
@elseif($data == 4)
    <span class="icon-star"></span>
    <span class="icon-star"></span>
    <span class="icon-star"></span>
    <span class="icon-star"></span>
@elseif($data == 5)
    <span class="icon-star"></span>
    <span class="icon-star"></span>
    <span class="icon-star"></span>
    <span class="icon-star"></span>
    <span class="icon-star"></span>
@else
    <span>ไม่มีคะแนน</span>
@endif