<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>@yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <style>
            table {
                border-collapse: collapse;
                font-family:sans-serif;
                font-size:14px;
                line-height:25px;
                color:#666666;
                /* border: 1px solid #e5e5e5; */
                width: 100%;
            }
            table > thead > tr > th,
            table > tbody > tr > td {
                padding: 1em;
                /* border: 1px solid #e5e5e5; */
            }
            table.main,
            table.content,
            table.main > thead > tr > th,
            table.main > tbody > tr > td,
            table.content > thead > tr > th,
            table.content > tbody > tr > td {
                border: 0px;
            }
            table.main {
                background: #ffffff;
                /* border: 1px solid #e5e5e5; */
                text-align:left; 
                margin: 20px auto;
                border-spacing: 0px;
                width: 600px;
            }
            table.main > tbody > tr > td,
            table.content > tbody > tr > td {
                padding:30px 30px 30px 30px;
            }
            div.section {
                margin-bottom: 1em;
            }
            div.title {
                font-weight: bold;
                font-size: 1.125em;
            }

            .warp-confirm-button {
                text-align: center;
                padding: 1em 0em;
            }
            .confirm-button {
                background-color: #52331E;
                color: #fff;
                padding: 1em 2em;
                text-decoration: none;
            }
        </style>

        @stack('styles')

    </head>
    <body>

        <div bgcolor="#f4f4f4" style="margin:0;padding:0;background:#f4f4f4">

            <div style="min-height: 30px;"></div>

            <table class="main" border="0" cellpadding="0" cellspacing="0" width="600" align="center">
                <tr>
                    <td align="center" valign="top" width="100%" class="templateColumnContainer">
                        <table class="content" border="0" cellpadding="10" cellspacing="0" width="600">
                            <tr>
                                <td style="text-align: center;border-bottom:1px solid #e5e5e5;">
                                    <img src="{{ asset('images/logo/logo-thailand-elite.png') }}" alt="LOGO" width="180" style="max-width:180px;">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    
                                    <div class="warp-title" style="margin-bottom:20px">
                                        <h2>
                                            @yield('title')
                                        </h2>
                                        <h4>
                                            @yield('sub_title')
                                        </h4>
                                    </div>
                                    
                                    @yield('content')

                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: center;border-top:1px solid #e5e5e5; fonts-size: 12px;">
                                    เยี่ยมชมเว็บไซต์ : <a href="{{ config('app.url') }}">{{ config('app.url') }}</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <div style="min-height: 30px;"></div>

        </div>

    </body>
</html>