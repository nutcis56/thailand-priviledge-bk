@extends('mail.layouts.master')

@section('title')
    {{ $subject }}
@endsection

@section('sub_title')
    {{-- สวั/สดีคุณ {{ $firstname }} {{ $lastname }} --}}
@endsection

@section('content')
    {!! $content !!}
@endsection