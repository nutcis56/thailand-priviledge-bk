@extends('mail.layouts.master')

@section('title')
{{ $lists->post_title }}
@endsection

@section('sub_title')
    {{-- สวั/สดีคุณ {{ $firstname }} {{ $lastname }} --}}
@endsection


@section('content')
    <p style="text-align: center;">
        <img src="{{ $lists->post_cover ?? asset('images/no_image.png') }}" alt="" class="img-fluid">
    </p>
    <p>
        {{ $lists->post_title }}
    </p>
   
    <p>{{$lists->post_excerpt }}</p>

@endsection