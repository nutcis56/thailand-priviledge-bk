@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;
?>

@push("css")
<style>
    .member_hidden {
        display: none !important;
    }
</style>
@endpush

@section("content")

@if (session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div>
@endif

@if (session('error'))
    <div class="alert alert-danger">
        {{ session('error') }}
    </div>
@endif

<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset('images/banner/banner-news.jpg')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">บริการสมาชิก</h2>
                                <p class="banner__text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="">                                                
                                                <option value="หมวดหมู่" selected="">หมวดหมู่</option>
                                                <option value="หมวดหมู่">หมวดหมู่</option>
                                                <option value="หมวดหมู่">หมวดหมู่</option>
                                                <option value="หมวดหมู่">หมวดหมู่</option>
                                                <option value="หมวดหมู่">หมวดหมู่</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="">                                                
                                                <option value="Tag" selected="">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="button" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">บริการสมาชิก</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="member--wrapper">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="member__aside">
                                        <h3 class="member__title">
                                            บริการสมาชิก
                                        </h3>
                                        <ul class="member__nav">
                                            <li class="member__item {{ $type == "contact_center" ? 'active' : '' }}">
                                                <a href="{{ route('memberIndex', ['type' => 'contact_center']) }}" class="member__link">
                                                    Member Contact Center
                                                </a>
                                            </li>
                                            <li class="member__item {{ $type == "personal_liaison" ? 'active' : '' }}">
                                                <a href="{{ route('memberIndex', ['type' => 'personal_liaison']) }}" class="member__link">
                                                    Elite Personal Liaison
                                                </a>
                                            </li>
                                            <li class="member__item {{ $type == "personal_assistant" ? 'active' : '' }}">
                                                <a href="{{ route('memberIndex', ['type' => 'personal_assistant']) }}" class="member__link">
                                                    Elite Personal Assistant
                                                </a>
                                            </li>
                                            <li class="member__item {{ $type == "vendor" ? 'active' : '' }}">
                                                <a href="{{ route('memberIndex', ['type' => 'vendor']) }}" class="member__link">
                                                    Vendor
                                                </a>
                                            </li>
                                            <li class="member__item {{ $type == "goverment_relation" ? 'active' : '' }}">
                                                <a href="{{ route('memberIndex', ['type' => 'goverment_relation']) }}" class="member__link">
                                                    Government Relation
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="member__form">
                                        <div class="form__title">
                                            เพิ่มคำถามใหม่
                                        </div>
                                        <form action="{{ route('memberStore') }}" class="form__body" method="POST">
                                            @csrf
                                            <div class="row">
                                                <div class="col-6 col-sm-4">
                                                    <div class="form-group form--select">
                                                        <select class="form-control select__input">                                                
                                                            <option value="แผนก" selected="">แผนก</option>
                                                            <option value="แผนก">แผนก</option>
                                                            <option value="แผนก">แผนก</option>
                                                            <option value="แผนก">แผนก</option>
                                                            <option value="แผนก">แผนก</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-4">
                                                    <div class="form-group form--select">
                                                        <select class="form-control select__input @error('categoryMain') is-invalid @enderror" name="categoryMain" id="categoryMain">
                                                            <option value="" selected="">หมวดหมู่</option>   
                                                            @if(count($category) > 0)
                                                                @foreach($category as $key => $value)
                                                                    <option value="{{ $value->id }}">{{$value->name}}</option>
                                                                @endforeach
                                                            @endif
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-6 col-sm-4">
                                                    <div class="form-group form--select">
                                                        <select class="form-control select__input">                                                
                                                            <option value="หมวดหมู่ย่อย" selected="">หมวดหมู่ย่อย</option>
                                                            <option value="หมวดหมู่ย่อย">หมวดหมู่ย่อย</option>
                                                            <option value="หมวดหมู่ย่อย">หมวดหมู่ย่อย</option>
                                                            <option value="หมวดหมู่ย่อย">หมวดหมู่ย่อย</option>
                                                            <option value="หมวดหมู่ย่อย">หมวดหมู่ย่อย</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <textarea class="form-control @error('question') is-invalid @enderror" name="question" id="" cols="30" rows="10" placeholder="กรอกคำถาม"></textarea>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <textarea class="form-control @error('answer') is-invalid @enderror" name="answer" id="" cols="30" rows="10" placeholder="กรอกคำตอบ"></textarea>
                                                </div>
                                            </div>
                                            <div class="row justify-content-center">
                                                <div class="col-6 col-md-4">
                                                    <button type="submit" class="btn btn--loadmore w-100">
                                                        <span class="text__link">บันทึก</span>
                                                    </button>
                                                </div>
                                                <div class="col-6 col-md-4">
                                                    <button type="button" class="btn btn--cancel w-100">
                                                        <span class="text__link">ยกเลิก</span>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
@push("scripts")
<script>

    const dom_member_form = {
        init: function() {

        },
        handleChangeSubmitForm: function() {
            
        },
        handldeChangeCategoryMain: function() {
            $("#categoryMain").change(function() {
                
            })
        }
    }


    $(document).ready(function(){
        $("#btn_loadmore").click(function(){
            $("a").removeClass("member_hidden");
            $(this).fadeOut();
        })
    })
</script>
@endpush