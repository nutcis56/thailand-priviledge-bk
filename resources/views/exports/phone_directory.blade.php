<table>
    <thead>
    <tr>
        <th>name</th>
        <th>email</th>
        <th>mobile</th>
        <th>phone1</th>
        <th>phone2</th>
        <th>detail</th>
        <th>department</th>
        <th>section</th>
    </tr>
    </thead>
    <tbody>
    @foreach($lists as $list)
    @php 
        $data = json_decode($list->value);
    @endphp
        <tr>
            <td>{{ $data->name ?? '' }}</td>
            <td>{{ $data->email ?? '' }}</td>
            <td>{{ $data->mobile ?? '' }}</td>
            <td>{{ $data->phone1 ?? '' }}</td>
            <td>{{ $data->phone2 ?? '' }}</td>
            <td>{{ $list->detail ?? '' }}</td>
            <td>{{ $list->level ?? '' }}</td>
            <td>{{ $list->category ?? '' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>