@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;

?>
@section("content")


<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{ route('phoneDirectory') }}" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-10">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น" value="{{ $keyword ?? '' }}">
                                        </div>
                                    </div>
                                    {{-- <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($category) > 0)
                                                    @foreach($category as $key => $list)
                                                        @if(!empty($list) && count($list) > 0)
                                                            <option value="{{ $list[0]->phoneDerectorySection->id }}" {{ $keywordCategory == $list[0]->phoneDerectorySection->id ? 'selected' : '' }}>{{ $list[0]->phoneDerectorySection->name }}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div> --}}
                                   
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Phone Directory</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="accordion accordion--primary" id="accordionExample">
                            
                            @if(count($lists) > 0)
                                @foreach($lists as $key => $list)
                                    <div data-toggle="collapse" data-target="#collapse-{{ $key + 1}}" aria-expanded="true" aria-controls="collapse-{{ $key + 1}}" class="accordion__title">
                                        <i class="more-less icon-angle-down"></i>
                                        
                                        {{ $list[0]['phone_derectory_section']['name'][app()->getLocale()] }}
                                    </div>
                                    <div id="collapse-{{ $key + 1}}" class="collapse" aria-labelledby="collapse-{{ $key + 1}}" data-parent="#accordionExample">
                                        <div class="accordion__body bg-white">
                                            <div class="table-responsive">
                                                <table class="table table--phone">
                                                    <thead>
                                                        <tr>
                                                        <th scope="col">ชื่อ - สกุล</th>
                                                        <th scope="col">เบอร์มือถือ</th>
                                                        <th scope="col">เบอร์สวัสดิการ</th>
                                                        <th scope="col">เบอร์โต๊ะ</th>
                                                        <th scope="col">อีเมล</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                       
                                                        @if(count($list) > 0)
                                                            @foreach($list as $val)
                                                                @php 
                                                                    $value = json_decode($val['value']);
                                                                @endphp
                                                                <tr>
                                                                    <th scope="row">
                                                                        {{ $value->name ?? '' }}
                                                                    </th>
                                                                    <td>
                                                                        <a href="tel:{{ $value->mobile ?? '' }}">{{ $value->mobile ?? '' }}</a>
                                                                    </td>
                                                                    <td>
                                                                        <a href="tel:{{ $value->phone1 ?? '' }}">{{ $value->phone1 ?? '' }}</a>
                                                                    </td>
                                                                    <td>
                                                                        <a href="tel:{{ $value->phone2 ?? '' }}">{{ $value->phone2 ?? '' }}</a>
                                                                    </td>
                                                                    <td>
                                                                        <a href="mailto:{{ $value->email ?? '' }}">{{ $value->email ?? '' }}</a>
                                                                    </td>
                                                                </tr>
                                                            @endforeach

                                                        @endif
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                @endforeach

                            @endif
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>


@endsection

@push('scripts')
<script>
    $('.accordion').on('hidden.bs.collapse', toggleIcon);
    $('.accordion').on('shown.bs.collapse', toggleIcon);
    $(".accordion").on("shown.bs.collapse", (function() {
        var t = $(this).find(".show").prev();
        $("html,body").animate({
            scrollTop: $(t).offset().top - 125
        }, 500)
    }));
</script>
@endpush