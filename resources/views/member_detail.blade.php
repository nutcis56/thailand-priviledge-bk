@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;
?>


@section("content")

<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{ asset('images/banner/banner-news.jpg') }})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">บริการสมาชิก</h2>
                                <p class="banner__text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="">                                                
                                                <option value="หมวดหมู่" selected="">หมวดหมู่</option>
                                                <option value="หมวดหมู่">หมวดหมู่</option>
                                                <option value="หมวดหมู่">หมวดหมู่</option>
                                                <option value="หมวดหมู่">หมวดหมู่</option>
                                                <option value="หมวดหมู่">หมวดหมู่</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="">                                                
                                                <option value="Tag" selected="">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="button" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">บริการสมาชิก</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="member--wrapper">
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="member__aside">
                                        <h3 class="member__title">
                                            บริการสมาชิก
                                        </h3>
                                        <ul class="member__nav">
                                            <li class="member__item {{ $type == "contact_center" ? 'active' : '' }}">
                                                <a href="{{ route('memberIndex', ['type' => 'contact_center']) }}" class="member__link">
                                                    Member Contact Center
                                                </a>
                                            </li>
                                            <li class="member__item {{ $type == "personal_liaison" ? 'active' : '' }}">
                                                <a href="{{ route('memberIndex', ['type' => 'personal_liaison']) }}" class="member__link">
                                                    Elite Personal Liaison
                                                </a>
                                            </li>
                                            <li class="member__item {{ $type == "personal_assistant" ? 'active' : '' }}">
                                                <a href="{{ route('memberIndex', ['type' => 'personal_assistant']) }}" class="member__link">
                                                    Elite Personal Assistant
                                                </a>
                                            </li>
                                            <li class="member__item {{ $type == "vendor" ? 'active' : '' }}">
                                                <a href="{{ route('memberIndex', ['type' => 'vendor']) }}" class="member__link">
                                                    Vendor
                                                </a>
                                            </li>
                                            <li class="member__item {{ $type == "goverment_relation" ? 'active' : '' }}">
                                                <a href="{{ route('memberIndex', ['type' => 'goverment_relation']) }}" class="member__link">
                                                    Government Relation
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-lg-8">
                                    <div class="member__content">
                                        <div class="member__article">
                                           
                                            @if(!empty($lists))
                                                <div class="article__title">
                                                    {{ $lists->post_title ?? '' }}
                                                </div>
                                                <div class="article__body">
                                                    {!! $lists->post_content ?? '' !!}
                                                </div>
                                            @endif
                                        </div>
                                        {{-- <div class="text-center">
                                            <a href="#" class="btn btn--loadmore">
                                                <span class="text__link">โหลดเพิ่มเติม</span>
                                            </a>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
@push("scripts")
<script>
    $(document).ready(function(){
       
    })
</script>
@endpush