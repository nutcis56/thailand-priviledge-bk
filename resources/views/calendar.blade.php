@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>
@section("content")
@push("css")
<style>
    .calendar_hidden {
        display: none;
    }
    .calendar_event:hover {
        cursor: pointer;
    }
</style>
@endpush
<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{route('calendarList')}}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น" value={{ $keywordSearch != "" ? $keywordSearch : "" }}>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group mb-3 mb-lg-0">
                                            @php 
                                                $tags = CoreConfigService::getTag();
                                            @endphp
                                            <select class="searchTag form-control" name="searchTag">
                                                <option value="">แท๊ก</option>
                                                @if(count($tags) > 0)
                                                    @foreach($tags as $key => $value)
                                                        <option value="{{ $value->name }}" @if(isset($keywordTag)) {{ $value->name == $keywordTag ? 'selected' : '' }} @endif>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>


                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item">ข่าวสาร</li>
                                    <li class="breadcrumb-item active"><a href="{{ route('calendarList') }}" class="breadcrumb-link">ปฏิทินกิจกรรม</a></li>
                                </ol>
                            </nav>
                        </div>
                        <div class="tabs--views">
                            <ul class="nav nav-tabs d-flex justify-content-end" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="RealEstateTab" data-toggle="tab" href="#RealEstate" role="tab" aria-controls="RealEstate" aria-selected="true"><span class="icon-list-view"></span></a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" id="ConstructionTab" data-toggle="tab" href="#Construction" role="tab" aria-controls="Construction" aria-selected="false"><span class="icon-date"></span></a>
                                </li>
                            </ul>


                            <div class="tab-content" id="operationTab">
                                <div class="tab-pane fade" id="RealEstate" role="tabpanel" aria-labelledby="RealEstateTab">
                                    <div class="news--list">
                                        <div class="card-deck card-deck--cards-1 card-deck--cards-md-2 card-deck--cards-lg-3">

                                            @if(count($lists) > 0)
                                                @foreach($lists as $key => $value)
                                                    @php
                                                        $strtotime = strtotime($value->post_date);
                                                        $date = date('d.m.Y', $strtotime);
                                                        $dateExplode = explode('.', $date);

                                                        $strtotimeStartDate =  strtotime($value->start_date);
                                                        $startDate = date('d-m-Y', $strtotimeStartDate);

                                                        $endotimeStartDate =  strtotime($value->end_date);
                                                        $endDate = date('d-m-Y', $endotimeStartDate);

                                                    @endphp
                                                    @if($key < 3)
                                                        <a href="/calendar-list/{{ $value->post_name != "" ? $value->post_name : $value->id }}" class="card card--calendar">
                                                            <figure class="card__figure">
                                                                <img src="{{ $value->post_cover }}" alt="" class="card__image card__zoom-in">
                                                                <div class="card__date">
                                                                    <p class="date__title">
                                                                        {{ $dateExplode[0] }}
                                                                    </p>
                                                                    <p class="date__text">
                                                                        {{$dateExplode[1]}}.{{$dateExplode[2]}}
                                                                    </p>
                                                                </div>
                                                            </figure>
            
                                                            <div class="card__body">
                                                                <div class="card__catalogue">
                                                                    <div class="card__type">
                                                                        {{ $value->position->name ?? '' }}
                                                                    </div>
                                                                    
                                                                </div>
                                                                <h3 class="card__title">
                                                                    {{$value->post_title ?? ''}}
                                                                </h3>
                                                                <ul class="list-group">
                                                                    <li class="list-group-item"><i class="icon-date"></i> {{$date}}</li>
                                                                    <li class="list-group-item"><i class="icon-create"></i> {{ $value->userInfo->name ?? '' }} </li>
                                                                </ul>
                                                                <p class="card__text">
                                                                    {{$value->post_excerpt ?? ''}}
                                                                </p>
                                                                <p class="card__text">
                                                                    วันที่จัดกิจกรรม {{$startDate ?? ''}} – {{$endDate ?? ''}}
                                                                </p>
                                                            </div>
                                                        </a>
                                                    @else 

                                                        <a href="/calendar-list/{{ $value->post_name != "" ? $value->post_name : $value->id }}" class="card card--calendar calendar_hidden">
                                                            <figure class="card__figure">
                                                                <img src="{{ $value->post_cover }}" alt="" class="card__image card__zoom-in">
                                                                <div class="card__date">
                                                                    <p class="date__title">
                                                                        {{ $dateExplode[0] }}
                                                                    </p>
                                                                    <p class="date__text">
                                                                        {{$dateExplode[1]}}.{{$dateExplode[2]}}
                                                                    </p>
                                                                </div>
                                                            </figure>
            
                                                            <div class="card__body">
                                                                <div class="card__catalogue">
                                                                    <div class="card__type">
                                                                        {{ $value->position->name ?? '' }}
                                                                    </div>
                                                                    
                                                                </div>
                                                                <h3 class="card__title">
                                                                    {{$value->post_title ?? ''}}
                                                                </h3>
                                                                <ul class="list-group">
                                                                    <li class="list-group-item"><i class="icon-date"></i> {{$date}}</li>
                                                                    <li class="list-group-item"><i class="icon-create"></i> {{ $value->userInfo->name ?? '' }} </li>
                                                                </ul>
                                                                <p class="card__text">
                                                                    {{$value->post_excerpt ?? ''}}
                                                                </p>
                                                                <p class="card__text">
                                                                    วันที่จัดกิจกรรม {{$startDate ?? ''}} – {{$endDate ?? ''}}
                                                                </p>
                                                            </div>
                                                        </a>

                                                    @endif
                                                @endforeach
                                            @endif

                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <a href="#" class="btn btn--loadmore" id="btn_loadmore">
                                            <span class="text__link">โหลดเพิ่มเติม</span>
                                        </a>
                                    </div>
                                </div>


                                <div class="tab-pane fade show active" id="Construction" role="tabpanel" aria-labelledby="ConstructionTab">
                                    <div class="calendar--wrapper">
                                        <div class="row">
                                            <div class="col-12 col-lg-7 wrap__calendar">
                                                <div id="calendar-container">
                                                    <div></div>
                                                </div>
                                            </div>
                                            <div id="event-list" class="col-12 col-lg-5 bg_right_calendar">
                                                <div id="event-list-content">
                                                    <h4>กิจกรรม</h4>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection

@push('css')
<link rel="stylesheet" href="{{asset('vendor/calendar-v3.9/fullcalendar.min.css')}}">
@endpush


@push('scripts')
<script src="{{asset('vendor/calendar-v3.9/lib/moment.min.js')}}"></script>
{{-- <script src="{{asset('vendor/calendar-v3.9/fullcalendar.min.js')}}"></script>
    <script src="{{asset('vendor/calendar-v3.9/locale-all.js')}}"></script> --}}
<script>
    //const skeletonLoader = $('#skeleton-loader').scheletrone();

    const categoryNameList = [
      'seminar', 'webinar', 'business_matching', 'information',
    ];
    const categoryEnableValue = {
      [categoryNameList[0]]: true,
      [categoryNameList[1]]: true,
      [categoryNameList[2]]: true,
      [categoryNameList[3]]: true,
    };

    function getFormattedDate(date) {
      const offset = date.getTimezoneOffset();
      const selectedDate = new Date(date.getTime() - (offset * 60 * 1000));
      return selectedDate.toISOString().split('T')[0];
    }

    function getClassName(categoryIdList) {
      const className = categoryIdList.reduce((total, categoryId) => {
        return total + ` ${categoryNameList[categoryId - 1]}`;
      }, 'event_date');

      return filterClass(className);
    }

    function filterClass(cl) {
      for (const category in categoryEnableValue) {
        if (categoryEnableValue[category] === false) {
          cl = cl.replace(category, '');
        }
      }
      return cl;
    }

    let selectedDate = null;
    const calendar = $('#calendar-container div');
    // const dateEvent = "{{json_encode($dateMarkerCalendar)}}";
    // const dateEvent = ['2021-12-01'];
    const dateEvent = "<?php echo implode(",", $dateMarkerCalendar); ?>".split(",");

    
    calendar.datepicker({
      todayHighlight: true,
      format: "mm/dd/yyyy",
      weekStart: 0,
      beforeShowDay: function (date) {
        const categoryIdList = [];
        const formattedDate = getFormattedDate(date);

        let monthFormat = date.getMonth()+1;

        if(monthFormat > 0 && monthFormat < 10) {
            monthFormat = "0"+monthFormat;
        }
        
        calender_date = date.getFullYear()+'-'+monthFormat+'-'+('0'+date.getDate()).slice(-2);


        // if(dateEvent.indexOf(calender_date) > -1) {
        //     console.log('have');
        //     return "event_date seminar"
        // }
           
        if(dateEvent.includes(calender_date)) {
            return "event_date seminar"
        }
        
      },
    }).on('changeDate', function (e) {
        selectedDate = e.date;

        let monthFormat = selectedDate.getMonth()+1;

        if(monthFormat > 0 && monthFormat < 10) {
            monthFormat = "0"+monthFormat;
        }
        
        calender_date = selectedDate.getFullYear()+'-'+monthFormat+'-'+('0'+selectedDate.getDate()).slice(-2);

        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            headers: {'X-CSRF-TOKEN': token},
            type: "GET",
            url: "{{route('calendarAjaxDetail')}}",
            data: {data: calender_date},
            success: function (response) {
               
                if(response.status == 'success') {
                    $('#event-list').html(response.data);

                    $('.eventDetailData').click(function() {
                        let id = $(this).attr('data-id');
                        window.open("/calendar-list/"+id, '_blank');
                    });
                }
                
            },
            error: function (response) {
                //
            }
        });

      //console.log(getFormattedDate(e.date));
    //   eventList.fetchEventByDate(getFormattedDate(e.date));
    });

    $(function () { //document ready
      $('.filter_calendar input').each(function () {
        $(this).change(function () {
          categoryEnableValue[$(this).attr('id')] = $(this).is(':checked');
          //console.log(categoryEnableValue);
          calendar.datepicker('update');
          if (selectedDate != null) {
            calendar.datepicker('setDate', selectedDate);
          }
          eventList.updateCategoryEnableValue();
        });
      });
    });
</script>

<script>
    $("#btn_loadmore").click(function(){
        $("a").removeClass("calendar_hidden");
        $(this).fadeOut();
    });

    $(document).ready(function() {
        $('.searchTag').select2();
    });
    
    
</script>
{{-- 
    <script src="{{asset('vendor/calendar-v3.9/fullcalendar.min.js')}}"></script>
    <script src="{{asset('vendor/calendar-v3.9/locale-all.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $('#calendar').fullCalendar('render');
            });

            $("#btn_loadmore").click(function(){
                $("a").removeClass("calendar_hidden");
                $(this).fadeOut();
            });

            $("#calendarDetailList").click(function() {
                let id = $("#calendar_event_value").val();
                var url = '{{ route("calendarDetail", ":id") }}';
                url = url.replace(':id', id);
                window.location.href = url;
            })
        });
    </script>
    <script>
        var d = new Date();
        var y = d.getFullYear();

       
        let data_render = [
            {
                "title": "aaaa",
                "start": "2021-09-23"
               
            }
        ];

        let data_calendarList = @json($calendarList);
        
        $('#calendar').fullCalendar({
            header: {
                left: 'prev',
                center: 'next',
                right: 'title'
            },
            locale: 'th',
            defaultView: 'month',
            defaultDate: moment().format("YYYY-MM-DD"),
            firstDay: '1',
            navLinks: false, // can click day/week names to navigate views
            editable: false,
            eventLimit: 5, // allow "more" link when too many events
            events: data_calendarList,
            eventClick: function(info) {
                console.log(info);
                $("#calendarDetailList").text(info.title);
                $("#calendar_event_value").val(info.id);
            }
        });
    </script> --}}
@endpush