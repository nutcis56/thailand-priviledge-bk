@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>
@section("content")


<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{route('eventActivityIndex')}}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group mb-3 mb-lg-0">
                                            @php 
                                                $tags = CoreConfigService::getTag();
                                            @endphp
                                            <select class="searchTag form-control" name="searchTag">
                                                <option value="">แท๊ก</option>
                                                @if(count($tags) > 0)
                                                    @foreach($tags as $key => $value)
                                                        <option value="{{ $value->name }}" @if(isset($keywordTag)) {{ $value->name == $keywordTag ? 'selected' : '' }} @endif>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item">ข่าวสาร</li>
                                    <li class="breadcrumb-item active"><a href="{{ route('eventActivityIndex') }}" class="breadcrumb-link">Event & Activity</a></li>
                                </ol>
                            </nav>
                        </div>
                        <div class="news">

                            @if(!empty($lists))
                                @php 
                                    $tags = json_decode($lists->post_tags);
                                    $date = date_format($lists->created_at, 'd.m.Y');    
                                @endphp

                                
                                <h1 class="news__title">
                                    {{ $lists->post_title ?? '' }}
                                </h1>
                                <div class="news__tools">
                                    <div class="dvleft">
                                        <div class="news__type">
                                            {{ $lists->position->name ?? "" }}
                                        </div>
                                        
                                    </div>
                                    <div class="dvright">
                                        <div class="news__date">
                                            <i class="icon-date"></i>  {{ $date ?? ''}}
                                        </div>
                                        <div class="news__by">
                                            <i class="icon-create"></i> {{ $lists->userInfo->name ?? '' }}
                                        </div>
                                    </div>
                                </div>
                                <div class="news__body">
                                    <p style="text-align: center;"><img src="{{$lists->post_cover ?? asset('images/no_image.png')}}" alt="" class="img-fluid"></p>
                                    {{ $lists->post_excerpt ?? '' }}
                                    {!! $lists->post_content ?? '' !!}
                                  
                                </div>
                                <div class="tag--container">
                                    <div class="tag__title">
                                        Tags
                                    </div>
                                    @if(is_array($tags))
                                        @foreach($tags as $tag)
                                            <div class="tag__text">
                                                <a href="{{ route('tagIndex', ['tag' => $tag]) }}">#{{$tag}}</a>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="share--container">
                                    {{-- <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--facebook">
                                        <div class="share--container-icon"><i class="fab fa-facebook-f"></i></div> 
                                        <div class="share--container-text">Facebook</div>
                                    </a> 
                                    <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--line">
                                        <div class="share--container-icon"><i class="fab fa-line"></i></div>
                                        <div class="share--container-text">Line</div>
                                    </a> --}}
                                    <a href="#" id="btn-email" data-id="{{ $lists->id }}" class="share--container-link share--container-link--email">
                                        <div class="share--container-icon"><i class="far fa-envelope"></i></div>
                                        <div class="share--container-text">Email</div>
                                    </a>
                                    <a href="#" id="btn-like" data-id="{{ $lists->id }}" class="share--container-link share--container-link--like {{ $status['like_status'] }}">
                                        <div class="share--container-icon"><i class="far fa-thumbs-up"></i></div>
                                        <div class="share--container-text">Like {{$lists->like_count ?? 0}}</div>
                                    </a>
                                    <a href="#" id="btn-favorite" data-id="{{ $lists->id }}" class="share--container-link share--container-link--fav {{ $status['favorite_status'] }}">
                                        <div class="share--container-icon"><i class="fas fa-heart"></i></div>
                                        <div class="share--container-text">Favorite</div>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">แจ้งเตือน</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                เพิ่มรายการโปรดเสร็จสิ้น
                </div>
                <div class="modal-footer">
                    <button type="button" style="width: 20%"  class="btn-primary btn" data-dismiss="modal">ตกลง</button>
                </div>
            </div>
            </div>
        </div>
    </section>
</main>

@include('elements.modal_email', ['id' => $lists->id])

@endsection

@push("scripts")
    <script>
         $(document).ready(function() {
            $('.searchTag').select2();

            $("#btn-email").click(function(){
                $("#modalEmailConfirm").modal('show');
            })
        });
    </script>
@endpush