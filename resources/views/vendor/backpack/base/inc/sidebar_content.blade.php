<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
@php
    // dd(backpack_user()->roles->count());
@endphp
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-area-chart nav-icon"></i> {{ trans('backpack::menu.dashboard') }}</a></li>

@include('vendor.backpack.base.inc.menu.knowledge-base')
@include('vendor.backpack.base.inc.menu.open-online-course')
@include('vendor.backpack.base.inc.menu.member')
@include('vendor.backpack.base.inc.menu.news')
@include('vendor.backpack.base.inc.menu.about-us')
@include('vendor.backpack.base.inc.menu.data-manage')

@if (backpack_user()->hasPermissionTo('ผู้ดูแลระบบ', 'backpack'))

    <li class="nav-title"><i class="la la-black-tie nav-icon"></i> {{ trans('backpack::menu.admin-menu') }}</li>
    <div class="wrap-each-menu">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('account') }}"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.admin-member') }}</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.admin-role') }}</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('core-config') }}"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.system-setting') }}</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user-position') }}"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.user-position') }}</a></li>

        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('core-config-menu') }}"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.core-config-menu') }}</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('core-config-big-banner') }}"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.core-config-big-banner') }}</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.file-manager') }}</a></li>


        @php
            $user = \Auth::guard('backpack')->user();
            $email_e = $user->email;
            $password = request()->session()->get('password');
            $EncodePassword = new \App\Helpers\EncodePassword;
            $password = $EncodePassword->getEncodePassword($password);
            $password = json_encode($password);
            $password = urlencode($password);
        @endphp


        <li class="nav-item"><a class="nav-link" target="_blank" href="{{ config('app.pydio_site_url') }}login?username={{ $email_e }}&ind={{ $password }}&force_login=true"><i class="la la-angle-right nav-icon"></i> Pydio</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('core-config-service-link') }}"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.core-config-service-link') }}</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('core-config-icon-link') }}"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.core-config-icon-link') }}</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('post') }}"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.post-manager') }}</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('visitor-log') }}'><i class='la la-angle-right nav-icon'></i> {{ trans('backpack::menu.log-all') }}</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('visitor-log-admin') }}'><i class='la la-angle-right nav-icon'></i> {{ trans('backpack::menu.log-admin') }}</a></li>

        @php
            $theme_setting = \App\Models\ConfigThemeColor::first();
            $theme_setting_id = $theme_setting ? $theme_setting->id : false;


            $apiService = \App\Models\ApiService::first();
            $apiServiceId = $apiService ? $apiService->id : false;
        @endphp

        @if ($theme_setting)
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('config-theme-color') }}/{{ $theme_setting_id}}/edit"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.theme-color') }}</a></li>
        @endif

        @if($apiService)
            <li class="nav-item"><a class="nav-link" href="{{ backpack_url('api-service') }}/{{ $apiServiceId}}/edit"><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.api-service') }}</a></li>
        @endif

        <li class='nav-item'><a class='nav-link' href='{{ config('app.bookstack_site_url') }}/force-sync' target="_blank"><i class='la la-angle-right nav-icon'></i> {{ trans('backpack::menu.force-sync') }}</a></li>
    </div>

@endif


{{-- <li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('backup') }}'><i class='nav-icon la la-hdd-o'></i> Backups</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('log') }}'><i class='nav-icon la la-terminal'></i> Logs</a></li>

<!-- Users, Roles, Permissions -->
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon la la-users"></i> Authentication</a>
    <ul class="nav-dropdown-items">
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('user') }}"><i class="nav-icon la la-user"></i> <span>Users</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('role') }}"><i class="nav-icon la la-id-badge"></i> <span>Roles</span></a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('permission') }}"><i class="nav-icon la la-key"></i> <span>Permissions</span></a></li>
    </ul>
</li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}\"><i class="nav-icon la la-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('post') }}'><i class='nav-icon la la-question'></i> Posts</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon la la-question'></i> Categories</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('document') }}'><i class='nav-icon la la-question'></i> Documents</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('core-config') }}'><i class='nav-icon la la-question'></i> Core configs</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('blog') }}'><i class='nav-icon la la-question'></i> Blogs</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('q-and-a') }}'><i class='nav-icon la la-question'></i> Q and as</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('knowledge-idea-sharing') }}'><i class='nav-icon la la-question'></i> Knowledge idea sharings</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('your-job-knowledge') }}'><i class='nav-icon la la-question'></i> Your job knowledges</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('t-p-c-sharing-gallery') }}'><i class='nav-icon la la-question'></i> T p c sharing galleries</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('t-p-c-sharing-video') }}'><i class='nav-icon la la-question'></i> T p c sharing videos</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('corse') }}'><i class='nav-icon la la-question'></i> Corses</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('new-release') }}'><i class='nav-icon la la-question'></i> New releases</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('new-announce') }}'><i class='nav-icon la la-question'></i> New announces</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('new-calendar') }}'><i class='nav-icon la la-question'></i> New calendars</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('new-event') }}'><i class='nav-icon la la-question'></i> New events</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('user-position') }}'><i class='nav-icon la la-question'></i> User positions</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('member-contact-center') }}'><i class='nav-icon la la-question'></i> Member contact centers</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('elite-personal-liaison') }}'><i class='nav-icon la la-question'></i> Elite personal liaisons</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('elite-personal-assistant') }}'><i class='nav-icon la la-question'></i> Elite personal assistants</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('vendor') }}'><i class='nav-icon la la-question'></i> Vendors</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('government-relation') }}'><i class='nav-icon la la-question'></i> Government relations</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('course-file') }}'><i class='nav-icon la la-question'></i> Course files</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('course-lesson') }}'><i class='nav-icon la la-question'></i> Course lessons</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('o-chart-main') }}'><i class='nav-icon la la-question'></i> O chart mains</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('o-chart-second') }}'><i class='nav-icon la la-question'></i> O chart seconds</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('o-chart-third') }}'><i class='nav-icon la la-question'></i> O chart thirds</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('config-static-page') }}'><i class='nav-icon la la-question'></i> Config static pages</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('config-theme-color') }}'><i class='nav-icon la la-question'></i> Config theme colors</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('visitor-log') }}'><i class='nav-icon la la-question'></i> Visitor logs</a></li> --}}
{{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('visitor-log-admin') }}'><i class='nav-icon la la-question'></i> Visitor log admins</a></li> --}}

@push('after_scripts')
<script>
	jQuery(document).ready(function() {
		$('li.nav-title').each(function() {
			console.log($(this).next()[0])
            // if ($($(this).next()[0]).hasClass('nav-title')) {
			// 	$(this).hide()
			// }
			if ($(this).next()[0].children.length == 0) {
				$(this).hide()
			}
		})
	})
</script>
@endpush