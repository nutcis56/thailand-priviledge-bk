@if (config('backpack.base.show_powered_by') || config('backpack.base.developer_link'))
    <div class="text-muted ml-auto mr-auto">
      {{-- @if (config('backpack.base.developer_link') && config('backpack.base.developer_name'))
      {{ trans('backpack::base.handcrafted_by') }} <a target="_blank" rel="noopener" href="{{ config('backpack.base.developer_link') }}">{{ config('backpack.base.developer_name') }}</a>.
      @endif
      @if (config('backpack.base.show_powered_by'))
      {{ trans('backpack::base.powered_by') }} <a target="_blank" rel="noopener" href="http://backpackforlaravel.com?ref=panel_footer_link">Backpack for Laravel</a>.
      @endif --}}
    </div>
    <div class="copyright">
      <div class="logo-footer">
        {{-- <img src="{{ asset(Setting::get('admin_logo_image_footer')) }}" /> --}}
        <div class="text_footer">
          <h4></h4>
          <address></address>
        </div>
      </div>
    </div>
@endif