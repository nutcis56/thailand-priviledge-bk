<li class="nav-title"><i class="la la-sitemap nav-icon"></i> {{ trans('backpack::menu.about-company') }}</li>
    <div class="wrap-each-menu">
        @if (backpack_user()->hasPermissionTo('เกี่ยวกับองค์กร - โครงสร้างองค์กร', 'backpack'))
            <li class="nav-item nav-dropdown" id="organization_menu">
                <a class="nav-link nav-dropdown-toggle" href="#">
                    <i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.company-structure') }}
                </a>
                <ul class="nav-dropdown-items">
                    <li class='nav-item' ><a class='nav-link' id="organization_menu_main" href='{{ backpack_url('o-chart-main') }}'><i class="la la-angle-double-right nav-icon"></i> {{ trans('backpack::menu.line-of-work') }}</a></li>
                    {{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('o-chart-second') }}'><i class="la la-angle-double-right nav-icon"></i> {{ trans('backpack::menu.part') }}</a></li>
                    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('o-chart-third') }}'><i class="la la-angle-double-right nav-icon"></i> {{ trans('backpack::menu.department') }}</a></li> --}}
                </ul>
            </li>
        @endif
        @php
            $vision = \App\Models\StaticAboutUsPage::where('post_name', 'vision')->first();
            $vision_id = $vision ? $vision->id : false;

            $mission = \App\Models\StaticAboutUsPage::where('post_name', 'mission')->first();
            $mission_id = $mission ? $mission->id : false;

            $policy = \App\Models\StaticAboutUsPage::where('post_name', 'policy')->first();
            $policy_id = $policy ? $policy->id : false;
            
        @endphp
        @if (backpack_user()->hasPermissionTo('เกี่ยวกับองค์กร - วิสัยทัศน์', 'backpack'))
            @if ($vision)
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('static-about-us-page') }}/{{$vision_id}}/edit' disabled><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.vision') }}</a></li>
            @endif
        @endif
        @if (backpack_user()->hasPermissionTo('เกี่ยวกับองค์กร - พันธกิจ', 'backpack'))
            @if ($mission)
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('static-about-us-page') }}/{{$mission_id}}/edit'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.mission') }}</a></li>
            @endif
        @endif
        @if (backpack_user()->hasPermissionTo('เกี่ยวกับองค์กร - นโยบาย', 'backpack'))
            @if ($policy)
                <li class='nav-item'><a class='nav-link' href='{{ backpack_url('static-about-us-page') }}/{{$policy_id}}/edit'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.policy') }}</a></li>
            @endif
        @endif
    </div>