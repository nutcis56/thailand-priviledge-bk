<li class="nav-title"><i class="la la-book nav-icon"></i> {{ trans('backpack::menu.knowledge-base') }}</li>
    <div class="wrap-each-menu">
        @if (backpack_user()->hasPermissionTo('คลังความรู้ - Blog', 'backpack'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('blog') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.blog') }}</a></li>
        @endif
        @if (backpack_user()->hasPermissionTo('คลังความรู้ - Q&A', 'backpack'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('q-and-a') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.q-and-a') }}</a></li>
        @endif
        @if (backpack_user()->hasPermissionTo('คลังความรู้ - Knowledge Idea Sharing', 'backpack'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('knowledge-idea-sharing') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.knowledge-idea-sharing-blog') }}</a></li>
        @endif
        @if (backpack_user()->hasPermissionTo('คลังความรู้ - Your Job Knowledge', 'backpack'))
            <li class='nav-item'>
                <a class='nav-link' href='{{ backpack_url('your-job-knowledge') }}'>
                    <i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.your-job-knowledge-blog') }}
                </a>
            </li>
        @endif
        @if (backpack_user()->hasPermissionTo('คลังความรู้ - TPC Sharing Gallery', 'backpack'))
            <li class='nav-item'>
                <a class='nav-link' href='{{ backpack_url('t-p-c-sharing-gallery') }}'>
                    <i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.tpc-sharing-gallery') }}
                </a>
            </li>
        @endif
        @if (backpack_user()->hasPermissionTo('คลังความรู้ - TPC Sharing VDO', 'backpack'))
            <li class='nav-item'>
                <a class='nav-link' href='{{ backpack_url('t-p-c-sharing-video') }}'>
                    <i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.tpc-sharing-vdo') }}
                </a>
            </li>
        @endif
    </div>