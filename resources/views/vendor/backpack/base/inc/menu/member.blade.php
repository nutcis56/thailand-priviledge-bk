{{-- <li class="nav-title"><a class='nav-link' href='http://147.50.76.81:81/'><i class="la la-users nav-icon"></i> {{ trans('backpack::menu.member-service') }}</a></li> --}}
{{-- <li class="nav-title"><i class="la la-video-camera nav-icon"></i> {{ trans('backpack::menu.open-online-course') }}</li> --}}
@if (backpack_user()->hasPermissionTo('บริการสมาชิก', 'backpack'))
    <li class='nav-title'><a class='nav-link' target="_blank" href='{{ config('app.bookstack_site_url') }}flibem?email={{ base64_encode(\Auth::user()->email) }}' style="padding: 0.5rem 0rem 0.5rem 0rem;"><i class="la la-users nav-icon"></i> {{ trans('backpack::menu.member-service') }}</a></li>
    <div class="wrap-each-menu">
        <div></div>
    </div>
@endif
    {{-- <li class='nav-item'><a class='nav-link' href='http://147.50.76.81:81/'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.member-contact-center') }}</a></li> --}}
    {{-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('elite-personal-liaison') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.elite-personal-liaison') }}</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('elite-personal-assistant') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.elite-personal-assistant') }}</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('vendor') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.vendor') }}</a></li>
    <li class='nav-item'><a class='nav-link' href='{{ backpack_url('government-relation') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.government-relation') }}</a></li> --}}