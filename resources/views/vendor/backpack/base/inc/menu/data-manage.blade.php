<li class="nav-title"><i class="la la-sitemap nav-icon"></i> {{ trans('backpack::menu.data-manage') }}</li>
    <div class="wrap-each-menu">
        @if (backpack_user()->hasPermissionTo('จัดการข้อมูล - หมวดหมู่', 'backpack'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.category') }}</a></li>
        @endif
        
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category-blog') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.category_blog') }}</a></li>

        @if (backpack_user()->hasPermissionTo('จัดการข้อมูล - Phone Directory', 'backpack'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('phone-directory') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.phone_directory') }}</a></li>
        @endif

        
    </div>