<li class="nav-title"><i class="la la-calendar-check-o nav-icon"></i> {{ trans('backpack::menu.news') }}</li>
    <div class="wrap-each-menu">
        @if (backpack_user()->hasPermissionTo('ข่าวสาร - ข่าวประชาสัมพันธ์', 'backpack'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('new-release') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.news-release') }}</a></li>
        @endif
        @if (backpack_user()->hasPermissionTo('ข่าวสาร - ประกาศ', 'backpack'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('new-announce') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.news-announcement') }}</a></li>
        @endif
        @if (backpack_user()->hasPermissionTo('ข่าวสาร - ปฏิทินกิจกรรม', 'backpack'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('new-calendar') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.news-calender') }}</a></li>
        @endif
        @if (backpack_user()->hasPermissionTo('ข่าวสาร - Event&Activity', 'backpack'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('new-event') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.news-event-and.activity') }}</a></li>
        @endif
    </div>