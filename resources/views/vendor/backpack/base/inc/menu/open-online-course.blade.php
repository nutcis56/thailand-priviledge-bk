@php
    $user = \Auth::guard('backpack')->user();
    $email_e = explode('@', $user->email);
    if ($email_e[0] == 'admin') {
        $username = 'admin';
        $password = 'admin';
    } else {
        $username = strtolower($email_e[0]);
        $password = request()->session()->get('password');
    }
    $username = $user->email;
    $EncodePassword = new \App\Helpers\EncodePassword;
    $password = $EncodePassword->getEncodePassword($password);
    $password = json_encode($password);
    $password = urlencode($password);
@endphp

<li class="nav-title"><i class="la la-video-camera nav-icon"></i> {{ trans('backpack::menu.open-online-course') }}</li>
    <div class="wrap-each-menu">
        @if (backpack_user()->hasPermissionTo('Open Online Course - Course', 'backpack'))
            <li class='nav-item'><a class='nav-link' href='{{ backpack_url('course') }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.course') }}</a></li>
        @endif
        {{-- <li class='nav-item'><a class='nav-link' href='{{ config('app.qat_site_url') }}flibem?email={{ base64_encode(\Auth::user()->email) }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.quiz-and-test') }}</a></li> --}}
        @if (backpack_user()->hasPermissionTo('Open Online Course - Quiz&Test', 'backpack'))
            <li class='nav-item'><a class='nav-link' target="_blank" href='{{ config('app.qat_site_url') }}?force_login=true&username={{ $username }}&ind={{ $password }}'><i class="la la-angle-right nav-icon"></i> {{ trans('backpack::menu.quiz-and-test') }}</a></li>
        @endif
    </div>