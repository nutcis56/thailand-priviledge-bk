@extends(backpack_view('blank'))

@php
	// ---------------------
	// JUMBOTRON widget demo
	// ---------------------
	// Widget::add([
 //        'type'        => 'jumbotron',
 //        'name' 		  => 'jumbotron',
 //        'wrapperClass'=> 'shadow-xs',
 //        'heading'     => trans('backpack::base.welcome'),
 //        'content'     => trans('backpack::base.use_sidebar'),
 //        'button_link' => backpack_url('logout'),
 //        'button_text' => trans('backpack::base.logout'),
 //    ])->to('before_content')->makeFirst();

	// -------------------------
	// FLUENT SYNTAX for widgets
	// -------------------------
	// Using the progress_white widget
	// 
	// Obviously, you should NOT do any big queries directly in the view.
	// In fact, it can be argued that you shouldn't add Widgets from blade files when you
	// need them to show information from the DB.
	// 
	// But you do whatever you think it's best. Who am I, your mom?

	//* WIDGET 1 KNOWLEDGE IDEA SHARING
	$categoryKnowledge = \App\Models\Category::where('name->'.app()->getLocale(), 'knowledge')->first();
	$cateId = (!empty($categoryKnowledge)) ? $categoryKnowledge->id : 0;
	$postCateIds = \App\Models\PostCate::where('cate_id', $cateId)->get()->toArray();
	$knowledgeIds = (!empty($postCateIds)) ? array_column($postCateIds, 'post_id') : [0];

	//* WIDGET 2 KNOWLEDGE IDEA SHARING
	$categoryJob = \App\Models\Category::where('name->'.app()->getLocale(), 'job')->first();
	$cateJobId = (!empty($categoryJob)) ? $categoryJob->id : 0;
	$postCateJobId = \App\Models\PostCate::where('cate_id', $cateJobId)->get()->toArray();
	$jobIds = (!empty($postCateJobId)) ? array_column($postCateJobId, 'post_id') : [0];

	//* WIDGET 3 TCP SHARING GALLERY
	$categoryGallery = \App\Models\Category::where('name->'.app()->getLocale(), 'gallery')->first();
	$cateGalleryId = (!empty($categoryGallery)) ? $categoryGallery->id : 0;
	$postCateGalleryId = \App\Models\PostCate::where('cate_id', $cateGalleryId)->get()->toArray();
	$galleryIds = (!empty($postCateGalleryId)) ? array_column($postCateGalleryId, 'post_id') : [0];
	
	//* WIDGET 4 USERS
	$userIds = \App\Models\User::get()->count();
	

	//* WIDGET AFTER CONTENT 1
	$lastedKnowledge = \App\Models\Post::whereIn('id', $knowledgeIds)->orderBy('id','DESC')->limit(1)->get();
	$widgetAfterContentKnowledgeTitle = (count($lastedKnowledge) > 0) ? $lastedKnowledge[0]->post_title : '';
	$widgetAfterContentKnowledgeExcerpt = (count($lastedKnowledge) > 0) ? $lastedKnowledge[0]->post_excerpt : '';

	//* WIDGET AFTER CONTENT 2
	$categoryAnnounce = \App\Models\Category::where('name->'.app()->getLocale(), 'announce')->first();
	$cateAnnounceId = (!empty($categoryAnnounce)) ? $categoryAnnounce->id : 0;
	$postCateAnnounceIds = \App\Models\PostCate::where('cate_id', $cateAnnounceId)->get()->toArray();
	$announceIds = (!empty($postCateAnnounceIds)) ? array_column($postCateAnnounceIds, 'post_id') : [0];
	$lastedAnnounce = \App\Models\Post::whereIn('id', $announceIds)->orderBy('id','DESC')->limit(1)->get();
	$widgetAfterContentAnnounceTitle = (count($lastedAnnounce) > 0) ? $lastedAnnounce[0]->post_title : '';
	$widgetAfterContentAnnounceExcerpt = (count($lastedAnnounce) > 0) ? $lastedAnnounce[0]->post_excerpt : '';

	//* WIDGET AFTER CONTENT 3
	$categoryEvent = \App\Models\Category::where('name->'.app()->getLocale(), 'event')->first();
	$cateEventId = (!empty($categoryEvent)) ? $categoryEvent->id : 0;
	$postCateEventIds = \App\Models\PostCate::where('cate_id', $cateEventId)->get()->toArray();
	$eventIds = (!empty($postCateEventIds)) ? array_column($postCateEventIds, 'post_id') : [0];
	$lastedEvent = \App\Models\Post::whereIn('id', $eventIds)->orderBy('id','DESC')->limit(1)->get();
	$widgetAfterContentEventTitle = (count($lastedEvent) > 0) ? $lastedEvent[0]->post_title : '';
	$widgetAfterContentEventExcerpt = (count($lastedEvent) > 0) ? $lastedEvent[0]->post_excerpt : '';
	
	

	$productCount = 200;
	$userCount = 425;
	$articleCount = 856;
	$lastArticle = \App\Models\Post::orderBy('post_date', 'DESC')->first();
	$lastArticleDaysAgo = 0;
	if ($lastArticle) {
		$lastArticleDaysAgo = \Carbon\Carbon::parse($lastArticle->post_date)->diffInDays(\Carbon\Carbon::today());
	}
 
 	// notice we use Widget::add() to add widgets to a certain group
	Widget::add()->to('before_content')->type('div')->class('row')->content([
		// notice we use Widget::make() to add widgets as content (not in a group)
		Widget::make()
			->type('progress')
			->class('card border-0 text-white bg-primary')
			->progressClass('progress-bar')
			->value(count($knowledgeIds))
			->description('Knowledge Idea Sharing')
			->progress(100*(int)count($knowledgeIds)/100)
			->hint(''),
		// alternatively, to use widgets as content, we can use the same add() method,
		// but we need to use onlyHere() or remove() at the end
		Widget::add()
		    ->type('progress')
		    ->class('card border-0 text-white bg-success')
		    ->progressClass('progress-bar')
		    ->value(count($jobIds))
		    ->description('Your Job Knowledge')
		    ->progress(80)
		    ->hint('')
		    ->onlyHere(), 
		// alternatively, you can just push the widget to a "hidden" group
		Widget::make()
			->group('hidden')
		    ->type('progress')
		    ->class('card border-0 text-white bg-warning')
		    ->value(count($galleryIds))
		    ->progressClass('progress-bar')
		    ->description('TPC Sharing Gallery')
		    ->progress(30)
		    ->hint(''),
		// both Widget::make() and Widget::add() accept an array as a parameter
		// if you prefer defining your widgets as arrays
	    Widget::make([
			'type' => 'progress',
			'class'=> 'card border-0 text-white bg-dark',
			'progressClass' => 'progress-bar',
			'value' => $userIds,
			'description' => 'จำนวนผู้ใช้งาน',
			'progress' => (int)$userIds/75*100,
			'hint' => ''
		]),
	]);



    $widgets['after_content'][] = [
	  'type' => 'div',
	  'class' => 'row mb-5',
	  'content' => [ // widgets 
	       [
			  'type' => 'card',
			  'wrapperClass' => 'col-xs-12 col-sm-4 col-md-4', // optional
			  'class' => 'card h-100', // optional
			  'content' => [
			      'header' => 'ข่าวประชาสัมพันธ์ล่าสุด', // optional
			      'body' => $widgetAfterContentKnowledgeExcerpt
			  ]
			],
			[
			  'type' => 'card',
			  'wrapperClass' => 'col-xs-12 col-sm-4 col-md-4', // optional
			  'class' => 'card h-100', // optional
			  'content' => [
			      'header' => 'ข่าวประกาศล่าสุด', // optional
			      'body' => $widgetAfterContentAnnounceExcerpt,
			  ]
			],
			[
			  'type' => 'card',
			  'wrapperClass' => 'col-xs-12 col-sm-4 col-md-4', // optional
			  'class' => 'card h-100', // optional
			  'content' => [
			      'header' => 'ข่าวกิจกรรมล่าสุด', // optional
			      'body' => $widgetAfterContentEventExcerpt,
			  ]
			],
	  ]
	];
    // $widgets['after_content'][] = [
	//   'type'         => 'alert',
	//   'class'        => 'alert alert-warning bg-dark border-0 mb-4',
	//   'heading'      => 'Demo Refreshes Every Hour on the Hour',
	//   'content'      => 'At hh:00, all custom entries are deleted, all files, everything. This cleanup is necessary because developers like to joke with their test entries, and mess with stuff. But you know that :-) Go ahead - make a developer smile.' ,
	//   'close_button' => true, // show close button or not
	// ];

	

	// BAR CHART
    // $widgets['after_content'][] = [
	//   'type' => 'div',
	//   'class' => 'row',
	//   'content' => [ // widgets 

	//     	[ 
	// 	        'type' => 'chart',
	// 	        'wrapperClass' => 'col-md-12',
	// 	        // 'class' => 'col-md-6',
	// 	        'controller' => \App\Http\Controllers\Admin\Charts\Lines\MenuChartControllerChartController::class,
	// 			'content' => [
	// 			    'header' => 'สถิติการเข้าชมเว็ปไซต์จากเมนู', // optional
	// 			    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
	// 	    	]
	//     	],

    // 	]
	// ];

	//* KNOWLEDGE MOST VIEW
	$visitKnowledge = \App\Models\VisitorLog::select('visitable_id')
						->where('visitable_type', 'knowledgeDetail')
						->groupBy('visitable_id')
						->orderByRaw('count(visitable_id) DESC')
						->first();
	$visitKnowledgeId = (!empty($visitKnowledge)) ? $visitKnowledge->visitable_id : 0;
	$sectionKnowledgeMostView = \App\Models\PostDashBoard::select('post_title', 'post_excerpt')->where('id', $visitKnowledgeId)->first();
	$sectionKnowledgeMostViewCount = \App\Models\VisitorLog::select('visitable_id')
										->where('visitable_type', 'knowledgeDetail')
										->where('visitable_id', $visitKnowledgeId)
										->count();
	

	//* NEW MOST VIEW
	$visitNew = \App\Models\VisitorLog::select('visitable_id')
						->where('visitable_type', 'newDetail')
						->groupBy('visitable_id')
						->orderByRaw('count(visitable_id) DESC')
						->first();
	$visitNewId = (!empty($visitNew)) ? $visitNew->visitable_id : 0;
	$sectionNewMostView = \App\Models\PostDashBoard::select('post_title', 'post_excerpt')->where('id', $visitNewId)->first();
	$sectionNewMostViewCount = \App\Models\VisitorLog::select('visitable_id')
								->where('visitable_type', 'newDetail')
								->where('visitable_id', $visitNewId)
								->count();

	//* ANNOUNCE MOST VIEW
	$visitAnnounce = \App\Models\VisitorLog::select('visitable_id')
						->where('visitable_type', 'announceDetail')
						->groupBy('visitable_id')
						->orderByRaw('count(visitable_id) DESC')
						->first();
	$visitAnnounceId = (!empty($visitAnnounce)) ? $visitAnnounce->visitable_id : 0;
	$sectionAnnounceMostView = \App\Models\PostDashBoard::select('post_title', 'post_excerpt')->where('id', $visitAnnounceId)->first();
	$sectionAnnounceMostViewCount = \App\Models\VisitorLog::select('visitable_id')
										->where('visitable_type', 'announceDetail')
										->where('visitable_id', $visitAnnounceId)
										->count();

	//* CALENDAR MOST VIEW
	$visitCalendar= \App\Models\VisitorLog::select('visitable_id')
						->where('visitable_type', 'calendarDetail')
						->groupBy('visitable_id')
						->orderByRaw('count(visitable_id) DESC')
						->first();
	$visitCalendarId = (!empty($visitCalendar)) ? $visitCalendar->visitable_id : 0;
	$sectionCalendarMostView = \App\Models\PostDashBoard::select('post_title', 'post_excerpt')->where('id', $visitCalendarId)->first();
	$sectionCalendarMostViewCount = \App\Models\VisitorLog::select('visitable_id')
										->where('visitable_type', 'calendarDetail')
										->where('visitable_id', $visitCalendarId)
										->count();

	//* EVENT MOST VIEW
	$visitEvent= \App\Models\VisitorLog::select('visitable_id')
						->where('visitable_type', 'eventActivityDetail')
						->groupBy('visitable_id')
						->orderByRaw('count(visitable_id) DESC')
						->first();
	$visitEventId = (!empty($visitEvent)) ? $visitEvent->visitable_id : 0;
	$sectionEventMostView = \App\Models\PostDashBoard::select('post_title', 'post_excerpt')->where('id', $visitEventId)->first();
	$sectionEventMostViewCount = \App\Models\VisitorLog::select('visitable_id')
										->where('visitable_type', 'eventActivityDetail')
										->where('visitable_id', $visitEventId)
										->count();

	//* COURSE MOST VIEW 
	$visitCourse= \App\Models\VisitorLog::select('visitable_id')
						->where('visitable_type', 'courseDetail')
						->groupBy('visitable_id')
						->orderByRaw('count(visitable_id) DESC')
						->first();
	$visitCourseId = (!empty($visitCourse)) ? $visitCourse->visitable_id : 0;
	$sectionCourseMostView = \App\Models\PostDashBoard::select('post_title', 'post_excerpt')->where('id', $visitCourseId)->first();
	$sectionCourseMostViewCount = \App\Models\VisitorLog::select('visitable_id')
										->where('visitable_type', 'courseDetail')
										->where('visitable_id', $visitCourseId)
										->count();

	

	//* WidGet knowledge,New,Announce 
	// $widgets['after_content'][] = [
	//   'type' => 'div',
	//   'class' => 'row',
	//   'content' => [ // widgets 
	//        [
	// 		  'type' => 'card',
	// 		  // 'wrapperClass' => 'col-sm-6 col-md-4', // optional
	// 		  'class' => 'card bg-dark text-white', // optional
	// 		  'content' => [
	// 		      'header' => 'คลังความรู้ ยอดนิยม', // optional
	// 		      'body' => $sectionKnowledgeMostView->post_excerpt
	// 		  ]
	// 		],
	// 		[
	// 		  'type' => 'card',
	// 		  // 'wrapperClass' => 'col-sm-6 col-md-4', // optional
	// 		  'class' => 'card bg-info text-white', // optional
	// 		  'content' => [
	// 		      'header' => 'ข่าวประชาสัมพันธ์ ยอดนิยม', // optional
	// 		      'body' => $sectionNewMostView->post_excerpt,
	// 		  ]
	// 		],
	// 		[
	// 		  'type' => 'card',
	// 		  // 'wrapperClass' => 'col-sm-6 col-md-4', // optional
	// 		  'class' => 'card bg-primary text-white', // optional
	// 		  'content' => [
	// 		      'header' => 'ข่าวประกาศ ยอดนิยม', // optional
	// 		      'body' => $sectionAnnounceMostView->post_excerpt,
	// 		  ]
	// 		],
	//   ]
	// ];

	

	// //* WidGet Calendar, Event, Course 
	// $widgets['after_content'][] = [
	//   'type' => 'div',
	//   'class' => 'row',
	//   'content' => [ // widgets 
	//        [
	// 		  'type' => 'card',
	// 		  // 'wrapperClass' => 'col-sm-6 col-md-4', // optional
	// 		  'class' => 'card bg-light text-dark', // optional
	// 		  'content' => [
	// 		      'header' => 'ปฏิทิน ยอดนิยม', // optional
	// 		      'body' => $sectionCalendarMostView->post_excerpt
	// 		  ]
	// 		],
	// 		[
	// 		  'type' => 'card',
	// 		  // 'wrapperClass' => 'col-sm-6 col-md-4', // optional
	// 		  'class' => 'card bg-danger  text-white', // optional
	// 		  'content' => [
	// 		      'header' => 'Event & Activity ยอดนิยม', // optional
	// 		      'body' => $sectionEventMostView->post_excerpt,
	// 		  ]
	// 		],
	// 		[
	// 		  'type' => 'card',
	// 		  // 'wrapperClass' => 'col-sm-6 col-md-4', // optional
	// 		  'class' => 'card bg-success text-white', // optional
	// 		  'content' => [
	// 		      'header' => 'Course ยอดนิยม', // optional
	// 		      'body' => $sectionCourseMostView->post_excerpt,
	// 		  ]
	// 		],
	//   ]
	// ];

    $widgets['before_content'][] = [
	  'type' => 'div',
	  'class' => 'row',
	  'content' => [ // widgets 
		  	[ 
		        'type' => 'chart',
		        'wrapperClass' => 'col-md-6',
		        // 'class' => 'col-md-6',
		        'controller' => \App\Http\Controllers\Admin\Charts\NewEntriesChartController::class,
				'content' => [
				    'header' => 'สถิติการเข้าชมเว็ปไซต์', // optional
				    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
					
		    	]
	    	],
	    	// [ 
		    //     'type' => 'chart',
		    //     'wrapperClass' => 'col-md-6',
		    //     // 'class' => 'col-md-6',
		    //     'controller' => \App\Http\Controllers\Admin\Charts\NewEntriesChartController::class,
			// 	'content' => [
			// 	    'header' => 'New Entries', // optional
			// 	    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
		    // 	]
	    	// ],
			[ 
		        'type' => 'chart',
		        'wrapperClass' => 'col-md-6',
		        // 'class' => 'col-md-6',
		        'controller' => \App\Http\Controllers\Admin\Charts\Lines\MenuChartControllerChartController::class,
				'content' => [
				    'header' => 'สถิติการเข้าชมเว็ปไซต์', // optional
				    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
		    	]
	    	],
    	]
	];

    // $widgets['after_content'][] = [
	//   'type' => 'div',
	//   'class' => 'row',
	//   'content' => [ // widgets 

	//     	[ 
	// 	        'type' => 'chart',
	// 	        'wrapperClass' => 'col-md-4',
	// 	        // 'class' => 'col-md-6',
	// 	        'controller' => \App\Http\Controllers\Admin\Charts\Pies\ChartjsPieController::class,
	// 			'content' => [
	// 			    'header' => 'Pie Chart - Chartjs', // optional
	// 			    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
	// 	    	]
	//     	],
	//     	[ 
	// 	        'type' => 'chart',
	// 	        'wrapperClass' => 'col-md-4',
	// 	        // 'class' => 'col-md-6',
	// 	        'controller' => \App\Http\Controllers\Admin\Charts\Pies\EchartsPieController::class,
	// 			'content' => [
	// 			    'header' => 'Pie Chart - Echarts', // optional
	// 			    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
	// 	    	]
	//     	],
	//     	[ 
	// 	        'type' => 'chart',
	// 	        'wrapperClass' => 'col-md-4',
	// 	        // 'class' => 'col-md-6',
	// 			'controller' => \App\Http\Controllers\Admin\Charts\Pies\HighchartsPieController::class,
	// 			'content' => [
	// 			    'header' => 'Pie Chart - Highcharts', // optional
	// 			    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
	// 	    	]
	//     	],

	//   ]
	// ];


	// // LINE CHART
    // $widgets['after_content'][] = [
	//   'type' => 'div',
	//   'class' => 'row',
	//   'content' => [ // widgets 

	//     	[ 
	// 	        'type' => 'chart',
	// 	        'wrapperClass' => 'col-md-6',
	// 	        // 'class' => 'col-md-6',
	// 	        'controller' => \App\Http\Controllers\Admin\Charts\Lines\ChartjsLineChartController::class,
	// 			'content' => [
	// 			    'header' => 'Line Chart - Chartjs', // optional
	// 			    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
	// 	    	]
	//     	],
	//     	[ 
	// 	        'type' => 'chart',
	// 	        'wrapperClass' => 'col-md-6',
	// 	        // 'class' => 'col-md-6',
	// 	        'controller' => \App\Http\Controllers\Admin\Charts\Lines\EchartsLineChartController::class,
	// 			'content' => [
	// 			    'header' => 'Line Chart - Echarts', // optional
	// 			    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
	// 	    	]
	//     	],
	//     	[ 
	// 	        'type' => 'chart',
	// 	        'wrapperClass' => 'col-md-6',
	// 	        // 'class' => 'col-md-6',
	// 	        'controller' => \App\Http\Controllers\Admin\Charts\Lines\HighchartsLineChartController::class,
	// 			'content' => [
	// 			    'header' => 'Line Chart - Highcharts', // optional
	// 			    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
	// 	    	]
	//     	],
	//     	[ 
	// 	        'type' => 'chart',
	// 	        'wrapperClass' => 'col-md-6',
	// 	        // 'class' => 'col-md-6',
	// 	        'controller' => \App\Http\Controllers\Admin\Charts\Lines\FrappeLineChartController::class,
	// 			'content' => [
	// 			    'header' => 'Line Chart - Frappe', // optional
	// 			    // 'body' => 'This chart should make it obvious how many new users have signed up in the past 7 days.<br><br>', // optional
	// 	    	]
	//     	],
	    	

    // 	]
	// ];

	$visitKnowledgeTenView = \App\Models\VisitorLog::select('visitable_id')
						->where('visitable_type', 'knowledgeDetail')
						->groupBy('visitable_id')
						->orderByRaw('count(visitable_id) DESC')
						->limit('10')
						->get()
						->toArray();
						
	$visitKnowledgeTenViewId = (!empty($visitKnowledgeTenView)) ? array_column($visitKnowledgeTenView, 'visitable_id') : [0];
	
	$dataKnowledgeTopTen = [];
	if(!empty($visitKnowledgeTenViewId)) {
		foreach($visitKnowledgeTenViewId as $key => $value) {
			$sectionKnowledgeTop10MostView = \App\Models\PostDashBoard::select('id','post_title', 'post_excerpt')->where('id', $value)->first();
			if(!empty($sectionKnowledgeTop10MostView)) {
				$count = App\Models\VisitorLog::select('visitable_id')
						->where('visitable_type', 'knowledgeDetail')
						->where('visitable_id', $value)
						->count();
				$sectionKnowledgeTop10MostView->countView = $count;
			}
			$dataKnowledgeTopTen[$key] = $sectionKnowledgeTop10MostView;
		}
	}

	// JOB TABLE RIGHT
	$visitjobTenView = \App\Models\VisitorLog::select('visitable_id')
						->where('visitable_type', 'jobDetail')
						->groupBy('visitable_id')
						->orderByRaw('count(visitable_id) DESC')
						->limit('10')
						->get()
						->toArray();
						
	$visitjobTenViewId = (!empty($visitjobTenView)) ? array_column($visitjobTenView, 'visitable_id') : [0];
	
	$datajobTopTen = [];
	if(!empty($visitjobTenViewId)) {
		foreach($visitjobTenViewId as $key => $value) {
			$sectionjobTop10MostView = \App\Models\PostDashBoard::select('id','post_title', 'post_excerpt')->where('id', $value)->first();
			if(!empty($sectionjobTop10MostView)) {
				$count = App\Models\VisitorLog::select('visitable_id')
						->where('visitable_type', 'jobDetail')
						->where('visitable_id', $value)
						->count();
				$sectionjobTop10MostView->countView = $count;
			}
			$datajobTopTen[$key] = $sectionjobTop10MostView;
		}
	}

	

	
@endphp

@section('content')
	{{-- In case widgets have been added to a 'content' group, show those widgets. --}}
	@include(backpack_view('inc.widgets'), [ 'widgets' => app('widgets')->where('group', 'content')->toArray() ])

	<section>
		<div class="row mb-5">
			<div class="col-6">
				<h4>ข่าวสารเนื้อหายอดนิยม</h4>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>ประเภท</th>
							<th>ชื่อบทความ</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>คลังความรู้</td>
							<td>{{ (!empty($sectionKnowledgeMostView)) ? $sectionKnowledgeMostView->post_title : '' }}</td>
							
						</tr>
						<tr>
							<td>ข่าวประชาสัมพันธ์</td>
							<td>{{(!empty($sectionNewMostView)) ? $sectionNewMostView->post_title : ''}}</td>
							
						</tr>
						<tr>
							<td>ประกาศ</td>
							<td>{{(!empty($sectionAnnounceMostView)) ? $sectionAnnounceMostView->post_title : ''}}</td>
						</tr>
						<tr>
							<td>ปฏิทิน</td>
							<td>{{(!empty($sectionCalendarMostView)) ? $sectionCalendarMostView->post_title : ''}}</td>
						</tr>
						<tr>
							<td>Event & Activity</td>
							<td>{{(!empty($sectionEventMostView)) ? $sectionEventMostView->post_title : ''}}</td>
						</tr>
						<tr>
							<td>Course</td>
							<td>{{(!empty($sectionCourseMostView)) ? $sectionCourseMostView->post_title : ''}}</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="col-6">
				<h4>10 อันดับคลังความรู้ยอดนิยม</h4>
				<ul class="nav nav-tabs" role="tablist">
					
					<li class="nav-item">
					  <a class="nav-link active" data-toggle="tab" href="#knowledge">Knowledge</a>
					</li>
					<li class="nav-item">
					  <a class="nav-link" data-toggle="tab" href="#job">Job</a>
					</li>
				  </ul>
				
				  <!-- Tab panes -->
				  <div class="tab-content">
					
					<div id="knowledge" class="container tab-pane active"><br>
					  <h3>Knowledge</h3>
						<table class="table">
							<thead>
								<th>ชื่อบทความ</th>
								<th>จำนวนผู้เข้าชม</th>
							</thead>
							<tbody>
								@if(!empty($dataKnowledgeTopTen))
									@foreach($dataKnowledgeTopTen as $value)
										<tr>
											<td>{{ $value->post_title ?? "" }}</td>
											<td>{{ $value->countView ?? 0 }}</td>
										</tr>
									@endforeach
								@endif
								
							</tbody>
						</table>
					</div>
					<div id="job" class="container tab-pane fade"><br>
						<h3>Job</h3>
						<table class="table">
							<thead>
								<th>ชื่อบทความ</th>
								<th>จำนวนผู้เข้าชม</th>
							</thead>
							<tbody>
								@if(!empty($datajobTopTen))
									@foreach($datajobTopTen as $value)
										<tr>
											<td>{{ $value->post_title ?? "" }}</td>
											<td>{{ $value->countView ?? 0 }}</td>
										</tr>
									@endforeach
								@endif
								
							</tbody>
						</table>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</section>
@endsection

