@if ($crud->hasAccess('list'))

<button type="button" id="btn-phone-info" class="btn btn-primary export-excel-btn">
    <i class="la la-info" aria-hidden="true"></i> INFO
</button>


@push('after_scripts')
<script>
    // backend_phonedirect_delete
    $("#btn-phone-info").click(function() {
        console.log('fn');
        swal({
		  title: "แจ้งเตือนการนำเข้าไฟล์",
          text: 'ขั้นตอนการนำเข้าไฟล์ 1.กดปุ่ม Example *คอลัมน์ Department(ฝ่าย) Section(แผนก)ให้นำรหัสจากเมนูจัดการแผนก และฝ่าย มากรอก',
		  html: true,
		  icon: "info",
		})
    })
</script>
@endpush

@endif
