<a href="{{ backpack_url('course-file') }}?doc_tb_id={{ $entry->getKey() }}" class="btn btn-sm btn-link">
    <i class="la la-file"></i> Course Files
</a>