@if ($crud->hasAccess('list'))

    <form action="{{ url($crud->route.'/export') }}" method="POST" enctype="multipart/form-data" class="form-export-excel" target="_blank" style="display: flex; justify-content: flex-end;">
        <div class="wrap-form">
            @csrf
            <div class="param-form">

            </div>
        </div>
        <div class="wrap-submit" style='margin-left: 5px;'>
            <button class="btn btn-primary export-excel-btn">
                <i class="la la-upload" aria-hidden="true"></i> Excel
            </button>
        </div>
    </form>

    @push('after_scripts')
        <script>
            function parse_query_string(query) {
                var vars = query.split("&");
                var query_string = {};
                for (var i = 0; i < vars.length; i++) {
                    var pair = vars[i].split("=");
                    var key = decodeURIComponent(pair[0]);
                    var value = decodeURIComponent(pair[1]);
                    // If first entry with this name
                    if (typeof query_string[key] === "undefined") {
                    query_string[key] = decodeURIComponent(value);
                    // If second entry with this name
                    } else if (typeof query_string[key] === "string") {
                    var arr = [query_string[key], decodeURIComponent(value)];
                    query_string[key] = arr;
                    // If third or later entry with this name
                    } else {
                    query_string[key].push(decodeURIComponent(value));
                    }
                }
                return query_string;
            }
            jQuery(document).ready(function() {
                $('.form-export-excel').submit(function(e) {
                    // e.preventDefault()
                    var url_string = window.location.href; //window.location.href
                    var url = new URL(url_string);
                    var c = url.searchParams.get("date_range");
                    c = encodeURI(c)
                    $('.form-export-excel .param-form').empty()
                    $('.form-export-excel .param-form').append(`<input type="hidden" name="date_range" value="${c}"/>`)
                    console.log(c);
                })
            })
        </script>
    @endpush
@endif
