@php
	$class_name = get_class($entry); // ex. App\Models\Blog
	$arrs = [
		'App\Models\Blog',
		'App\Models\QAndA',
		'App\Models\KnowledgeIdeaSharing',
		'App\Models\YourJobKnowledge',
		'App\Models\TPCSharingGallery',
		'App\Models\TPCSharingVideo',
		'App\Models\NewRelease',
		'App\Models\NewAnnounce',
		'App\Models\NewCalendar',
		'App\Models\NewEvent',
		'App\Models\Course',
	];
@endphp

@if (in_array($class_name, $arrs))

	@if($class_name == 'App\Models\Course') 
		<a href="{{ route('admin.course.example-by-id', ['id' => $entry->id]) }}" class="btn btn-sm btn-link"><i class="la la-eye"></i> {{ trans('backpack::crud.preview') }}</a>
	@else 
		<a href="{{ route('admin.post.example-by-id', ['id' => $entry->id]) }}" class="btn btn-sm btn-link"><i class="la la-eye"></i> {{ trans('backpack::crud.preview') }}</a>
	@endif
@else

	@if ($crud->hasAccess('show'))
		@if (!$crud->model->translationEnabled())

		<!-- Single edit button -->
		<a href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}" class="btn btn-sm btn-link"><i class="la la-eye"></i> {{ trans('backpack::crud.preview') }}</a>

		@else

		<!-- Edit button group -->
		<div class="btn-group">
		<a href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}" class="btn btn-sm btn-link pr-0"><i class="la la-eye"></i> {{ trans('backpack::crud.preview') }}</a>
		<a class="btn btn-sm btn-link dropdown-toggle text-primary pl-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="caret"></span>
		</a>
		<ul class="dropdown-menu dropdown-menu-right">
			<li class="dropdown-header">{{ trans('backpack::crud.preview') }}:</li>
			@foreach ($crud->model->getAvailableLocales() as $key => $locale)
				<a class="dropdown-item" href="{{ url($crud->route.'/'.$entry->getKey().'/show') }}?locale={{ $key }}">{{ $locale }}</a>
			@endforeach
		</ul>
		</div>

		@endif
	@endif

@endif