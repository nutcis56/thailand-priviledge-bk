@if ($crud->hasAccess('list'))
    <form action="{{ url($crud->route.'/export-csv') }}" method="POST" enctype="multipart/form-data" class="form-export-text" target="_blank" style="display: flex; justify-content: flex-end;">
        <div class="wrap-form">
            @csrf
            <div class="param-form">

            </div>
        </div>
        <div class="wrap-submit" style='margin-left: 5px;'>
            <button class="btn btn-primary export-excel-btn">
                <i class="la la-upload" aria-hidden="true"></i> CSV
            </button>
        </div>
    </form>

    @push('after_scripts')
        <script>
            
        </script>
    @endpush
@endif
