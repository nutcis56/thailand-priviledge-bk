@if ($crud->get('reorder.enabled') && $crud->hasAccess('reorder'))
  @php
      $next_params = "?params=true";
      if (isset($_REQUEST['doc_tb_id'])) {
        $next_params .= "&doc_tb_id={$_REQUEST['doc_tb_id']}";
      }
  @endphp
  <a href="{{ url($crud->route.'/reorder'.$next_params) }}" class="btn btn-outline-primary" data-style="zoom-in"><span class="ladda-label"><i class="la la-arrows"></i> {{ trans('backpack::crud.reorder') }} {{ $crud->entity_name_plural }}</span></a>
@endif