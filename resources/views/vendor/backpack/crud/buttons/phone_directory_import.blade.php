@if ($crud->hasAccess('list'))
    <form action="{{ url($crud->route.'/import') }}" method="POST" enctype="multipart/form-data" class="form-import" target="_blank" style="display: flex; justify-content: flex-end;">
        <div class="wrap-form">
            @csrf
            <div class="param-form">

            </div>
        </div>
        <div class="wrap-submit" style='margin-left: 5px;'>
            <button type="button" id="falseinput" class="btn btn-primary export-excel-btn">
                <i class="la la-upload" aria-hidden="true"></i> IMPORT
            </button>
            <input style="display:none;" id="fileinput" class="btn btn-primary export-excel-btn" type="file" name="file">
        </div>
    </form>

    @push('after_scripts')
        <script>
            $(document).ready( function() {
                $('#falseinput').click(function(){
                    $("#fileinput").click();
                });

                $('#fileinput').change(function() {
                    console.log('file change');
                    $('form.form-import').submit();
                });
            });
        </script>
    @endpush
@endif
