@if ($crud->hasAccess('list'))
    <form action="{{ url($crud->route.'/export-example') }}" method="POST" enctype="multipart/form-data" class="form-export-text" target="_blank" style="display: flex; justify-content: flex-end;">
        <div class="wrap-form">
            @csrf
            <div class="param-form">

            </div>
        </div>
        <div class="wrap-submit" style='margin-left: 5px;'>
            <button class="btn btn-primary export-excel-btn">
                <i class="la la-upload" aria-hidden="true"></i> Example
            </button>
        </div>
    </form>

    <form id="delete_data" method="POST" action="{{ route('backend_phonedirect_delete') }}">
        @csrf
    </form>
    <button type="button" id="btn-phone-danger" class="btn btn-danger export-excel-btn ml-2">
        <i class="las la-trash"></i> ลบข้อมูล
    </button>

    @push('after_scripts')
        <script>
            $("#btn-phone-danger").click(function() {
               
                swal({
                    title: "Are you sure?",
                    text: ("You want to delete data !"),
                    type: "warning", //type and imageUrl have been replaced with a single icon option.
                    icon:'warning', //The right way
                    showCancelButton: true, //showCancelButton and showConfirmButton are no longer needed. Instead, you can set buttons: true to show both buttons, or buttons: false to hide all buttons. By default, only the confirm button is shown.
                    confirmButtonColor: '#d33', //you should specify all stylistic changes through CSS. As a useful shorthand, you can set dangerMode: true to make the confirm button red. Otherwise, you can specify a class in the button object.
                    confirmButtonText: "Yes", // everything is in the buttons argument now
                    closeOnConfirm: false,
                    buttons:true,//The right way
                    buttons: ["No", "Yes"] //The right way to do it in Swal1
                })
                .then(function (isConfirm) {
                    if (isConfirm) {
                        $("#delete_data").submit();
                    }
                });
            })
        </script>
    @endpush
@endif
