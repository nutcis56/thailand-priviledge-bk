@if ($crud->hasAccess('list'))

    <form action="{{ url($crud->route.'/export') }}" method="POST" enctype="multipart/form-data" class="form-export-excel" target="_blank" style="display: flex; justify-content: flex-end;">
        <div class="wrap-form">
            @csrf
            <div class="param-form">

            </div>
        </div>
        <div class="wrap-submit" style='margin-left: 5px;'>
            <button type="submit" class="btn btn-primary export-excel-btn">
                <i class="la la-upload" aria-hidden="true"></i> Excel
            </button>
        </div>
    </form>

    @push('after_scripts')
       
    @endpush
@endif
