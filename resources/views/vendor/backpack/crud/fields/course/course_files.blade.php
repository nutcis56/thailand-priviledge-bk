@php
    $id = '';
    if (isset($entry)) {
        $id = $entry->id;
    }
@endphp
@include('crud::fields.inc.wrapper_start')

    <label>{!! $field['label'] !!}</label>

    <div class="form-group {{ isset($field['name']) ? $field['name'] : '' }}">

        <input type="hidden" name="course_id" value="{{ $id }}">

        @if ($id)
            <a href="{{ backpack_url('course-file') }}/create?doc_tb_id={{ $id }}" class="btn btn-sm btn-default" target="_blank">
                เพิ่มไฟล์
            </a>
            <a href="{{ backpack_url('course-file') }}?doc_tb_id={{ $id }}" class="btn btn-sm btn-default" target="_blank">
                จัดการไฟล์
            </a>
            <div class="show-{{ $field['name'] }}" style="margin-top: 1em;">

            </div>
        @else
            กรุณาบันทึกก่อนเพิ่มไฟล์
        @endif

    </div>

    {{-- HINT --}}
    @if (isset($field['hint']))
        <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
 
@include('crud::fields.inc.wrapper_end')

{{-- FIELD CSS - will be loaded in the after_styles section --}}
@push('crud_fields_styles')

@endpush

@push('crud_fields_scripts')
    <script type="text/javascript">
        var CourseFile_{{ $field['name'] }} = {
            init: function() {
                let _this = this

                _this.getLoopData()
               
            },
            getLoopData: function() {
                let _this = this
                let course_id = $('input[name="course_id"]').val()
                if (course_id) {
                    _this.getData(course_id)
                    setInterval(() => {
                        _this.getData(course_id)
                    }, 5000);
                }
            },
            getData: function(course_id) {
                $.ajax({
                    url: '{{ route('course-file.get-by-ajax') }}',
                    data: {
                        'id': course_id
                    },
                    type: 'GET',
                    success: function (result) {
                        console.log(result)
                        let html = `<ul>`
                        result.forEach(function(v, i) {
                            html += `<li style="display: flex;justify-content: space-between;flex-direction: row;max-width: 450px; border-bottom: 1px solid #eee; padding: 0.5em 0em;">
                                <div>${i+1}) ${v.name.th_TH} - ${v.excerpt.th_TH}</div>
                                <div><a href="{{ backpack_url('course-lesson') }}/${v.id}/edit" target="_blank">แก้ไข</a></div>
                            </li>`
                        })
                        html += `</ul>`
                        $('.show-{{ $field['name'] }}').empty().html(html)
                    },
                    error: function (result) {

                    }
                });
            }
        }

        jQuery(document).ready(function() {
            CourseFile_{{ $field['name'] }}.init()
        });
    </script>
@endpush