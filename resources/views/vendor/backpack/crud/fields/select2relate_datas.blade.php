@include('crud::fields.inc.wrapper_start')

    <label>{!! $field['label'] !!}</label>

    <div class="form-group {{ isset($field['name']) ? $field['name'] : '' }}">
        
        @php   
            // $value = "";
            // if (old($field['name'])) {
            //     $value = old($field['name']);
            // } elseif (isset($request) && $request[$field['name']]) {
                
            //     $value = $request[$field['name']];
                
            // }
            $value = [];
            if (isset($field['value'])) {
                $value = $field['value'];
            }
            
            $data = $field['data'];

            $columns = Schema::getColumnListing(app($data)->getTable());

            $data = app($data);

            if (array_key_exists('post_status', $columns)) {
                $data = $data->where('post_status', '1');
            }

            if (isset($entry)) {
                $data = $data->where('id', '!=', $entry->id);
            }

            $data = $data->orderBy('id', 'DESC');

            $data = $data->get();
            
            $selected_datas = [];
            if (isset($entry) && $entry->relatePosts->count()) {
                $selected_datas = $entry->relatePosts->pluck('id')->toArray();
            }
        @endphp

        <select @include('crud::fields.inc.attributes', ['default_class' =>  'form-control']) name="{{ $field['name'] }}[]" id="{{ $field['name'] }}"
            multiple="multiple"
            @if (isset($field['attrs']))
                @if (is_array($field['attrs']))
                    @foreach ($field['attrs'] as $attr_name => $attr_value)
                        {{ $attr_name."='{$attr_value}'" }}
                    @endforeach
                @endif
            @endif
            style="width:100%;"
        >
            @foreach($data as $valueData)
                @php
                    $selected = false;
                    if (in_array($valueData[$field['key_value']], $selected_datas)) {
                        $selected = 'selected';
                    }
                @endphp
                <option value="{{ $valueData[$field['key_value']] }}" {{ $selected ? 'selected' : '' }}>
                    ID {{ $valueData[$field['key_value']] }} - {{ $valueData[$field['key_data']] }}
                </option>
            @endforeach
        </select>

    </div>

    {{-- HINT --}}
    @if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
 
@include('crud::fields.inc.wrapper_end')

{{-- FIELD CSS - will be loaded in the after_styles section --}}
@push('crud_fields_styles')
    <!-- include select2 css-->
    <link href="{{ asset('packages/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
    <!-- include select2 js-->
    <script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
    @if (app()->getLocale() !== 'en')
    <script src="{{ asset('packages/select2/dist/js/i18n/' . str_replace('_', '-', app()->getLocale()) . '.js') }}"></script>
    @endif
@endpush

@push('crud_fields_scripts')
    <script type="text/javascript">
        var NewRelate_{{ $field['name'] }} = {
            init: function() {
                let _this = this

                _this.select2()
               
            },
            select2: function() {
                $('#{{ $field['name'] }}').select2({
                    tags: true,
                    tokenSeparators: [',', ' '],
                    createTag: function (params) {
                        var term = $.trim(params.term);

                        if (term === '') {
                            return null;
                        }

                        return {
                            id: term,
                            text: term,
                            newTag: true // add additional parameters
                        }
                    }
                })
            }
        }

        jQuery(document).ready(function() {
            NewRelate_{{ $field['name'] }}.init()
        });
    </script>
@endpush