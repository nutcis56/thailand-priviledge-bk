@include('crud::fields.inc.wrapper_start')

    <label>{!! $field['label'] !!}</label>

    <div class="form-group {{ isset($field['name']) ? $field['name'] : '' }}">
        
        @php   
            // $value = "";
            // if (old($field['name'])) {
            //     $value = old($field['name']);
            // } elseif (isset($request) && $request[$field['name']]) {
                
            //     $value = $request[$field['name']];
                
            // }
            $value = [];
            if (isset($field['value'])) {
                $value = json_decode($field['value']);
            }
            if (!$value) {
                $value = [];
            }
            
            $data = $field['data'];

            $columns = Schema::getColumnListing(app($data)->getTable());

            $data = app($data);

            if (array_key_exists('status', $columns)) {
                $data = $data->where('status', '1');
            }

            $locale = config('translatable.fallback_locale');
            if (isset($_GET['locale']) && $_GET['locale']) {
                $locale = $_GET['locale'];
            }

            $data = $data->where("name->{$locale}", '!=', '');
            $data = $data->get();
            
            $pivot_data = [];
            if (isset($request->id)) {
                $pivot_data = $field['pivot']['model'];
                $pivot_data = app($pivot_data)->where($field['pivot']['local_column'], $request->id)->get()->keyBy($field['pivot']['foreign_column'])->toArray();
            }
        @endphp

        <select @include('crud::fields.inc.attributes', ['default_class' =>  'form-control']) name="{{ $field['name'] }}[]" id="{{ $field['name'] }}"
            multiple="multiple"
            @if (isset($field['attrs']))
                @if (is_array($field['attrs']))
                    @foreach ($field['attrs'] as $attr_name => $attr_value)
                        {{ $attr_name."='{$attr_value}'" }}
                    @endforeach
                @endif
            @endif
            style="width:100%;"
        >
            @foreach($data as $valueData)
                @php
                    $valueData->setLocale($locale);
                    $selected = false;
                    if (in_array($valueData[$field['key_data']], $value)) {
                        $selected = 'selected';
                    }
                @endphp
                <option value="{{ $valueData[$field['key_data']] }}" {{ $selected ? 'selected' : '' }}>
                    {{ $valueData[$field['key_data']] }}
                </option>
            @endforeach
        </select>

    </div>

    {{-- HINT --}}
    @if (isset($field['hint']))
    <p class="help-block">{!! $field['hint'] !!}</p>
    @endif
 
@include('crud::fields.inc.wrapper_end')

{{-- FIELD CSS - will be loaded in the after_styles section --}}
@push('crud_fields_styles')
    <!-- include select2 css-->
    <link href="{{ asset('packages/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('packages/select2-bootstrap-theme/dist/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    @endpush

    {{-- FIELD JS - will be loaded in the after_scripts section --}}
    @push('crud_fields_scripts')
    <!-- include select2 js-->
    <script src="{{ asset('packages/select2/dist/js/select2.full.min.js') }}"></script>
    @if (app()->getLocale() !== 'en')
    <script src="{{ asset('packages/select2/dist/js/i18n/' . str_replace('_', '-', app()->getLocale()) . '.js') }}"></script>
    @endif
@endpush

@push('crud_fields_scripts')
    <script type="text/javascript">
        var NewTag_{{ $field['name'] }} = {
            init: function() {
                let _this = this

                _this.select2()
                _this.addNewTagToTable()
               
            },
            select2: function() {
                $('#{{ $field['name'] }}').select2({
                    tags: true,
                    tokenSeparators: [',', ' '],
                    createTag: function (params) {
                        var term = $.trim(params.term);

                        if (term === '') {
                            return null;
                        }

                        return {
                            id: term,
                            text: term,
                            newTag: true // add additional parameters
                        }
                    }
                })
            },
            addNewTagToTable: function() {
                $('form[method="post"]').on('submit', function(e) {
                    let tags = $('#{{ $field['name'] }}').val()
                    let urlSearchParams = new URLSearchParams(window.location.search);
                    let params = Object.fromEntries(urlSearchParams.entries());
                    let locale = 'th_TH';
                    if (typeof(params.locale) != 'undefined') {
                        locale = params.locale
                    }
                    $.ajax({
                        url: '{{ route('tag.new-tag-by-ajax') }}',
                        data: {
                            'tags': tags,
                            'locale': locale
                        },
                        type: 'POST',
                        success: function (result) {
                            console.log(result)
                        },
                        error: function (result) {

                        }
                    });
                })
            }
        }

        jQuery(document).ready(function() {
            NewTag_{{ $field['name'] }}.init()
        });
    </script>
@endpush