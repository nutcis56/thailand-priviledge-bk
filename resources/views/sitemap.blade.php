@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>
@section("content")
<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ __('tpc.sitemap_menu') }}</h2>
                                <p class="banner__text">
                                    
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item active" aria-current="page"> {{$name ?? ''}}</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="menus">
                            
                            {!! \App\Models\ConfigMenu::getHTMLMenu($all_menus) !!}

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <style>
        .menus ul li {
            list-style-type: none;
        }
        .menus ul {
            padding-left: 2em;
        }
        .menus ul li.level-0 > a{
            background: #fbfbfb;
            font-weight: 500;
        }
        .menus ul li a {
            display: block;
            border: 1px solid #eee;
            border-radius: 5px;
            padding: 0.5em 1em;
            margin-bottom: 0.5em;
            font-size: 1.25em;
        }
        .menus ul li a:hover {
            background: #eee;
        }
    </style>

</main>

@include('elements.modal_favorite')
{{-- @include('elements.modal_email', ['id' => $lists->id]) --}}

@endsection

@push("scripts")
    <script>
        
    </script>
@endpush
