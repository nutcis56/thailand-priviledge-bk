@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>

@push("css")
<style>
    .knowledge_hidden {
        display: none;
    }
</style>
@endpush

@section("content")

<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{ route('jobList') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น" value="{{ (isset($keyword)) ? ($keyword != '') ? $keyword : '' : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group mb-3 mb-lg-0">
                                            @php 
                                                $tags = CoreConfigService::getTag();
                                            @endphp
                                            <select class="searchTag form-control" name="searchTag">
                                                <option value="">แท๊ก</option>
                                                @if(count($tags) > 0)
                                                    @foreach($tags as $key => $value)
                                                        <option value="{{ $value->name }}" @if(isset($keywordTag)) {{ $value->name == $keywordTag ? 'selected' : '' }} @endif>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item">คลังความรู้</li>
                                    <li class="breadcrumb-item"><a href="{{ route('jobList') }}" class="breadcrumb-link">Your Job Knowledge</a></li>
                                </ol>
                            </nav>
                        </div>
                        <div class="tab--dropdown">
                            <div class="tabs--wrapper">
                                <ul class="nav nav-tabs d-none d-md-block" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="RealEstateTab" data-toggle="tab" href="#RealEstate" role="tab" aria-controls="RealEstate" aria-selected="true"><span class="icon-file"></span> บทความ</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="ConstructionTab" data-toggle="tab" href="#Construction" role="tab" aria-controls="Construction" aria-selected="false"><span class="icon-folder"></span> ไฟล์</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="operationTab">
                                    <div class="tab-pane fade show active" id="RealEstate" role="tabpanel" aria-labelledby="RealEstateTab">
                                        <div class="content__wrapper--list p-0">
                                            <div class="news--list">
                                                <div class="card-deck card-deck--cards-1 card-deck--cards-md-2 card-deck--cards-lg-3">

                                                    @if(count($lists) > 0)
                                                        @foreach($lists as $key => $val)
                                                            @php 
                                                                $date = date_format($val->created_at, "d.m.Y");
                                                            @endphp

                                                            @if($key <= 9)
                                                                <a href="/job-list/{{ $val->post_name != "" ? $val->post_name : $val->id }}" class="card card--knowledge">
                                                                    <figure class="card__figure">
                                                                        <img src="{{$val->post_cover ?? asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in">
                                                                    </figure>
            
                                                                    <div class="card__body">
                                                                        <div class="card__type">
                                                                            {{ $val->position->name ?? "" }}
                                                                        </div>
                                                                        <h3 class="card__title">
                                                                            {{ $val->post_title ?? "" }}
                                                                        </h3>
                                                                        <ul class="list-group">
                                                                            <li class="list-group-item"><i class="icon-date"></i> {{ $date ?? '' }}</li>
                                                                            <li class="list-group-item"><i class="icon-create"></i>{{ $val->userInfo->name ?? '' }} </li>
                                                                            <li class="list-group-item"><i class="icon-view"></i> {{ $val->visitlog_count ?? 0 }}</li>
                                                                            <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$val->like_count}}</li>
                                                                        </ul>
                                                                        <p class="card__text">
                                                                            {!! $val->post_excerpt ?? "" !!} 
                                                                        </p>
                                                                    </div>
                                                                </a>

                                                            @else 
                                                                <a href="/job-list/{{ $val->post_name != "" ? $val->post_name : $val->id }}" class="card card--knowledge knowledge_hidden">
                                                                    <figure class="card__figure">
                                                                        <img src="{{$val->post_cover ?? asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in">
                                                                    </figure>
            
                                                                    <div class="card__body">
                                                                        <div class="card__type">
                                                                            {{ $val->position->name ?? "" }}
                                                                        </div>
                                                                        <h3 class="card__title">
                                                                            {{ $val->post_title ?? "" }}
                                                                        </h3>
                                                                        <ul class="list-group">
                                                                            <li class="list-group-item"><i class="icon-date"></i> {{ $date ?? '' }}</li>
                                                                            <li class="list-group-item"><i class="icon-create"></i> {{ $val->userInfo->name ?? '' }} </li>
                                                                            <li class="list-group-item"><i class="icon-view"></i> {{ $val->visitlog_count ?? 0 }}</li>
                                                                            <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$val->like_count}}</li>
                                                                        </ul>
                                                                        <p class="card__text">
                                                                            {!! $val->post_excerpt ?? "" !!} 
                                                                        </p>
                                                                    </div>
                                                                </a>
                                                            @endif
                                                            
                                                        @endforeach
                                                    @endif
                                                    
                                                    
                                                </div>
                                            </div>
                                            <div class="text-center">
                                                <a href="#" class="btn btn--loadmore" id="btn_loadmore">
                                                    <span class="text__link">โหลดเพิ่มเติม</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    {{-- TAB FILE --}}
                                    <div class="tab-pane fade" id="Construction" role="tabpanel" aria-labelledby="ConstructionTab">
                                        <div class="content__wrapper p-0">
                                            <div class="table-responsive">
                                                <table class="table table--primary">
                                                    <thead>
                                                        <tr>
                                                        <th scope="col"><i class="icon-file"></i> Name</th>
                                                        <th scope="col">Modified</th>
                                                        <th scope="col">Modified by</th>
                                                        <th scope="col">File size</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if(count($listFiles) > 0)
                                                            @foreach($listFiles as $key => $file)
                                                                @php 
                                                                    $dateFile = date_format($file->created_at, "d.m.Y");
                                                                @endphp
                                                                <tr>
                                                                    <th scope="row">
                                                                        <a href="{{ asset($file->post_cover) }}">
                                                                            <i class="icon-folder"></i> {{ $file->post_title }}
                                                                        </a>
                                                                    </th>
                                                                    <td>{{ $dateFile }}</td>
                                                                    <td>{{ $file->userInfo->name ?? '' }}</td>
                                                                    <td>2 mb</td>
                                                                </tr>
                                                            @endforeach
                                                        @endif
                                                        
                                                    </tbody>
                                                </table>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

@endsection
@push("scripts")
<script>
    $(document).ready(function(){
        $("#btn_loadmore").click(function(){
            $("a").removeClass("knowledge_hidden");
            $(this).fadeOut();
        })
        $('.searchTag').select2();
    })
</script>
@endpush