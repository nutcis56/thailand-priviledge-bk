@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>
@section("content")

<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{ route('blogList') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น" value="{{ ($keyword != '') ? $keyword : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-4">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="">
                                                <option value="Tag" selected="">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item">คลังความรู้</li>
                                    <li class="breadcrumb-item active" aria-current="page">Blog</li>
                                </ol>
                            </nav>
                        </div>
                        @if(count($lists) > 0)

                        @foreach($lists as $key => $val)

                            @php
                               
                               $strtotime = strtotime($val->post_date);
                                $date = date('m.d.Y', $strtotime);
                               
                                $tags = json_decode($val->post_tags);
                            @endphp
                            <div class="news">
                                <h1 class="news__title">
                                    {{ $val->post_title ?? "" }}
                                </h1>
                                <div class="news__tools">
                                    <div class="dvleft">
                                        <div class="news__type">
                                            {{ $val->position->name ?? "" }}
                                        </div>
                                        <div class="news__date">
                                            <i class="icon-date"></i>  {{$date ?? ''}}
                                        </div>
                                        <div class="news__by">
                                            <i class="icon-create"></i>{{ $val->userInfo->name ?? ''}}
                                        </div>
                                    </div>
                                </div>
                                <div class="news__body">
                                    <p style="text-align: center;"><img src="{{$val->post_cover ?? asset('images/no_image.png')}}" alt="" class="img-fluid"></p>
                                    {{$val->post_excerpt ?? ''}}
                                    {!! $val->post_content ?? ""  !!}
                                </div>
                                <div class="tag--container">
                                    <div class="tag__title">
                                        Tags
                                    </div>
                                    @if(is_array($tags))
                                        @foreach($tags as $tag)
                                            <div class="tag__text">
                                                <a href="{{ route('tagIndex', ['tag' => $tag]) }}">#{{$tag}}</a>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="share--container">
                                    {{-- <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--facebook">
                                        <div class="share--container-icon"><i class="fab fa-facebook-f"></i></div> 
                                        <div class="share--container-text">Facebook</div>
                                    </a> 
                                    <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--line">
                                        <div class="share--container-icon"><i class="fab fa-line"></i></div>
                                        <div class="share--container-text">Line</div>
                                    </a> --}}
                                    <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--email">
                                        <div class="share--container-icon"><i class="far fa-envelope"></i></div>
                                        <div class="share--container-text">Email</div>
                                    </a>
                                    <a href="#" data-id="{{ $val->id }}" class="share--container-link share--container-link--like btn-like">
                                        <div class="share--container-icon"><i class="far fa-thumbs-up"></i></div>
                                        <div class="share--container-text">Like {{$val->like_count ?? 0}}</div>
                                    </a>
                                    <a href="#"  data-id="{{ $val->id }}" class="share--container-link share--container-link--fav btn-favorite">
                                        <div class="share--container-icon"><i class="fas fa-heart"></i></div>
                                        <div class="share--container-text">Favorite</div>
                                    </a>
                                </div>
                            </div>
                            <hr style="border-width: 20px;">
                        @endforeach
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- <div class="content--section content--article bg-light-gray">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper--list">
                        <div class="content--title">
                            <h3 class="title__main">
                                บทความที่เกี่ยวข้อง
                            </h3>
                        </div>
                       
                        @if(count($lists) && count($lists[0]['relate']) > 0)
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <a href="{{ route('relateData', ['id' => $lists[0]->id]) }}" class="btn btn--primary mb-3">ดูเพิ่มเติม</a>
                                </div>
                            </div>
                            <div class="news--list">
                                <div class="card-deck card-deck--cards-1 card-deck--cards-md-2 card-deck--cards-lg-3">
                                    @foreach($lists[0]['relate'] as $relate)
                                        @php
                                            $dateRelate = date_format($relate->productInfo->created_at, 'd.m.Y');
                                        @endphp
                                        <a href="#" class="card card--knowledge">
                                            <figure class="card__figure">
                                                <img src="{{ ($relate->productInfo->post_cover) ? asset($relate->productInfo->post_cover) : asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                            </figure>
        
                                            <div class="card__body">
                                                <div class="card__type">
                                                    ประชาสัมพันธ์
                                                </div>
                                                <h3 class="card__title">
                                                    {{ $relate->productInfo->post_title ?? '' }} 
                                                </h3>
                                                <ul class="list-group">
                                                    <li class="list-group-item"><i class="icon-date"></i> {{ $dateRelate }}</li>
                                                    <li class="list-group-item"><i class="icon-create"></i> By Sirawat </li>
                                                    <li class="list-group-item"><i class="icon-view"></i> 0</li>
                                                </ul>
                                                <p class="card__text">
                                                    {{ $relate->productInfo->post_excerpt ?? '' }}  
                                                </p>
                                            </div>
                                        </a>
                                    @endforeach
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
</main>

@include('elements.modal_favorite')
@endsection


@push("scripts")
    <script>
        const domElement =  {
            init: function() {
                this.handleFavorite();
                this.handleLike();
            },
            handleFavorite: function() {
                $(".btn-favorite").click(function() {
                    let id = $(this).attr('data-id');
                    var token = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': token},
                        type: "POST",
                        url: "/favorite/store",
                        data: {data: id},
                        success: function (response) {
                            console.log(response);
                            if (response.status == 'successfully') {
                                $("#exampleModal").modal('show');
                            }else if(response.status == 'unfavorite') {
                                $("#unfavoriteModal").modal('show');
                            }
                            return false;
                        },
                        error: function (response) {
                            //
                        }
                    });
                });
            },
            handleLike: function() {
                $(".btn-like").click(function() {
                    let id = $(this).attr('data-id');
                    var token = $('meta[name="csrf-token"]').attr('content');
                    $.ajax({
                        headers: {'X-CSRF-TOKEN': token},
                        type: "POST",
                        url: "/favorite/store",
                        data: {data: id, type: 'like'},
                        success: function (response) {
                            if (response.status == 'successfully') {
                                $("#favoriteModal").modal('show');
                            }else if(response.status == 'unlike') {
                                $("#unlikeModal").modal('show');
                            }

                            return false;
                        },
                        error: function (response) {
                            //
                        }
                    });
                });
            }
        }

        $(document).ready(function() {
            domElement.init();
        })
    </script>
@endpush