@extends('layouts.master_login')

@section('content')
<main class="main content main--content">
    <div class="fullpage--wrapper">
        <div class="login--wrapper">
            <div class="login__container">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-lg-10">
                            <div class="row no-gutters">
                                <div class="col-sm-6">
                                    <div class="figure w-100">
                                        <img src="{{ asset('images/img-login.jpg') }}" alt="" class="w-100">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="login__box">
                                        <div class="login__title">
                                            <a href="/" title="ไทยแลนด์ พริวิเลจ คาร์ด">
                                                <img class="login__logo" src="{{ asset('images/logo/logo-thailand-elite.png') }}" alt="ไทยแลนด์ พริวิเลจ คาร์ด">
                                            </a>
                                            <h1>
                                            ระบบศูนย์กลางแบ่งปันความรู้ภายใน
                                            </h1>
                                            <h2>(Knowledge Sharing Center System)</h2>
                                        </div>
                                        <form class="login__form" method="POST" action="{{ route('frontend.auth.loginSubmit') }}">
                                            {!! csrf_field() !!}

                                            <div class="login__field">
                                                <label for="" class="login__icon">
                                                    <i class="icon-user-login"></i>
                                                </label>
                                                <input type="text" class="login__input {{ $errors->has($username) ? ' is-invalid' : '' }}" placeholder="Email" name="{{ $username }}" value="{{ (Cookie::has('email')) ? Cookie::get('email') : old($username) }}" id="{{ $username }}">

                                                @if ($errors->has($username))
                                                    <span class="invalid-feedback">
                                                        <strong>{{ $errors->first($username) }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                            <div class="login__field">
                                                <i class="login__icon icon-pass-login"></i>
                                                <input type="password" name="password" id="password" value="{{ (Cookie::has('password')) ? Cookie::get('password') : '' }}"  class="login__input {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password">
                                            </div>
                                            <div class="login__text">
                                                <div class="text__item">
                                                    <input type="checkbox" {{ Cookie::has('email') ? 'checked' : '' }} name="rememberMe" id="rememberMe"> <label for="rememberMe">จดจำฉันไว้ในระบบ</label>
                                                </div>
                                                {{-- <div class="text__item">
                                                    <a href="#"><i class="icon-q-login"></i> ลืมรหัสผ่าน</a>
                                                </div> --}}
                                            </div>
                                            <button type="submit" class="btn w-100 login__submit">
                                                <span class="button__text">เข้าสู่ระบบ</span>
                                            </button>				
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection