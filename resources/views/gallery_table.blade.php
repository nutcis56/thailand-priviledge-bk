@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>

@push("css")
<style>
    .knowledge_hidden {
        display: none;
    }
</style>
@endpush

@section("content")
<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{ route('galleryAlbumIndex') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-4">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="">                                                
                                                <option value="Tag" selected="">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer mb-md-0">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item">คลังความรู้</li>
                                    <li class="breadcrumb-item"><a href="{{ route('galleryAlbumIndex') }}" class="breadcrumb-link">คลังภาพ</a></li>
                                    <li class="breadcrumb-item active">TCP Gallery Table</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="gallery--wrapper">
                            <div class="tabs--views">
                                <ul class="nav nav-tabs d-flex justify-content-end" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link" href="/gallery-list/{{ $lists->post_name != "" ? $lists->post_name : $lists->id }}"><span class="icon-list-view"></span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="icon-table-view"></span></a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="operationTab">
                                    <div class="tab-pane fade show active">
                                        <div class="table-responsive">
                                            <table class="table table--secondary">
                                                <thead>
                                                    <tr>
                                                    <th scope="col"><i class="icon-file"></i> Image</th>
                                                    <th scope="col"> Name</th>
                                                    <th scope="col">Modified</th>
                                                    <th scope="col">Modified by</th>
                                                    <th scope="col">File size</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(!empty($lists))
                                                        @php 
                                                            $imageAll = json_decode(json_encode($lists->files), true);
                                                            $date = date_format($lists->created_at, 'd.m.Y');
                                                        @endphp
                                                        @foreach($imageAll as $key => $image)
                                                       
                                                            @if(!empty($image)) 

                                                                @php 
                                                                    $filesize = 'ค้นหาไฟล์ไม่พบ';
                                                                    if(file_exists(public_path($image['image']))) {
                                                                        $filesize = filesize(public_path($image['image'])); 
                                                                        $filesize = round($filesize / 1024 / 1024, 1);
                                                                    }
                                                                @endphp
                                                                @if($key <= 2)
                                                                    <tr>
                                                                        <th scope="row">
                                                                            <a href="#" class="video--cover spotlight" data-media="image" data-src="{{$image['image'] != "" ? asset($image['image']) : asset('images/no_image.png')}}">
                                                                                <img src="{{$image['image'] != "" ? asset($image['image']) : asset('images/no_image.png')}}" alt="" class="img-fluid">
                                                                            </a>
                                                                        </th>
                                                                        <td>
                                                                            <a href="#" class="video--cover">
                                                                                <i class="icon-folder"></i> {{$image['title']}}
                                                                            </a>
                                                                        </td>
                                                                        <td>{{$date ?? ""}}</td>
                                                                        <td>{{ $lists->userInfo->name ?? '' }}</td>
                                                                        <td>{{ $filesize ?? 0 }} mb</td>
                                                                    </tr>
                                                                @else 
                                                                    <tr class="knowledge_hidden">
                                                                        <th scope="row">
                                                                            <a href="#" class="video--cover spotlight" data-media="image" data-src="{{$image['image'] != "" ? asset($image['image']) : asset('images/no_image.png')}}">
                                                                                <img src="{{$image['image'] != "" ? asset($image['image']) : asset('images/no_image.png')}}" alt="" class="img-fluid">
                                                                            </a>
                                                                        </th>
                                                                        <td>
                                                                            <a href="#" class="video--cover">
                                                                                <i class="icon-folder"></i> {{$image['title']}}
                                                                            </a>
                                                                        </td>
                                                                        <td>{{$date ?? ""}}</td>
                                                                        <td>{{ $lists->userInfo->name ?? '' }}</td>
                                                                        <td>{{ $filesize ?? 0 }} mb</td>
                                                                    </tr>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="text-center">
                                            <a href="#" class="btn btn--loadmore" id="btn_loadmore">
                                                <span class="text__link">โหลดเพิ่มเติม</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
@push("scripts")
<script src="{{ asset('vendor/spotlight/spotlight.bundle.js') }}"></script>
<script>
    $(document).ready(function(){
        $("#btn_loadmore").click(function(){
            $("tr").removeClass("knowledge_hidden");
            $(this).fadeOut();
        })
    })
</script>
@endpush