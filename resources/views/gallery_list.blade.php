@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>

@push("css")
<style>
    .knowledge_hidden {
        display: none;
    }
</style>
@endpush

@section("content")
<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{ route('galleryAlbumIndex') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-6">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-4">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    {{-- <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="">                                                
                                                <option value="Tag" selected="">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer mb-md-0">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item">คลังความรู้</li>
                                    <li class="breadcrumb-item"><a href="{{ route('galleryAlbumIndex') }}" class="breadcrumb-link">คลังภาพ</a></li>
                                    <li class="breadcrumb-item active">TCP Gallery List</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="gallery--wrapper">
                            <div class="tabs--views">
                                <ul class="nav nav-tabs d-flex justify-content-end" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#"><span class="icon-list-view"></span></a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="/gallery-table/{{ $lists->post_name != "" ? $lists->post_name : $lists->id }}"><span class="icon-table-view"></span></a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="operationTab">
                                    <div class="tab-pane fade show active" id="RealEstate" role="tabpanel" aria-labelledby="RealEstateTab">
                                        <div class="news--list">
                                            <div class="card-deck card-deck--cards-1 card-deck--cards-md-2 card-deck--cards-lg-3" data-fit="cover" data-autohide="all">

                                                @if(!empty($lists))
                                                    @php 
                                                        $imageAll = json_decode(json_encode($lists->files), true);
                                                        $date = date_format($lists->created_at, 'd.m.Y');
                                                    @endphp
                                                    @if( (!empty($imageAll)) && count($imageAll) > 0)
                                                   
                                                        @foreach($imageAll as $key => $image)
                                                            @if(!empty($image)) 
                                                                @if($key <= 2)
                                                                    <a href="{{$image['image'] != "" ? asset($image['image']) : '#'}}" class="card card--knowledge spotlight">
                                                                        <figure class="card__figure">
                                                                            <img src="{{$image['image'] != "" ? asset($image['image']) : asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in">
                                                                        </figure>

                                                                        {{-- <div class="card__body">
                                                                            <div class="card__type">
                                                                                {{ $lists->position->name ?? "" }}
                                                                            </div>
                                                                            <h3 class="card__title text-uppercase">
                                                                                {{ $image['title'] ?? '' }}
                                                                            </h3>
                                                                            <ul class="list-group">
                                                                                <li class="list-group-item"><i class="icon-date"></i> {{$date}}</li>
                                                                                <li class="list-group-item"><i class="icon-create"></i> {{ $lists->userInfo->name ?? '' }} </li>
                                                                            </ul>
                                                                        </div> --}}
                                                                    </a>
                                                                @else 
                                                                    <a href="{{$image['image'] != "" ? asset($image['image']) : '#'}}" class="card card--knowledge spotlight knowledge_hidden">
                                                                        <figure class="card__figure">
                                                                            <img src="{{$image['image'] != "" ? asset($image['image']) : asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in">
                                                                        </figure>

                                                                        {{-- <div class="card__body">
                                                                            <div class="card__type">
                                                                                {{ $lists->position->name ?? "" }}
                                                                            </div>
                                                                            <h3 class="card__title text-uppercase">
                                                                                {{ $image['title'] ?? '' }}
                                                                            </h3>
                                                                            <ul class="list-group">
                                                                                <li class="list-group-item"><i class="icon-date"></i> {{$date}}</li>
                                                                                <li class="list-group-item"><i class="icon-create"></i> {{ $lists->userInfo->name ?? '' }} </li>
                                                                            </ul>
                                                                        </div> --}}
                                                                    </a>
                                                                @endif

                                                            @endif
                                                            
                                                            
                                                        @endforeach
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <a href="#" class="btn btn--loadmore" id="btn_loadmore">
                                                <span class="text__link">โหลดเพิ่มเติม</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>

                                {{-- FAVORITE SOCIAL --}}
                                <div class="share--container">
                                    {{-- <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--facebook">
                                        <div class="share--container-icon"><i class="fab fa-facebook-f"></i></div> 
                                        <div class="share--container-text">Facebook</div>
                                    </a> 
                                    <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--line">
                                        <div class="share--container-icon"><i class="fab fa-line"></i></div>
                                        <div class="share--container-text">Line</div>
                                    </a> --}}
                                    <a href="#" id="btn-email" data-id="{{ $lists->id }}" class="share--container-link share--container-link--email">
                                        <div class="share--container-icon"><i class="far fa-envelope"></i></div>
                                        <div class="share--container-text">Email</div>
                                    </a>
                                    <a href="#" id="btn-like" data-id="{{ $lists->id }}" class="share--container-link share--container-link--like {{ $status['like_status'] }}">
                                        <div class="share--container-icon"><i class="far fa-thumbs-up"></i></div>
                                        <div class="share--container-text">Like {{$lists->like_count ?? 0}}</div>
                                    </a>
                                    <a href="#" id="btn-favorite" data-id="{{ $lists->id }}" class="share--container-link share--container-link--fav {{ $status['favorite_status'] }}">
                                        <div class="share--container-icon"><i class="fas fa-heart"></i></div>
                                        <div class="share--container-text">Favorite</div>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('elements.modal_favorite')
    @include('elements.modal_email', ['id' => $lists->id])
</main>


@endsection
@push("scripts")
<script src="{{ asset('vendor/spotlight/spotlight.bundle.js') }}"></script>
<script>
    $(document).ready(function(){
        $("#btn_loadmore").click(function(){
            $("a").removeClass("knowledge_hidden");
            $(this).fadeOut();
        });

        $("#btn-email").click(function(){
            $("#modalEmailConfirm").modal('show');
        })
    })
</script>
@endpush

