@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;

if(count($lists) > 0) {
    $firstList = $lists[0];
    unset($lists[0]);
}
?>
@section("content")

@push("css")
<style>
    .knowledge_hidden {
        display: none;
    }
</style>
@endpush

<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{ route('tagIndex') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น" value="{{ ($keyword != '') ? $keyword : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="">
                                                <option value="Tag" selected="">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                                <option value="Tag">Tag</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item active" aria-current="page">TAG</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="news--highlight">
                            <div class="card card--news-highlight">

                                @if(!empty($firstList))
                                    @php 
                                        $dateFirstList = date_format($firstList->created_at, "d.m.Y");
                                    @endphp
                                   
                                    <a href="{{ route('tagDetail', ['id' => $firstList->id]) }}" class="card__figure card__item">
                                        <img src="{{ $firstList->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                    </a>

                                    <div class="card__body card__item">
                                        <div class="card__type">
                                            {{ $firstList->position->name ?? "" }}
                                        </div>
                                        <a href="{{ route('tagDetail', ['id' => $firstList->id]) }}" class="card__title">
                                            {{ $firstList->post_title ?? "ไม่มีข้อมูล" }}
                                        </a>
                                        <ul class="list-group">
                                            <li class="list-group-item"><i class="icon-date"></i> {{$dateFirstList ?? ""}}</li>
                                            <li class="list-group-item"><i class="icon-create"></i> {{ $firstList->userInfo->name ?? '' }} </li>
                                            <li class="list-group-item"><i class="icon-view"></i> {{ count($firstList->visitlog) ?? 0 }}</li>
                                        </ul>
                                        <p class="card__text">
                                            {{ $firstList->post_excerpt ?? "" }}
                                        </p>
                                    </div>
                                @endif
                               
                            </div>
                        </div>
                        <div class="news--list">
                            <div class="card-deck card-deck--cards-1 card-deck--cards-md-2 card-deck--cards-lg-3">

                                @if(count($lists) > 0)
                                    @foreach($lists as $key => $val)
                                    @php 
                                        $date = date_format($val->created_at, "d.m.Y");
                                    @endphp

                                    @if($key <= 9)
                                        <a href="{{ route('tagDetail', ['id' => $val->id]) }}" class="card card--knowledge">
                                            <figure class="card__figure">
                                                <img src="{{ $val->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                            </figure>
        
                                            <div class="card__body">
                                                <div class="card__type">
                                                    {{ $val->position->name ?? "" }}
                                                </div>
                                                <h3 class="card__title">
                                                    {{$val->post_title ?? ""}}
                                                </h3>
                                                <ul class="list-group">
                                                    <li class="list-group-item"><i class="icon-date"></i> {{$date ?? ""}}</li>
                                                    <li class="list-group-item"><i class="icon-create"></i> {{ $val->userInfo->name ?? '' }} </li>
                                                    <li class="list-group-item"><i class="icon-view"></i> {{ $val->visitlog_count ?? 0 }}</li>
                                                </ul>
                                                <p class="card__text">
                                                    {{ $val->post_excerpt ?? '' }}
                                                </p>
                                            </div>
                                        </a>

                                    @else 
                                        <a href="{{ route('tagDetail', ['id' => $val->id]) }}" class="card card--knowledge knowledge_hidden">
                                            <figure class="card__figure">
                                                <img src="{{ $val->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                            </figure>
        
                                            <div class="card__body">
                                                <div class="card__type">
                                                    {{ $val->position->name ?? "" }}
                                                </div>
                                                <h3 class="card__title">
                                                    {{$val->post_title ?? ""}}
                                                </h3>
                                                <ul class="list-group">
                                                    <li class="list-group-item"><i class="icon-date"></i> {{$date ?? ""}}</li>
                                                    <li class="list-group-item"><i class="icon-create"></i> {{ $val->userInfo->name ?? '' }}</li>
                                                    <li class="list-group-item"><i class="icon-view"></i> {{ $val->visitlog_count ?? 0 }}</li>
                                                </ul>
                                                <p class="card__text">
                                                    {{ $val->post_excerpt ?? '' }}
                                                </p>
                                            </div>
                                        </a>


                                    @endif

                                    @endforeach


                                @endif
                                
                                
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="#" class="btn btn--loadmore" id="btn_loadmore">
                                <span class="text__link">โหลดเพิ่มเติม</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@push("scripts")
<script>
    $(document).ready(function(){
        $("#btn_loadmore").click(function(){
            $("a").removeClass("knowledge_hidden");
            $(this).fadeOut();
        })
    })
</script>
@endpush