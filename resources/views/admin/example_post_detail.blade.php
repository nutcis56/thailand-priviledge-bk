@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>
@section("content")
<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="news">

                            @if(!empty($list))

                            @php
                                $date = date_format($list->created_at, 'd.m.Y');
                                $tags = json_decode($list->post_tags);
                            @endphp
                                <h1 class="news__title">
                                    {{$list->post_title ?? ""}}
                                </h1>
                                <div class="news__tools">
                                    <div class="dvleft">
                                        <div class="news__type">
                                            {{ $list->position->name ?? '' }}
                                        </div>
                                        <div class="news__date">
                                            <i class="icon-date"></i>  {{ $date ?? '' }}
                                        </div>
                                        <div class="news__by">
                                            <i class="icon-create"></i>{{ $list->userInfo->name ?? '' }}
                                        </div>
                                    </div>
                                </div>
                                <div class="news__body">
                                    <p style="text-align: center;"><img src="{{ $list->post_cover }}" alt="" class="img-fluid"></p>
                                    <p>
                                        {{ $list->post_excerpt }}
                                    </p>
                                   
                                    {!! $list->post_content ?? "" !!}
                                </div>
                                <div class="tag--container">
                                    <div class="tag__title">
                                        Tags
                                    </div>
                                    @if(is_array($tags))
                                        @foreach($tags as $tag)
                                            <div class="tag__text">
                                                 {{-- <a href="{{ route('tagIndex', ['tag' => $tag]) }}">#{{$tag}}</a> --}}
                                                 <a href="#">#{{$tag}}</a>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="share--container">
                                    <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--facebook">
                                        <div class="share--container-icon"><i class="fab fa-facebook-f"></i></div> 
                                        <div class="share--container-text">Facebook</div>
                                    </a> 
                                    <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--line">
                                        <div class="share--container-icon"><i class="fab fa-line"></i></div>
                                        <div class="share--container-text">Line</div>
                                    </a>
                                    <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--email">
                                        <div class="share--container-icon"><i class="far fa-envelope"></i></div>
                                        <div class="share--container-text">Email</div>
                                    </a>
                                    <a href="#" onclick="window.open('', 'newwindow', 'width=450, height=300'); return false;" class="share--container-link share--container-link--like">
                                        <div class="share--container-icon"><i class="far fa-thumbs-up"></i></div>
                                        <div class="share--container-text">Like</div>
                                    </a>
                                    <a href="#" id="btn-favorite" data-id="{{ $list->id }}" class="share--container-link share--container-link--fav">
                                        <div class="share--container-icon"><i class="fas fa-heart"></i></div>
                                        <div class="share--container-text">Favorite</div>
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('elements.modal_favorite')
</main>


<style>
    .header,
    .share--container {
        display: none !important;
    }
</style>


@endsection

@push("scripts")
    <script>
        
    </script>
@endpush