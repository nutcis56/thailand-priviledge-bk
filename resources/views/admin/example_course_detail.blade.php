@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>
@section("content")


<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        {{-- <div class="search--wrapper">
                            <form action="{{ route('courseIndex') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น" value="{{ (isset($keyword)) ? ($keyword != '') ? $keyword : '' : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            @php 
                                                $tags = CoreConfigService::getTag();
                                            @endphp
                                            <select class="searchTag form-control" name="searchTag">
                                                <option value="">เลือก</option>
                                                @if(count($tags) > 0)
                                                    @foreach($tags as $key => $value)
                                                        <option value="{{ $value->name }}" @if(isset($keywordTag)) {{ $value->name == $keywordTag ? 'selected' : '' }} @endif>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div> --}}
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item">Open Online Course</li>
                                    <li class="breadcrumb-item active"><a href="{{ route('courseIndex') }}" class="breadcrumb-link">Course</a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article bg-white">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">

                    @if(!empty($lists))
                        @php 
                            $date = date_format($lists->created_at, 'd.m.Y');
                            $tags = json_decode($lists->post_tags);

                            $attribute = json_decode($lists->post_attribute);
                           
                        @endphp
                        <div class="course--wrapper">
                            <div class="course__highlight">
                                <div class="card--course-horizontal">
                                    <div class="card__figure">
                                        <img src="{{$lists->post_cover ?? asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in">
                                    </div>
                                    <div class="card__body">
                                        <div class="card__title">
                                        {{ $lists->post_title ?? '' }}
                                        </div>
                                        <ul class="list-group">
                                            <li class="list-group-item">
                                                <div class="card__type">
                                                    {{ $lists->position->name ?? "" }}
                                                </div>
                                            </li>
                                            <li class="list-group-item"><i class="icon-date"></i>{{$date ?? ''}}</li>
                                            <li class="list-group-item"><i class="icon-create"></i> {{ $lists->userInfo->name ?? '' }} </li>
                                        </ul>
                                        <div class="list--detail">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="list--detail__label">
                                                        <i class="icon-lecturer"></i> วิทยากร : 
                                                    </div>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="list--detail__text">{{ ($attribute != null && $attribute != "") ? $attribute->name : '' }}</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="list--detail__label">
                                                        <i class="icon-time"></i> ระยะเวลารวม : 
                                                    </div>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="list--detail__text">{{ ($attribute != null && $attribute != "") ? $attribute->time : '' }}</div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="list--detail__label">
                                                        <i class="icon-media"></i> สื่อการสอน : 
                                                    </div>
                                                </div>
                                                <div class="col-sm-8">
                                                    <div class="list--detail__text">{{ ($attribute != null && $attribute != "") ? $attribute->file : '' }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="course__detail">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="detail-tab" data-toggle="tab" href="#detail" role="tab" aria-controls="detail" aria-selected="true">รายละเอียดหลักสูตร</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="structure-tab" data-toggle="tab" href="#structure" role="tab" aria-controls="structure" aria-selected="false">โครงสร้างหลักสูตร</a>
                                    </li>
                                </ul>
                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" id="detail" role="tabpanel" aria-labelledby="detail-tab">
                                        <div class="course__detail-body">
                                            <div class="course__detail-text">
                                                <h3>คำอธิบายหลักสูตร</h3>
                                                {!! $lists->post_content !!}
                                            </div>
                                            <div class="course__detail-level">
                                                <h3>ระดับเนื้อหา</h3>
                                                <div class="select--level">
                                                    <p>
                                                        <input type="radio" disabled id="test1" name="radio-group" {{ ($lists->post_priority == 0) ? 'checked' : '' }}>
                                                        <label for="test1">ระดับต้น (Beginner)</label>
                                                    </p>
                                                    <p>
                                                        <input type="radio" disabled id="test2" name="radio-group" {{ ($lists->post_priority == 1) ? 'checked' : '' }}>
                                                        <label for="test2">ระดับกลาง (Intermediate)</label>
                                                    </p>
                                                    <p>
                                                        <input type="radio" disabled id="test3" name="radio-group" {{ ($lists->post_priority == 2) ? 'checked' : '' }}>
                                                        <label for="test3">ระดับสูง (Advance)</label>
                                                    </p>
                                                </div>
                                            </div>
                                            {{-- <div class="course__detail-file">
                                                <h3>ไฟล์ประกอบ</h3>
                                                <ul class="list__file">
                                                    @if (isset($lists->courseFiles) && $lists->courseFiles->count())
                                                        @foreach ($lists->courseFiles as $file)
                                                            <li class="list__file-item">
                                                                <div class="list__file-title">
                                                                    <i class="icon-file"></i> {{ $file->name }}
                                                                </div>
                                                                @if ($file->file)
                                                                    <div class="list__file-download">
                                                                        <a href="{{ asset($file->file) }}" target="_blank">Download</a>
                                                                    </div>
                                                                @endif
                                                            </li>
                                                        @endforeach
                                                    @endif
                                                </ul>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="structure" role="tabpanel" aria-labelledby="structure-tab">
                                        {!! $lists->post_excerpt !!}
                                    </div>
                                </div>



                                <div class="course__detail-file" style="background-color: #F8F8F8; padding: 30px;">
                                    <div class="course__detail-body">
                                        <h3>ไฟล์ประกอบ</h3>
                                        <ul class="list__file">
                                            @if (isset($lists->courseFiles) && $lists->courseFiles->count())
                                                @foreach ($lists->courseFiles as $file)
                                                    <li class="list__file-item">
                                                        <div class="list__file-title">
                                                            <i class="icon-file"></i> {{ $file->name }}
                                                        </div>
                                                        @if ($file->file)
                                                            <div class="list__file-download">
                                                                <a href="{{ asset($file->file) }}" target="_blank">Download</a>
                                                            </div>
                                                        @endif
                                                    </li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>

                                @php 
                                    $forcePreTest = '';
                                    $forcePostTest = '';
                                    if(($attribute != null) && ($attribute != "") && isset($attribute->pre_test)) {
                                        
                                        $user = \Auth::guard('backpack')->user();
                                        $email_e = explode('@', $user->email);
                                        if ($email_e[0] == 'admin') {
                                            $username = 'admin';
                                            $password = 'admin';
                                        } else {
                                            $username = strtolower($email_e[0]);
                                            $password = request()->session()->get('password');
                                        }
                                        $username = $user->email;
                                        $EncodePassword = new \App\Helpers\EncodePassword;
                                        $password = $EncodePassword->getEncodePassword($password);
                                        $password = json_encode($password);
                                        $password = urlencode($password);
                                        $forcePreTest = "{$attribute->pre_test}?force_login=true&username={$username}&ind={$password}";
                                        $forcePostTest = "{$attribute->post_test}?force_login=true&username={$username}&ind={$password}";
                                    }
                                @endphp

                                <div class="row justify-content-center">
                                    <div class="col-6 col-sm-4 col-md-3">
                                        <input type="hidden" name="pre_test" value="{{ $forcePreTest }}">
                                        <button type="button" id="btn-pre-test" class="btn btn--loadmore w-100">
                                            <span class="text__link">Pre-Test</span>
                                        </button>
                                    </div>
                                    <div class="col-6 col-sm-4 col-md-3">
                                        <input type="hidden" name="post_test" value="{{ $forcePostTest }}">
                                        <button type="button" id="btn-post-test" class="btn btn--cancel w-100">
                                            <span class="text__link">Post-Test</span>
                                        </button>
                                    </div>
                                </div>
                            </div>


                            <div class="cousrse__media">
                                <h3>วิดีโอสื่อการสอน</h3>
                                <div class="accordion accordion--primary" id="accordionExample">
                                    <div data-toggle="collapse" data-target="#collapse-1" aria-expanded="true" aria-controls="collapse-1" class="accordion__title">
                                        <i class="more-less icon-angle-down"></i>
                                        <div class="title__media">
                                            <div class="title__main">
                                                {{-- @if(isset($lists->courseLessons) && $lists->courseLessons->count())
                                                 {{ count($lists->courseLessons) > 0 ? $lists->courseLessons[0]->name : ''  }}
                                                @endif --}}
                                                วิดีโอสื่อการสอน
                                            </div>
                                            <div class="media__detail">
                                                {{-- <p>1 สืื่อการสอน</p>
                                                <p><i class="icon-time"></i> 00:01:00</p> --}}
                                            </div>
                                        </div>
                                    </div>
                                    <div id="collapse-1" class="collapse show" aria-labelledby="collapse-1" data-parent="#accordionExample">
                                        <div class="accordion__body bg-white">
                                            <div class="media__list">

                                                @if (isset($lists->courseLessons) && $lists->courseLessons->count())
                                                    @foreach ($lists->courseLessons as $file)
                                                        <div class="media__item active">
                                                            {{-- <a href="{{ asset($file->file) }}" class="media__link" target="_blank">
                                                                <i class="icon-play-media"></i> {{ $file->name }}
                                                            </a> --}}
                                                            {{ $file->name }}

                                                            {{-- <a href="{{route('courseLesson', ['id'=> $file->id])}}" class="media__text">
                                                                ตัวอย่าง
                                                            </a> --}}

                                                            <a href="{{ asset($file->file) }}" class="media__text">
                                                                <i class="icon-play-media"></i>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                @endif

                                            </div>
                                        </div>
                                    </div>

                                   
                                </div>
                            </div>

                            <div class="tag--container">
                                <div class="tag__title">
                                    Tags
                                </div>
                                @if(is_array($tags))
                                    @foreach($tags as $tag)
                                        <div class="tag__text">
                                            <a href="{{ route('tagIndex', ['tag' => $tag]) }}">#{{$tag}}</a>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                            
                        </div>

                    @endif
                </div>
            </div>
        </div>
    </section>
</main>

@endsection

@push("scripts")
    {{-- <script>
        const eventDom = {
            init: function() {
                
                this.btnPreAndPostTest();
                this.handleClickRating();
            },
            btnPreAndPostTest: function() {
                $("#btn-pre-test").click(function() {
                    var pre_test_link = $('input[name="pre_test"]').val();
                    window.open(pre_test_link, '_blank');
                });

                $("#btn-post-test").click(function() {
                    var post_test_link = $('input[name="post_test"]').val();
                    window.open(post_test_link, '_blank');
                });
            },
            handleClickRating: function() {
                $("#btn-rating").click(function() {
                    let id = $(this).attr('data-id');
                    $("#modalRating").modal('show');
                });

                
            }
        }

        $(document).ready(function() {
            eventDom.init();
            $('.searchTag').select2();

          
            let data_value = 0;
            $('.label_rating').click(function() {
                data_value = $(this).attr('data-value');
            });

            $("#btn-modalRating-save").click(function() {
                console.log(data_value);
                let contentId = "{{ $lists->id }}";
                var token = $('meta[name="csrf-token"]').attr('content');
                $.ajax({
                    headers: {'X-CSRF-TOKEN': token},
                    type: "POST",
                    url: "/ajax-rating-course-store",
                    data: {courseId: contentId, score: data_value},
                    success: function (response) {
                        console.log(response);
                        if (response.status == 'success') {
                            $('#modalRating').modal('hide')
                          
                            if(!$("#btn-rating").hasClass('active')) {
                                $("#btn-rating").addClass('active');
                            }
                        }else {
                           console.log('error')
                        }
                        return false;
                    },
                    error: function (response) {
                        //
                    }
                });
                
            })

            $("#btn-email").click(function(){
                $("#modalEmailConfirm").modal('show');
            })
                    
        })
    </script> --}}
@endpush


@push('css')

<style>
    ul > li.ratingLi {
        border: none !important;
    }
    .rating {
        display: flex;
        flex-direction: row-reverse;
        justify-content: center
    }

    .rating>input {
        display: none
    }

    .rating>label {
        position: relative;
        width: 1em;
        font-size: 3.5vw;
        color: #FFD600;
        cursor: pointer
    }

    .rating>label::before {
        content: "\2605";
        position: absolute;
        opacity: 0
    }

    .rating>label:hover:before,
    .rating>label:hover~label:before {
        opacity: 1 !important
    }

    .rating>input:checked~label:before {
        opacity: 1
    }

    .rating:hover>input:checked~label:before {
        opacity: 0.4
    }
</style>
@endpush