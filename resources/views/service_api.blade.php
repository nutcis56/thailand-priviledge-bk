@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>
@section("content")
<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ __('tpc.service_api') }}</h2>
                                <p class="banner__text">
                                    
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item active"><a href="{{ route('newList') }}" class="breadcrumb-link">Service Api</a></li>
                                </ol>
                            </nav>
                        </div>
                        
                        <div class="news--list">
                            <div class="card-deck card-deck--cards-1 card-deck--cards-md-2 card-deck--cards-lg-3">

                                @if(!empty($lists))
                                    {!! $lists->post_content !!}
                                @endif
                                
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </section>


</main>

@endsection

@push("scripts")
    <script>
        
    </script>
@endpush
