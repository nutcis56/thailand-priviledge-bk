@extends("layouts.master")

@push('css')
    <style>
        .big_banner_hover:hover {
            cursor: pointer;
        }

        .select2-container--default .select2-selection--single {
            background-color: transparent !important;
            
            border: 1px solid white !important;
        }
        .select2-selection--single .select2-selection__rendered {
            color: white !important;
        }
    </style>
@endpush

@section("content")

<main class="main home main--home">

    {{-- BIG BANNER --}}
    <section class="home--section home--banner">
        <div class="slider slick--hero-banner">
            @if(count($bigBanner) > 0)
                @foreach($bigBanner as $key => $value)
                    <div class="slick__item big_banner_hover div_big_banner" data-url="{{ $value->link ?? '#' }}">
                        <div class="slick__image lazyload" data-bgset="{{asset($value->image)}}" style="background-image: url({{asset($value->image)}});"> 
                        </div>
                        <div class="slick__body">
                            <div class="container-fluid">
                                <div class="row justify-content-center">
                                    <div class="col-lg-10">
                                        <div class="slick__highlight">
                                            <h2 class="slick__title">{{ $value->name ?? '' }}</h2>
                                            <h3 class="slick__sub">{{ $value->value ?? '' }}</h3>
                                        </div>
                                        <div class="slick__text">
                                            {!! $value->detail ?? '' !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </section>

    {{-- EXTERNAL LINK --}}
    <section class="home--section home--highlight-menu">
        <div class="menu__lg d-none d-md-block">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="slider slick--hightlight-menu">

                            @if(count($externalLink) > 0)
                                @foreach($externalLink as $key => $value)
                                    <div class="slick__item">
                                        <a href="{{ $value->link ?? '#' }}" target="{{ $value->target }}" class="slick__body">
                                            <div class="slick__icon">
                                                <img src="{{asset($value->image)}}" alt="">
                                            </div>
                                            <div class="slick__text">
                                                {{ $value->name ?? '' }}
                                            </div>
                                        </a>
                                    </div>
                                @endforeach 
                            @endif
                            
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="menu__sm d-md-none">
            <div class="dropdown">
                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ระบบสารบรรณอิเล็กทรอนิกส์ (E-Doc)
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> 
                    @if(count($externalLink) > 0)
                        @foreach($externalLink as $key => $value)
                            <a class="dropdown-item" href="{{ $value->link ?? '#' }}" target="{{ $value->target }}">{{ $value->name ?? '' }}</a>
                        @endforeach 
                    @endif
                </div>
            </div>
        </div>
    </section>

    {{-- MEMBER SERVICE --}}
    <section class="home--section home--member">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="home--title">
                        <h3 class="title__main">
                            {{ __('tpc.index_memeber_service') }}
                        </h3>
                    </div>
                    <div class="slider slick--member-menu">
                        @php 
                            $user = \Auth::guard('backpack')->user();
                            $email_base64 = base64_encode($user->email);
                            $serviceConfigLink = config('app.bookstack_site_url');
                        @endphp
                        @if(count($memberLink) > 0)
                            @foreach($memberLink as $key => $value)
                                @php
                                    // $linkService = "{$value->link}/flibem?email={$email_base64}";
                                    $linkService = "{$serviceConfigLink}flibem?email={$email_base64}&redirect={$value->link}";
                                @endphp
                                <div class="slick__item">
                                    <a href="{{ $linkService ?? '#' }}" target="{{ $value->target }}" class="card card--member">
                                        <figure class="card__figure">
                                            <img src="{{asset($value->image)}}" class="card__image">
                                            <img src="{{asset($value->icon)}}" class="card__image image__hover">
                                        </figure>
                                        <div class="card__body">
                                            <p class="card__title">
                                                {{ $value->name ?? '' }}<br>{{ $value->value ?? '' }}
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach 
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- NEWS, ANNOUNCE, CALENDAR --}}
    <section class="home--section home--news">
        <div class="container-fluid">
            <div class="row justify-content-center tab--dropdown">
                <div class="col-lg-10">
                    <div class="home--title">
                        <h3 class="title__main">
                            {{__('tpc.index_new_event')}}
                        </h3>
                    </div>

                    <div class="tabs--wrapper">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-12 col-md-10 col-lg-8">
                                    <ul class="nav nav-tabs nav-justified d-none d-md-flex justify-content-md-between" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="RealEstateTab" data-toggle="tab" href="#RealEstate" role="tab" aria-controls="RealEstate" aria-selected="true">{{__('tpc.index_new_event_news')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="ConstructionTab" data-toggle="tab" href="#Construction" role="tab" aria-controls="Construction" aria-selected="false">{{__('tpc.index_new_event_announce')}}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="RecurringTab" data-toggle="tab" href="#Recurring" role="tab" aria-controls="Recurring" aria-selected="false">{{__('tpc.index_new_event_calendar')}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="tab-content" id="operationTab">

                            {{-- NEWS --}}
                            <div class="tab-pane fade show active" id="RealEstate" role="tabpanel" aria-labelledby="RealEstateTab">
                                <div class="news--highlight">

                                    @if(!empty($newsFirst))
                                        @php $dateNewsFirst = date_format($newsFirst->created_at, 'd.m.Y')  @endphp
                                        <div class="card card--news-highlight">
                                            <a href="/news-list/{{ $newsFirst->post_name != "" ? $newsFirst->post_name : $newsFirst->id }}" class="card__figure card__item">
                                                <img src="{{ $newsFirst->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                            </a>

                                            <div class="card__body card__item">
                                                <div class="card__type">
                                                    {{ $newsFirst->position->name ?? '' }}
                                                </div>
                                                <a href="/news-list/{{ $newsFirst->post_name != "" ? $newsFirst->post_name : $newsFirst->id }}" class="card__title">
                                                    {{ $newsFirst->post_title ?? '' }}
                                                </a>
                                                <ul class="list-group">
                                                    <li class="list-group-item"><i class="icon-date"></i> {{ $dateNewsFirst ?? '' }}</li>
                                                    <li class="list-group-item"><i class="icon-create"></i> {{ $newsFirst->userInfo->name ?? '' }} </li>
                                                    <li class="list-group-item"><i class="icon-view"></i> {{ $newsFirst->visitlog_count ?? 0 }}</li>
                                                    <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$newsFirst->like_count}}</li>
                                                </ul>
                                                <p class="card__text">
                                                    {!! $newsFirst->post_excerpt ?? '' !!}
                                                </p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="slider slick--news">
                                    @if(count($news) > 0)
                                        @foreach($news as $key => $new)
                                            @if($key != 0)
                                            @php $dateNew = date_format($new->created_at, 'd.m.Y')  @endphp
                                                <div class="slick__item">
                                                    <a href="/news-list/{{ $new->post_name != "" ? $new->post_name : $new->id }}" class="card card--news">
                                                        <figure class="card__figure">
                                                            <img src="{{ $new->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                                        </figure>
            
                                                        <div class="card__body">
                                                            <h3 class="card__title">
                                                                {{ $new->post_title ?? '' }}
                                                            </h3>
                                                            <ul class="list-group">
                                                                <li class="list-group-item"><i class="icon-date"></i> {{ $dateNew ?? '' }}</li>
                                                                <li class="list-group-item"><i class="icon-create"></i> {{ $new->userInfo->name ?? '' }} </li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <div class="text-center">
                                    <a href="{{ route('newList') }}" class="btn btn--link-primary">
                                        <span class="text__link">{{ __('tpc.read_more') }}</span>
                                        <span class="icon__link">
                                            <i class="icon-arrow-right"></i>
                                        </span>
                                    </a>
                                </div>
                            </div>

                            {{-- ANNOUNCE --}}
                           
                            <div class="tab-pane fade" id="Construction" role="tabpanel" aria-labelledby="ConstructionTab">
                                <div class="news--highlight">
                                    @if(!empty($announceFirst))
                                        @php $dateAnnounceFirst = date_format($announceFirst->created_at, 'd.m.Y')  @endphp
                                        <div class="card card--news-highlight">
                                            <a href="/announce-list/{{ $announceFirst->post_name != "" ? $announceFirst->post_name : $announceFirst->id }}" class="card__figure card__item">
                                                <img src="{{ $announceFirst->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                            </a>

                                            <div class="card__body card__item">
                                                <div class="card__type">
                                                    {{ $announceFirst->position->name ?? '' }}
                                                </div>
                                                <a href="/announce-list/{{ $announceFirst->post_name != "" ? $announceFirst->post_name : $announceFirst->id }}" class="card__title">
                                                    {{ $announceFirst->post_title ?? '' }}
                                                </a>
                                                <ul class="list-group">
                                                    <li class="list-group-item"><i class="icon-date"></i> {{ $dateAnnounceFirst ?? '' }}</li>
                                                    <li class="list-group-item"><i class="icon-create"></i> {{ $announceFirst->userInfo->name ?? '' }} </li>
                                                    <li class="list-group-item"><i class="icon-view"></i> {{ $announceFirst->visitlog_count ?? 0 }}</li>
                                                    <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$announceFirst->like_count ?? 0}}</li>
                                                </ul>
                                                <p class="card__text">
                                                    {!! $announceFirst->post_excerpt ?? '' !!}
                                                </p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="slider slick--news">
                                    @if(count($announces) > 0)
                                        @foreach($announces as $key => $announce)
                                            @if($key != 0)
                                                @php $dateAnnounce = date_format($announce->created_at, 'd.m.Y')  @endphp
                                                <div class="slick__item">
                                                    <a href="/announce-list/{{ $announce->post_name != "" ? $announce->post_name : $announce->id }}" class="card card--news">
                                                        <figure class="card__figure">
                                                            <img src="{{ $announce->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                                        </figure>

                                                        <div class="card__body">
                                                            <h3 class="card__title">
                                                                {{ $announce->post_title ?? '' }}
                                                            </h3>
                                                            <ul class="list-group">
                                                                <li class="list-group-item"><i class="icon-date"></i> {{$dateAnnounce ?? ''}}</li>
                                                                <li class="list-group-item"><i class="icon-create"></i> {{ $announce->userInfo->name ?? '' }} </li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                                <div class="text-center">
                                    <a href="{{ route('announceIndex') }}" class="btn btn--link-primary">
                                        <span class="text__link">{{ __('tpc.read_more') }}</span>
                                        <span class="icon__link">
                                            <i class="icon-arrow-right"></i>
                                        </span>
                                    </a>
                                </div>
                            </div>

                            {{-- CALENDAR --}}
                            <div class="tab-pane fade" id="Recurring" role="tabpanel" aria-labelledby="RecurringTab">
                                <div class="news--highlight">
                                    @if(!empty($calendarFirst))
                                        @php $dateCalendarFirst = date_format($calendarFirst->created_at, 'd.m.Y')  @endphp
                                        <div class="card card--news-highlight">
                                            <a href="/calendar-list/{{ $calendarFirst->post_name != "" ? $calendarFirst->post_name : $calendarFirst->id }}" class="card__figure card__item">
                                                <img src="{{ $calendarFirst->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                            </a>

                                            <div class="card__body card__item">
                                                <div class="card__type">
                                                    ปฎิทินกิจกรรม
                                                </div>
                                                <a href="/calendar-list/{{ $calendarFirst->post_name != "" ? $calendarFirst->post_name : $calendarFirst->id }}" class="card__title">
                                                    {{ $calendarFirst->post_title ?? '' }}
                                                </a>
                                                <ul class="list-group">
                                                    <li class="list-group-item"><i class="icon-date"></i> {{ $dateCalendarFirst ?? '' }}</li>
                                                    <li class="list-group-item"><i class="icon-create"></i> {{ $calendarFirst->userInfo->name ?? '' }} </li>
                                                    <li class="list-group-item"><i class="icon-view"></i> {{ $calendarFirst->visitlog_count ?? 0 }}</li>
                                                    <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$calendarFirst->like_count ?? 0}}</li>
                                                </ul>
                                                <p class="card__text">
                                                    {!! $calendarFirst->post_excerpt ?? '' !!}
                                                </p>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="slider slick--news">
                                    @if(count($calendars) > 0)
                                        @foreach($calendars as $key => $calendar)
                                            @if($key != 0)
                                                @php $dateCalendar = date_format($calendar->created_at, 'd.m.Y')  @endphp
                                                <div class="slick__item">
                                                    <a href="/calendar-list/{{ $calendar->post_name != "" ? $calendar->post_name : $calendar->id }}" class="card card--news">
                                                        <figure class="card__figure">
                                                            <img src="{{ $calendar->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                                        </figure>

                                                        <div class="card__body">
                                                            <h3 class="card__title">
                                                                {{ $calendar->post_title ?? '' }}
                                                            </h3>
                                                            <ul class="list-group">
                                                                <li class="list-group-item"><i class="icon-date"></i> {{ $dateCalendar ?? '' }}</li>
                                                                <li class="list-group-item"><i class="icon-create"></i> {{ $calendar->userInfo->name ?? '' }} </li>
                                                            </ul>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                    
                                </div>
                                <div class="text-center">
                                    <a href="{{ route('calendarList') }}" class="btn btn--link-primary">
                                        <span class="text__link">{{ __('tpc.read_more') }}</span>
                                        <span class="icon__link">
                                            <i class="icon-arrow-right"></i>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home--section home--search bg-cover" style="background-image: url('images/bg/bg--home-search.jpg');">
        <div class="bg-black-opacity">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 offset-lg-3">
                        <div class="search--wrapper">
                            <div class="search--title">
                                <h3 class="title__main">
                                    {{ __('tpc.index_system_center_knowledge') }}
                                </h3>
                                <h3 class="title__main">
                                    {{ __('tpc.index_knowledge_sharing_center_system') }}
                                </h3>
                            </div>
                            <form action="{{ route('knowledgeList') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="{{ __('tpc.search') }}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select-white mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">{{ __('tpc.category') }}</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}">{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    @php 
                                        $tags = CoreConfigService::getTag();
                                    @endphp
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select-white mb-3 mb-lg-0">
                                            <select class="tagsKnowledgeHome select__input form-control" name="searchTag">
                                                <option value="">{{ __('tpc.choice_tag') }}</option>
                                                @if(count($tags) > 0)
                                                    @foreach($tags as $key => $value)
                                                        <option value="{{ $value->name }}">{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">{{ __('tpc.search') }}</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="graphic--search d-none d-lg-block">
                <img src="{{asset("images/homepage/search/graphic-search.png")}}" class="img-fluid">
            </div>
        </div>
    </section>

    <section class="home--section home--knowledge">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="home--title">
                        <h3 class="title__main">
                            {{ __('tpc.index_my_knowledge_sharing') }}
                        </h3>
                    </div>
                    <div class="slider slick--knowledge">

                        @if(count($myKnowledges) > 0)
                            @foreach($myKnowledges as $knowledge)
                                @php $dateKnowledge = date_format($knowledge->created_at, 'd.m.Y') @endphp
                                <div class="slick__item">
                                    <a href="/knowledge-list/{{ $knowledge->post_name != "" ? $knowledge->post_name : $knowledge->id }}" class="card card--knowledge">
                                        <figure class="card__figure">
                                            <img src="{{ $knowledge->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                        </figure>
        
                                        <div class="card__body">
                                            <div class="card__type">
                                                {{ $knowledge->position->name ?? '' }}
                                            </div>
                                            <h3 class="card__title">
                                                {{ $knowledge->post_title ?? '' }}
                                            </h3>
                                            <ul class="list-group">
                                                <li class="list-group-item"><i class="icon-date"></i> {{ $dateKnowledge ?? '' }}</li>
                                                <li class="list-group-item"><i class="icon-create"></i> {{ $knowledge->userInfo->name ?? '' }} </li>
                                                <li class="list-group-item"><i class="icon-view"></i> {{ $knowledge->visitlog_count ?? 0 }}</li>
                                                <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$knowledge->like_count ?? 0}}</li>
                                            </ul>
                                            <p class="card__text">
                                                {!! $knowledge->post_excerpt ?? '' !!}
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        @endif
                        
                        
                        
                    </div>
                    <div class="text-center">
                        <a href="{{ route('MyknowLedge') }}" class="btn btn--link-primary">
                            <span class="text__link">{{ __('tpc.read_more') }}</span>
                            <span class="icon__link">
                                <i class="icon-arrow-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home--section home--widget">
        <div class="menu--wrapper">
            <div class="row no-gutters">
                <div class="col-lg-6">
                    <a href="{{ route('phoneDirectory') }}" class="menu__item bg__brown">
                        <div class="menu__icon">
                            <img src="{{asset("images/homepage/icon-phone.png")}}" alt="" class="img-fluid">
                        </div>
                        <div class="menu__text">
                            {{ __('tpc.index_phone_directory') }}
                        </div>
                    </a>
                </div>
                <div class="col-lg-6">
                    <a href="{{ route('jobList') }}" class="menu__item bg__gold">
                        <div class="menu__icon">
                            <img src="{{asset("images/homepage/icon-job.png")}}" alt="" class="img-fluid">
                        </div>
                        <div class="menu__text">
                            {{ __('tpc.index_your_job_knowledge') }}
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <section class="home--section home--media">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="row">
                        <div class="col-lg-6 mb-5 mb-lg-0">
                            <div class="home--title text-left">
                                <h3 class="title__sub">
                                    {{ __('tpc.index_knowledge_idea_sharing') }}
                                </h3>
                            </div>

                            
                            <div class="card-deck card-deck--cards-1 card-deck--cards-md-2">
                                @if(count($knowledges) > 0)
                                    @foreach($knowledges as $knowledge)
                                        @php 
                                            $dateKnowledge = date_format($knowledge->created_at, 'd.m.Y') 
                                        @endphp
                                        <a href="/knowledge-list/{{ $knowledge->post_name != "" ? $knowledge->post_name : $knowledge->id }}" class="card card--knowledge">
                                            <figure class="card__figure">
                                                <img src="{{asset("images/homepage/knowledge/pic-01.jpg")}}" alt="" class="card__image card__zoom-in">
                                            </figure>
        
                                            <div class="card__body">
                                                <div class="card__type">
                                                    {{ $knowledge->position->name ?? '' }}
                                                </div>
                                                <h3 class="card__title">
                                                    {{ $knowledge->post_title ?? '' }} 
                                                </h3>
                                                <ul class="list-group">
                                                    <li class="list-group-item"><i class="icon-date"></i> {{ $dateKnowledge ?? '' }}</li>
                                                    <li class="list-group-item"><i class="icon-create"></i> {{ $knowledge->userInfo->name ?? '' }} </li>
                                                    <li class="list-group-item"><i class="icon-view"></i> {{ $knowledge->visitlog_count ?? 0 }}</li>
                                                    <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$knowledge->like_count ?? 0}}</li>
                                                </ul>
                                                <p class="card__text">
                                                   {!! $knowledge->post_excerpt !!}
                                                </p>
                                            </div>
                                        </a>
                                    @endforeach
                                @endif
                            </div>
                            <div class="text-center btn--readmore">
                                <a href="{{ route('knowledgeList') }}" class="btn btn--link-primary">
                                    <span class="text__link">{{ __('tpc.read_more') }}</span>
                                    <span class="icon__link">
                                        <i class="icon-arrow-right"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="home--title text-left">
                                <h3 class="title__sub">
                                    {{ __('tpc.index_tpc_sharing_video') }}
                                </h3>
                            </div>

                            @if(count($videies) > 0)
                                @foreach($videies as $video)
                                    @php 
                                        $date = date_format($video->created_at, 'd.m.Y');
                                    @endphp
                                    <a href="/video-list/{{ $video->post_name != "" ? $video->post_name : $video->id }}"  class="card card--knowledge card--knowledge__video">
                                        <figure class="card__figure">
                                            {{-- <div class="card__image" style="background-image: url({{$video->post_cover }});">
                                                <div class="card__icon">
                                                    <span class="icon-video"></span>
                                                </div>
                                            </div> --}}
                                            <img src="{{$video->post_cover ?? asset('images/no_image.png') }}" alt="" class="card__image card__zoom-in">
                                        </figure>
                                        <div class="card__body">
                                            <div class="card__type">
                                                {{ $video->position->name ?? '' }}
                                            </div>
                                            <h3 class="card__title">
                                                {{ $video->post_title ?? '' }}
                                            </h3>
                                            <ul class="list-group">
                                                <li class="list-group-item"><i class="icon-date"></i> {{ $date ?? ''}}</li>
                                                <li class="list-group-item"><i class="icon-create"></i> {{ $video->userInfo->name ?? '' }} </li>
                                                <li class="list-group-item"><i class="icon-view"></i> {{ $video->visitlog_count ?? 0 }}</li>
                                                <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$video->like_count ?? 0}}</li>
                                            </ul>
                                        </div>
                                    </a>
                                @endforeach
                            @endif
                            
                            <div class="btn--readmore text-center">
                                <a href="{{ route('videoAlbumIndex') }}" class="btn btn--link-primary">
                                    <span class="text__link">{{ __('tpc.read_more') }}</span>
                                    <span class="icon__link">
                                        <i class="icon-arrow-right"></i>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home--section home--online bg-cover" style="background-image: url('images/bg/bg--home-search.jpg');"> 
        <div class="bg-black-opacity">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-10">
                        <div class="course--wrapper">
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="dvleft">
                                        <span class="icon-video"></span>
                                        <h3 class="title__main">
                                            {{ __('tpc.index_open_online_course') }}
                                        </h3>
                                        <div class="text-left">
                                            <a href="{{ route('courseIndex') }}" class="btn btn--link-secondary">
                                                <span class="text__link">{{ __('tpc.read_more') }}</span>
                                                <span class="icon__link">
                                                    <i class="icon-arrow-right"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">

                                    @if(count($courses) > 0)
                                        @foreach($courses as $course)
                                        
                                        {{-- data-rel="lightcase" --}}
                                            <a href="/course-list/{{ $course->post_name != "" ? $course->post_name : $course->id }}" target="_top" class="card card--course">
                                                <figure class="card__figure">
                                                    <img src="{{$course->post_cover ?? asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in"> 
                                                    <div class="card__icon">
                                                        {{-- <span class="icon-video"></span> --}}
                                                    </div>
                                                </figure>
                                                <div class="card__body">
                                                    <h3 class="card__title">
                                                        {{ $course->post_title ?? '' }}
                                                    </h3>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">ระดับคอร์ส: {{ ($course->post_priority == '0') ? 'ระดับต้น' : ($course->post_priority == '1' ? 'ระดับกลาง' : 'ระดับสูง') }}</li>
                                                        <li class="list-group-item">
                                                            <div class="rating__star">
                                                                <span>{{($course->ratingScore > 0) ? round($course->ratingScore) : '' ?? 0}}</span>
                                                                {{-- <span class="icon-star"></span>
                                                                <span class="icon-star"></span>
                                                                <span class="icon-star"></span>
                                                                <span class="icon-star"></span> --}}
                                                                @include('elements.rating_score.icon_rating_score', ['data' => round($course->ratingScore)])
                                                            </div>
                                                        </li>
                                                    </ul>
                                                    <p class="card__text" style="color: #fff !importants">
                                                        {!! $course->post_excerpt ?? '' !!}
                                                    </p>
                                                </div>
                                            </a>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="home--section home--blog">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="home--title">
                        <h3 class="title__main text-left">
                            {{ __('tpc.blog') }}
                        </h3>
                    </div>
                    <div class="blog--wrapper">
                        <div class="row">
                            <div class="col-lg-6">
                                @if(!empty($blogFirst))
                                @php 
                                    $dateBlogFirst = date_format($blogFirst->created_at, 'd.m.Y');
                                @endphp
                                    <div class="card card--blog-highlight">
                                        <a href="{{ route('blogList') }}" class="card__figure">
                                            <div class="card__image" style="background-image: url({{$blogFirst->post_cover ?? asset('images/no_image.png')}});"></div>
                                        </a>

                                        <div class="card__body card__item">
                                            <div class="card__type">
                                                {{ $blogFirst->position->name ?? '' }}
                                            </div>
                                            <a href="{{ route('blogList') }}" class="card__title">
                                                {{ $blogFirst->post_title ?? '' }}
                                            </a>
                                            <ul class="list-group">
                                                <li class="list-group-item"><i class="icon-date"></i> {{ $dateBlogFirst ?? '' }}</li>
                                                <li class="list-group-item"><i class="icon-create"></i> {{ $blogFirst->userInfo->name ?? '' }} </li>
                                                <li class="list-group-item"><i class="icon-view"></i> {{ $blogFirst->visitlog_count ?? 0 }}</li>
                                                <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$blogFirst->like_count ?? 0}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                @endif
                            </div>
                            <div class="col-lg-6">

                                @if(count($blogs) > 0)
                                    @foreach($blogs as $key => $blog)
                                        @if($key != 0)

                                            @php 
                                                $dateBlog = date_format($blog->created_at, 'd.m.Y');
                                            @endphp
                                            <div class="card card--blog" style="justify-content: initial;">
                                                <a href="{{ route('blogList') }}" class="card__figure card__item">
                                                    <img src="{{ $blog->post_cover ?? asset('images/no_image.png') }}" height="100%" alt="" class="card__image card__zoom-in">
                                                </a>
            
                                                <div class="card__body card__item">
                                                    <div class="card__type">
                                                        {{ $blog->position->name ?? '' }}
                                                    </div>
                                                    <a href="{{ route('blogList') }}" class="card__title">
                                                        {{ $blog->post_title ?? '' }}
                                                    </a>
                                                    <ul class="list-group">
                                                        <li class="list-group-item"><i class="icon-date"></i> {{ $dateBlog ?? ''}}</li>
                                                        <li class="list-group-item"><i class="icon-create"></i> {{ $blog->userInfo->name ?? '' }} </li>
                                                        <li class="list-group-item"><i class="icon-view"></i> {{ $blog->visitlog_count ?? 0 }}</li>
                                                        <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$blog->like_count ?? 0}}</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        @endif

                                    @endforeach
                                @endif
                               
                                
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <a href="{{ route('blogList') }}" class="btn btn--link-primary">
                            <span class="text__link">{{ __('tpc.read_more') }}</span>
                            <span class="icon__link">
                                <i class="icon-arrow-right"></i>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection

@push('scripts')
    <script>
        const domInit = {
            init: function() {
                this.handleClickBigBanner();
                this.select2TagKnowledge();
            },
            handleClickBigBanner: function() {
                $(".div_big_banner").click(function() {
                    let url = $(this).attr('data-url');

                    if(url != '#') {
                        window.open(url, "_blank");
                    }
                   
                })
            },
            select2TagKnowledge: function() {
                $('.tagsKnowledgeHome').select2();
            }
        }
        $(document).ready(function() {
            domInit.init();
        })
    </script>
@endpush