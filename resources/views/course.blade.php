@extends("layouts.master")
<?php
$page = 'home';
$lang = 'th';
$firstList = null;


?>


@push("css")
<style>
    .knowledge_hidden {
        display: none;
    }
</style>
@endpush


@section("content")



<main class="main content main--content">
    <section class="content--section content--banner">
        <div class="content__banner" style="background-image: url({{asset($pageTitle->image ?? '')}})">
            <div class="bg-absolute bg-black-opacity">
                <div class="banner__body">
                    <div class="container-fluid">
                        <div class="row justify-content-center">
                            <div class="col-auto">
                                <h2 class="banner__title">{{ $pageTitle->name ?? '' }}</h2>
                                <p class="banner__text">
                                    {!! $pageTitle->detail ?? '' !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article mt-n5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper bg-white">
                        <div class="search--wrapper">
                            <form action="{{ route('courseIndex') }}" method="GET" class="form form-srch">
                                <div class="form-row align-items-center justify-content-center">
                                    <div class="col-md-6 col-lg-4">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <input type="text" class="form-control search__input" name="search" placeholder="คำค้น" value="{{ (isset($keyword)) ? ($keyword != '') ? $keyword : '' : '' }}">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group form--select mb-3 mb-lg-0">
                                            <select class="form-control select__input" name="category">                                                
                                                <option value="" selected="">หมวดหมู่</option>
                                                @if(count($section) > 0)
                                                    @foreach($section as $key => $value)
                                                        <option value="{{ $value->id }}" {{ $keywordCategory == $value->id ? 'selected' : '' }}>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-6 col-lg-3">
                                        <div class="form-group mb-3 mb-lg-0">
                                            @php 
                                                $tags = CoreConfigService::getTag();
                                            @endphp
                                            <select class="searchTag form-control" name="searchTag">
                                                <option value="">แท๊ก</option>
                                                @if(count($tags) > 0)
                                                    @foreach($tags as $key => $value)
                                                        <option value="{{ $value->name }}" @if(isset($keywordTag)) {{ $value->name == $keywordTag ? 'selected' : '' }} @endif>{{ $value->name ?? '' }}</option>
                                                    @endforeach
                                                @endif
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-2">
                                        <div class="form-group mb-3 mb-lg-0">
                                            <button type="submit" class="btn btn--primary w-100"><span class="btn__text btn__text--sm">ค้นหา</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="breadcrumb__outer">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="/" class="breadcrumb-link"><span class="icon-home"></span></a></li>
                                    <li class="breadcrumb-item">Open Online Course</li>
                                    <li class="breadcrumb-item active"><a href="{{ route('courseIndex') }}" class="breadcrumb-link">Course</a></li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="content--section content--article bg-light-gray">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper">
                        <div class="content--title">
                            <h3 class="title__main">
                                คอร์สแนะนำ
                            </h3>
                        </div>
                        <div class="slider slick--course">
                            @if(count($recommends) > 0)
                                @foreach($recommends as $recommend)

                                @php 
                                    $date = date_format($recommend->created_at, 'd.m.Y');
                                @endphp
                                    <div class="slick__item">
                                        <a href="/course-list/{{ $recommend->post_name != "" ? $recommend->post_name : $recommend->id }}" class="card card--course-list">
                                            <figure class="card__figure">
                                                <img src="{{$recommend->post_cover ?? asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in">
                                            </figure>
        
                                            <div class="card__body">
                                                <div class="card__type">
                                                    {{ $recommend->position->name ?? "" }}
                                                </div>
                                                <h3 class="card__title">
                                                    {{ $recommend->post_title ?? "" }}
                                                </h3>
                                                <ul class="list-group">
                                                    <li class="list-group-item"><i class="icon-date"></i> {{ $date  ?? '' }}</li>
                                                    <li class="list-group-item"><i class="icon-create"></i> {{ $recommend->userInfo->name ?? '' }} </li>
                                                    <li class="list-group-item"><i class="icon-view"></i> {{ $recommend->visitlog_count ?? 0 }}</li>
                                                    <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$recommend->like_count ?? 0}}</li>
                                                </ul>
                                                <p class="card__text">
                                                    {!! $recommend->post_excerpt ?? "" !!}
                                                </p>
                                            </div>
        
                                            <div class="card__footer">
                                                <div class="card__time">
                                                    <span class="icon-time"></span> 1.30 นาที
                                                </div>
                                                <div class="card__price">
                                                    ฟรี
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach


                            @endif
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="content--section content--article bg-white">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-10">
                    <div class="content__wrapper--list">
                        <div class="news--list">
                            <div class="content--title">
                                <h3 class="title__main">
                                    คอร์สทั้งหมด
                                </h3>
                            </div>
                            <div class="card-deck card-deck--cards-1 card-deck--cards-md-2 card-deck--cards-lg-3">
                                @if(count($lists) > 0)
                                    @foreach($lists as $key => $list)
                                        @php 
                                            $date = date_format($list->created_at, 'd.m.Y');
                                        @endphp

                                        @if($key <= 5)
                                            <a href="/course-list/{{ $list->post_name != "" ? $list->post_name : $list->id }}" class="card card--course-list">
                                                <figure class="card__figure">
                                                    <img src="{{$list->post_cover ?? asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in">
                                                </figure>

                                                <div class="card__body">
                                                    <div class="card__type">
                                                        {{ $list->position->name ?? "" }}
                                                    </div>
                                                    <h3 class="card__title">
                                                        {{ $list->post_title ?? '' }}
                                                    </h3>
                                                    <ul class="list-group">
                                                        <li class="list-group-item"><i class="icon-date"></i> {{ $date ?? '' }}</li>
                                                        <li class="list-group-item"><i class="icon-create"></i> {{ $list->userInfo->name ?? '' }} </li>
                                                        <li class="list-group-item"><i class="icon-view"></i> {{ $list->visitlog_count ?? 0 }}</li>
                                                        <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$list->like_count ?? 0}}</li>
                                                    </ul>
                                                    <p class="card__text">
                                                    {!! $list->post_excerpt ?? '' !!}
                                                    </p>
                                                </div>

                                                <div class="card__footer">
                                                    <div class="card__time">
                                                        <span class="icon-time"></span> 1.30 นาที
                                                    </div>
                                                    <div class="card__price">
                                                        ฟรี
                                                    </div>
                                                </div>
                                            </a>
                                        @else 
                                            <a href="/course-list/{{ $list->post_name != "" ? $list->post_name : $list->id }}" class="card card--course-list knowledge_hidden">
                                                <figure class="card__figure">
                                                    <img src="{{$list->post_cover ?? asset('images/no_image.png')}}" alt="" class="card__image card__zoom-in">
                                                </figure>

                                                <div class="card__body">
                                                    <div class="card__type">
                                                        {{ $list->position->name ?? "" }}
                                                    </div>
                                                    <h3 class="card__title">
                                                        {{ $list->post_title ?? '' }}
                                                    </h3>
                                                    <ul class="list-group">
                                                        <li class="list-group-item"><i class="icon-date"></i> {{ $date ?? '' }}</li>
                                                        <li class="list-group-item"><i class="icon-create"></i> {{ $list->userInfo->name ?? '' }} </li>
                                                        <li class="list-group-item"><i class="icon-view"></i> {{ $list->visitlog_count ?? 0 }}</li>
                                                        <li class="list-group-item"><i class="far fa-thumbs-up"></i> {{$list->like_count ?? 0}}</li>
                                                    </ul>
                                                    <p class="card__text">
                                                    {!! $list->post_excerpt ?? '' !!}
                                                    </p>
                                                </div>

                                                <div class="card__footer">
                                                    <div class="card__time">
                                                        <span class="icon-time"></span> 1.30 นาที
                                                    </div>
                                                    <div class="card__price">
                                                        ฟรี
                                                    </div>
                                                </div>
                                            </a>
                                        @endif
                                    @endforeach
                                @endif

                                
                                
                            </div>
                        </div>
                        <div class="text-center">
                            <a href="#" class="btn btn--loadmore" id="btn_loadmore">
                                <span class="text__link">โหลดเพิ่มเติม</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection
@push("scripts")
<script>
    $(document).ready(function(){
        $("#btn_loadmore").click(function(){
            $("a").removeClass("knowledge_hidden");
            $(this).fadeOut();
        });
        $('.searchTag').select2();
    });
</script>
@endpush