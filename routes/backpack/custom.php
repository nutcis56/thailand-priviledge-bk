<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin'),
        ['one_device_loggedin']
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes

    Route::get('dashboard', '\Backpack\CRUD\app\Http\Controllers\AdminController@dashboard')->name('backpack.dashboard');

    Route::crud('post', 'PostCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('category-blog', 'CategoryBlogCrudController');

    Route::crud('document', 'DocumentCrudController');
    Route::crud('core-config', 'CoreConfigCrudController');

    // new tag
    Route::post('tag/new-tag-by-ajax', 'TagCrudController@newTagByAjax')->name('tag.new-tag-by-ajax');

    Route::get('charts/users', 'Charts\LatestUsersChartController@response');
    Route::get('charts/new-entries', 'Charts\NewEntriesChartController@response');
    Route::crud('blog', 'BlogCrudController');
    Route::crud('q-and-a', 'QAndACrudController');
    Route::crud('knowledge-idea-sharing', 'KnowledgeIdeaSharingCrudController');
    Route::crud('your-job-knowledge', 'YourJobKnowledgeCrudController');
    Route::crud('t-p-c-sharing-gallery', 'TPCSharingGalleryCrudController');
    Route::crud('t-p-c-sharing-video', 'TPCSharingVideoCrudController');
    Route::crud('course', 'CorseCrudController');
    Route::crud('new-release', 'NewReleaseCrudController');
    Route::crud('new-announce', 'NewAnnounceCrudController');
    Route::crud('new-calendar', 'NewCalendarCrudController');
    Route::crud('new-event', 'NewEventCrudController');
    Route::crud('tag', 'TagCrudController');
    Route::crud('user-position', 'UserPositionCrudController');
    Route::crud('account', 'UserCrudController');
    Route::crud('member-contact-center', 'MemberContactCenterCrudController');
    Route::crud('elite-personal-liaison', 'ElitePersonalLiaisonCrudController');
    Route::crud('elite-personal-assistant', 'ElitePersonalAssistantCrudController');
    Route::crud('vendor', 'VendorCrudController');
    Route::crud('government-relation', 'GovernmentRelationCrudController');
    Route::crud('course-file', 'CourseFileCrudController');
    Route::crud('course-lesson', 'CourseLessonCrudController');

    Route::get('course-file/get-course-by-ajax', 'CourseFileCrudController@getCourseByIdAjax')->name('course-file.get-by-ajax');
    Route::get('course-lesson/get-course-by-ajax', 'CourseLessonCrudController@getCourseByIdAjax')->name('course-lesson.get-by-ajax');
    Route::crud('o-chart-main', 'OChartMainCrudController');
    Route::crud('o-chart-second', 'OChartSecondCrudController');
    Route::crud('o-chart-third', 'OChartThirdCrudController');

    Route::crud('phone-directory', 'PhoneDirectoryCrudController');
    Route::post('phone-directory/export', ['uses' => 'PhoneDirectoryCrudController@export'])->name('backend_phonedirect_export');
    Route::post('phone-directory/export-csv', ['uses' => 'PhoneDirectoryCrudController@exportCSV']);
    Route::post('phone-directory/export-example', ['uses' => 'PhoneDirectoryCrudController@exportExample']);
    Route::post('phone-directory/import', ['uses' => 'PhoneDirectoryCrudController@import']);
    Route::post('phone-directory/delete', ['uses' => 'PhoneDirectoryCrudController@destroyData'])->name('backend_phonedirect_delete');

    // Route::crud('config-static-page', 'ConfigStaticPageCrudController');
    Route::crud('static-about-us-page', 'StaticAboutUsPageCrudController');
    Route::crud('config-theme-color', 'ConfigThemeColorCrudController');
    Route::crud('your-job-file', 'YourJobKnowledgeFileCrudController');

    Route::crud('core-config-menu', 'ConfigMenuCrudController');
    Route::crud('core-config-big-banner', 'ConfigBigBannerCrudController');
    // Route::crud('file-manager', 'ConfigThemeColorCrudController');
    Route::crud('core-config-service-link', 'ConfigServiceLinkCrudController');
    Route::crud('core-config-icon-link', 'ConfigIconLinkCrudController');

    Route::get('post-example/{id}', 'PostCrudController@showExamplePost')->name('admin.post.example-by-id');
    Route::get('course-example/{id}', 'PostCrudController@showExampleCourse')->name('admin.course.example-by-id');

    Route::crud('visitor-log', 'VisitorLogCrudController');
    Route::post('visitor-log/export', ['uses' => 'VisitorLogCrudController@export']);
    Route::post('visitor-log/export-csv', ['uses' => 'VisitorLogCrudController@exportCSV']);
    Route::crud('visitor-log-admin', 'VisitorLogAdminCrudController');
    Route::post('visitor-log-admin/export', ['uses' => 'VisitorLogAdminCrudController@export']);
    Route::post('visitor-log-admin/export-csv', ['uses' => 'VisitorLogAdminCrudController@exportCSV']);
    Route::get('charts/lines/-menu-chart-controller', 'Charts\Lines/MenuChartControllerChartController@response')->name('charts.lines/-menu-chart-controller.index');

    Route::crud('api-service', 'ApiServiceCrudController');

    Route::get('file/get-upload-file-size-from-static-path', 'FileController@getUploadFileSizeFromStaticPath')->name('file.get-upload-file-size');

}); // this should be the absolute last line of this file