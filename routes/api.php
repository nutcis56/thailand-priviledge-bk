<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('store-member','App\Http\Controllers\Api\PostController@storeMember')->name('api_member_store');
Route::get('blog','App\Http\Controllers\Api\PostController@blog')->name('api_blog');
Route::get('faq','App\Http\Controllers\Api\PostController@faq');
Route::get('knowledge-list','App\Http\Controllers\Api\PostController@knowledge');
Route::get('job-list','App\Http\Controllers\Api\PostController@job');
Route::get('gallery-album','App\Http\Controllers\Api\PostController@galleryAlbum');
Route::get('gallery-album/{id}','App\Http\Controllers\Api\PostController@galleryList');
Route::get('video-album','App\Http\Controllers\Api\PostController@videoAlbum');
Route::get('announce-list','App\Http\Controllers\Api\PostController@announce');
Route::get('calendar-list','App\Http\Controllers\Api\PostController@calendar');
Route::get('event-activity','App\Http\Controllers\Api\PostController@event');


Route::get('post/{id}','App\Http\Controllers\Api\PostController@view')->name('api_post_detail');

// SYNC PYDIO TO ANOTHER SERVER
Route::prefix('sync-pydio-user-to-another-server')->group(function() {
    Route::post('send-data', [\App\Http\Controllers\AuthController::class, 'SyncPydioUserFromAnotherServer']);
});