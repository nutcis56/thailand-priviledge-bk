<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AboutUsController;
use App\Http\Controllers\ADController;
use App\Http\Controllers\WishListController;

$app_url = config("app.url");
if (!empty($app_url)) {
    URL::forceRootUrl($app_url);
    $schema = explode(':', $app_url)[0];
    URL::forceScheme($schema);
}

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Send email test
Route::get('/send-email-test', [\App\Http\Controllers\HomeController::class, 'sendEmailTest']);

Route::get('/login', [\App\Http\Controllers\AuthController::class, 'showLoginForm'])->name('frontend.auth.login');
Route::post('/login', [\App\Http\Controllers\AuthController::class, 'login'])->name('frontend.auth.loginSubmit');
Route::post('/logout', [\App\Http\Controllers\AuthController::class, 'logout'])->name('frontend.auth.logout');

Route::get('/admin/login', [\App\Http\Controllers\AuthAdminController::class, 'showLoginForm'])->name('backend.auth.login');
Route::get('/admin/logout', [\App\Http\Controllers\AuthAdminController::class, 'logout'])->name('backend.auth.logout');

Route::middleware(['language', 'basic_auth', 'test_log', 'one_device_loggedin'])->group(function(){
   
    Route::get('change-lang/{locale}', function ($locale) {
        Session::put('language', $locale);
        
        App::setLocale($locale);
        
        return Redirect::back();
    })->name('change-lang');
    
    Route::get('/', [HomeController::class, 'index'])->name('home');
    
    Route::get('/ad/test', [ADController::class, 'test'])->name('ad.test');
    
    
    Route::prefix('knowledge-list')->group(function () {
        Route::get('/',[PostController::class, 'KnowLedgeIndex'])->name("knowledgeList");
      
        Route::get('{slug}',[PostController::class, 'KnowLedgeDetail'])->name("knowledgeDetail");
          // Route::get('/{slug}',[PostController::class, 'KnowLedgeDetailSlug'])->name("KnowLedgeDetailSlug");
    });

    Route::get('my-knowledge',[PostController::class, 'MyknowLedge'])->name("MyknowLedge");
    
    
    Route::prefix('job-list')->group(function () {
        Route::get('/',[PostController::class, 'JobIndex'])->name("jobList");
        Route::get('{slug}',[PostController::class, 'JobDetail'])->name("jobDetail");
    });
    
    Route::prefix('blog')->group(function () {
        Route::get('/',[PostController::class, 'BlogIndex'])->name("blogList");
    });
    
    Route::prefix('news-list')->group(function () {
        Route::get('/',[PostController::class, 'NewsIndex'])->name("newList");
        Route::get('{slug}',[PostController::class, 'NewsDetail'])->name("newDetail");
    });
    
    Route::prefix('calendar-list')->group(function () {
        Route::get('/',[PostController::class, 'CalendarIndex'])->name("calendarList");
        Route::get('{slug}',[PostController::class, 'CalendarDetail'])->name("calendarDetail");
    });
    
    Route::prefix('course-list')->group(function () {
        Route::get('/',[PostController::class, 'CourseIndex'])->name("courseIndex");
        Route::get('{slug}',[PostController::class, 'CourseDetail'])->name("courseDetail");
        Route::get('lessons/{id}',[PostController::class, 'CourseLesson'])->name("courseLesson");
    });
    
    Route::prefix('announce-list')->group(function () {
        Route::get('/',[PostController::class, 'AnnounceIndex'])->name("announceIndex");
        Route::get('{slug}',[PostController::class, 'AnnounceDetail'])->name("announceDetail");
    });

    Route::prefix('event-activity')->group(function () {
        Route::get('/',[PostController::class, 'EventActivityIndex'])->name("eventActivityIndex");
        Route::get('{slug}',[PostController::class, 'EventActivityDetail'])->name("eventActivityDetail");
    });
    
    Route::prefix('faq')->group(function () {
        Route::get('/',[PostController::class, 'FaqIndex'])->name("faqList");
    });
    
    Route::prefix('about-us')->group(function () {
        Route::get('{name}',[AboutUsController::class, 'index'])->name("abousName");
    });
    
    Route::prefix('gallery-album')->group(function () {
        Route::get('/',[PostController::class, 'GalleryAlbumIndex'])->name("galleryAlbumIndex");
    });
    
    Route::prefix('gallery-list')->group(function () {
        Route::get('/{slug}',[PostController::class, 'GalleryListIndex'])->name("gallertListIndex");
    });
    
    Route::prefix('gallery-table')->group(function () {
        Route::get('/{slug}',[PostController::class, 'GalleryTableIndex'])->name("gallertTableIndex");
    });
    
    Route::prefix('video-album')->group(function () {
        Route::get('/',[PostController::class, 'VideoAlbumIndex'])->name("videoAlbumIndex");
    });
    
    Route::prefix('video-list')->group(function () {
        Route::get('/{slug}',[PostController::class, 'VideoListIndex'])->name("videoListIndex");
    });
    
    Route::prefix('video-table')->group(function () {
        Route::get('/{slug}',[PostController::class, 'VideoTableIndex'])->name("videoTableIndex");
    });
    
    
    Route::prefix('member')->group(function () {
        Route::get('/',[PostController::class, 'MemberIndex'])->name("memberIndex");
        Route::get('/detail/{id}', [PostController::class, 'memberDetail'])->name("memberDetail");
        Route::get('/form', [PostController::class, 'MemberForm'])->name("memberForm");
        Route::post('member/store', [PostController::class, 'memberStore'])->name('memberStore');
    });
    
    Route::prefix('relate')->group(function () {
        Route::get('/{id}',[PostController::class, 'RelateData'])->name("relateData");
    });
    
    Route::prefix('search-result')->group(function () {
        Route::get('/',[PostController::class, 'SearchResult'])->name("searchResult");
    });
    
    Route::prefix('organization')->group(function () {
        Route::get('/{id}',[PostController::class, 'Organization'])->name("organization");
    });

    Route::prefix('phone-directory')->group(function () {
        Route::get('',[PostController::class, 'PhoneDirectory'])->name("phoneDirectory");
    });

    

    Route::prefix('favorite')->group(function () {
        Route::get('',[WishListController::class, 'index'])->name("wishlistIndex");
        Route::get('{id}',[WishListController::class, 'view'])->name("wishlistDetail");
        Route::post('store', [WishListController::class, 'store'])->name("wishlistStore");
        
    });
    
    Route::prefix('like')->group(function () {
        Route::get('', [WishListController::class, 'like'])->name('wishlistLike');
        Route::get('{id}',[WishListController::class, 'view'])->name("wishlistDetail");
    });
   

    Route::prefix('tags')->group(function () {
        Route::get('',[PostController::class, 'TagIndex'])->name("tagIndex");
        Route::get('{id}',[PostController::class, 'TagDetail'])->name("tagDetail");
    });

    Route::get('/test',[PostController::class, 'test'])->name("test");

    Route::get('ajax-detail',[PostController::class, 'CalendarAjaxDetail'])->name("calendarAjaxDetail");

    Route::post('ajax-rating-course-store',[PostController::class, 'ratingCourseStore'])->name("ajaxRatingCourseStore");
    Route::post('send-mail-post',[PostController::class, 'SendMail'])->name("SendMail");

    Route::get('sitemap',[HomeController::class, 'siteMap'])->name("frontend.sitemap");
    Route::get('service-api',[HomeController::class, 'serviceApi'])->name("frontend.service_api");
});