/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100411
 Source Host           : localhost:3306
 Source Schema         : thailandprivilage

 Target Server Type    : MySQL
 Target Server Version : 100411
 File Encoding         : 65001

 Date: 02/09/2021 10:59:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส',
  `parent_id` int(11) DEFAULT NULL COMMENT 'รหัสอ้างอิง',
  `name` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ชื่อหมวดหมู่',
  `code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'โค้ดอ้างอิงหมวดหมู่',
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ลิงค์',
  `detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'รายละเอียด',
  `type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ประเภท',
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'สถานะ',
  `sort_order` int(11) DEFAULT NULL COMMENT 'จัดเรียง',
  `group` int(11) DEFAULT NULL COMMENT 'กลุ่ม',
  `level` int(11) DEFAULT NULL COMMENT 'ระดับ',
  `created_at` timestamp(0) NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่สร้าง',
  `updated_at` timestamp(0) NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่แก้ไข',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for core_configs
-- ----------------------------
DROP TABLE IF EXISTS `core_configs`;
CREATE TABLE `core_configs`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส',
  `code` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'โค้ดอ้างอิง',
  `name` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'ชื่อข้อมูล',
  `detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'รายละเอียด',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ค่าตัวแปร',
  `link` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ลิงค์',
  `target` char(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'รูปแบบการแสดงผลหน้าต่างลิงค์',
  `icon` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ไอค่อน',
  `image` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'รูปภาพ',
  `class` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ชั้นข้อมูล',
  `level` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ระดับข้อมูล',
  `group` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'กลุ่มข้อมูล',
  `category` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'หมวดหมู่ข้อมูล',
  `scope` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ขอบเขต',
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'สถานะ',
  `ref_sub_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'รหัสอ้างอิงฝ่าย',
  `ref_main_id` bigint(20) UNSIGNED DEFAULT NULL COMMENT 'รหัสอ้างอิงเขต',
  `created_at` timestamp(0) NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่สร้าง',
  `updated_at` timestamp(0) NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่แก้ไข',
  `tb_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'ตารางอ้างอิง',
  `tb_id` int(11) DEFAULT NULL COMMENT 'รหัสอ้างอิง',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for documents
-- ----------------------------
DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส',
  `doc_code` int(11) DEFAULT NULL COMMENT 'โค้ดอ้างอิง',
  `doc_type` int(11) NOT NULL COMMENT 'เอกสารประเภท',
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ชื่อเอกสาร',
  `file` text CHARACTER SET utf8 COLLATE utf8_general_ci COMMENT 'ไฟล์',
  `file_type` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ประเภทไฟล์',
  `target_link` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT 'ลิงค์',
  `status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'สถานะ',
  `detail` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'รายละเอียด',
  `excerpt` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'รายละเอียดย่อ',
  `cate_id` int(11) DEFAULT NULL COMMENT 'รหัสหมวดหมู่',
  `group` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'กลุ่ม',
  `level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ระดับ',
  `doc_tb` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ตารางอ้างอิง',
  `doc_tb_id` int(11) DEFAULT NULL COMMENT 'ข้อมูลอ้างอิง',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส',
  `post_name` char(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ชื่ออ้างอิง',
  `post_author` bigint(20) DEFAULT NULL COMMENT 'รหัสผู้สร้าง',
  `post_tags` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci COMMENT 'แท็กอ้างอิงเนื้อหา',
  `post_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'หัวข้อ',
  `post_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'รายละเอียด',
  `post_status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT 'สถานะ',
  `private_status` enum('0','1') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '0' COMMENT 'สถานะส่วนตัว',
  `post_date` datetime(0) DEFAULT NULL COMMENT 'วันที่เนื้อหา',
  `post_parent` bigint(20) DEFAULT NULL COMMENT 'เนื้อหาหลักอ้างอิง',
  `post_sort` int(11) DEFAULT NULL COMMENT 'จัดเรียง',
  `post_type` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'ประเภทข้อมูลเนื้อหา',
  `created_by` bigint(20) DEFAULT NULL COMMENT 'สร้างโดย',
  `updated_by` bigint(20) DEFAULT NULL COMMENT 'แก้ไขโดย',
  `created_at` timestamp(0) NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่สร้าง',
  `updated_at` timestamp(0) NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่แก้ไข',
  `ping_status` char(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'สถานะอนุญาตกาเข้าถึงจากแหล่งอื่น',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for post_cates
-- ----------------------------
DROP TABLE IF EXISTS `post_cates`;
CREATE TABLE `post_cates`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'รหัส',
  `post_id` int(11) NOT NULL COMMENT 'รหัสข้อมูลเนื้อหา',
  `cate_id` int(11) NOT NULL COMMENT 'รหัสหมวดหมู่',
  `created_at` timestamp(0) NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่สร้าง',
  `updated_at` timestamp(0) NOT NULL DEFAULT current_timestamp() COMMENT 'วันที่แก้ไข',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;