<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TranslatePost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // new column
        Schema::table('posts', function (Blueprint $table) {
            $table->text('post_title_temp')->nullable();
            $table->text('post_title')->nullable()->change();

            $table->longText('post_content_temp')->nullable();
            $table->longText('post_content')->nullable()->change();

            $table->mediumText('post_excerpt_temp')->nullable();
            $table->mediumText('post_excerpt')->nullable()->change();
        });

        // post_title
        DB::select(DB::raw("UPDATE posts SET post_title_temp = post_title;"));
        DB::select(DB::raw("UPDATE posts SET post_title = NULL;"));

        // post_content
        DB::select(DB::raw("UPDATE posts SET post_content_temp = post_content;"));
        DB::select(DB::raw("UPDATE posts SET post_content = NULL;"));

        // post_excerpt
        DB::select(DB::raw("UPDATE posts SET post_excerpt_temp = post_excerpt;"));
        DB::select(DB::raw("UPDATE posts SET post_excerpt = NULL;"));

        // change column
        Schema::table('posts', function (Blueprint $table) {
            $table->json('post_title')->nullable()->change();
            $table->json('post_content')->nullable()->change();
            $table->json('post_excerpt')->nullable()->change();
        });

        foreach (\App\Models\Post::get() as $key => $value) {
            $value->setTranslation('post_title', 'th_TH', $value->post_title_temp);
            $value->setTranslation('post_content', 'th_TH', $value->post_content_temp);
            $value->setTranslation('post_excerpt', 'th_TH', $value->post_excerpt_temp);
            $value->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
