<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->unsigned();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('description')->nullable();
            $table->string('physical_delivery_office_name')->nullable();
            $table->string('telephone_number')->nullable();
            $table->string('department')->nullable();
            $table->string('company')->nullable();
            $table->string('sam_accountname')->nullable();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('source')->nullable(); // 'ad', 'local'
            $table->string('status')->nullable(); // 'active', 'inactive', 'suspend'
        });

        foreach (\App\Models\User::get() as $key => $value) {
            $value->source = 'local';
            $value->status = 'active';
            $value->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropIfExists('user_infos');
    }
}
