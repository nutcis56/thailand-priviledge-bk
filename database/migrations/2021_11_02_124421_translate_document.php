<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TranslateDocument extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // new column
        Schema::table('documents', function (Blueprint $table) {
            $table->text('name_temp')->nullable();
            $table->text('name')->nullable()->change();

            $table->text('detail_temp')->nullable();
            $table->text('detail')->nullable()->change();

            $table->text('excerpt_temp')->nullable();
            $table->text('excerpt')->nullable()->change();
        });

        // name
        DB::select(DB::raw("UPDATE documents SET name_temp = name;"));
        DB::select(DB::raw("UPDATE documents SET name = NULL;"));

        // detail
        DB::select(DB::raw("UPDATE documents SET detail_temp = detail;"));
        DB::select(DB::raw("UPDATE documents SET detail = NULL;"));

        // excerpt
        DB::select(DB::raw("UPDATE documents SET excerpt_temp = excerpt;"));
        DB::select(DB::raw("UPDATE documents SET excerpt = NULL;"));

        // change column
        Schema::table('documents', function (Blueprint $table) {
            $table->json('name')->nullable()->change();
            $table->json('detail')->nullable()->change();
            $table->json('excerpt')->nullable()->change();
        });

        foreach (\App\Models\Document::get() as $key => $value) {
            $value->setTranslation('name', 'th_TH', $value->name_temp);
            $value->setTranslation('detail', 'th_TH', $value->detail_temp);
            $value->setTranslation('excerpt', 'th_TH', $value->excerpt_temp);
            $value->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
