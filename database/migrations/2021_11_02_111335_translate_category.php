<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class TranslateCategory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // new column
        Schema::table('categories', function (Blueprint $table) {
            $table->text('name_temp')->nullable();
        });

        // name
        DB::select(DB::raw("UPDATE categories SET name_temp = name;"));
        DB::select(DB::raw("UPDATE categories SET name = NULL;"));

        // change column
        Schema::table('categories', function (Blueprint $table) {
            $table->json('name')->nullable()->change();
        });

        foreach (\App\Models\Category::get() as $key => $value) {
            $value->setTranslation('name', 'th_TH', $value->name_temp);
            $value->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
