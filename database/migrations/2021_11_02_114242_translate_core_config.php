<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class TranslateCoreConfig extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // new column
        Schema::table('core_configs', function (Blueprint $table) {
            $table->text('name_temp')->nullable();
            $table->text('name')->nullable()->change();

            $table->text('detail_temp')->nullable();
            $table->text('detail')->nullable()->change();
        });

        // name
        DB::select(DB::raw("UPDATE core_configs SET name_temp = name;"));
        DB::select(DB::raw("UPDATE core_configs SET name = NULL;"));

        // detail
        DB::select(DB::raw("UPDATE core_configs SET detail_temp = detail;"));
        DB::select(DB::raw("UPDATE core_configs SET detail = NULL;"));

        // change column
        Schema::table('core_configs', function (Blueprint $table) {
            $table->json('name')->nullable()->change();
            $table->json('detail')->nullable()->change();
        });

        foreach (\App\Models\CoreConfig::get() as $key => $value) {
            $value->setTranslation('name', 'th_TH', $value->name_temp);
            $value->setTranslation('detail', 'th_TH', $value->detail_temp);
            $value->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
