<?php

namespace App\Service;

class MemberMenuService
{
    public static function getLinkMemberService()
    {
        $coreConfigRepo = app()->make('App\Repository\CoreConfigRepository');

        return $coreConfigRepo->listGroupAndCategory('member_service','service_link_system');
    }
}