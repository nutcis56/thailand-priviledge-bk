<?php 

namespace App\Service;

class KnowledgeCategoryService
{
    public static function knowledgeCategory()
    {
        $categoryRepo = app()->make("\App\Repository\CategoryRepository");
        return $categoryRepo->findName('knowledge');
    }
}