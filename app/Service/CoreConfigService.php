<?php 

namespace App\Service;


class CoreConfigService
{
    public static function theme()
    {
        $repo = app()->make("App\Repository\CoreConfigRepository");
        $config = $repo->listGroupAndCategory('template', 'website');
        $theme = 'default';

        if(count($config) > 0) {
            $theme = $config[0]->code;
        }

        return $theme;
    }

    public static function menuOrganizeStruture()
    {
        $repo = app()->make("App\Repository\CoreConfigRepository");
        $menu = $repo->listGroupAndCategory('office', 'main');

        return $menu;
    }


    public static function homePageTopLogo()
    {
        $repo = app()->make("App\Repository\CoreConfigRepository");
        $logo = $repo->listGroupAndCategoryHomePage('homepage', 'top_logo');

        return $logo;
    }

    public static function getTopMenu()
    {
        $repo = app()->make("App\Repository\CoreConfigRepository");
        
        return $repo->getTopMenu();
       
    }

    public static function getFooter()
    {
        $repo = app()->make("App\Repository\CoreConfigRepository");
        return $repo->getFooter();
    }

    public static function department($sectionId)
    {
        $repo = app()->make("App\Repository\CoreConfigRepository");

        return $repo->getDepartment($sectionId);
    }

    public static function getSection()
    {
        $repo = app()->make("App\Repository\CoreConfigRepository");
        $data = $repo->listGroupAndCategory('office','section');

        return $data;
    }

    public static function getTag()
    {
        $repo = app()->make("App\Repository\CoreConfigRepository");
        $data = $repo->listGroupAndCategory('tag','tag');

        return $data;
    }


    public static function getFormatDateThai($date) {
        $monthThai = [
            '01' => 'มกราคม',
            '02' => 'กุมภาพันธ์',
            '03' => 'มีนาคม',
            '04' => 'เมษายน',
            '05' => 'พฤษภาคม',
            '06' => 'มิถุนายน',
            '07' => 'กรกฎาคม',
            '08' => 'สิงหาคม',
            '09' => 'กันยายน',
            '10' => 'ตุลาคม',
            '11' => 'พฤศจิกายน',
            '12' => 'ธันวาคม',
        ];

        $dateExplode = explode('-', $date);
        return "{$dateExplode[2]} {$monthThai[$dateExplode[1]]} {$dateExplode[0]}";
    }
}