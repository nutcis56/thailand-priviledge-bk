<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class VideoFormat implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $allow_formats = ['mp4', 'webm', 'ogv', 'mov'];
        $info = pathinfo($value);
        if (is_array($info)) {
            if (isset($info['extension']) && !in_array($info['extension'], $allow_formats)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'รองรับไฟล์วีดีโอเท่านั้น (mp4, webm, ogv, mov)';
    }
}
