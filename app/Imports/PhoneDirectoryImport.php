<?php

namespace App\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\PhoneDirectory;
use App\Models\CoreConfig;

class PhoneDirectoryImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        
        if(count($collection) > 1) {
            //* delete header file (FIELD)
            unset($collection[0]);
            foreach($collection as $data) {
                $model = new CoreConfig();
                $lastedData = $model->latest('id')->first();
                $lastedIdPush = $lastedData->id + 1;
                $model->code = "phone_directory_{$lastedIdPush}";
                $model->name = $data[0];
                $setValue = [
                    'name' => $data[0],
                    'mobile' => $data[2],
                    'phone1' => $data[3],
                    'phone2' => $data[4],
                    'email' => $data[1],
                ];
                $model->value = \json_encode($setValue, JSON_UNESCAPED_UNICODE);
                $model->detail = "สมุดโทรศัพท์ภายในสำนักงาน";
                $model->class = $data[6];
                $model->level = 1;
                $model->group ="phone_directory";
                $model->category = $data[7];
                $model->status = "1";
                $model->save();
            } 
        }
    }
}
