<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncUserFromAD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:ad';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync user from Active Directory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $position = \App\Models\UserPosition::where('name', 'like', "%แผนกเทคโนโลยีสารสนเทศ%")->first();
        // $users = \Adldap\Laravel\Facades\Adldap::search()->users()->get();
        $users = \Adldap\Laravel\Facades\Adldap::search()->find('km');
        
        $lists = [];
        foreach ($users as $userData) {
            $data = [
                'name' => $userData->cn && count($userData->cn) ? $userData->cn[0] : NULL,
                'first_name' => $userData->givenname && count($userData->givenname) ? $userData->givenname[0] : NULL,
                'last_name' => $userData->sn && count($userData->sn) ? $userData->sn[0] : NULL,
                'description' => $userData->description && count($userData->description) ? $userData->description[0] : NULL,
                'physical_delivery_office_name' => $userData->physicaldeliveryofficename && count($userData->physicaldeliveryofficename) ? $userData->physicaldeliveryofficename[0] : NULL,
                'telephone_number' => $userData->telephonenumber && count($userData->telephonenumber) ? $userData->telephonenumber[0] : NULL,
                'department' => $userData->department && count($userData->department) ? $userData->department[0] : NULL,
                'company' => $userData->company && count($userData->company) ? $userData->company[0] : NULL,
                'email' => $userData->userprincipalname && count($userData->userprincipalname) ? $userData->userprincipalname[0] : NULL,
                'sam_accountname' => $userData->samaccountname && count($userData->samaccountname) ? $userData->samaccountname[0] : NULL
            ];

            array_push($lists, $data);

            if ($data['email'] && $data['name']) {
                $user = \App\Models\User::where('email', $data['email'])->first();
                if (!$user) {
                    $user = new \App\Models\User;
                    $user->name = $data['name'];
                    $user->email = $data['email'];
                    $user->password = 'syncFromAdWithNoPassword';
                    $user->status = 'inActive';
                    $user->source = 'ad';

                    if ($position) {
                        $user->position_id = $position->id;
                    }

                    $user->save();

                    $userInfo = new \App\Models\UserInfo($data);
                    $user->userInfo()->save($userInfo);
                } else {
                    $user->userInfo()->update($data);
                }
            }
        }
    }
}
