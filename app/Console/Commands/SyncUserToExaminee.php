<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncUserToExaminee extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:user:examinee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return false;
        $main_users = \App\Models\User::all();
        $book_stack_users = \App\Models\UserExaminee::all();
        $book_stack_users_email_only = \App\Models\UserExaminee::pluck('email')->toArray();
        
        $users = [

        ];

        foreach ($main_users as $main_user) {
            \Spatie\Sluggable\SlugOptions::create()->generateSlugsFrom('name');
            $temp = [
                'name' => $main_user->name,
                'email' => $main_user->email,
                'password' => $main_user->password,
            ];
            array_push($users, $temp);

            if (in_array($main_user->email, $book_stack_users_email_only)) {
                // exists
                \App\Models\UserExaminee::where('email', $main_user->email)->update($temp);
            } else {
                // not exists
                \App\Models\UserExaminee::create($temp);
            }
        }


        // dd($users);
        // \App\Models\UserExaminee::upsert(
        //     $users, 
        //     ['email']
        // );
    }
}
