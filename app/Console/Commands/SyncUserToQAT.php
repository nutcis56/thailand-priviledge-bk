<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncUserToQAT extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:user:qat';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        return false;
        $main_users = \App\Models\User::all();
        $book_stack_users = \App\Models\UserQAT::all();
        $book_stack_users_email_only = \App\Models\UserQAT::pluck('email')->toArray();
        
        $users = [

        ];

        foreach ($main_users as $main_user) {
            \Spatie\Sluggable\SlugOptions::create()->generateSlugsFrom('name');
            $email = $main_user->email;
            $email_e = explode('@', $user->email);
            $password = strtoupper($email_e[0]);
            $temp = [
                'username' => $email_e[0],
                'password' => $password,
                'email' => $main_user->email,
                'full_name' => $email_e[0],
                'account_type_id' => 2,
                'group_ids' => 1
            ];
            array_push($users, $temp);

            if (in_array($main_user->email, $book_stack_users_email_only)) {
                // exists
                \App\Models\UserQAT::where('email', $main_user->email)->update($temp);
            } else {
                // not exists
                \App\Models\UserQAT::create($temp);
            }
        }


        // dd($users);
        // \App\Models\UserQAT::upsert(
        //     $users, 
        //     ['email']
        // );
    }
}
