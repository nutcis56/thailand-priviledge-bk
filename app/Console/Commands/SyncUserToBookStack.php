<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncUserToBookStack extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:user:book-stack';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $main_users = \App\Models\User::all();
        $book_stack_users = \App\Models\UserBookStack::all();
        $book_stack_users_email_only = \App\Models\UserBookStack::pluck('email')->toArray();
        
        $users = [

        ];

        foreach ($main_users as $main_user) {
            \Spatie\Sluggable\SlugOptions::create()->generateSlugsFrom('name');
            $temp = [
                'name' => $main_user->name,
                'email' => $main_user->email,
                'password' => $main_user->password,
                'email_confirmed' => 1,
                'external_auth_id' => '',
            ];
            array_push($users, $temp);

            if (in_array($main_user->email, $book_stack_users_email_only)) {
                // exists
                \App\Models\UserBookStack::where('email', $main_user->email)->update($temp);
            } else {
                // not exists
                \App\Models\UserBookStack::create($temp);
            }
        }


        // dd($users);
        // \App\Models\UserBookStack::upsert(
        //     $users, 
        //     ['email']
        // );
    }
}
