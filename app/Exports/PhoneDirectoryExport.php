<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use App\Models\PhoneDirectory;

class PhoneDirectoryExport implements FromView
{

    public function view(): View
    {
        $data['lists'] = PhoneDirectory::all();

        return view('exports.phone_directory', $data);
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
    }
}
