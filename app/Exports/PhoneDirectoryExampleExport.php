<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class PhoneDirectoryExampleExport implements FromView
{

    public function view(): View
    {
        return view('exports.example_phone_directory');
    }



    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //
    }
}
