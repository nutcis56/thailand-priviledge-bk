<?php

namespace App\Exports;

use App\Models\VisitorLog;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class VisitorLogExport implements FromQuery, WithHeadings
{
    use Exportable;
    
    public $select = [
        'id',
        'request',
        'url',
        'referer',
        'languages',
        'useragent',
        'headers',
        'device',
        'platform',
        'browser',
        'ip',
        'created_at',
        'updated_at',
        // 'scope',
        // 'action',
        // 'info'
    ];

    public function query()
    {
        $request = request();
        $logs =  VisitorLog::query()->select($this->select);

        if (isset($request->date_range) && $request->date_range) {
            $date_range = json_decode(urldecode($request->date_range));
            if (isset($date_range->from) && isset($date_range->to)) {
                $logs = $logs->where('created_at', '>=', $date_range->from)->where('created_at', '<=', $date_range->to);
            }
        }

        return $logs;
    }

    public function headings(): array
    {
        return $this->select;
    }
}
