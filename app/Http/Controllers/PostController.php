<?php

namespace App\Http\Controllers;

use Auth;
use stdClass;
use App\Models\PostCate;
use App\Models\CoreConfig;
use Illuminate\Http\Request;
use App\Repository\PostRepository;
use App\Repository\CategoryRepository;
use App\Repository\PostCateRepository;
use App\Repository\WishListRepository;
use App\Repository\CoreConfigRepository;
use App\Repository\RelateDataRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\AboutUsController;
use App\Http\Requests\Frontend\MemberContactCenterRequestRequest;
use App\Repository\RatingRepository;
use Illuminate\Support\Facades\Mail;
use App\Mail\Post;

class PostController extends Controller
{
    private $categoryRepository;
    private $postRepository;
    private $postCateRepository;
    protected $relateDataRepository;
    protected $coreConfigRepository;
    protected $objCategory;
    protected $wishListRepository;
    protected $ratetingRepository;

    function __construct(
        CategoryRepository $categoryRepository,
        PostRepository $postRepository,
        PostCateRepository $postCateRepository,
        RelateDataRepository $relateDataRepository,
        CoreConfigRepository $coreConfigRepository,
        WishListRepository $wishListRepository,
        RatingRepository $ratetingRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
        $this->postCateRepository = $postCateRepository;
        $this->relateDataRepository = $relateDataRepository;
        $this->coreConfigRepository = $coreConfigRepository;
        $this->wishListRepository = $wishListRepository;
        $this->ratetingRepository = $ratetingRepository;

        $objCategory = new stdClass();
        $this->objCategory = $objCategory;
        $this->objCategory->id = 0;
    }

    /*
    |--------------------------------------------------------------------------
    | KNOWLEDGE
    |--------------------------------------------------------------------------
    */
    public function KnowLedgeIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('knowledge');
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordCategory'] = '';

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateKnowledgeId = array_column($postCateId, "post_id");
        $data['keywordTag'] = '';
        $data["keyword"] = '';
        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $data["keyword"] = $search;

            if($request->searchTag != "") {
                $data['keywordTag'] = $request->searchTag;
                if($request->category != '') {
                    $data["keywordCategory"] = $request->category;
                    $data['lists'] = $this->postRepository->searchTag($postCateKnowledgeId, $request->searchTag, $request->category);
                }else {
                    $data['lists'] = $this->postRepository->searchTag($postCateKnowledgeId, $request->searchTag);
                }
            }else {
                if($request->category != '') {
                    $data['keywordCategory'] = $request->category;
                    $data["lists"] = $this->postRepository->listCategorySearchAndCategory($postCateKnowledgeId, $search, $request->category);
                }else {
                    $data["lists"] = $this->postRepository->listCategorySearch($postCateKnowledgeId, $search);
                }
            }
        }else {
            $data["lists"] = $this->postRepository->listCategory($postCateKnowledgeId);
        }
       
        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'knowledge');

        return view('knowledge', $data);
    }

    

    /*
    |--------------------------------------------------------------------------
    | MyknowLedge
    |--------------------------------------------------------------------------
    */
    public function MyknowLedge(Request $request)
    {
        $category = $this->categoryRepository->findName('knowledge');
       
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordCategory'] = '';
        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateKnowledgeId = array_column($postCateId, "post_id");
        $data['keywordTag'] = '';
        $data["keyword"] = '';
        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            if($request->category != '') {
                $postCateId = PostCate::where("cate_id", $request->category)->get()->toArray();
                $postCateKnowledgeId = array_column($postCateId, "post_id");
                $data["lists"] = $this->postRepository->listCategorySearchMyknowledge($postCateKnowledgeId, $search);
            }else {
                $data["lists"] = $this->postRepository->listCategorySearchMyknowledge($postCateKnowledgeId, $search);
            }
            $data["keyword"] = $search;
        }else {
            $data["lists"] = $this->postRepository->listCategoryMyknowledge($postCateKnowledgeId);
        }

       
        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'knowledge');

        return view('knowledge', $data);
    }

    public function KnowLedgeDetail($slug)
    {
        $data["lists"] = $this->postRepository->findPostName($slug);
       
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordCategory'] = '';
        $data["keywordTag"] = '';

       
        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'knowledge');

        return view('knowledge_detail', $data);
    }


    public function KnowLedgeDetailSlug($slug)
    {
       
        $data["lists"] = $this->postRepository->findPostName($slug);
       
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordCategory'] = '';
        $data["keywordTag"] = '';

       
        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'knowledge');

        return view('knowledge_detail', $data);
    }
    


   /*
    |--------------------------------------------------------------------------
    | JOBLIST
    |--------------------------------------------------------------------------
    */
    public function JobIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('job');

        $user = \Auth::guard('backpack')->user();
        $sectionInfo = $this->coreConfigRepository->list($user->position_id);
        $departmentId = (!empty($sectionInfo)) ? $sectionInfo->parent_id : 0;
       
        $data['section'] = $this->coreConfigRepository->getSectionInDepartmentId($departmentId);
        
        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateKnowledgeId = array_column($postCateId, "post_id");

        $data['keywordTag'] = '';
        $data["keyword"] = "";
        $data['keywordCategory'] = '';
        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $data["keyword"] = $search;
            $data['keywordTag'] = $request->searchTag;
            
            if($request->searchTag != "") {
                if($request->category != '') {
                    $data["keywordCategory"] = $request->category;
                    $data['lists'] = $this->postRepository->searchTagJob($postCateKnowledgeId, $request->searchTag, $request->category);
                }else {
                    $data['lists'] = $this->postRepository->searchTagJob($postCateKnowledgeId, $request->searchTag);
                }
            }else {
                if($request->category != '') {
                    $data['keywordCategory'] = $request->category;
                    $data["lists"] = $this->postRepository->listCategorySearchAndCategory($postCateKnowledgeId, $search, $request->category);
                }else { 
                    $data["lists"] = $this->postRepository->listCategorySearch($postCateKnowledgeId, $search, 'job');
                }
            }


            
        }else {
            $data["lists"] = $this->postRepository->listCategory($postCateKnowledgeId, 'job');
            
        }
        
        $data["listFiles"] = $this->postRepository->listCategory($postCateKnowledgeId, 'jobFile');

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'job');
        
        return view('job', $data);
    }

    public function JobDetail($slug)
    {
        $data["lists"] = $this->postRepository->findPostName($slug);
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;

        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordCategory'] = '';
        $data['keywordTag'] = '';
        
        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'job');

        return view('job_detail', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | BLOGLIST
    |--------------------------------------------------------------------------
    */
    public function BlogIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('blog');

        $data['section'] = $this->categoryRepository->listType('blog');

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateBlogId = array_column($postCateId, "post_id");
        
        
        $data['keyword'] = '';
        $data['keywordCategory'] = '';
        if($request->search || $request->search == null) {
           
            $data['keyword'] = $request->search;
            if($request->category != '') {
                $data['keywordCategory'] = $request->category;
                $postCateBlogId = PostCate::where("cate_id", $request->category)->get()->toArray();
                $postCateBlogId = array_column($postCateBlogId, "post_id");
                $data["lists"] = $this->postRepository->listCategorySearchAndCategory($postCateBlogId, $request->search, $request->category, 'blog');
            }else {
                $data["lists"] = $this->postRepository->listCategorySearch($postCateBlogId, $request->search, 'blog');
            }
           
        }else {
            
            $data["lists"] = $this->postRepository->listCategory($postCateBlogId);
        }
        
        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'blog');

        return view('blog', $data);
    }


    /*
    |--------------------------------------------------------------------------
    | NEWS
    |--------------------------------------------------------------------------
    */
    public function NewsIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('news');
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateNewsId = array_column($postCateId, "post_id");
        
        $data["keyword"] = "";
        $data['keywordCategory'] = '';
        $data["keywordTag"] = '';
        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $data["keyword"] = $search;
            $data['keywordTag'] = $request->searchTag;

            if($request->searchTag != "") {
                if($request->category != '') {
                    $data["keywordCategory"] = $request->category;
                    $data['lists'] = $this->postRepository->searchTag($postCateNewsId, $request->searchTag, $request->category);
                }else {
                    $data['lists'] = $this->postRepository->searchTag($postCateNewsId, $request->searchTag);
                }
            }else {

                if($request->category != '') {
                    $data['keywordCategory'] = $request->category;
                    $data["lists"] = $this->postRepository->listCategorySearchAndCategory($postCateNewsId, $search, $request->category);
                }else {
                    $data["lists"] = $this->postRepository->listCategorySearch($postCateNewsId, $search);
                }
            }

            
        }else {
            $data["lists"] = $this->postRepository->listCategory($postCateNewsId);
        }

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'new');

        return view('new', $data);
    }


    public function NewsDetail($slug)
    {
        $data["lists"] = $this->postRepository->findPostName($slug);
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;

        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordCategory'] = '';
        $data["keywordTag"] = '';

        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'new');

        return view('new_detail', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | CALENDAR
    |--------------------------------------------------------------------------
    */
    public function CalendarIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('calendar');

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateCalendarId = array_column($postCateId, "post_id");
        $data["keywordSearch"] = "";
        $data["keywordCategory"] = "";
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordTag'] = '';

        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $dataArry = $this->postRepository->listArraySearch($postCateCalendarId, $search);
            $data["keywordSearch"] = $search;
            $data['keywordCategory'] = $request->category;
            $data['keywordTag'] = $request->searchTag;

            if($request->searchTag != "") {
                if($request->category != '') {
                    $data['lists'] = $this->postRepository->searchTag($postCateCalendarId, $request->searchTag, $request->category);
                }else {
                    $data['lists'] = $this->postRepository->searchTag($postCateCalendarId, $request->searchTag);
                }
            }else {
                if($request->category != '') {
                   
                    $data["lists"] = $this->postRepository->listCategorySearchAndCategory($postCateCalendarId, $search, $request->category);
                }else {
                    $data["lists"] = $this->postRepository->listCategorySearch($postCateCalendarId, $search);
                }
            }
        }else {
            $dataArry = $this->postRepository->listArray($postCateCalendarId);
            $data["lists"] = $this->postRepository->listCategory($postCateCalendarId);
        }
       
        $dateMarkerCalendar = [];
        if(!empty($dataArry)) {
            foreach($dataArry as $key => $val) {
                
                $strtotimeDate = strtotime($val['start_date']);
                $dateStart = date('Y-m-d',$strtotimeDate);
                array_push($dateMarkerCalendar, $dateStart);
            }
        }
        
        $data['dateMarkerCalendar'] = $dateMarkerCalendar;

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'calendar');

        return view('calendar', $data);
    }

    public function CalendarDetail($slug)
    {
        $data["lists"] = $this->postRepository->findPostName($slug);
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;

        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data["keywordCategory"] = "";
        $data['keywordTag'] = '';

        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'calendar');

        return view('calendar_detail', $data);
    }

    public function CalendarAjaxDetail(Request $request)
    {
        $category = $this->categoryRepository->findName('calendar');

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateCalendarId = array_column($postCateId, "post_id");

        $data["lists"] = $this->postRepository->listCalendarEvent($postCateCalendarId, $request->data);
        $formatDate = getFormatDateThai($request->data);
        $data['dateEvent'] = $formatDate;
        $view = view('elements.calendar.event', $data);
        $viewRender = $view->render();

        return response()->json(['status' => 'success', 'data' => $viewRender]);
    }
    

     /*
    |--------------------------------------------------------------------------
    | COURSE
    |--------------------------------------------------------------------------
    */
    public function CourseIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('course');
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateCourseId = array_column($postCateId, "post_id");
       
        $data["keyword"] = "";
        $data['keywordCategory'] = '';
        $data["keywordTag"] = '';
        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $data["keyword"] = $search;
            $data['keywordTag'] = $request->searchTag;

            if($request->searchTag != "") {
                if($request->category != '') {
                    $data["keywordCategory"] = $request->category;
                    $data['lists'] = $this->postRepository->searchTag($postCateCourseId, $request->searchTag, $request->category);
                }else {
                    $data['lists'] = $this->postRepository->searchTag($postCateCourseId, $request->searchTag);
                }
            }else  {
                if($request->category != '') {
                    $data['keywordCategory'] = $request->category;
                    $data["lists"] = $this->postRepository->listCategorySearchAndCategory($postCateCourseId, $search, $request->category);
                }else {
                    $data["lists"] = $this->postRepository->listCategorySearch($postCateCourseId, $search);
                }
            }


            
        }else {
            $data["lists"] = $this->postRepository->listCategory($postCateCourseId);
        }

        $data["recommends"] = $this->postRepository->listCourseRecommend($postCateCourseId);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'course');

        return view('course', $data);
    }


    public function CourseDetail($slug)
    {
        if(is_numeric($slug)) {
            $data["lists"] = \App\Models\Course::where('id', $slug)->with('courseLessons')->with('courseFiles')->first();
        }else {
            $data["lists"] = \App\Models\Course::where('post_name', $slug)->with('courseLessons')->with('courseFiles')->first();
        }
       
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data["keyword"] = "";
        $data['keywordCategory'] = '';

        $data['statusRating'] = $this->getStatusRating($id);


        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'course');

        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        return view('course_detail', $data);
    }

    public function CourseLesson($id)
    {
        $data["lists"] = \App\Models\Document::where('id', $id)->first();
        $data["courseInfo"] = \App\Models\Course::where('id', $data['lists']->doc_tb_id)->first();
       
        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'course');

        return view('course_lesson', $data);
    }


    /*
    |--------------------------------------------------------------------------
    | ANNOUNCE
    |--------------------------------------------------------------------------
    */
    public function AnnounceIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('announce');

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateCourseId = array_column($postCateId, "post_id");
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');

        $data["keywordSearch"] = "";
        $data["keywordCategory"] = "";
        $data['keywordTag'] = '';

        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $data["keywordCategory"] = $request->category;
            $data['keywordTag'] = $request->searchTag;
            if($request->searchTag != "") {
                if($request->category != '') {
                    $data['lists'] = $this->postRepository->searchTag($postCateCourseId, $request->searchTag, $request->category);
                }else {
                    $data['lists'] = $this->postRepository->searchTag($postCateCourseId, $request->searchTag);
                }
            }else {
                if($request->category != '') {
                    $data["keywordSearch"] = $search;
                    $data["lists"] = $this->postRepository->listCategorySearchAndCategory($postCateCourseId, $search, $request->category, 'announce');
                }else {
                    $data["lists"] = $this->postRepository->listCategorySearch($postCateCourseId, $search, 'announce');
                }
            }
        }else {
            $data["lists"] = $this->postRepository->listCategory($postCateCourseId, 'announce');
        }
       
        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'announce');

        return view('announce', $data);
    }


    public function AnnounceDetail($slug)
    {
        $data["lists"] = $this->postRepository->findPostName($slug);
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data["keywordCategory"] = "";
        $data['keywordTag'] = '';

        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'announce');

        return view('announce_detail', $data);
    }



    /*
    |--------------------------------------------------------------------------
    | FAQ
    |--------------------------------------------------------------------------
    */
    public function FaqIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('faq');

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateFAQId = array_column($postCateId, "post_id");
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordCategory'] = '';
        $data['keyword'] = '';
        if(isset($_REQUEST['search'])) {
            if($request->category != "") {
                $data["lists"] = $this->postRepository->listFAQSearch($postCateFAQId, $request->search, $request->category);
            }else {
                $data["lists"] = $this->postRepository->listFAQSearch($postCateFAQId, $request->search);
            }
           
            $data['keyword'] = $request->search;
            $data['keywordCategory'] = $request->category;
            
        }else {
            $data["lists"] = $this->postRepository->listFAQ($postCateFAQId);
        }
            
        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'faq');

        return view('faq', $data);
    }



    /*
    |--------------------------------------------------------------------------
    | GALLERY ALBUM
    |--------------------------------------------------------------------------
    */
    public function GalleryAlbumIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('gallery');

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateFAQId = array_column($postCateId, "post_id");
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');


        $data["keyword"] = "";
        $data['keywordCategory'] = '';
        $data['keywordTag'] = "";

        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $data["keyword"] = $search;


            if($request->searchTag != "") {
                $data['keywordTag'] = $request->searchTag;
                if($request->category != '') {
                    $data["keywordCategory"] = $request->category;
                    $data['lists'] = $this->postRepository->searchTag($postCateFAQId, $request->searchTag, $request->category);
                }else {
                    $data['lists'] = $this->postRepository->searchTag($postCateFAQId, $request->searchTag);
                }
            }else {
                if($request->category != '') {
                    $data['keywordCategory'] = $request->category;
                    $data["lists"] = $this->postRepository->listCategorySearchAndCategory($postCateFAQId, $search, $request->category);
                }else {
                    $data["lists"] = $this->postRepository->listCategorySearch($postCateFAQId, $search);
                }
            }


           
        }else {
            $data["lists"] = $this->postRepository->listCategory($postCateFAQId);
        }


        if(!empty($data['lists'])) {
            foreach($data['lists'] as $value) {
                $statusFavorite = $this->getFavoriteAndLike($value->id);
                $value->statusFavorite = ($statusFavorite['favorite_status'] == 'active') ? 'active' : '';
            }
        }

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'tpc_sharing_gallery');

        return view('gallery_album', $data);
    }
    
    /*
    |--------------------------------------------------------------------------
    | GALLERY LIST
    |--------------------------------------------------------------------------
    */
    public function GalleryListIndex($slug)
    {
        $data["lists"] = $this->postRepository->findPostName($slug);
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;
        $data['keywordCategory'] = '';
        
         //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'tpc_sharing_gallery');

        return view('gallery_list', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | GALLERY LIST
    |--------------------------------------------------------------------------
    */
    public function GalleryTableIndex($slug)
    {
        $data["lists"] = $this->postRepository->findPostName($slug);
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordCategory'] = '';

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'tpc_sharing_gallery');

        return view('gallery_table', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | VIDEO ALBUM
    |--------------------------------------------------------------------------
    */
    public function VideoAlbumIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('vdo');

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateFAQId = array_column($postCateId, "post_id");
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');

        $data["keyword"] = "";
        $data['keywordCategory'] = '';

        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $data["keyword"] = $search;
            if($request->category != '') {
                $data['keywordCategory'] = $request->category;
                $data["lists"] = $this->postRepository->listCategorySearchAndCategory($postCateFAQId, $search, $request->category);
            }else {
                $data["lists"] = $this->postRepository->listCategorySearch($postCateFAQId, $search);
            }
        }else {
            $data["lists"] = $this->postRepository->listCategory($postCateFAQId);
        }

        if(!empty($data['lists'])) {
            foreach($data['lists'] as $value) {
                $statusFavorite = $this->getFavoriteAndLike($value->id);
                $value->statusFavorite = ($statusFavorite['favorite_status'] == 'active') ? 'active' : '';
            }
        }

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'tpc_sharing_video');

        return view('video_album', $data);
    }
    
    /*
    |--------------------------------------------------------------------------
    | VIDEO LIST
    |--------------------------------------------------------------------------
    */
    public function VideoListIndex($slug)
    {
        $data["lists"] = $this->postRepository->findPostName($slug);
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordCategory'] = '';
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;
        
        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'tpc_sharing_video');

        return view('video_list', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | VIDEO LIST
    |--------------------------------------------------------------------------
    */
    public function VideoTableIndex($slug)
    {
        $data["lists"] = $this->postRepository->findPostName($slug);
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data['keywordCategory'] = '';

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'tpc_sharing_video');

        return view('video_table', $data);
    }



    /*
    |--------------------------------------------------------------------------
    | MEMBER INDEX
    |--------------------------------------------------------------------------
    */
    public function MemberIndex(Request $request)
    {
        $data['type'] = 'contact_center';

        if($request->search) {
            $category = $this->categoryRepository->findName($request->category_search);
        }else {
            if(!$request->type || $request->type == "contact_center") {
                $category = $this->categoryRepository->findName('Member Contact Center');
    
            }else if($request->type == "personal_liaison") {
                $category = $this->categoryRepository->findName('Elite Personal Liaison');
                $data['type'] = 'personal_liaison';
    
            }else if($request->type == "personal_assistant") {
                $category = $this->categoryRepository->findName('Elite Personal Assistant');
                $data['type'] = 'personal_assistant';
    
            }else if($request->type == "vendor") {
                $category = $this->categoryRepository->findName('Vendor');
                $data['type'] = 'vendor';
    
            }else if($request->type == "goverment_relation") {
                $category = $this->categoryRepository->findName('Government Relation');
                $data['type'] = 'goverment_relation';
    
            }else {
                $category = (object)['id' => 0];
                
            }
        }

        
       
       
        //* CHECK EMPTY CATEGORY DATA
        if(!empty($category)) {
            $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
            $postCatId = array_column($postCateId, "post_id");
        }else {
            $postCatId = [0];
        }
       
        
        $data["keyword"] = "";
        $data['categorySelecred'] = '';
        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $data["lists"] = $this->postRepository->listCategorySearch($postCatId, $search);
            $data["keyword"] = $search;
            $data['categorySelecred'] = $request->category_search;
        }else {
            $data["lists"] = $this->postRepository->listCategory($postCatId);
        }

        $data['category'] = $this->categoryRepository->findType('member_services');

        return view('member', $data);
    }
    
    
    public function memberDetail($id)
    {
        $data["lists"] = $this->postRepository->find($id);
        $data['type'] = 'contact_center';

        return view('member_detail', $data);
    }

    public function MemberForm()
    {
        $data['type'] = 'contact_center';
        $data['category'] = $this->categoryRepository->findType('member_services');

        return view('member_form', $data);
    }

    public function memberStore(MemberContactCenterRequestRequest $request)
    {
        //* CREATE POST
        $dataPost['post_name'] = $request->question;
        $dataPost['post_title'] = $request->question;
        $dataPost['post_content'] = $request->answer;  
        $dataPost['post_status'] = '1';
        $dataPost['post_date'] = date('Y-m-d H:i:s');  
        $dataPost['post_sort'] = 1; 
        $dataPost['created_at'] = date('Y-m-d H:i:s');  
        $dataPost['updated_at'] = date('Y-m-d H:i:s');  
        $postId = $this->postRepository->createGetId($dataPost); 

        //* CREATE POST_CATEGORY
        $dataPostCate['post_id'] = $postId;  
        $dataPostCate['cate_id'] = $request->categoryMain;  
        $dataPostCate['created_at'] = date('Y-m-d H:i:s');  
        $dataPostCate['updated_at'] = date('Y-m-d H:i:s');  
        
        $response = $this->postCateRepository->create($dataPostCate);
        if($response) {
            return redirect()->route('memberForm')->with('success', 'create data successfully!!');
        }

        return redirect()->route('memberForm')->with('error', 'someting error!!');
    }

    public function RelateData($id, Request $request)
    {
        $postCateInfo = $this->postCateRepository->findCategoryByPostId($id);
        $categoryInfo = $this->categoryRepository->find($postCateInfo->cate_id);
        $post = $this->postRepository->find($id);
        $slug = (!empty($post) && $post->post_name != "") ? $post->post_name : $id;
        if($categoryInfo->name == "vdo") {
            $methodName = "VideoListIndex";

        }else if($categoryInfo->name == "knowledge") {
            $methodName = "KnowLedgeDetail";

        }else if($categoryInfo->name == "job") {
            $methodName = "JobDetail";

        }else if($categoryInfo->name == "news") {
            $methodName = "NewsDetail";

        }else if($categoryInfo->name == "announce") {
            $methodName = "AnnounceDetail";

        }else if($categoryInfo->name == "calendar") {
            $methodName = "CalendarDetail";

        }else if($categoryInfo->name == "event") {
            $methodName = "EventActivityDetail";

        }else if($categoryInfo->name == "course") {
            $methodName = "CourseDetail";
            
        }else if($categoryInfo->name == "gallery") {
            $methodName = "GalleryListIndex";
            
        }else if($categoryInfo->name == "vision" || $categoryInfo->name == "policy" || $categoryInfo->name == "mission") {
            return redirect()->action([AboutUsController::class, 'index'], ['name' => $categoryInfo->name]);
        }else if($categoryInfo->name == "blog") {
            return redirect()->action([PostController::class, 'BlogIndex']);

        }else if($categoryInfo->name == "faq") {
            return redirect()->action([PostController::class, 'FaqIndex']);
        }
        
        return redirect()->action([PostController::class, $methodName], ['slug' => $slug]);
    }

    public function Organization($id, Request $request)
    {
        $data['type'] = 'contact_center';

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'organization');

        if($request->search) {
            $category = $this->categoryRepository->findName($request->category_search);
        }

        $data["keyword"] = "";
        $data['categorySelecred'] = '';

        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $data["lists"] = "";
            $data["keyword"] = $search;
            $data['categorySelecred'] = $request->category_search;
        }else {
            $data["lists"] = $this->coreConfigRepository->listOrganization($id);
            $dataId = (isset($request->data)) ? $request->data : 0;
            $data['data'] = $this->coreConfigRepository->list($dataId);
        }

        $data['category'] = $this->categoryRepository->findType('member_services');

        return view('organization', $data);
       
    }

    public function PhoneDirectory(Request $request)
    {
        $data['keyword'] = ($request->search) ? $request->search : '';
        $data['keywordCategory'] = ($request->category) ? $request->category : '';
        $data['lists'] = $this->coreConfigRepository->listPhoneDirectory($request);
        $category = CoreConfig::where('group', 'phone_directory')->with('phoneDerectorySection')->distinct()->orderBy('level','ASC')->get();
        $data['category'] = collect($category)->groupBy('category');

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'phone_directory');

        return view('phone', $data);
    }



    /*
    |--------------------------------------------------------------------------
    | EVENT & ACTIVITY
    |--------------------------------------------------------------------------
    */
    public function EventActivityIndex(Request $request)
    {
        $category = $this->categoryRepository->findName('event');

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateCourseId = array_column($postCateId, "post_id");
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');

        $data["keywordSearch"] = "";
        $data["keywordCategory"] = "";
        $data['keywordTag'] = '';

        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            $data["keywordSearch"] = $search;
            $data['keywordTag'] = $request->searchTag;

            if($request->searchTag != "") {
                if($request->category != '') {
                    $data['lists'] = $this->postRepository->searchTag($postCateCourseId, $request->searchTag, $request->category);
                }else {
                    $data['lists'] = $this->postRepository->searchTag($postCateCourseId, $request->searchTag);
                }
            }else{
                if($request->category != '') {
                    $data['keywordCategory'] = $request->category;
                    $data["lists"] = $this->postRepository->listCategorySearchAndCategory($postCateCourseId, $search, $request->category);
                }else {
                    $data["lists"] = $this->postRepository->listCategorySearch($postCateCourseId, $search);
                }
            }

          
        }else {
            
            $data["lists"] = $this->postRepository->listCategory($postCateCourseId);
        }
       
        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'eventActivity');

        return view('eventActivity', $data);
    }


    public function EventActivityDetail($slug)
    {
        $data["lists"] = $this->postRepository->findPostName($slug);
        $id = (!empty($data['lists'])) ? $data['lists']->id : 0;
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        $data["keywordCategory"] = "";
        $data['keywordTag'] = '';

        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'eventActivity');

        return view('eventActivity_detail', $data);
    }

    /*
    |--------------------------------------------------------------------------
    | KNOWLEDGE
    |--------------------------------------------------------------------------
    */
    public function TagIndex(Request $request)
    {
        $data["keyword"] = '';
        if(isset($_REQUEST['search'])) {
            $search = $request->search;
            if($request->category != '') {
                $postCateId = PostCate::where("cate_id", $request->category)->get()->toArray();
                $postCateKnowledgeId = array_column($postCateId, "post_id");
                $data["lists"] = $this->postRepository->searchTags($request->tag, $search);
            }else {
                $data["lists"] = $this->postRepository->searchTags($request->tag, $search);
            }
            $data["keyword"] = $search;
        }else {
            $data["lists"] = $this->postRepository->searchTags($request->tag);
        }

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'tag');

        return view('tag', $data);
    }


    public function TagDetail($id)
    {
        $data["lists"] = $this->postRepository->find($id);

        //* CHECK DATA HAS FAVORITE AND LIKE  
        $data['status'] = $this->getFavoriteAndLike($id);

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'tag');

        return view('tag_detail', $data);
    }


    public function SearchResult(Request $request)
    {
        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'search_result');
        $data['lists'] = '';
        $data["keyword"] = '';
        $data["keywordCategory"] = "";
        $data['keywordTag'] = '';
        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');

       
        if(isset($_REQUEST['search'])) {
            $data["keyword"] = $request->search;

            if($request->searchTag != "") {
                $data['keywordTag'] = $request->searchTag;
                if($request->category != '') {
                    $data["keywordCategory"] = $request->category;
                    $data['lists'] = $this->postRepository->searchTagResult($request->searchTag, $request->category);
                }else {
                    $data['lists'] = $this->postRepository->searchTagResult($request->searchTag);
                }
            }else {
                if($request->category != '') {
                    $data['lists'] = $this->postRepository->searchResult($request->search, $request->category);
                    $data["keywordCategory"] = $request->category;
                }else {
                    $data['lists'] = $this->postRepository->searchResult($request->search);
                }
            }
        }
        
        return view('search_result', $data);
    }

    //* CHECK FAVORITE DATA 
    public function getFavoriteAndLike($id) {
        $user = \Auth::guard('backpack')->user();
        $favorite = $this->wishListRepository->listByUser($user->id, NULL);
        $postIdInFavorite = (count($favorite) > 0) ? $favorite->toArray() : [];
        $favoriteId = (!empty($postIdInFavorite)) ? array_column($postIdInFavorite, 'post_id') : [];
        $data['favorite_status'] = (in_array($id, $favoriteId)) ? 'active' : '';


        $like = $this->wishListRepository->listByUser($user->id, 'like');
        $postIdInLike = (count($like) > 0) ? $like->toArray() : [];
        $likeId = (!empty($postIdInLike)) ? array_column($postIdInLike, 'post_id') : [];
        $data['like_status'] = (in_array($id, $likeId)) ? 'active' : '';

        return $data;
    }

    public function getStatusRating($id) {
        $user = \Auth::guard('backpack')->user();
        $rating = $this->ratetingRepository->checkRating('course', $id);
        if(!empty($rating)) {
            $ratingId = $rating->id;
            $data = $this->ratetingRepository->checkRatingScoreByUser($ratingId, $user->id);
            return (!empty($data)) ? "active" : '';
        }else {
            return "";
        }
    }

    public function test()
    {
        return view('test');
    }

    public function ratingCourseStore(Request $request)
    {
        
        $checkRating = $this->ratetingRepository->checkRating('course', $request->courseId);
        $user = \Auth::guard('backpack')->user();
        if(!empty($checkRating)) {
            $ratingId = $checkRating->id;

            // CHECK USER HAS SCORE
            $userScore = $this->ratetingRepository->checkRatingScoreByUser($ratingId, $user->id);
            
            if(!empty($userScore)) {
                //* UPDATE SCORE
               
                $this->ratetingRepository->updateRatingScore($userScore->id, ['rating_score' => $request->score]);
            }else {
                // SAVING Rating Score
                $setRatingScore = [
                    'rating_id' => $ratingId,
                    'rating_score' => $request->score,
                    'created_at' => date('Y-m-d H:i:s'),
                    'customer_id' => $user->id
                ];
                $this->ratetingRepository->createRatingScore($setRatingScore);
            }
        }else {
            // create rating
            $setRating = [
                'title' => "course_rating_{$request->courseId}",
                'rate_tb' => 'course',
                'rate_row_id' => $request->courseId,
                'is_custom' => '0',
                'store_id' => 0,
                'vendor_id' => 0,
                'created_at' => date('Y-m-d H:i:s'),
            ];
            $ratingId = $this->ratetingRepository->createRatingGetId($setRating);

            // SAVING Rating Score
            $setRatingScore = [
                'rating_id' => $ratingId,
                'rating_score' => $request->score,
                'created_at' => date('Y-m-d H:i:s'),
                'customer_id' => $user->id
            ];
            $this->ratetingRepository->createRatingScore($setRatingScore);

            
        }
        
        return response()->json(['status' => 'success']);
       
    }


    public function SendMail(Request $request)
    {
        $data['lists'] = $this->postRepository->findMail($request->id);

        Mail::to($request->email)->send(new Post($data));

        return response()->json(['status' => 'success']);
    }
}
