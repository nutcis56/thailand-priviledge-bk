<?php

namespace App\Http\Controllers\Api;

use App\Models\PostCate;
use Illuminate\Http\Request;
use App\Repository\PostRepository;
use App\Http\Controllers\Controller;
use App\Repository\CategoryRepository;

class PostController extends Controller
{

    protected $postRepository;
    protected $categoryRepository;

    function __construct(
        PostRepository $postRepository,
        CategoryRepository $categoryRepository
    )
    {
       $this->postRepository = $postRepository;
       $this->categoryRepository = $categoryRepository;
    }


    public function storeMember(Request $request)
    {
        return response()->json(['1','2','3']);
    }

    public function blog()
    {
        $id = $this->getPostIdInCategory('blog');
        $data = $this->postRepository->getApiPostInAllId($id);
        return response()->json($data);
    }

    public function faq()
    {
        $id = $this->getPostIdInCategory('faq');
        $data = $this->postRepository->getApiPostInAllId($id);
        return response()->json($data);
    }

    public function knowledge()
    {
        $id = $this->getPostIdInCategory('knowledge');
        $data = $this->postRepository->getApiPostInAllId($id);
        return response()->json($data);
    }

    public function job()
    {
        $id = $this->getPostIdInCategory('job');
        $data = $this->postRepository->getApiPostInAllId($id);
        return response()->json($data);
    }

    public function galleryAlbum()
    {
        $id = $this->getPostIdInCategory('gallery');
        $data = $this->postRepository->getApiPostInAllId($id);
        return response()->json($data);
    }

    public function galleryList($id)
    {
        $id = $this->getPostIdInCategory('gallery');
        $data = $this->postRepository->find($id);
        return response()->json($data);
    }

    public function videoAlbum()
    {
        $id = $this->getPostIdInCategory('vdo');
        $data = $this->postRepository->getApiPostInAllId($id);
        return response()->json($data);
    }


    public function announce()
    {
        $id = $this->getPostIdInCategory('announce');
        $data = $this->postRepository->getApiPostInAllId($id);
        return response()->json($data);
    }

    public function calendar()
    {
        $id = $this->getPostIdInCategory('calendar');
        $data = $this->postRepository->getApiPostInAllId($id);
        return response()->json($data);
    }

    public function event()
    {
        $id = $this->getPostIdInCategory('event');
        $data = $this->postRepository->getApiPostInAllId($id);
        return response()->json($data);
    }

    function getPostIdInCategory($name) 
    {
        $category = $this->categoryRepository->findName($name);
        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        return (!empty($postCateId)) ? array_column($postCateId, "post_id") : [0];
    }


    public function view($id) 
    {
        $data = $this->postRepository->getApiPostDetail($id);
        return response()->json($data);
    }
}
