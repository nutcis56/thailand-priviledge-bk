<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\WishListRepository;
use App\Repository\PostRepository;
use App\Repository\CoreConfigRepository;
class WishListController extends Controller
{
    protected $wishListRepository;
    protected $postRepository;
    protected $coreConfigRepository;

    function __construct(
        WishListRepository $wishListRepository,
        PostRepository $postRepository,
        CoreConfigRepository $coreConfigRepository
    )
    {
        $this->wishListRepository = $wishListRepository;    
        $this->postRepository = $postRepository;
        $this->coreConfigRepository = $coreConfigRepository;
    }

    public function index(Request $request)
    {
        $user = \Auth::guard('backpack')->user();
        $getPostFavorite = $this->wishListRepository->listByUser($user->id, NULL);
        $getPostFavoriteToArray = $getPostFavorite->toArray();
        $postId = (!empty($getPostFavoriteToArray)) ? array_column($getPostFavoriteToArray, 'post_id') : [0];

        $data['keywordTag'] = '';
        $data["keywordSearch"] = "";
       
        if(isset($_REQUEST['search'])) {
            $search = $request->search;
           
            if($request->searchTag != "") {
                $data['keywordTag'] = $request->searchTag;
                if($request->category != '') {
                   
                    $data["keywordCategory"] = $request->category;
                    $data['lists'] = $this->postRepository->searchTag($postId, $request->searchTag, $request->category);
                }else {
                    
                    $data['lists'] = $this->postRepository->searchTag($postId, $request->searchTag);
                }
            }else {
                $data["lists"] = $this->postRepository->listCategorySearch($postId, $search);
            }

          
            $data["keywordSearch"] = $search;
        }else {
            
            $data["lists"] = $this->postRepository->listCategory($postId);
        }
       
        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'favorite');

        return view('favorite', $data);
        
    }

    public function store(Request $request)
    {
        $user = \Auth::guard('backpack')->user();

        $type = (isset($_REQUEST['type'])) ? $request->type : null;

        $setWishList['user_id'] = $user->id;
        $setWishList['post_id'] = $request->data;
        $setWishList['shared_status'] = '1';
        $setWishList['sharing_code'] = $request->data;
        $setWishList['type'] = $type;
        $setWishList['created_at'] = date('Y-m-d H:i:s');

        //* CHECK FAVORITE
        $check = $this->wishListRepository->checkByUser($user->id, $request->data, $type);
        
        if(empty($check)) {
            $this->wishListRepository->create($setWishList);
            $statusMessage = 'successfully';
        }else {
            
            if($check->type == 'like') {
                $statusMessage = 'unlike';
            }else {
                $statusMessage = 'unfavorite';
            }

            // un like and favorite
            $this->wishListRepository->delete($check->id);
            
        }

        return response()->json(['status' => $statusMessage], 200);
    }

    public function view($id)
    {
        $data["lists"] = $this->postRepository->find($id);
        $data['keywordTag'] = '';

        $postController = app()->make('App\Http\Controllers\PostController');
        $data['status'] = $postController->getFavoriteAndLike($id);
        
        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'favorite');

        return view('favorite_detail', $data);
    }

    public function like(Request $request)
    {
        $user = \Auth::guard('backpack')->user();
        $getPostFavorite = $this->wishListRepository->listByUser($user->id, 'like');
        $getPostFavoriteToArray = $getPostFavorite->toArray();
        $postId = (!empty($getPostFavoriteToArray)) ? array_column($getPostFavoriteToArray, 'post_id') : [0];

        $data['keywordTag'] = '';
        $data["keywordSearch"] = "";
       
        if(isset($_REQUEST['search'])) {
            $search = $request->search;
           
            if($request->searchTag != "") {
                $data['keywordTag'] = $request->searchTag;
                if($request->category != '') {
                   
                    $data["keywordCategory"] = $request->category;
                    $data['lists'] = $this->postRepository->searchTag($postId, $request->searchTag, $request->category);
                }else {
                    
                    $data['lists'] = $this->postRepository->searchTag($postId, $request->searchTag);
                }
            }else {
                $data["lists"] = $this->postRepository->listCategorySearch($postId, $search);
            }

          
            $data["keywordSearch"] = $search;
        }else {
            
            $data["lists"] = $this->postRepository->listCategory($postId);
        }
       
        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'like');

        return view('like', $data);
        
    }
}
