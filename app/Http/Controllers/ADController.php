<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ADController extends Controller
{

    public function test()
    {

        dd('please use command `php artisan sync:ad`');

        $position = \App\Models\UserPosition::where('name', 'like', "%ฝ่ายบริหารข้อมูลและสารสนเทศ%")->first();
        $users = \Adldap\Laravel\Facades\Adldap::search()->users()->get();
        $lists = [];
        foreach ($users as $userData) {
            $data = [
                'name' => $userData->cn && count($userData->cn) ? $userData->cn[0] : NULL,
                'first_name' => $userData->givenname && count($userData->givenname) ? $userData->givenname[0] : NULL,
                'last_name' => $userData->sn && count($userData->sn) ? $userData->sn[0] : NULL,
                'description' => $userData->description && count($userData->description) ? $userData->description[0] : NULL,
                'physical_delivery_office_name' => $userData->physicaldeliveryofficename && count($userData->physicaldeliveryofficename) ? $userData->physicaldeliveryofficename[0] : NULL,
                'telephone_number' => $userData->telephonenumber && count($userData->telephonenumber) ? $userData->telephonenumber[0] : NULL,
                'department' => $userData->department && count($userData->department) ? $userData->department[0] : NULL,
                'company' => $userData->company && count($userData->company) ? $userData->company[0] : NULL,
                'email' => $userData->userprincipalname && count($userData->userprincipalname) ? $userData->userprincipalname[0] : NULL,
                'sam_accountname' => $userData->samaccountname && count($userData->samaccountname) ? $userData->samaccountname[0] : NULL
            ];

            array_push($lists, $data);

            if ($data['email'] && $data['name']) {
                $user = \App\Models\User::where('email', $data['email'])->first();
                if (!$user) {
                    $user = new \App\Models\User;
                    $user->name = $data['name'];
                    $user->email = $data['email'];
                    $user->password = 'syncFromAdWithNoPassword';
                    $user->status = 'inActive';
                    $user->source = 'ad';

                    if ($position) {
                        $user->position_id = $position->id;
                    }

                    $user->save();

                    $userInfo = new \App\Models\UserInfo($data);
                    $user->userInfo()->save($userInfo);
                } else {
                    $user->userInfo()->update($data);
                }
            }
        }
    }
}