<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use \App\Jobs\SyncUserBookStack;
use \App\Jobs\SyncUserPydio;
use \App\Jobs\SyncUserQAT;
use \App\Jobs\SyncUserPydioOtherServer;

trait SyncUserTrait
{
    /**
     * Start sync user
     * 
     * @param   Request $request
     * @param   User    $user
     * @return  
     * 
     */
    public function syncUser($request, $user) {
        $credentials = $this->credentials($request);
        
        dispatch(new SyncUserBookStack($credentials, $user));
        dispatch(new SyncUserQAT($credentials, $user));

        if (config('app.sync_pydio_to_another_server')) {
            dispatch(new SyncUserPydioOtherServer($credentials, $user));
        } else {
            dispatch(new SyncUserPydio($credentials, $user));
        }
    }

    /**
     * Sync data to Pydio Request called from another server
     *
     * @param   string  email
     * @param   string  password
     * @return
     */
    public function SyncPydioUserFromAnotherServer(Request $request) {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
        $user = \App\Models\User::where('email', $request->email)->first();
        if ($user) {
            dispatch(new SyncUserPydio($credentials, $user));
        }
    }
}