<?php

namespace App\Http\Controllers\Traits;

use \Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Traits\SyncUserTrait;
use Illuminate\Validation\ValidationException;

trait AuthTrait
{

    use SyncUserTrait;

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        $user = \App\Models\User::where('email', '=', $request->email)->first();
        if ($user && Hash::check($request->password, $user->password)) {
            // check if ldap
            if ($user->objectguid || $user->source == 'ad') {
                return $this->loginLDAP($request);
            } else {
                // if email exists try to login ldap
                if ($this->loginLDAP($request)) {
                    return true;
                }
                return $this->loginBasic($request);
            }
        } else {
            return $this->loginLDAP($request);
        }
    }

    /**
     * Login by ldap
     * 
     * @param   $request    email, password, remember
     * @return  boolean
     */
    public function loginLDAP($request) {
        $active = config('app.ldap_login');
        if (!$active) {
            return false;
        }
        $ldap_login = Auth::guard('ldap')->attempt($this->credentials($request));
        if ($ldap_login) {
            $this->guard()->login(Auth::guard('ldap')->user(), $request->filled('remember'));
            
            $this->afterLoggedIn($request, $this->guard()->user());

            // login completed 
            return true;
        }
        return false;
    }

    /**
     * Login by basic
     * 
     * @param   $request    email, password, remember
     * @return  boolean
     */
    public function loginBasic($request) {
        $login = $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
        if ($login) {

            $this->afterLoggedIn($request, $this->guard()->user());
            
            // login completed 
            return true;
        }
        return false;
    }

    /**
     * if user action if user has/no hash position
     * 
     * @param   User    $user
     * @return  none
     */
    public function checkPosition($user) {
        if ($user) {
            if (!$user->position_id) {
                $this->assignNormalPosition($user);
            }
        }
    }

    /**
     * Assign Normal position to user
     * 
     * @param   User    $user
     * @return  none
     */
    public function assignNormalPosition($user) {
        $position = $this->getNormalPosition();
        if ($position) {
            $user->position_id = $position->id;
            $user->save();
        }
    }

    /**
     * get normal position if user no position return this position
     * 
     * @param   none
     * @return  UserPosition
     */
    public function getNormalPosition() {
        return \App\Models\UserPosition::where('name', 'like', "%แผนกเทคโนโลยีสารสนเทศ%")->first();
    }

    /**
     * Store user login info to session for use thrid payty login
     * 
     * @param   Request 
     */
    public function storeLoginUserInformation($request) {
        $request->session()->put('email', $request->email);
        $request->session()->put('password', $request->password);
    }

    /**
     * Set Default Role if role unset
     * 
     * @param   User    $user
     * 
     */
    public function checkRole($user) {
        if (!$user->roles->count()) {
            $default_role = $this->getDefaultRole();
            if ($default_role) {
                $user->assignRole($default_role);
            }
        }
    }

    /**
     * Get Default role
     * 
     * @return Role $role
     */
    public function getDefaultRole() {
        $default_role = $this->getDefaultRoleName();
        return \Backpack\PermissionManager\app\Models\Role::where('name', $default_role)->first();
    }

    /**
     * Get Default role name
     * 
     * @return string
     */
    public function getDefaultRoleName() {
        return 'General User';
    }

    /**
     * Function to check login 1 device only if logged in force logout
     * 
     * @param   User    $user
     * 
     * 
     */
    public function checkLoginOneDevice($user) {
        if (!$user->isLoggedInAnotherDevice()) {
            $user->clearSessionTimeout();
            // $user->addLoggedInDevice();
        } else {
            // has another device login force logout and send message
            $this->guard()->logout();
            $this->sendFailedLoginByAlreadyLoggedInResponse();
        }
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function sendFailedLoginByAlreadyLoggedInResponse() {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed_session')],
        ]);
    }

    /**
     * Regerante Token
     * 
     */
    public function reGenerateToken($request) {
        $request->session()->regenerate();
    }
    /**
     * Function do after logged in
     * 
     * @param   Request
     * 
     */
    public function afterLoggedIn($request) {
        $this->reGenerateToken($request);
        $this->checkLoginOneDevice($this->guard()->user());
        $this->checkPosition($this->guard()->user());
        $this->checkRole($this->guard()->user());
        $this->syncUser($request, $this->guard()->user());
        $this->storeLoginUserInformation($request);
    }
}