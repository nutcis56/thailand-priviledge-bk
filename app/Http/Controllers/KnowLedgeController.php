<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Models\PostCate;


class KnowLedgeController extends Controller
{
    private $categoryRepository;
    private $postRepository;

    function __construct(
        CategoryRepository $categoryRepository,
        PostRepository $postRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
    }

    public function index()
    {
        $category = $this->categoryRepository->findName('knowledge');

        $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();
        $postCateKnowledgeId = array_column($postCateId, "post_id");
        

        $data["lists"] = $this->postRepository->listCategory($postCateKnowledgeId);

        
        return view('knowledge', $data);
    }
}
