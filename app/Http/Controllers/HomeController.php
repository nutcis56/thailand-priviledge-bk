<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Models\PostCate;
use App\Repository\CoreConfigRepository;
use Auth;
use Debugbar;
use App\Repository\RatingRepository;
class HomeController extends Controller
{

    private $categoryRepository;
    private $postRepository;
    protected $coreConfigRepository;
    protected $ratingRepository;

    function __construct(
        CategoryRepository $categoryRepository,
        PostRepository $postRepository,
        CoreConfigRepository $coreConfigRepository,
        RatingRepository $ratingRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
        $this->coreConfigRepository = $coreConfigRepository;
        $this->ratingRepository = $ratingRepository;
       
    }

    public function sendEmailTest(Request $request) {
        if (!$request->bypass) {
            return false;
        }
        dispatch(new \App\Jobs\SendEmailGeneral([
            'subject' => 'ทดสอบ ส่งอีเมล',
            'to' => 'kittidet.sathum22@gmail.com',
            'content' => '<h2>ทดสอบส่งอีเมล</h2>'
        ]));
    }

    public function index()
    {
        // $session = \Session::getId();
        // $user = \Auth::guard('backpack')->user();
        // dd($user->session, $session);
        // $session = \Session::getId();
        // dd($session);
        // $password = 'admin';
        // $hashedPassword = \Illuminate\Support\Facades\Hash::make('admin');
        // dd(md5($password), $hashedPassword, \Illuminate\Support\Facades\Hash::check('admin', $hashedPassword));

        //  news
        $category = $this->categoryRepository->findName('news');
        $postCateId = PostCate::select('post_id')->where("cate_id", $category->id)->orderBy('id', 'DESC')->take(4)->get()->toArray();
        
        $postCateNewsId = array_column($postCateId, "post_id");
        $news = $this->postRepository->listPostFNLimit($postCateNewsId, 4);
        $data['newsFirst'] = [];
        if(count($news) > 0) {
            $data['newsFirst'] = $news[0];
        }
        $data['news'] = $news;

        
        // announce
        $categoryAnnounce = $this->categoryRepository->findName('announce');
        $postAnnounceCateId = PostCate::select('post_id')->where("cate_id", $categoryAnnounce->id)->orderBy('id', 'DESC')->take(4)->get()->toArray();
        $postCateAnnounceId = array_column($postAnnounceCateId, "post_id");
        $announce = $this->postRepository->listPostFNLimit($postCateAnnounceId, 4);
        $data['announceFirst'] = [];
        if(count($announce) > 0) {
            $data['announceFirst'] = $announce[0];
        }
        $data['announces'] = $announce;
       

        // calendar
        $categoryCalendar = $this->categoryRepository->findName('calendar');
        $postCalendarCateId = PostCate::select('post_id')->where("cate_id", $categoryCalendar->id)->orderBy('id', 'DESC')->take(4)->get()->toArray();
        $postCateCalendarId = array_column($postCalendarCateId, "post_id");
        $calendars = $this->postRepository->listPostFNLimit($postCateCalendarId, 4);
        $data['calendarFirst'] = [];
        if(count($calendars) > 0) {
            $data['calendarFirst'] = $calendars[0];
        }
        $data['calendars'] = $calendars;

        //* KNOWLEDGE SHARING
        $categoryKnowledge = $this->categoryRepository->findName('knowledge');
        $postKnowledgeCateId = PostCate::select('post_id')->where("cate_id", $categoryKnowledge->id)->orderBy('id', 'DESC')->take(4)->get()->toArray();
        $postCateKnowledgeId = array_column($postKnowledgeCateId, "post_id");
        $data['knowledges'] = $this->postRepository->listPostFNLimit($postCateKnowledgeId, 2);


        //postion list all 
        $data['myKnowledges'] = $this->postRepository->listMyKnowledge($postCateKnowledgeId, 4);

        //* COURSE
        $categoryCourse = $this->categoryRepository->findName('course');
        $postCourseCateId = PostCate::select('post_id')->where("cate_id", $categoryCourse->id)->orderBy('id', 'DESC')->take(4)->get()->toArray();
        $postCateCourseId = array_column($postCourseCateId, "post_id");
        $courses = $this->postRepository->listPostFNLimit($postCateCourseId, 2);
        if(!empty($courses)) {
            foreach($courses as $course) {
                $ratingCourseInfo = $this->ratingRepository->checkRating('course', $course->id);
                if(!empty($ratingCourseInfo)) {
                 
                    if($ratingCourseInfo->is_custom == "1") {
                        $course->ratingScore = $ratingCourseInfo->custom_score;
                    }else {
                        $ratingScore = $this->ratingRepository->getRatingScoreById($ratingCourseInfo->id);
                        if(!empty($ratingScore)) {
                            $ratingScoreArr = $ratingScore->toArray();
                            $scoresRating = array_column($ratingScoreArr, 'rating_score');
                            $scoreCount = count($scoresRating);
                            $course->ratingScore = (array_sum($scoresRating)) / $scoreCount;
                        }else {
                            $course->ratingScore = 0;
                        }
                    }
                }else {
                    $course->ratingScore = 0;
                }
            }
        }
        $data['courses'] = $courses;


        //* BLOG
        $categoryBlog = $this->categoryRepository->findName('blog');
        $postBlogCateId = PostCate::select('post_id')->where("cate_id", $categoryBlog->id)->orderBy('id', 'DESC')->take(4)->get()->toArray();
        $postCateBlogId = array_column($postBlogCateId, "post_id");
        

        //* VIDEO
        $categoryVideo = $this->categoryRepository->findName('vdo');
        $postVideoCateId = PostCate::select('post_id')->where("cate_id", $categoryVideo->id)->orderBy('id', 'DESC')->take(4)->get()->toArray();
        $postVideoCateId = array_column($postVideoCateId, "post_id");
        $data['videies'] = $this->postRepository->listPostFNLimit($postVideoCateId, 1);
        
        $blogs = $this->postRepository->listPostFNLimit($postCateBlogId, 4);
        
        $data['blogFirst'] = [];
        if(count($blogs) > 0) {
            $data['blogFirst'] = $blogs[0];
        }
        $data['blogs'] = $blogs;

        

        $category = $this->categoryRepository->findName('knowledge');

        $data['categoryKnowledge'] = $category;

        //* CONFIG BIG BANNER
        $data['bigBanner'] = $this->coreConfigRepository->listGroupAndCategoryHomePage('homepage', 'big_banner', true);

        //* CONFIG EXTERNAL LINK
        $data['externalLink'] = $this->coreConfigRepository->listGroupAndCategoryHomePage('homepage', 'external_link', true);

        //* CONFIG MEMBER LINK
        $data['memberLink'] = $this->coreConfigRepository->listGroupAndCategoryHomePage('homepage', 'member_link', true);

        $data['section'] = $this->coreConfigRepository->listGroupAndCategory('office','section');
        
       

        return view('index', $data);
    }

    /**
     * Site map page
     * 
     * @param   Request     $request
     * @return  View
     */
    public function siteMap(Request $request) {
        $data = [];

        $data['name'] = "Sitemap";

        $data['all_menus'] = \App\Models\ConfigMenu::where('category', 'top_menu')->where('status', '1')->orderBy('scope', 'ASC')->get();

        return view('sitemap', $data);
    }


    public function serviceApi(Request $request)
    {
        $data['name'] = "Service Api";

        $data['lists'] = \App\Models\ApiService::first();
        
        return view('service_api', $data);
    }
}
