<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repository\CategoryRepository;
use App\Repository\PostRepository;
use App\Models\PostCate;
use App\Repository\CoreConfigRepository;

class AboutUsController extends Controller
{
    private $categoryRepository;
    private $postRepository;
    protected $coreConfigRepository;

    function __construct(
        CategoryRepository $categoryRepository,
        PostRepository $postRepository,
        CoreConfigRepository $coreConfigRepository
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->postRepository = $postRepository;
        $this->coreConfigRepository = $coreConfigRepository;
    }

    public function index($name) 
    {
       
        $category = $this->categoryRepository->findName($name);
        $data["lists"] = [];
        if(!empty($category)) {
            $postCateId = PostCate::where("cate_id", $category->id)->get()->toArray();

            $postController = app()->make('App\Http\Controllers\PostController');
           
            $id = (!empty($postCateId)) ? $postCateId[0]['post_id'] : 0;
            $data['status'] = $postController->getFavoriteAndLike($id);
            
            $data["lists"] = $this->postRepository->find($id);
        }

        if($name == "vision") {
            $data["name"] = "วิสัยทัศน์";
        }else if($name == "mission") {
            $data["name"] = "พันธกิจ";
        }else if($name == "policy") {
            $data["name"] = "นโยบาย";
        }else {
            $data["name"] = "";
        }

        //* LIST PAGE TITLE 
        $data['pageTitle'] = $this->coreConfigRepository->listPageTitle('page_title', 'page_title', 'aboutUs');
       
        return view('about_us', $data);
    }
}
