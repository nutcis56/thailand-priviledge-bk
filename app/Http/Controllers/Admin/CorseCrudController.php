<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CorseRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CorseCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CorseCrudController extends \App\Http\Controllers\Admin\PostCrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Course::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/course');
        CRUD::setEntityNameStrings('Course', 'Courses');
        CRUD::setEntityNameStrings('Course', 'Courses');

        $this->crud->crud_description = '(กรอกรายละเอียดสื่อการสอน)';
    }

    public function setActionListButton() {
        CRUD::addButtonFromView('line', 'course_lessons_manage', 'course_lessons_manage', 'beginning');
        CRUD::addButtonFromView('line', 'course_files_manage', 'course_files_manage', 'beginning');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns
        $this->setActionListButton();

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */

        CRUD::addColumn([
            'name' => 'post_title',
            'label' => 'หัวข้อ',
        ]);

        CRUD::addColumn([
            'name' => 'post_date',
            'label' => 'วันที่',
        ]);

        CRUD::addColumn([
            'name' => 'post_status',
            'label' => 'สถานะ',
            'type' => 'select_from_array',
            'options' => [
                "1" => 'ใช้งาน',
                "0" => 'ไม่ใช้งาน'
            ]
        ]);
        CRUD::addColumn([
            'label' => 'ระดับเนื้อหา',
            'name' => 'post_priority', 
            'type' => 'select_from_array',
            'options' => [
                "0" => 'Beginner',
                "1" => 'Intermediate',
                "2" => 'Advance',
            ]
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CorseRequest::class);

        // CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
        CRUD::addField([
            'name' => 'post_title', 
            'label' => 'ชื่อคอร์ส',
            'type' => 'text'
        ]);
        CRUD::addField([
            'name' => 'post_name', 
            'label' => 'URL Friendly',
            'type' => 'text'
        ]);
        CRUD::addField([
            'name' => 'post_tags',
            'label' => 'แท็ก (สามารถเลือกได้มากกว่า 1 รายการ)',
            'type' => 'select2freetext',
            'data' => '\App\Models\Tag',
            'key_data' => 'name',
            'key_value' => 'name',
        ]);

        CRUD::addField([
            'name' => 'post_cover', 
            'label' => 'รูปภาพปก (รูปภาพปกควรมีขนาด 1200x648 PX)',
            'type' => 'image',
        ]);
        CRUD::addField([
            'name' => 'post_excerpt', 
            'label' => 'รายละเอียดหลักสูตร แบบย่อ',
            'type' => 'textarea',
        ]);
        
        CRUD::addField([
            'name' => 'post_content', 
            'label' => 'รายละเอียดหลักสูตร',
            'type' => 'ckeditor',
                'options' => [
                    'extraPlugins' => config('app.ckeditor_extra_olugins')
                ]
        ]);

        CRUD::addField([
            'name' => 'course_files',
            'label' => 'ไฟล์ประกอบ',
            'type' => 'course.course_files',
        ]);

        CRUD::addField([
            'name' => 'course_lessons',
            'label' => 'วีดีโอสื่อการสอน',
            'type' => 'course.course_lessons',
        ]);

        if (get_class($this) == 'App\Http\Controllers\Admin\PostCrudController') {
            CRUD::addField([
                'name' => 'category', 
                'label' => 'Category',
                'type' => 'select2_multiple',
                'entity' => 'category',
                'model' => 'App\Models\Category',
                'attribute' => 'name',
                'pivot' => true
            ]);
        }
        
        CRUD::addField([
            'name' => 'post_status', 
            'label' => 'สถานะ',
            'type' => 'select_from_array',
            'options' => [
                1 => 'ใช้งาน',
                0 => 'ไม่ใช้งาน'
            ],
            'default' => 1
        ]);
        CRUD::addField([
            'label' => 'ระดับเนื้อหา',
            'name' => 'post_priority', 
            'type' => 'select_from_array',
            'options' => [
                0 => 'Beginner',
                1 => 'Intermediate',
                2 => 'Advance',
            ],
            'default' => 0
        ]);

        CRUD::addField([
            'name' => 'post_lecturer', 
            'type' => 'text',
            'label' => 'วิทยากร'
        ]);

        CRUD::addField([
            'name' => 'post_lecturer_time', 
            'type' => 'text',
            'label' => 'ระยะเวลารวม'
        ]);

        CRUD::addField([
            'name' => 'post_lecturer_file', 
            'type' => 'text',
            'label' => 'สื่อการสอน'
        ]);

        // CRUD::addField([   // Browse multiple
        //     'name'          => 'post_lecturer_file',
        //     'type'          => 'browse',
        //     'label' =>      'ระบุข้อความ',
        //     'multiple'   => false, // enable/disable the multiple selection functionality
        //     'mime_types'    => ['video/mp4'],
        //     // 'sortable'   => false, // enable/disable the reordering with drag&drop
        //     //'mime_types' => ['video/mp4', 'video/quicktime', 'video/webm', 'video/x-ms-wmv', 'video/x-msvideo'], // visible mime prefixes; ex. ['image'] or ['application/pdf']
        // ]);

        CRUD::addField([
            'name' => 'pre_test', 
            'type' => 'text',
            'label' => 'แบบทดสอบ Pre-Test (URL)'
        ]);

        CRUD::addField([
            'name' => 'post_test', 
            'type' => 'text',
            'label' => 'แบบทดสอบ Post-Test (URL)'
        ]);

        // CRUD::addField([
        //     'name' => 'ping_status', 
        //     'label' => 'เลือกเมื่อต้องการปล่อย API',
        //     'type' => 'select_from_array',
        //     'options' => [
        //         1 => 'ใช้งาน',
        //         0 => 'ไม่ใช้งาน'
        //     ],
        //     'default' => 1
        // ]);
        CRUD::addField([
            'name' => 'post_sort', 
            'label' => 'ลำดับ',
            'type' => 'number',
            'default' => 1
        ]);
        CRUD::addField([
            'name' => 'post_date',
            'label' => 'วันที่', 
            'type' => 'datetime',
            'default' => date('Y-m-d H:i:s')
        ]);


        $this->crud->addField([
            'label' => 'แผนก',
            'type' => 'post_position',
            'name' => 'position_id',
            'force_default' => true
        ]);
      
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

       
        $attributes = \json_decode($this->crud->getCurrentEntry()->post_attribute);

        if($attributes != null && $attributes != "") {
            
            $this->crud->modifyField('post_lecturer', [
                'default' => (isset($attributes->name) ? $attributes->name : '')
            ]);

            $this->crud->modifyField('post_lecturer_time', [
                'default' => (isset($attributes->time) ? $attributes->time : '')
            ]);

            $this->crud->modifyField('post_lecturer_file', [
                'default' => (isset($attributes->file) ? $attributes->file : '')
            ]);
           
            $this->crud->modifyField('pre_test', [
                'default' => (isset($attributes->pre_test) ? $attributes->pre_test : '')
            ]);

            $this->crud->modifyField('post_test', [
                'default' => (isset($attributes->post_test) ? $attributes->post_test : '')
            ]);

        }
       
        
    }
}
