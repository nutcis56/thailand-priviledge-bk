<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CoreConfigServiceLinkRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CoreConfigCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ConfigServiceLinkCrudController extends CoreConfigCrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ConfigServiceLink::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/core-config-service-link');
        CRUD::setEntityNameStrings(trans('backpack::menu.core-config-service-link'), trans('backpack::menu.core-config-service-link'));
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CoreConfigServiceLinkRequest::class);

        // CRUD::setFromDb(); // fields

        $hide_in_array = [
            'App\Http\Controllers\Admin\ConfigMenuCrudController',
            'App\Http\Controllers\Admin\ConfigBigBannerCrudController',
            'App\Http\Controllers\Admin\ConfigIconLinkCrudController',
            'App\Http\Controllers\Admin\ConfigServiceLinkCrudController'
        ];

        CRUD::addField([
            'name' => 'name', 
            'label' => 'ชื่อ',
            'type' => 'text',
            'tab' => '1. รายละเอียดเบื้องต้น',
        ]);

        if (!in_array(get_class($this), $hide_in_array)) {
            CRUD::addField([
                'name' => 'code', 
                'label' => 'รหัสตั้งค่า',
                'type' => 'text',
                'tab' => '1. รายละเอียดเบื้องต้น',
            ]);
        } else {
            CRUD::addField([
                'name' => 'code',
                'type' => 'hidden',
                'value' => 'hidden_value_'.get_Class($this)
            ]);
        }

        CRUD::addField([
            'name' => 'scope', 
            'label' => 'การเรียงลำดับ',
            'type' => 'number',
            'tab' => '1. รายละเอียดเบื้องต้น',
        ]);

        CRUD::addField([
            'name' => 'status', 
            'label' => 'สถานะ',
            'type' => 'select_from_array',
            'options' => [
                '1' => 'ใช้งาน',
                '0' => 'ไม่ใช้งาน'
            ],
            'tab' => '1. รายละเอียดเบื้องต้น',
        ]);

        CRUD::addField([
            'name' => 'link', 
            'label' => 'ระบุ URL ที่ต้องการเชื่อมโยง เช่น http://www.google.com',
            'type' => 'text',
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

        CRUD::addField([
            'name' => 'target', 
            'label' => 'การเปิดลิงค์',
            'type' => 'select_from_array',
            'options' => [
                '_self' => 'เปิดหน้าเดิม (ค่ามาตราฐาน)',
                '_blank' => 'เปิดหน้าใหม่',
            ],
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

        CRUD::addField([
            'name' => 'image', 
            'label' => 'รูปภาพ',
            'type' => 'browse',
            'hint' => '- icon ควรมีขนาด 51 x 32 PX <br/> 
            - รองรับไฟล์ png, svg <br/> 
            - รูปภาพควรมีขนาดไม่เกิน 2 MB',
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

    }
}
