<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\YourJobFileRequest;
use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class KnowledgeIdeaSharingCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class YourJobKnowledgeFileCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\KnowledgeFile::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/your-job-file');
        CRUD::setEntityNameStrings('ไฟล์', 'your job files');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns

        CRUD::addColumn([
            'name' => 'post_title', 
            'label' => 'ชื่อไฟล์',
            'type' => 'text'
        ]); 
        
        CRUD::addColumn([
            'name' => 'post_status', 
            'label' => 'สถานะ',
            'type' => 'text'
        ]);

        // remove a button
        $this->crud->removeButton('show');
        $this->crud->removeButton('update');
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(YourJobFileRequest::class);

        // CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */

        CRUD::addField([
            'name' => 'post_title', 
            'label' => 'ชื่อไฟล์',
            'type' => 'text'
        ]);

        CRUD::addField([
            'name' => 'post_name', 
            'label' => 'URL Friendly',
            'type' => 'hidden',
            'value' => 'job_km_file'
        ]);
        
        CRUD::addField([
            'name' => 'post_cover', 
            'label' => 'อัพโหลดไฟล์',
            'type' => 'browse',
        ]);
     
        CRUD::addField([
            'name' => 'post_status',
            'label' => 'สถานะ',
            'type' => 'select_from_array',
            'options' => [
                1 => 'ใช้งาน',
                0 => 'ไม่ใช้งาน'
            ],
            'default' => 1
        ]);
        // CRUD::addField([
        //     'name' => 'post_priority', 
        //     'type' => 'select_from_array',
        //     'options' => [
        //         0 => 'Unset',
        //         1 => 'General',
        //         2 => 'Medium',
        //         3 => 'High ',
        //     ],
        //     'default' => 1
        // ]);
        CRUD::addField([
            'name' => 'ping_status', 
            'label' => 'เลือกเมื่อต้องการปล่อย API',
            'type' => 'select_from_array',
            'options' => [
                1 => 'ใช้งาน',
                0 => 'ไม่ใช้งาน'
            ],
            'default' => 1
        ]);
        CRUD::addField([
            'name' => 'post_sort',
            'label' => 'ลำดับ',
            'type' => 'number',
            'default' => 1
        ]);
        CRUD::addField([
            'name' => 'post_date', 
            'label' => 'วันที่',
            'type' => 'datetime',
            'default' => date('Y-m-d H:i:s')
        ]);

        CRUD::addField([
            'label' => 'แผนก',
            'type' => 'post_position',
            'name' => 'position_id',
            'force_default' => true
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $id = \Route::current()->parameter('id');
        $coreConfigRepo = new CoreConfigRepository();
        $data = $coreConfigRepo->list($id);
        $dataValue = \json_decode($data->value);
       
        $this->setupCreateOperation();
        
        
        CRUD::modifyField('mobile',[
            'name' => 'mobile', 
            'type' => 'text',
            'value' => $dataValue->mobile
        ]);

        CRUD::modifyField('phone1',[
            'name' => 'phone1', 
            'type' => 'text',
            'value' => $dataValue->phone1
        ]);

        CRUD::modifyField('phone2',[
            'name' => 'phone2', 
            'type' => 'text',
            'value' => $dataValue->phone2
        ]);

        CRUD::modifyField('email',[
            'name' => 'email', 
            'type' => 'email',
            'value' => $dataValue->email
        ]);


        CRUD::modifyField('department',[
            'default' => $data->class
        ]);

        CRUD::modifyField('section',[
            'default' => $data->category
        ]);

        CRUD::modifyField('sort_order',[
            'value' => $data->level
        ]);
    }


}
