<?php

namespace App\Http\Controllers\Admin;

use App\Exports\PhoneDirectoryExampleExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Http\Requests\PhoneDirectoryRequest;
use App\Repository\CoreConfigRepository;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PhoneDirectoryExport;
use App\Imports\PhoneDirectoryImport;
use App\Mail\PhonedirectoryMail;
use App\Models\PhoneDirectory;
use Illuminate\Support\Facades\Mail;
/**
 * Class CategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PhoneDirectoryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\PhoneDirectory::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/phone-directory');
        CRUD::setEntityNameStrings('phone directory', 'phone directories');
        CRUD::addButtonFromView('before-top', 'phone_directory_export_example', 'phone_directory_export_example', 'beginning');
        CRUD::addButtonFromView('before-top', 'phone_directory_export_excel', 'phone_directory_export_excel', 'beginning');
        CRUD::addButtonFromView('before-top', 'phone_directory_export_csv', 'phone_directory_export_csv', 'beginning');
        CRUD::addButtonFromView('before-top', 'phone_directory_import', 'phone_directory_import', 'beginning');
        CRUD::addButtonFromView('before-top', 'phone_directory_information', 'phone_directory_information', 'beginning');
       
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns
    
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
        // $courses = \App\Models\Course::pluck('post_title', 'id')->toArray();
        // CRUD::addFilter([
        //     'name'  => 'doc_tb_id',
        //     'type'  => 'dropdown',
        //     'label' => 'Course'
        //   ], 
        //   $courses, 
        //   function($value) { // if the filter is active
        //     $this->crud->addClause('where', 'doc_tb_id', $value);
        //   });

        CRUD::addColumn([
            'name' => 'name', 
            'type' => 'text',
            'searchLogic' => function ($query, $column, $searchTerm) {
                $query->orWhere('value', 'like', '%'.$searchTerm.'%');
            }
        ]); 

        // CRUD::addColumn([
        //     'name' => 'value', 
        //     'type' => 'json'
        // ]);

        CRUD::addColumn([
            'label' => 'ฝ่าย',
            'type' => 'select',
            'name' => 'class',
            'entity' => 'department',
            'attribute' => 'name',
            'searchLogic' => function ($query, $column, $searchTerm) {
                $departmentData = \App\Models\CoreConfig::where('group','office')->where('category','department')->where('name','LIKE',"%{$searchTerm}%")->first();
                if(!empty($departmentData)) {
                    $query->orWhere('class', '=', $departmentData->id);
                }
            }
        ]);

        CRUD::addColumn([
            'label' => 'แผนก',
            'type' => 'select',
            'name' => 'category',
            'entity' => 'section',
            'attribute' => 'name',
            'searchLogic' => function ($query, $column, $searchTerm) {
                $sectionData = \App\Models\CoreConfig::where('group','office')->where('category','section')->where('name','LIKE',"%{$searchTerm}%")->first();
                if(!empty($sectionData)) {
                    $query->orWhere('category', '=', $sectionData->id);
                }
            }
        ]);
       

        // CRUD::addColumn([
        //     'name' => 'class', 
        //     'type' => 'text',
        //     'label' => 'ฝ่าย',
        // ]);
        
        // CRUD::addColumn([
        //     'name' => 'category', 
        //     'type' => 'text',
        //     'label' => 'แผนก',
        // ]);


      
        
        CRUD::addColumn([
            'name' => 'status', 
            'type' => 'select_from_array',
            'options' => ['0' => 'Draf', '1' => 'published']
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PhoneDirectoryRequest::class);

        // CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */

        CRUD::addField([
            'name' => 'name', 
            'type' => 'text'
        ]);

        CRUD::addField([
            'name' => 'mobile', 
            'type' => 'text'
        ]);

        CRUD::addField([
            'name' => 'phone1', 
            'type' => 'text'
        ]);

        CRUD::addField([
            'name' => 'phone2', 
            'type' => 'text'
        ]);

        CRUD::addField([
            'name' => 'email', 
            'type' => 'email'
        ]);

        CRUD::addField([
            'name' => 'sort_order', 
            'type' => 'number',
            'default' => 1
        ]);
        
        $departments = \App\Models\CoreConfig::where('group','office')->where('category','department')->get();
       
        if(count($departments) > 0) {
            $newDepartment = [];
            foreach($departments as $department) {
                $newDepartment[$department['id']] =$department->name;
            }
        }
        CRUD::addField([
            'name'        => 'department',
            'label'       => "Department",
            'type'        => 'select2_from_array',
            'options'     => $newDepartment,
            'allows_null' => false,
            'default'     => '',
        ]);

        $sections = \App\Models\CoreConfig::where('group','office')->where('category','section')->get();
       
        if(count($sections) > 0) {
            $newSection = [];
            foreach($sections as $section) {
                $newSection[$section->id] =$section->name;
            }
        }
        CRUD::addField([
            'name'        => 'section',
            'label'       => "Section",
            'type'        => 'select2_from_array',
            'options'     => $newSection,
            'allows_null' => false,
            'default'     => '',
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        
        $id = \Route::current()->parameter('id');
       
        $coreConfigRepo = new CoreConfigRepository();
       
        $data = PhoneDirectory::find($id);
       
        $dataValue = \json_decode($data->value);
       
        $this->setupCreateOperation();
        
        
        CRUD::modifyField('mobile',[
            'name' => 'mobile', 
            'type' => 'text',
            'value' => $dataValue->mobile
        ]);

        CRUD::modifyField('phone1',[
            'name' => 'phone1', 
            'type' => 'text',
            'value' => $dataValue->phone1
        ]);

        CRUD::modifyField('phone2',[
            'name' => 'phone2', 
            'type' => 'text',
            'value' => $dataValue->phone2
        ]);

        CRUD::modifyField('email',[
            'name' => 'email', 
            'type' => 'email',
            'value' => $dataValue->email
        ]);


        CRUD::modifyField('department',[
            'default' => $data->class
        ]);

        CRUD::modifyField('section',[
            'default' => $data->category
        ]);

        CRUD::modifyField('sort_order',[
            'value' => $data->level
        ]);
    }

    public function export() 
    {
        return Excel::download(new PhoneDirectoryExport, 'phone_directory.xlsx');
    }

    public function exportCSV() {
        // return Excel::download(new VisitorLogExport, 'log.csv');
        // return (new PhoneDirectoryExport)->download('phone_directory.csv', \Maatwebsite\Excel\Excel::CSV);

        return Excel::download(new PhoneDirectoryExport, 'phone_directory.csv');
    }

    public function exportExample() {
        // return Excel::download(new VisitorLogExport, 'log.csv');
        // return (new PhoneDirectoryExport)->download('phone_directory.csv', \Maatwebsite\Excel\Excel::CSV);

        return Excel::download(new PhoneDirectoryExampleExport, 'phone_directory.xlsx');
    }

    public function import(Request $request) 
    {
        $file = $request->file('file');
        
        
       

        Excel::import(new PhoneDirectoryImport, $file);
        
       
         //* SEND MAIL
         $config = \App\Models\CoreConfig::where('group', 'office')
         ->where('name', 'LIKE', "%แผนกบริหารทรัพยากรบุคคล%")
         ->first();

        if(!empty($config)) {
            // $data = \App\Models\User::get();
            $data = \App\Models\User::where('position_id', $config->id)->get();
            if(!empty($data)) {
                foreach($data as $value) {
                    Mail::to($value->email)->send(new PhonedirectoryMail());
                }
            }
        }




        \Alert::success(trans('backpack::crud.insert_success'))->flash();

        return redirect()->back();
    }


    public function destroyData(Request $request)
    {
        $data = PhoneDirectory::select('id')->get()->toArray();
        $ids = (!empty($data)) ? array_column($data, 'id') : [0];
        $res = PhoneDirectory::whereIn('id', $ids)->delete();
        if($res) {
            \Alert::success(trans('backpack::crud.delete_success'))->flash();
            return redirect()->back();
            
            return response()->json(['status' => 'success']);
        }
        return response()->json(['status' => 'fail']);
        
    }
}
