<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\QAndARequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class QAndACrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class QAndACrudController extends \App\Http\Controllers\Admin\PostCrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\QAndA::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/q-and-a');
        CRUD::setEntityNameStrings('q and a', 'q and a');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns
        CRUD::addColumn([
            'name' => 'post_title',
            'label' => 'คำถาม', 
            'type' => 'text'
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(QAndARequest::class);

        // CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
        CRUD::addField([
            'name' => 'post_title', 
            'label' => 'คำถาม',
            'type' => 'text'
        ]);
        // CRUD::addField([
        //     'name' => 'post_name', 
        //     'label' => 'URL Friendly',
        //     'type' => 'text'
        // ]);

        CRUD::addField([
            'name' => 'post_cover', 
            'label' => 'รูปภาพปก (รูปภาพปกควรมีขนาด 1200x648 PX)',
            'type' => 'image',
        ]);
        CRUD::addField([
            'name' => 'post_content', 
            'label' => 'คำตอบ',
            'type' => 'ckeditor',
                'options' => [
                    'extraPlugins' => config('app.ckeditor_extra_olugins')
                ]
        ]);

        if (get_class($this) == 'App\Http\Controllers\Admin\PostCrudController') {
            CRUD::addField([
                'name' => 'category', 
                'label' => 'Category',
                'type' => 'select2_multiple',
                'entity' => 'category',
                'model' => 'App\Models\Category',
                'attribute' => 'name',
                'pivot' => true
            ]);
        }
        
        CRUD::addField([
            'name' => 'post_status', 
            'label' => 'สถานะ',
            'type' => 'select_from_array',
            'options' => [
                1 => 'ใช้งาน',
                0 => 'ไม่ใช้งาน'
            ],
            'default' => 1
        ]);
        
        CRUD::addField([
            'name' => 'post_sort', 
            'label' => 'ลำดับ',
            'type' => 'number',
            'default' => 1
        ]);
        CRUD::addField([
            'name' => 'post_date', 
            'label' => 'วันที่',
            'type' => 'datetime',
            'default' => date('Y-m-d H:i:s')
        ]);

        // CRUD::addField([
        //     'name' => 'releated_with_ids',
        //     'label' => 'หัวข้อที่เกี่ยวข้อง',
        //     'type' => 'select2relate_datas',
        //     'data' => get_class(CRUD::getModel()),
        //     'key_data' => 'post_title',
        //     'key_value' => 'id',
        //     'pivot_type' => get_class(CRUD::getModel()),
        // ]);

        $this->crud->addField([
            'label' => 'แผนก',
            'type' => 'post_position',
            'name' => 'position_id',
            'force_default' => true
        ]);

        $this->crud->addField([
            'label' => 'Sync ไประบบบริการสมาชิก',
            'type' => 'sync_status',
            'name' => 'sync_status',
        ]);
    }

}
