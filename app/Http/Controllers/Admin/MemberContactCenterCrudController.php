<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TPCSharingGalleryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ElitePersonalAssistantCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MemberContactCenterCrudController extends \App\Http\Controllers\Admin\PostCrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\MemberContactCenter::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/member-contact-center');
        CRUD::setEntityNameStrings('Member Contact Center', 'Member Contact Center');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */

        CRUD::addColumn([
            'name' => 'post_name'
        ]);

        CRUD::addColumn([
            'name' => 'post_date'
        ]);

        CRUD::addColumn([
            'name' => 'post_status'
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(\App\Http\Requests\MemberContactCenterRequest::class);

        // CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
        CRUD::addField([
            'name' => 'post_title', 
            'type' => 'text'
        ]);
        CRUD::addField([
            'name' => 'post_name', 
            'type' => 'text'
        ]);
        CRUD::addField([
            'name' => 'post_tags',
            'type' => 'select2freetext',
            'data' => '\App\Models\Tag',
            'key_data' => 'name',
            'key_value' => 'name',
        ]);

        CRUD::addField([
            'name' => 'post_cover', 
            'type' => 'image',
        ]);
        CRUD::addField([
            'name' => 'post_excerpt', 
            'type' => 'textarea',
        ]);
        
        CRUD::addField([
            'name' => 'post_content', 
            'type' => 'ckeditor',
                'options' => [
                    'extraPlugins' => config('app.ckeditor_extra_olugins')
                ]
        ]);

        if (get_class($this) == 'App\Http\Controllers\Admin\PostCrudController') {
            CRUD::addField([
                'name' => 'category', 
                'label' => 'Category',
                'type' => 'select2_multiple',
                'entity' => 'category',
                'model' => 'App\Models\Category',
                'attribute' => 'name',
                'pivot' => true
            ]);
        }
        
        CRUD::addField([
            'name' => 'post_status', 
            'type' => 'select_from_array',
            'options' => [
                1 => 'ใช้งาน',
                0 => 'ไม่ใช้งาน'
            ],
            'default' => 1
        ]);
        CRUD::addField([
            'name' => 'post_priority', 
            'type' => 'select_from_array',
            'options' => [
                0 => 'Unset',
                1 => 'General',
                2 => 'Medium',
                3 => 'High ',
            ],
            'default' => 1
        ]);
        CRUD::addField([
            'name' => 'ping_status', 
            'type' => 'select_from_array',
            'options' => [
                1 => 'ใช้งาน',
                0 => 'ไม่ใช้งาน'
            ],
            'default' => 1
        ]);
        // CRUD::addField([
        //     'name' => 'post_sort', 
        //     'type' => 'number',
        //     'default' => 1
        // ]);
        CRUD::addField([
            'name' => 'post_date', 
            'type' => 'datetime',
            'default' => date('Y-m-d H:i:s')
        ]);

      
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
