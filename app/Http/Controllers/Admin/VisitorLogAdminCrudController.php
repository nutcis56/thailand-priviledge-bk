<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VisitorLogAdminRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Exports\VisitorLogAdminExport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class VisitorLogAdminCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VisitorLogAdminCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\VisitorLogAdmin::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/visitor-log-admin');
        CRUD::setEntityNameStrings('ประวัติการใช้งานผู้ดูแลระบบ', 'ประวัติการใช้งานผู้ดูแลระบบ');
        CRUD::denyAccess('operation');
        CRUD::denyAccess(['update', 'create', 'delete']);
        // CRUD::enableExportButtons();
        CRUD::addButtonFromView('before-top', 'export_excel', 'export_excel', 'beginning');
        CRUD::addButtonFromView('before-top', 'export_csv', 'export_csv', 'beginning');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns

        CRUD::addColumn([
            'name' => 'ip',
            'label' => 'IP'
        ]);

        CRUD::addColumn([
            'name' => 'url',
            'label' => 'URL'
        ]);

        CRUD::addColumn([
            'name' => 'visitor_id',
            'label' => 'ผู้ใช้งาน',
            'entity' => 'visitor',
            'attribute' => 'email',
        ]);

        CRUD::addColumn([
            'name' => 'action',
            'label' => 'การกระทำ'
        ]);

        CRUD::addColumn([
            'name' => 'info',
            'label' => 'รายละเอียด'
        ]);

        CRUD::addColumn([
            'name' => 'created_at',
            'label' => 'วันที่'
        ]);

        CRUD::addFilter([ // daterange filter
            'type' => 'date_range',
            'name' => 'date_range',
            'label'=> 'Date range',
            'locale' => [
                'format' => 'DD/MM/YYYY'
            ]
        ],
        false,
        function ($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            CRUD::addClause('where', 'created_at', '>=', $dates->from);
            CRUD::addClause('where', 'created_at', '<=', $dates->to);
        });

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(VisitorLogAdminRequest::class);

        CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function export() 
    {
        return Excel::download(new VisitorLogAdminExport, 'log.xlsx');
    }

    public function exportCSV() {
        // return Excel::download(new VisitorLogExport, 'log.csv');
        return (new VisitorLogAdminExport)->download('log.csv', \Maatwebsite\Excel\Excel::CSV, [
            'Content-Type' => 'text/csv',
        ]);
    }
}
