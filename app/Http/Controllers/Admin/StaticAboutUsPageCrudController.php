<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StaticAboutUsPageRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class StaticAboutUsPageCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StaticAboutUsPageCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\StaticAboutUsPage::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/static-about-us-page');
        CRUD::setEntityNameStrings('หน้าทั่วไป', 'หน้าทั่วไป');
        CRUD::denyAccess(['create']);
        CRUD::denyAccess(['delete']);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */

        CRUD::addColumn([
            'name' => 'post_title',
            'label' => 'ชื่อหน้า'
        ]);

        // CRUD::addColumn([
        //     'name' => 'code',
        //     'label' => 'ลิ้งค์'
        // ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StaticAboutUsPageRequest::class);

        // CRUD::setFromDb(); // fields

        CRUD::addField([
            'name' => 'post_title', 
            'type' => 'text',
            'label' => 'ชื่อหน้า'
        ]);

        CRUD::addField([
            'name' => 'post_name', 
            'type' => 'text',
            'label' => 'ลิ้งค์'
        ]);

        CRUD::addField([
            'name' => 'post_cover', 
            'type' => 'browse',
            'label' => 'รูปภาพ'
        ]);

        CRUD::addField([
            'name' => 'post_content', 
            'type' => 'ckeditor',
            'options' => [
                'extraPlugins' => config('app.ckeditor_extra_olugins')
            ],
            'label' => 'รายละเอียด'
        ]);

        CRUD::addField([
            'name' => 'post_status', 
            'type' => 'select_from_array',
            'options' => [
                '1' => 'Active',
                '0' => 'Inactive'
            ],
            'label' => 'สถานะ'
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    /**
     * Show the form for creating inserting a new row.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
        $this->crud->hasAccessOrFail('create');

        // prepare the fields you need to show
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->crud->getSaveAction();
        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.add').' '.$this->crud->entity_name;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('vendor.backpack.crud.static_page.create', $this->data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $this->crud->hasAccessOrFail('update');
        // get entry ID from Request (makes sure its the last ID for nested resources)
        $id = $this->crud->getCurrentEntryId() ?? $id;
        $this->crud->setOperationSetting('fields', $this->crud->getUpdateFields());
        // get the info for that entry
        $this->data['entry'] = $this->crud->getEntry($id);
        $this->data['crud'] = $this->crud;
        $this->data['saveAction'] = $this->crud->getSaveAction();
        $this->data['title'] = $this->crud->getTitle() ?? trans('backpack::crud.edit').' '.$this->crud->entity_name;

        $this->data['id'] = $id;

        // load the view from /resources/views/vendor/backpack/crud/ if it exists, otherwise load the one in the package
        return view('vendor.backpack.crud.static_page.edit', $this->data);
    }
}
