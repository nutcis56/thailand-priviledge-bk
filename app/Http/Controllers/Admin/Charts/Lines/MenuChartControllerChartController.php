<?php

namespace App\Http\Controllers\Admin\Charts\Lines;

use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use App\Models\VisitorLog;
/**
 * Class MenuChartControllerChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MenuChartControllerChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels([
            'หน้าหลัก',
            'เกี่ยวกับองค์กร',
            'คลังความรู้',
            'Open Online Course',
            'ข่าวสาร'
        ]);

        // HOME
        $homeData = 1000;

        // Organizaion
        $organizationData = VisitorLog::where('url', 'LIKE', '%about-us%')
                            ->orWhere('url', 'LIKE', '%organizaion%')
                            ->count();

        // Knowledge
        $knowledgeData = VisitorLog::where('url', 'LIKE', '%blog%')
                            ->orWhere('url', 'LIKE', '%faq%')
                            ->orWhere('url', 'LIKE', '%knowledge-list%')
                            ->orWhere('url', 'LIKE', '%job-list%')
                            ->orWhere('url', 'LIKE', '%gallery%')
                            ->count();

        // Course
        $courseData = VisitorLog::where('url', 'LIKE', '%course%')
                                ->count();
                                
        // NEWS
        $newData = VisitorLog::where('url', 'LIKE', '%news%')
                            ->orWhere('url', 'LIKE', '%announce%')
                            ->orWhere('url', 'LIKE', '%calendar%')
                            ->orWhere('url', 'LIKE', '%event%')
                            ->count();
        

        $this->chart->dataset('Total', 'bar', [
            $homeData, $organizationData, $knowledgeData, $courseData, $newData
        ])
        ->color(['rgba(205, 32, 31, 1)', 'rgba(70, 127, 208, 1)', 'rgb(255, 193, 7)', 'rgba(66, 186, 0, 1)', 'rgba(96, 92, 168)'])
        ->backgroundColor(['rgba(205, 32, 31, 0.4)', 'rgba(70, 127, 208, 0.4)' , 'rgba(255, 193, 7, 0.4)', 'rgba(66, 186, 0, 0.4)', 'rgba(96, 92, 168, 0.4)']);

        

        // // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        // $this->chart->load(backpack_url('charts/new-entries'));

        // // // OPTIONAL
        // $this->chart->minimalist(false);
        // $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    // public function data()
    // {
        

    //     $this->chart->dataset('Today', 'bar', [
    //         100
    //     ])
    //     ->color('rgba(205, 32, 31, 1)')
    //     ->backgroundColor('rgba(205, 32, 31, 0.4)');

    //     $this->chart->dataset('Yesterday', 'bar', [
    //         150
    //     ])
    //     ->color('rgba(70, 127, 208, 1)')
    //     ->backgroundColor('rgba(70, 127, 208, 0.4)');

    //     $this->chart->dataset('AF', 'bar', [
    //         70
    //     ])
    //     ->color('rgb(255, 193, 7)')
    //     ->backgroundColor('rgba(255, 193, 7, 0.4)');
    // }
}