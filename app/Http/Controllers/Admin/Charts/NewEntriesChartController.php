<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\User;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use App\Models\Post;
use App\Models\Category;
use App\Models\VisitorLog;
// use Backpack\NewsCRUD\app\Models\Tag;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;

class NewEntriesChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        // MANDATORY. Set the labels for the dataset points
        // $labels = [];
        // for ($days_backwards = 30; $days_backwards >= 0; $days_backwards--) {
        //     if ($days_backwards == 1) {
        //     }
        //     $labels[] = $days_backwards.' days ago';
        // }

        $labels = ['','หน้าหลัก', 'เกี่ยวกับองค์กร', 'คลังความรู้', 'Course', 'ข่าวสาร'];
        $this->chart->labels($labels);

        // HOME
        $homeData = 1000;

        // Organizaion
        $organizationData = VisitorLog::where('url', 'LIKE', '%about-us%')
                            ->orWhere('url', 'LIKE', '%organizaion%')
                            ->count();

        // Knowledge
        $knowledgeData = VisitorLog::where('url', 'LIKE', '%blog%')
                            ->orWhere('url', 'LIKE', '%faq%')
                            ->orWhere('url', 'LIKE', '%knowledge-list%')
                            ->orWhere('url', 'LIKE', '%job-list%')
                            ->orWhere('url', 'LIKE', '%gallery%')
                            ->count();

        // Course
        $courseData = VisitorLog::where('url', 'LIKE', '%course%')
                                ->count();
                                
        // NEWS
        $newData = VisitorLog::where('url', 'LIKE', '%news%')
                            ->orWhere('url', 'LIKE', '%announce%')
                            ->orWhere('url', 'LIKE', '%calendar%')
                            ->orWhere('url', 'LIKE', '%event%')
                            ->count();


        $test = [0, $homeData, $organizationData, $knowledgeData, $courseData, $newData];
        $co = ['rgb(66, 186, 150)', 'rgb(96, 92, 168)', 'rgb(255, 193, 7)', 'rgba(70, 127, 208, 1)', 'rgb(242, 12, 12, 1)'];
        $bgCo = ['rgba(66, 186, 150, 0.4)', 'rgba(96, 92, 168, 0.4)', 'rgba(255, 193, 7, 0.4)', 'rgba(70, 127, 208, 0.4)', 'rgba(242, 12, 12, 0.4)'];

        $this->chart->dataset('หน้าหลัก', 'line', $test)
            ->color($co)
            ->backgroundColor($bgCo);

        // $this->chart->dataset('เกี่ยวกับองค์กร', 'line', $organizationData)
        //     ->color('rgb(96, 92, 168)')
        //     ->backgroundColor('rgba(96, 92, 168, 0.4)');

        // $this->chart->dataset('คลังความรู้', 'line', $knowledgeData)
        //     ->color('rgb(255, 193, 7)')
        //     ->backgroundColor('rgba(255, 193, 7, 0.4)');

        // $this->chart->dataset('Open Online Course', 'line', $courseData)
        //     ->color('rgba(70, 127, 208, 1)')
        //     ->backgroundColor('rgba(70, 127, 208, 0.4)');

        // $this->chart->dataset('ข่าวสาร', 'line', $newData)
        //     ->color('rgb(242, 12, 12, 1)')
        //     ->backgroundColor('rgba(242, 12, 12, 0.4)');

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        // $this->chart->load(backpack_url('charts/new-entries'));

        // // OPTIONAL
        // $this->chart->minimalist(false);
        $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {

        // $categoryKnowledge = \App\Models\Category::where('name->'.app()->getLocale(), 'knowledge')->first();
        // $cateId = (!empty($categoryKnowledge)) ? $categoryKnowledge->id : 0;
        // $postCateIds = \App\Models\PostCate::where('cate_id', $cateId)->get()->toArray();
        // $knowledgeIds = (!empty($postCateIds)) ? array_column($postCateIds, 'post_id') : [0];

        // $categoryJob = \App\Models\Category::where('name->'.app()->getLocale(), 'job')->first();
        // $cateJobId = (!empty($categoryJob)) ? $categoryJob->id : 0;
        // $postCateJobId = \App\Models\PostCate::where('cate_id', $cateJobId)->get()->toArray();
        // $jobIds = (!empty($postCateJobId)) ? array_column($postCateJobId, 'post_id') : [0];

        // $categoryGallery = \App\Models\Category::where('name->'.app()->getLocale(), 'gallery')->first();
        // $cateGalleryId = (!empty($categoryGallery)) ? $categoryGallery->id : 0;
        // $postCateGalleryId = \App\Models\PostCate::where('cate_id', $cateGalleryId)->get()->toArray();
        // $galleryIds = (!empty($postCateGalleryId)) ? array_column($postCateGalleryId, 'post_id') : [0];
        
        // $categoryVdo = \App\Models\Category::where('name->'.app()->getLocale(), 'vdo')->first();
        // $cateGalleryVdoId = (!empty($categoryVdo)) ? $categoryVdo->id : 0;
        // $postCateGalleryVdoId = \App\Models\PostCate::where('cate_id', $cateGalleryVdoId)->get()->toArray();
        // $galleryVdoIds = (!empty($postCateGalleryVdoId)) ? array_column($postCateGalleryVdoId, 'post_id') : [0];

        // $knowledges[] = Post::whereIn('id', $knowledgeIds)->count();
        // $job[] = Post::whereIn('id', $jobIds)->count();
        // $TPCSharingGallery[] = Post::whereIn('id', $galleryIds)->count();
        // $TPCSharingGalleryVdo[] = Post::whereIn('id', $galleryVdoIds)->count();

        // HOME
        $homeData[] = 1000;

        // Organizaion
        $organizationData[] = VisitorLog::where('url', 'LIKE', '%about-us%')
                            ->orWhere('url', 'LIKE', '%organizaion%')
                            ->count();

        // Knowledge
        $knowledgeData[] = VisitorLog::where('url', 'LIKE', '%blog%')
                            ->orWhere('url', 'LIKE', '%faq%')
                            ->orWhere('url', 'LIKE', '%knowledge-list%')
                            ->orWhere('url', 'LIKE', '%job-list%')
                            ->orWhere('url', 'LIKE', '%gallery%')
                            ->count();

        // Course
        $courseData[] = VisitorLog::where('url', 'LIKE', '%course%')
                                ->count();
                                
        // NEWS
        $newData[] = VisitorLog::where('url', 'LIKE', '%news%')
                            ->orWhere('url', 'LIKE', '%announce%')
                            ->orWhere('url', 'LIKE', '%calendar%')
                            ->orWhere('url', 'LIKE', '%event%')
                            ->count();


        

        $this->chart->dataset('หน้าหลัก', 'line', $homeData)
            ->color('rgb(66, 186, 150)')
            ->backgroundColor('rgba(66, 186, 150, 0.4)');

        $this->chart->dataset('เกี่ยวกับองค์กร', 'line', $organizationData)
            ->color('rgb(96, 92, 168)')
            ->backgroundColor('rgba(96, 92, 168, 0.4)');

        $this->chart->dataset('คลังความรู้', 'line', $knowledgeData)
            ->color('rgb(255, 193, 7)')
            ->backgroundColor('rgba(255, 193, 7, 0.4)');

        $this->chart->dataset('Open Online Course', 'line', $courseData)
            ->color('rgba(70, 127, 208, 1)')
            ->backgroundColor('rgba(70, 127, 208, 0.4)');

        $this->chart->dataset('ข่าวสาร', 'line', $newData)
            ->color('rgb(242, 12, 12, 1)')
            ->backgroundColor('rgba(242, 12, 12, 0.4)');
    }
}
