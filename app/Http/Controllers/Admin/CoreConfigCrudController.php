<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CoreConfigRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CoreConfigCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CoreConfigCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\CoreConfig::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/core-config');
        CRUD::setEntityNameStrings('core config', 'core configs');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */

        CRUD::addColumn([
            'name' => 'name',
            'label' => 'ชื่อ'
        ]);

        // CRUD::addColumn([
        //     'name' => 'code',
        //     'label' => 'รหัส'
        // ]);

        CRUD::addColumn([
            'name' => 'group',
            'label' => 'กลุ่ม'
        ]);

        CRUD::addColumn([
            'name' => 'category',
            'label' => 'หมวดหมู่'
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CoreConfigRequest::class);

        // CRUD::setFromDb(); // fields

        $hide_in_array = [
            'App\Http\Controllers\Admin\ConfigMenuCrudController',
            'App\Http\Controllers\Admin\ConfigBigBannerCrudController',
            'App\Http\Controllers\Admin\ConfigIconLinkCrudController',
            'App\Http\Controllers\Admin\ConfigServiceLinkCrudController'
        ];

        CRUD::addField([
            'name' => 'name', 
            'label' => 'ชื่อ',
            'type' => 'text',
            'tab' => '1. รายละเอียดเบื้องต้น',
        ]);

        if (!in_array(get_class($this), $hide_in_array)) {
            CRUD::addField([
                'name' => 'code', 
                'label' => 'รหัสตั้งค่า',
                'type' => 'text',
                'tab' => '1. รายละเอียดเบื้องต้น',
            ]);
        } else {
            CRUD::addField([
                'name' => 'code',
                'type' => 'hidden',
                'value' => 'hidden_value_'.get_Class($this)
            ]);
        }

        CRUD::addField([
            'name' => 'status', 
            'label' => 'สถานะ',
            'type' => 'select_from_array',
            'options' => [
                '1' => 'ใช้งาน',
                '0' => 'ไม่ใช้งาน'
            ],
            'tab' => '1. รายละเอียดเบื้องต้น',
        ]);

        CRUD::addField([
            'name' => 'detail', 
            'label' => 'รายละเอียด',
            'type' => 'ckeditor',
            'options' => [
                'extraPlugins' => config('app.ckeditor_extra_olugins')
            ],
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

        CRUD::addField([
            'name' => 'value', 
            'label' => 'ค่า',
            'type' => 'text',
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

        CRUD::addField([
            'name' => 'link', 
            'label' => 'ลิงค์',
            'type' => 'text',
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

        CRUD::addField([
            'name' => 'target', 
            'label' => 'การเปิดลิงค์',
            'type' => 'select_from_array',
            'options' => [
                '_self' => 'เปิดหน้าเดิม (ค่ามาตราฐาน)',
                '_blank' => 'เปิดหน้าใหม่',
            ],
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

        CRUD::addField([
            'name' => 'icon', 
            'label' => 'ไอคอน',
            'type' => 'browse',
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

        CRUD::addField([
            'name' => 'image', 
            'label' => 'รูปภาพ',
            'type' => 'browse',
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

        if (!in_array(get_class($this), $hide_in_array)) {

            CRUD::addField([
                'name' => 'class', 
                'label' => 'คลาส',
                'type' => 'text',
                'tab' => '3. ตั้งค่าหมวดหมู่',
            ]);

            CRUD::addField([
                'name' => 'level', 
                'label' => 'เลเวล',
                'type' => 'text',
                'tab' => '3. ตั้งค่าหมวดหมู่',
            ]);

            CRUD::addField([
                'name' => 'group', 
                'label' => 'กลุ่ม',
                'type' => 'text',
                'tab' => '3. ตั้งค่าหมวดหมู่',
            ]);

            CRUD::addField([
                'name' => 'category', 
                'label' => 'หมวดหมู่',
                'type' => 'text',
                'tab' => '3. ตั้งค่าหมวดหมู่',
            ]);

            CRUD::addField([
                'name' => 'scope', 
                'label' => 'สโคป',
                'type' => 'text',
                'tab' => '3. ตั้งค่าหมวดหมู่',
            ]);

            

            CRUD::addField([
                'name' => 'ref_sub_id', 
                'type' => 'text',
                'tab' => '4. ตั้งค่า Reference',
            ]);

            CRUD::addField([
                'name' => 'ref_main_id', 
                'type' => 'text',
                'tab' => '4. ตั้งค่า Reference',
            ]);

            CRUD::addField([
                'name' => 'tb_name', 
                'type' => 'text',
                'tab' => '4. ตั้งค่า Reference',
            ]);

            CRUD::addField([
                'name' => 'tb_id', 
                'type' => 'text',
                'tab' => '4. ตั้งค่า Reference',
            ]);

        }

    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
