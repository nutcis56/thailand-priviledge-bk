<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CoreConfigMenuRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CoreConfigCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ConfigMenuCrudController extends CoreConfigCrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \App\Http\Controllers\Admin\Traits\CRUDControllerHelperTrait;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\ConfigMenu::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/core-config-menu');
        CRUD::setEntityNameStrings(trans('backpack::menu.core-config-menu'), trans('backpack::menu.core-config-menu'));
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */

        $main_menu_lists = \App\Models\ConfigMenu::where('level', '<', '2')->pluck('name', 'id')->toArray();
        CRUD::addFilter([
            'name'  => 'sub_id',
            'type'  => 'dropdown',
            'label' => 'ภายใต้เมนู'
        ], 
        $main_menu_lists, 
        function($value) { // if the filter is active
            $this->crud->addClause('where', 'class', $value);
        });

        CRUD::addColumn([
            'name' => 'name',
            'label' => 'ชื่อ'
        ]);

        // CRUD::addColumn([
        //     'name' => 'code',
        //     'label' => 'รหัส'
        // ]);

        // CRUD::addColumn([
        //     'name' => 'group',
        //     'label' => 'กลุ่ม'
        // ]);

        // CRUD::addColumn([
        //     'name' => 'category',
        //     'label' => 'หมวดหมู่'
        // ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CoreConfigMenuRequest::class);

        // CRUD::setFromDb(); // fields

        $hide_in_array = [
            'App\Http\Controllers\Admin\ConfigMenuCrudController',
            'App\Http\Controllers\Admin\ConfigBigBannerCrudController',
            'App\Http\Controllers\Admin\ConfigIconLinkCrudController',
            'App\Http\Controllers\Admin\ConfigServiceLinkCrudController'
        ];

        CRUD::addField([
            'name' => 'category', 
            'label' => 'ตำแหน่งเมนู',
            'type' => 'select_from_array',
            'options' => [
                'top_menu' => 'เมนูหลัก'
            ],
            'default' => 'top_menu',
            'tab' => '1. รายละเอียดเบื้องต้น',
        ]);

        CRUD::addField([
            'name' => 'name', 
            'label' => 'ชื่อ',
            'type' => 'text',
            'tab' => '1. รายละเอียดเบื้องต้น',
        ]);

        // CRUD::addField([
        //     'name' => 'level', 
        //     'label' => 'ลำดับชั้นเมนู เมนูหลัก(1), เมนูย่อย(2)',
        //     'type' => 'text',
        //     'tab' => '1. รายละเอียดเบื้องต้น',
        // ]);
        if ($this->getId()) {
            // not self
            $main_menu_lists = \App\Models\ConfigMenu::where('level', '<', '2')->where('id', '!=', $this->getId())->pluck('name', 'code')->toArray();
        } else {
            $main_menu_lists = \App\Models\ConfigMenu::where('level', '<', '2')->pluck('name', 'code')->toArray();
        }
       
       
        CRUD::addField([
            'name' => 'class', 
            'label' => 'อยู่ภายใต้เมนู (เลือกกรณีเป็นเมนูย่อย)',
            'type' => 'select_from_array',
            'options' => $main_menu_lists,
            'tab' => '1. รายละเอียดเบื้องต้น',
        ]);

        CRUD::addField([
            'name' => 'scope', 
            'label' => 'การเรียงลำดับ',
            'type' => 'number',
            'tab' => '1. รายละเอียดเบื้องต้น',
        ]);

        if (!in_array(get_class($this), $hide_in_array)) {
            CRUD::addField([
                'name' => 'code', 
                'label' => 'รหัสตั้งค่า',
                'type' => 'text',
                'tab' => '1. รายละเอียดเบื้องต้น',
            ]);
        } else {
            CRUD::addField([
                'name' => 'code',
                'type' => 'hidden',
                'value' => 'hidden_value_'.get_Class($this)
            ]);
        }

        CRUD::addField([
            'name' => 'status', 
            'label' => 'สถานะ',
            'type' => 'select_from_array',
            'options' => [
                '1' => 'ใช้งาน',
                '0' => 'ไม่ใช้งาน'
            ],
            'tab' => '1. รายละเอียดเบื้องต้น',
        ]);

        CRUD::addField([
            'name' => 'link', 
            'label' => 'ระบุ URL ที่ต้องการเชื่อมโยง เช่น http://www.google.com',
            'type' => 'text',
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

        CRUD::addField([
            'name' => 'target', 
            'label' => 'การเปิดลิงค์',
            'type' => 'select_from_array',
            'options' => [
                '_self' => 'เปิดหน้าเดิม (ค่ามาตราฐาน)',
                '_blank' => 'เปิดหน้าใหม่',
            ],
            'tab' => '2. การตั้งค่าต่างๆ',
        ]);

    }
}
