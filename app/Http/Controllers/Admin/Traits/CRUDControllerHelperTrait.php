<?php

namespace App\Http\Controllers\Admin\Traits;

trait CRUDControllerHelperTrait
{
    /**
     * Get the mode of CRUD (edit, add, view, index, etc.)
     *
     * @return void|string
     */
    public function getMode()
    {
        $path = \Route::currentRouteName();
        // dd($this->crud);
        // $entity = strtolower($this->crud->entity_name);
        // if (preg_match('/^crud\.'.$entity.'\.(.+?)$/', $path, $out)) {
        //     return $out[1];
        // }
        if (preg_match("/.edit/", $path, $out)) {
            return 'edit';
        }
        return null;
    }

    /**
     * Get the ID - if any (eg. only in edit/view mode)
     *
     * @return void|int
     */
    public function getId()
    {
        $parameters = \Route::current()->parameters();
        if (isset($parameters['id']) && $parameters['id']) {
            return $parameters['id'];
        }
        return null;
    }

    public function addCustomSaveActions() {
        $this->crud->addSaveAction([
            'name' => 'save_and_list',
            'redirect' => function($crud, $request, $itemId) {
                return $crud->route;
            }, // what's the redirect URL, where the user will be taken after saving?
        
            // OPTIONAL:
            'button_text' => 'Custom save message', // override text appearing on the button
            // You can also provide translatable texts, for example:
            // 'button_text' => trans('backpack::crud.save_action_one'),
            'visible' => function($crud) {
                return true;
            }, // customize when this save action is visible for the current operation
            'referrer_url' => function($crud, $request, $itemId) {
                return $crud->route;
            }, // override http_referrer_url
            'order' => 1, // change the order save actions are in
        ]);
    }
}