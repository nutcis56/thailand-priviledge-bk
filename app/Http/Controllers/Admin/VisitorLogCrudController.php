<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VisitorLogRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Exports\VisitorLogExport;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class VisitorLogCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VisitorLogCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\VisitorLog::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/visitor-log');
        CRUD::setEntityNameStrings('ประวัติการใช้งาน', 'ประวัติการใช้งาน');
        CRUD::denyAccess('operation');
        CRUD::denyAccess(['update', 'create', 'delete']);
        CRUD::addButtonFromView('before-top', 'export_excel', 'export_excel', 'beginning');
        CRUD::addButtonFromView('before-top', 'export_csv', 'export_csv', 'beginning');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'ip',
            'label' => 'IP'
        ]);

        CRUD::addColumn([
            'name' => 'url',
            'label' => 'URL'
        ]);

        CRUD::addColumn([
            'name' => 'visitor_id',
            'label' => 'ผู้ใช้งาน',
            'entity' => 'visitor',
            'attribute' => 'email',
        ]);

        CRUD::addColumn([
            'name' => 'created_at',
            'label' => 'วันที่'
        ]);

        CRUD::addFilter([ // daterange filter
                'type' => 'date_range',
                'name' => 'date_range',
                'label'=> 'Date range',
                'locale' => [
                    'format' => 'DD/MM/YYYY'
                ]
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                CRUD::addClause('where', 'created_at', '>=', $dates->from);
                CRUD::addClause('where', 'created_at', '<=', $dates->to);
        });
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(VisitorLogRequest::class);

        CRUD::field('browser');
        CRUD::field('created_at');
        CRUD::field('device');
        CRUD::field('headers');
        CRUD::field('id');
        CRUD::field('ip');
        CRUD::field('languages');
        CRUD::field('method');
        CRUD::field('platform');
        CRUD::field('referer');
        CRUD::field('request');
        CRUD::field('updated_at');
        CRUD::field('url');
        CRUD::field('useragent');
        CRUD::field('visitable_id');
        CRUD::field('visitable_type');
        CRUD::field('visitor_id');
        CRUD::field('visitor_type');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function export() 
    {
        return Excel::download(new VisitorLogExport, 'log.xlsx');
    }

    public function exportCSV() {
        // return Excel::download(new VisitorLogExport, 'log.csv');
        return (new VisitorLogExport)->download('log.csv', \Maatwebsite\Excel\Excel::CSV, [
            'Content-Type' => 'text/csv',
        ]);
    }
}
