<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\CourseLessonRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CourseLessonCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CourseLessonCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\CourseLesson::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/course-lesson');
        CRUD::setEntityNameStrings('course lesson', 'course lessons');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
        $courses = \App\Models\Course::pluck('post_title', 'id')->toArray();
        CRUD::addFilter([
            'name'  => 'doc_tb_id',
            'type'  => 'dropdown',
            'label' => 'Course'
          ], 
          $courses, 
          function($value) { // if the filter is active
            $this->crud->addClause('where', 'doc_tb_id', $value);
          });

        CRUD::addColumn([
            'name' => 'name', 
            'type' => 'text',
            'label' => 'ชื่อ'
        ]); 

        CRUD::addColumn([
            'name' => 'file', 
            'type' => 'text',
            'label' => 'ไฟล์'
        ]); 

        // CRUD::addColumn([
        //     'name' => 'excerpt', 
        //     'type' => 'text',
        //     'label' => 'รายละเอียดย่อ'
        // ]);
        
        CRUD::addColumn([
            'name' => 'status', 
            'type' => 'text',
            'label' => 'สถานะ'
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CourseLessonRequest::class);

        // CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */

        $courses = \App\Models\Course::pluck('post_title', 'id')->toArray();
        $doc_tb_id_field = [
            'name' => 'doc_tb_id',
            'label' => 'Course',
            'type' => 'select_from_array',
            'options' => $courses,
        ];
        if (isset($_REQUEST['doc_tb_id'])) {
            $doc_tb_id_field['value'] = $_REQUEST['doc_tb_id'];
        }
        CRUD::addField($doc_tb_id_field);

        CRUD::addField([
            'name' => 'name', 
            'type' => 'text',
            'label' => 'ชื่อ'
        ]);

        CRUD::addField([   // Browse multiple
            'name'          => 'file',
            'type'          => 'browse',
            'label' => 'ไฟล์',
            'multiple'   => false, // enable/disable the multiple selection functionality
            // 'sortable'   => false, // enable/disable the reordering with drag&drop
            // 'mime_types' => ['video/mp4', 'video/quicktime', 'video/webm', 'video/x-ms-wmv', 'video/x-msvideo'], // visible mime prefixes; ex. ['image'] or ['application/pdf']
        ]);

        CRUD::addField([
            'name' => 'youtube_link', 
            'type' => 'text',
            'label' => 'ลิ้งค์ Youtube'
        ]);

        // CRUD::addField([
        //     'name' => 'excerpt', 
        //     'type' => 'textarea',
        //     'label' => 'รายละเอียดย่อ'
        // ]);

        // CRUD::addField([
        //     'name' => 'detail', 
        //     'type' => 'ckeditor',
        //     'label' => 'รายละเอียด',
        //         'options' => [
        //             'extraPlugins' => config('app.ckeditor_extra_olugins')
        //         ]
        // ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupReorderOperation()
    {
        CRUD::set('reorder.label', 'name');
        CRUD::set('reorder.max_level', 1);
    }

    /**
     * Get data by ajax
     * 
     */
    public function getCourseByIdAjax(Request $request) {
        $data = [];

        if (isset($request->id)) {
            $id = $request->id;
            $data = \App\Models\CourseLesson::where('doc_tb_id', $id)->orderBy('lft', 'ASC')->get();
        }
        
        return response()->json($data);
    }

    /**
     * Delete data by ajax
     * 
     */
    public function deleteCourseByIdAjax(Request $request) {
        $data = [];

        if (isset($request->id)) {
            $id = $request->id;
            $data = \App\Models\CourseLesson::find($id);
            if ($data) {
                $data->delete();
                return response()->json(['status' => true], 204);
            } else {
                return response()->json(['status' => false], 404);
            }
        }
    }
}
