<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OChartSecondRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class OChartSecondCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OChartSecondCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\OChartSecond::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/o-chart-second');
        CRUD::setEntityNameStrings('ฝ่าย', 'ฝ่าย');
        CRUD::setListView('backends.ochart_secounds.list');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'name'
        ]);

        foreach(CRUD::buttons() as $val) {
            if($val->name == 'update') {
                $val->content = 'crud::buttons.organization_secound_update';
            }
           
        }
        
        CRUD::addButtonFromView('line', 'organization_secound_to_third', 'organization_secound_to_third', 'beginning');
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OChartSecondRequest::class);

        CRUD::addField([
            'name' => 'name', 
            'label' => 'ชื่อ',
            'type' => 'text'
        ]);

        CRUD::addField([
            'name' => 'detail', 
            'label' => 'รายละเอียด',
            'type' => 'ckeditor',
            'options' => [
                'extraPlugins' => config('app.ckeditor_extra_olugins')
            ]
        ]);

        $ochartMain = \App\Models\CoreConfig::find($_REQUEST['parent_id']);
        CRUD::addField([
            'name' => 'show_department', 
            'label' => 'สายงาน',
            'type' => 'text',
            'default' => $ochartMain->name,
            'attributes' => [
                'readonly' => 'readonly'
            ]
        ]);

        CRUD::addField([
            'name' => 'parent_id', 
            'label' => 'สายงาน',
            'type' => 'hidden',
            'default' => $_REQUEST['parent_id'],
            'attributes' => [
                'readonly' => 'readonly'
            ]
        ]);

        // $parents = \App\Models\OChartMain::pluck('name', 'id')->toArray();
        // CRUD::addField([
        //     'name' => 'parent_id', 
        //     'label' => 'สายงาน',
        //     'type' => 'select_from_array',
        //     'options' => $parents
        // ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
