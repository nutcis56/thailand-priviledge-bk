<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\TPCSharingVideoRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class TPCSharingVideoCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class TPCSharingVideoCrudController extends \App\Http\Controllers\Admin\PostCrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\TPCSharingVideo::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/t-p-c-sharing-video');
        CRUD::setEntityNameStrings('t p c sharing video', 't p c sharing videos');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        // CRUD::setFromDb(); // columns

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */

        $o_ids = \App\Models\OChartThird::pluck('name', 'id')->toArray();
        CRUD::addFilter([
                'name'  => 'position_id',
                'type'  => 'dropdown',
                'label' => 'แผนก'
            ], 
            $o_ids, 
            function($value) { // if the filter is active
                $this->crud->addClause('where', 'position_id', $value);
            }
        );

        // CRUD::setFromDb(); // columns
        CRUD::addColumn([
            'name' => 'post_title',
            'label' => 'หัวข้อ', 
            'type' => 'text'
        ]);

        // CRUD::addColumn([
        //     'name' => 'post_excerpt', 
        //     'label' => 'รายละเอียดย่อ', 
        //     'type' => 'text'
        // ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(TPCSharingVideoRequest::class);

        // CRUD::setFromDb(); // fields

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
        CRUD::addField([
            'name' => 'post_title', 
            'label' => 'หัวข้อ',
            'type' => 'text'
        ]);
        CRUD::addField([
            'name' => 'post_name', 
            'label' => 'URL Friendly',
            'type' => 'text'
        ]);
        CRUD::addField([
            'name' => 'post_tags',
            'label' => 'แท็ก (สามารถเลือกได้มากกว่า 1 รายการ)',
            'type' => 'select2freetext',
            'data' => '\App\Models\Tag',
            'key_data' => 'name',
            'key_value' => 'name',
        ]);

        CRUD::addField([
            'name' => 'post_cover', 
            'label' => 'รูปภาพปก',
            'type' => 'image',
            'hint' => 'รูปภาพปกควรมีขนาด 1200x648 PX'
        ]);

        CRUD::addField([   // Browse multiple
            'name'          => 'files',
            'label'         => 'VDO',
            'type'          => 'gallery.multiple_video',
            'columns'         => [
                'title'  => 'ชื่อวีดีโอ',
                'image'  => 'วีดีโอ',
                'youtube_link' => 'ลิ้งค์ Youtube'
            ],
            'hint' => '- รองรับไฟล์ VDO นามสกุล mp3,mp4,wmv,mov,avi และ Youtube link <br/>
            - ไฟล์ VDO ควรมีขนาดไฟล์ไม่เกิน 10 MB'
        ]);

        // CRUD::addField([
        //     'name' => 'post_excerpt', 
        //     'label' => 'รายละเอียดแบบย่อ',
        //     'type' => 'textarea',
        // ]);
        
        // CRUD::addField([
        //     'name' => 'post_content', 
        //     'label' => 'รายละเอียด',
        //     'type' => 'ckeditor',
        //         'options' => [
        //             'extraPlugins' => config('app.ckeditor_extra_olugins')
        //         ]
        // ]);

        if (get_class($this) == 'App\Http\Controllers\Admin\PostCrudController') {
            CRUD::addField([
                'name' => 'category', 
                'label' => 'Category',
                'type' => 'select2_multiple',
                'entity' => 'category',
                'model' => 'App\Models\Category',
                'attribute' => 'name',
                'pivot' => true
            ]);
        }
        
        CRUD::addField([
            'name' => 'post_status', 
            'label' => 'สถานะ',
            'type' => 'select_from_array',
            'options' => [
                1 => 'ใช้งาน',
                0 => 'ไม่ใช้งาน'
            ],
            'default' => 1
        ]);
        CRUD::addField([
            'name' => 'ping_status', 
            'label' => 'เลือกเมื่อต้องการปล่อย API',
            'type' => 'select_from_array',
            'options' => [
                1 => 'ใช้งาน',
                0 => 'ไม่ใช้งาน'
            ],
            'default' => 1
        ]);
        // CRUD::addField([
        //     'name' => 'post_sort', 
        //     'label' => 'ลำดับ',
        //     'type' => 'number',
        //     'default' => 1
        // ]);
        CRUD::addField([
            'name' => 'post_date', 
            'label' => 'วันที่',
            'type' => 'datetime',
            'default' => date('Y-m-d H:i:s')
        ]);

        CRUD::addField([
            'label' => 'แผนก',
            'type' => 'post_position',
            'name' => 'position_id',
            'force_default' => true
        ]);
      
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
