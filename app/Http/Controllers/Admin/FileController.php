<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{

    /**
     * Get uploads file size 
     * 
     * @param   Request     $request
     * 
     */
    public function getUploadFileSizeFromStaticPath(Request $request) {
        if ($request->path) {
            $size = Storage::disk('public_path')->size($request->path);
            return response()->json([
                'status' => true,
                'static_path' => $request->path,
                'size' => $size 
            ]);
        }
        return response()->json([
            'status' => false
        ]);
    }
}
