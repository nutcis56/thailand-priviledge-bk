<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OChartMainRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class OChartMainCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OChartMainCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\OChartMain::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/o-chart-main');
        CRUD::setEntityNameStrings('สายงาน', 'สายงาน');
        
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'id',
            'label' => 'รหัส'
        ]);

        CRUD::addColumn([
            'name' => 'name',
            'label' => 'ชื่อ'
        ]);

        CRUD::addButtonFromView('line', 'organization_department', 'organization_department', 'beginning');
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OChartMainRequest::class);

        CRUD::addField([
            'name' => 'name', 
            'label' => 'ชื่อ',
            'type' => 'text'
        ]);

        CRUD::addField([
            'name' => 'detail', 
            'label' => 'รายละเอียด',
            'type' => 'ckeditor',
            'options' => [
                'extraPlugins' => config('app.ckeditor_extra_olugins')
            ]
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
