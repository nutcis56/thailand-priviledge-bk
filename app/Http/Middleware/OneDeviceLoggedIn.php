<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class OneDeviceLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $user = \Auth::guard('backpack')->user();
        if ($user) {
            if ($user->isSessionValid()) {
                $user->inCreaseSessionTimeout();
            } else {
                \Auth::guard('backpack')->logout();
                return redirect()->route('frontend.auth.login');
            }
        }
        return $next($request);
    }
}
