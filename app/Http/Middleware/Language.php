<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Session::has('language')) {
            $locale = Session::get('language'); // ถ้ามีอยู่ก็ดึงค่าออกมาเก็บไว้
            App::setLocale($locale);
        } else {
            Session::put('language', config('app.locale'));
            App::setLocale(config('app.locale'));
        }

        return $next($request);
    }
}
