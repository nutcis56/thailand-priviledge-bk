<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class BasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        // dd($request);

        // $user = backpack_auth();
        // dd($user);
        $user = \Auth::guard('backpack')->user();
        // dd($user);
        if (!$user) {
            return redirect()->route('frontend.auth.login');
        }

        return $next($request);
    }
    
}
