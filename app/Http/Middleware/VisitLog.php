<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use \Shetabit\Visitor\Middlewares\LogVisits as MainVisitLog;
use Illuminate\Database\Eloquent\Model;
use App\Models\VisitorLog;
use App\Models\PostVisilog;

class VisitLog extends MainVisitLog
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $logHasSaved = false;
        

        // create log for first binded model
        foreach ($request->route()->parameters() as $parameter) {
            if ($parameter instanceof Model) {
                $dataResponse =  visitor()->visit($parameter);
                $this->updateLog($dataResponse);
                $logHasSaved = true;

                break;
            }
        }

        // create log for normal visits
        if (!$logHasSaved) {
            $dataResponse = visitor()->visit();
            $this->updateLog($dataResponse);
        }

        return $next($request);
    }


    function updateLog($dataResponse) {
        //* GET ROUTE NAME AND PARAMETER REQUEST
        $routeName = \Request::route()->getName();
        $params = \Request::route()->parameters();
      
        if(\array_key_exists('slug', $params)) {
            if(is_numeric($params['slug'])) {
                $id = $params['slug'];
            }else {
                $postData = PostVisilog::where('post_name', $params['slug'])->first();
                $id = (!empty($postData)) ? $postData->id : 0;
            }
            
        }else if(\array_key_exists('id', $params)) {
            $id = $params['id'];
        }
        

        
        //* FIX CHECK ROUTE NAME IN REQUEST 
        $fixedRoute = [
            'knowledgeDetail',
            'jobDetail',
            'newDetail',
            'calendarDetail',
            'courseDetail',
            'courseLesson',
            'announceDetail',
            'eventActivityDetail',
            'gallertListIndex',
            'videoListIndex'
            
        ];
        
        //* ROUTE NAME IN FIX ROUTE UPDATE VISITORLOG 
        if(in_array($routeName, $fixedRoute)) {
            $data['visitable_type'] = $routeName;
            $data['visitable_id'] = $id;

            VisitorLog::where('id', $dataResponse->id)->update($data);
        }
    }
}
