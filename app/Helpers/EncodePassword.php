<?php

namespace App\Helpers;

// https://stackoverflow.com/questions/41222162/encrypt-in-php-openssl-and-decrypt-in-javascript-cryptojs
// 
class EncodePassword
{
    /**
     * Main function for encode password
     * 
     * @param   string  $password
     * @return  array    $encoded
     * 
     */
    public function getEncodePassword($password) {
        $passphrase = $this->getPassphrase();
        return $this->CryptoJSAesEncrypt($passphrase, $password);
    }

    /**
     * Get passphrase for encode password and sent to another third party
     * 
     * @return  string
     */
    private function getPassphrase() {
        return config('app.password_passphrase');
    }

    /**
     * Get Raw password of each user
     * Password has been stored by session after login km project
     * 
     * @return  string  $password
     */
    private function getRawPassword() {
        $password = request()->session()->get('password');
        return $password;
    }

    private function CryptoJSAesEncrypt($passphrase, $plain_text){

        $salt = openssl_random_pseudo_bytes(256);
        $iv = openssl_random_pseudo_bytes(16);
        //on PHP7 can use random_bytes() istead openssl_random_pseudo_bytes()
        //or PHP5x see : https://github.com/paragonie/random_compat
    
        $iterations = 999;  
        $key = hash_pbkdf2("sha512", $passphrase, $salt, $iterations, 64);
    
        $encrypted_data = openssl_encrypt($plain_text, 'aes-256-cbc', hex2bin($key), OPENSSL_RAW_DATA, $iv);
    
        $data = array("ciphertext" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "salt" => bin2hex($salt));

        return $data;

        // to use in javascript encode before send and decode in javascript later
        // return json_encode($data);
    }
}
