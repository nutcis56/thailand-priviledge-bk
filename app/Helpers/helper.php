<?php 

function getFormatDateThai($date) {
    $monthThai = [
        '01' => 'มกราคม',
        '02' => 'กุมภาพันธ์',
        '03' => 'มีนาคม',
        '04' => 'เมษายน',
        '05' => 'พฤษภาคม',
        '06' => 'มิถุนายน',
        '07' => 'กรกฎาคม',
        '08' => 'สิงหาคม',
        '09' => 'กันยายน',
        '10' => 'ตุลาคม',
        '11' => 'พฤศจิกายน',
        '12' => 'ธันวาคม',
    ];

    $dateExplode = explode('-', $date);
    return "{$dateExplode[2]} {$monthThai[$dateExplode[1]]} {$dateExplode[0]}";
}


function getDepartment($id) {
   
    $model = app()->make('App\Models\CoreConfig');
   
    return $model->find($id);
}