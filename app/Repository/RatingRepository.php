<?php 

namespace App\Repository;

use App\Models\Rating;
use App\Models\RatingScore;

class RatingRepository
{
    public function checkRating($rateTb, $rateId) {
        return Rating::where('rate_tb', $rateTb)
                ->where('rate_row_id', $rateId)
                ->first();
    }

    

    public function createRatingGetId($data) {
        return Rating::insertGetId($data);
    }

    public function checkRatingScoreByUser($ratingId, $userId) {
        return RatingScore::where('rating_id', $ratingId)
                ->where('customer_id', $userId)
                ->first();
    }

    public function updateRatingScore($id, $data) {
        return RatingScore::where('id', $id)
                ->update($data);
    }

    public function createRatingScore($data) {
        return RatingScore::insert($data);
    }

    public function getRatingScoreById($ratingId) {
        return RatingScore::where('rating_id', $ratingId)
                ->get();
    }
}