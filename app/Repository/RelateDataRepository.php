<?php 

namespace App\Repository;

use App\Models\RelateData;

class RelateDataRepository
{
    public function listRelate($type, $typeTb, $typeId) {
        return RelateData::where('type', $type)
                ->where('type_tb', $typeTb)
                ->where('type_id', $typeId)
                ->get();
    }
}