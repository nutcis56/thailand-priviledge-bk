<?php 

namespace App\Repository;

use App\Models\PostCate;

class PostCateRepository
{
    public function create($data) {
        return PostCate::insert($data);
    }

    public function findCategoryByPostId($postId) {
        return PostCate::where('post_id', $postId)->first();
    }
}