<?php 

namespace App\Repository;

use App\Models\WishList;

class WishListRepository
{
    
    public function create($data) {
        return WishList::insert($data);
    }

    public function checkByUser($userId, $id, $type) {
        return WishList::where('user_id', $userId)
                ->where('post_id', $id)
                ->where('type', $type)
                ->first();
    }

    public function listByUser($userId, $type) {
        return WishList::where('user_id', $userId)->where('type', $type)->get();
    }
    

    public function delete($id) {
        return WishList::where('id', $id)->delete();
    }

}