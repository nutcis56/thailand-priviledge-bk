<?php 

namespace App\Repository;

use App\Models\Post;
use App\Models\CoreConfig;

class PostRepository
{
    public function listCategory($id, $type = '') {
        
        if($type == 'job') {
            $sectionInDepartmentId = $this->getSectionInDepartment();
            return Post::whereIntegerInRaw('id', $id)
                    ->where('post_name', '!=', 'job_km_file')
                    ->whereIntegerInRaw('position_id', $sectionInDepartmentId)
                    ->with('relate', function($q) {
                        $q->with('productInfo');
                        // $q->where('type_tb', 'posts');
                        // $q->where('type', 'posts_relate');
                        $q->orderBy('sort_orders', 'asc');
                        $q->limit(3);
                        $q->get();
                    })
                    ->withCount('visitlog')
                    ->withCount(['like' => function($q) {
                        $q->where('type', 'like');
                    }])
                    ->where('post_status', '1')
                    ->with('position')
                    ->with('userInfo')
                    ->orderBy('post_priority', 'DESC')
                    ->orderBy('post_sort','ASC')
                    ->orderBy('id','DESC')
                    ->get();
        }else if($type == 'jobFile') {
            $sectionInDepartmentId = $this->getSectionInDepartment();

            return Post::whereIntegerInRaw('id', $id)
                    ->where('post_name','job_km_file')
                    ->whereIntegerInRaw('position_id', $sectionInDepartmentId)
                    ->with('relate', function($q) {
                        $q->with('productInfo');
                        // $q->where('type_tb', 'posts');
                        // $q->where('type', 'posts_relate');
                        $q->orderBy('sort_orders', 'asc');
                        $q->limit(3);
                        $q->get();
                    })
                    ->withCount('visitlog')
                    ->withCount(['like' => function($q) {
                        $q->where('type', 'like');
                    }])
                    ->where('post_status', '1')
                    ->with('position')
                    ->with('userInfo')
                    ->orderBy('post_priority', 'DESC')
                    ->orderBy('post_sort','ASC')
                    ->orderBy('id','DESC')
                    ->get();
        }else if($type == 'blog') {
            return  Post::whereIntegerInRaw('id', $id)
                        ->withCount('visitlog')
                        ->withCount(['like' => function($q) {
                            $q->where('type', 'like');
                        }])
                        ->where('post_status', '1')
                        ->with('position')
                        ->with('userInfo')
                        ->orderBy('post_date', 'DESC')
                        ->get();
        }else if($type == 'announce') {
            return  Post::whereIntegerInRaw('id', $id)
                ->with('relate', function($q) {
                    $q->with('productInfo');
                    // $q->where('type_tb', 'posts');
                    // $q->where('type', 'posts_relate');
                    $q->orderBy('sort_orders', 'asc');
                    $q->limit(3);
                    $q->get();
                })
                ->withCount('visitlog')
                ->withCount(['like' => function($q) {
                    $q->where('type', 'like');
                }])
                ->where('post_status', '1')
                ->with('position')
                ->with('userInfo')
                ->orderBy('post_date', 'DESC')
                // ->orderBy('post_priority', 'DESC')
                // ->orderBy('post_sort','ASC')
                ->orderBy('id','DESC')
                ->get();
        }

        return  Post::whereIntegerInRaw('id', $id)
                ->with('relate', function($q) {
                    $q->with('productInfo');
                    // $q->where('type_tb', 'posts');
                    // $q->where('type', 'posts_relate');
                    $q->orderBy('sort_orders', 'asc');
                    $q->limit(3);
                    $q->get();
                })
                ->withCount('visitlog')
                ->withCount(['like' => function($q) {
                    $q->where('type', 'like');
                }])
                ->where('post_status', '1')
                ->with('position')
                ->with('userInfo')
                ->orderBy('post_sort','ASC')
                ->orderBy('id','DESC')
                ->get();
        
    }

    public function listCategoryMyknowledge($id, $type = '') {
        
        $user = \Auth::guard('backpack')->user();

        if($type == 'job') {
            return Post::whereIntegerInRaw('id', $id)
                    ->where('post_name', '!=', 'job_km_file')
                    ->where('created_by', $user->id)
                    ->where('post_status', '1')
                    ->with('relate', function($q) {
                        $q->with('productInfo');
                        // $q->where('type_tb', 'posts');
                        // $q->where('type', 'posts_relate');
                        $q->orderBy('sort_orders', 'asc');
                        $q->limit(3);
                        $q->get();
                    })
                    ->with('position')
                    ->with('userInfo')
                    ->withCount('visitlog')
                    ->withCount(['like' => function($q) {
                        $q->where('type', 'like');
                    }])
                    ->orderBy('post_priority', 'DESC')
                    ->orderBy('post_sort','ASC')
                    ->orderBy('id','DESC')
                    ->get();
        }else if($type == 'jobFile') {
            return Post::whereIntegerInRaw('id', $id)
                    ->where('post_name','job_km_file')
                    ->where('created_by', $user->id)
                    ->where('post_status', '1')
                    ->with('relate', function($q) {
                        $q->with('productInfo');
                        // $q->where('type_tb', 'posts');
                        // $q->where('type', 'posts_relate');
                        $q->orderBy('sort_orders', 'asc');
                        $q->limit(3);
                        $q->get();
                    })
                    ->withCount('visitlog')
                    ->withCount(['like' => function($q) {
                        $q->where('type', 'like');
                    }])
                    ->with('position')
                    ->with('userInfo')
                    ->orderBy('post_priority', 'DESC')
                    ->orderBy('post_sort','ASC')
                    ->orderBy('id','DESC')
                    ->get();
        }


        return  Post::whereIntegerInRaw('id', $id)
                ->where('created_by', $user->id)
                ->where('post_status', '1')
                ->with('relate', function($q) {
                    $q->with('productInfo');
                    // $q->where('type_tb', 'posts');
                    // $q->where('type', 'posts_relate');
                    $q->orderBy('sort_orders', 'asc');
                    $q->limit(3);
                    $q->get();
                })
                ->withCount('visitlog')
                ->withCount(['like' => function($q) {
                    $q->where('type', 'like');
                }])
                ->with('position')
                ->with('userInfo')
                ->orderBy('post_priority', 'DESC')
                ->orderBy('post_sort','ASC')
                ->orderBy('id','DESC')
                ->get();
        
    }

    public function listCategorySearch($id, $keyword, $type = '') {

        if($type == 'job') {
            $sectionInDepartmentId = $this->getSectionInDepartment();
            return Post::where(function($q) use($id, $keyword, $sectionInDepartmentId) {
                $q->where('post_name', '!=', 'job_km_file');
                $q->where("post_title", "LIKE", "%{$keyword}%");
                $q->whereIntegerInRaw('position_id', $sectionInDepartmentId);
                $q->whereIntegerInRaw("id", $id);
            })
            ->where('post_status', '1')
            ->withCount('visitlog')
            ->withCount(['like' => function($q) {
                $q->where('type', 'like');
            }])
            ->orderBy('post_priority', 'DESC')
            ->orderBy('post_sort','ASC')
            ->orderBy('id','DESC')
            ->get();
        }else if($type == 'blog') {
            return Post::where(function($q) use($id, $keyword) {
                $q->where("post_name", "LIKE", "%{$keyword}%");
                $q->whereIntegerInRaw("id", $id);
            })
            ->orWhere(function($q) use($id, $keyword) {
                $q->where("post_title", "LIKE", "%{$keyword}%");
                $q->whereIntegerInRaw("id", $id);
            })
            ->where('post_status', '1')
            ->withCount('visitlog')
            ->withCount(['like' => function($q) {
                $q->where('type', 'like');
            }])
            ->orderBy('post_date', 'DESC')
            ->get();
        }else if($type == 'announce') {
            return Post::where(function($q) use($id, $keyword) {
                $q->where("post_name", "LIKE", "%{$keyword}%");
                $q->whereIntegerInRaw("id", $id);
            })
            ->orWhere(function($q) use($id, $keyword) {
                $q->where("post_title", "LIKE", "%{$keyword}%");
                $q->whereIntegerInRaw("id", $id);
            })
            ->where('post_status', '1')
            ->withCount('visitlog')
            ->withCount(['like' => function($q) {
                $q->where('type', 'like');
            }])
            ->orderBy('post_priority', 'DESC')
            ->orderBy('post_sort','ASC')
            ->orderBy('id','DESC')
            ->get();
        }


        return Post::where(function($q) use($id, $keyword) {
            $q->where("post_name", "LIKE", "%{$keyword}%");
            $q->whereIntegerInRaw("id", $id);
        })
        ->orWhere(function($q) use($id, $keyword) {
            $q->where("post_title", "LIKE", "%{$keyword}%");
            $q->whereIntegerInRaw("id", $id);
        })
        ->where('post_status', '1')
        ->withCount('visitlog')
        ->withCount(['like' => function($q) {
            $q->where('type', 'like');
        }])
        ->orderBy('post_sort','ASC')
        ->orderBy('id','DESC')
        ->get();
    }

    public function searchTag($id, $keyword, $position = '') {

        if($position != '') {
            return Post::where(function($q) use($id, $keyword, $position) {
                $q->where("post_tags", "LIKE", "%{$keyword}%");
                $q->where('position_id', $position);
                $q->whereIntegerInRaw("id", $id);
            })
            ->where('post_status', '1')
            ->orderBy('post_priority', 'DESC')
            ->orderBy('post_sort','ASC')
            ->orderBy('id','DESC')
            ->get();
        }

        return Post::where(function($q) use($id, $keyword) {
            $q->where("post_tags", "LIKE", "%{$keyword}%");
            $q->whereIntegerInRaw("id", $id);
        })
        ->where('post_status', '1')
        ->orderBy('post_priority', 'DESC')
        ->orderBy('post_sort','ASC')
        ->orderBy('id','DESC')
        ->get();
    }

    public function searchTagJob($id, $keyword, $position = '') {
        $sectionInDepartmentId = $this->getSectionInDepartment();
        if($position != '') {
            return Post::where(function($q) use($id, $keyword, $position) {
                $q->where('post_name', '!=', 'job_km_file');
                $q->where("post_tags", "LIKE", "%{$keyword}%");
                $q->where('position_id', $position);
                $q->whereIntegerInRaw("id", $id);
            })
            ->where('post_status', '1')
            ->orderBy('post_priority', 'DESC')
            ->orderBy('post_sort','ASC')
            ->orderBy('id','DESC')
            ->get();
        }

        
        return Post::where(function($q) use($id, $keyword, $sectionInDepartmentId) {
            $q->where('post_name', '!=', 'job_km_file');
            $q->where("post_tags", "LIKE", "%{$keyword}%");
            $q->whereIntegerInRaw("id", $id);
            $q->whereIntegerInRaw('position_id', $sectionInDepartmentId);
        })
        ->where('post_status', '1')
        ->orderBy('post_priority', 'DESC')
        ->orderBy('post_sort','ASC')
        ->orderBy('id','DESC')
        ->get();
    }

    public function listCategorySearchAndCategory($id, $keyword, $category, $type = "") {
        

        if($type == 'blog') {
          
            return Post::where(function($q) use($id, $keyword, $category) {
                $q->where("post_name", "LIKE", "%{$keyword}%");
                $q->whereIntegerInRaw("id", $id);
            })
            ->orWhere(function($q) use($id, $keyword, $category) {
                $q->where("post_title", "LIKE", "%{$keyword}%");
                $q->whereIntegerInRaw("id", $id);
            })
            ->where('post_status', '1')
            ->withCount(['like' => function($q) {
                $q->where('type', 'like');
            }])
            ->with('position')
            ->with('userInfo')
            ->orderBy('post_date', 'DESC')
            ->get();
        }else if($type == 'announce') {
            return Post::where(function($q) use($id, $keyword, $category) {
                $q->where("post_name", "LIKE", "%{$keyword}%");
                $q->where('position_id', $category);
                $q->whereIntegerInRaw("id", $id);
            })
            ->orWhere(function($q) use($id, $keyword, $category) {
                $q->where("post_title", "LIKE", "%{$keyword}%");
                $q->where('position_id', $category);
                $q->whereIntegerInRaw("id", $id);
            })
            ->where('post_status', '1')
            ->with('relate', function($q) {
                $q->with('productInfo');
                $q->orderBy('sort_orders', 'asc');
                $q->limit(3);
                $q->get();
            })
            ->withCount('visitlog')
            ->withCount(['like' => function($q) {
                $q->where('type', 'like');
            }])
            ->with('position')
            ->with('userInfo')
            ->orderBy('post_priority', 'DESC')
            ->orderBy('post_sort','ASC')
            ->orderBy('id','DESC')
            ->get();
        }


        return Post::where(function($q) use($id, $keyword, $category) {
            $q->where("post_name", "LIKE", "%{$keyword}%");
            $q->where('position_id', $category);
            $q->whereIntegerInRaw("id", $id);
        })
        ->orWhere(function($q) use($id, $keyword, $category) {
            $q->where("post_title", "LIKE", "%{$keyword}%");
            $q->where('position_id', $category);
            $q->whereIntegerInRaw("id", $id);
        })
        ->where('post_status', '1')
        ->with('relate', function($q) {
            $q->with('productInfo');
            $q->orderBy('sort_orders', 'asc');
            $q->limit(3);
            $q->get();
        })
        ->withCount('visitlog')
        ->withCount(['like' => function($q) {
            $q->where('type', 'like');
        }])
        ->with('position')
        ->with('userInfo')
        ->orderBy('post_sort','ASC')
        ->orderBy('id','DESC')
        ->get();
    }


    public function listCategorySearchMyknowledge($id, $keyword) {

        $user = \Auth::guard('backpack')->user();

        return Post::where(function($q) use($id, $keyword, $user) {
            $q->where("post_name", "LIKE", "%{$keyword}%");
            $q->whereIntegerInRaw("id", $id);
            $q->where('created_by', $user->id);
        })
        ->orWhere(function($q) use($id, $keyword, $user) {
            $q->where("post_title", "LIKE", "%{$keyword}%");
            $q->whereIntegerInRaw("id", $id);
            $q->where('created_by', $user->id);
        })
        ->where('post_status', '1')
        ->orderBy('post_priority', 'DESC')
        ->orderBy('post_sort','ASC')
        ->orderBy('id','DESC')
        ->get();
    }
    

    public function find($id) {
        $data = Post::where('id', $id)
            ->where('post_status', '1')
            ->with('relate', function($q) {
                $q->with('productInfo');
                // $q->where('type_tb', 'posts');
                // $q->where('type', 'posts_relate');
                $q->orderBy('sort_orders', 'asc');
                $q->limit(3);
                $q->get();
            })
            ->with('userInfo')
            ->withCount(['like' => function($q) {
                $q->where('type', 'like');
            }])
            ->first();

            $ids = [0];

        if(!empty($data)) {
            //* GET TAGS IN DATA 
            $tags = json_decode($data->post_tags);
                    
            if($tags != null && count($tags) > 0) {
                //* QUERY DATA IN TAGS 
                foreach($tags as $tag) {
                    $dataTags = Post::where('post_tags', 'LIKE', "%{$tag}%")->where('post_status', '1')->get()->toArray();
                    $dataTagsId = (count($dataTags) > 0) ? array_column($dataTags, 'id') : [0];
                    $ids = array_merge($ids, $dataTagsId);
                }
            }

            //* MAP DATA_TAGS TO MAIN DATA
            $postTag = Post::whereIntegerInRaw('id', $ids)->where('id','!=', $id)->limit(3)->get();
            $section = $this->getSectionInDepartment();

            $postSection = Post::whereIntegerInRaw('position_id', $section)->limit(3)->get();
                    
            $data->RelateTags = $postTag;
            $data->RelateSection = $postSection;

        }
       

        

        return $data;
    }

    public function findPostName($name) {

        if(is_numeric($name)) {
            $data = Post::where('id', $name)
            ->where('post_status', '1')
            ->with('relate', function($q) {
                $q->with('productInfo');
                // $q->where('type_tb', 'posts');
                // $q->where('type', 'posts_relate');
                $q->orderBy('sort_orders', 'asc');
                $q->limit(3);
                $q->get();
            })
            ->with('userInfo')
            ->withCount(['like' => function($q) {
                $q->where('type', 'like');
            }])
            ->first();
        }else {
            $data = Post::where('post_name', $name)
            ->where('post_status', '1')
            ->with('relate', function($q) {
                $q->with('productInfo');
                // $q->where('type_tb', 'posts');
                // $q->where('type', 'posts_relate');
                $q->orderBy('sort_orders', 'asc');
                $q->limit(3);
                $q->get();
            })
            ->with('userInfo')
            ->withCount(['like' => function($q) {
                $q->where('type', 'like');
            }])
            ->first();
        }

            $ids = [0];
       

        if(!empty($data)) {
            $id = $data->id;
            //* GET TAGS IN DATA 
            $tags = json_decode($data->post_tags);
                    
            if($tags != null && count($tags) > 0) {
                //* QUERY DATA IN TAGS 
                foreach($tags as $tag) {
                    $dataTags = Post::where('post_tags', 'LIKE', "%{$tag}%")->where('post_status', '1')->get()->toArray();
                    $dataTagsId = (count($dataTags) > 0) ? array_column($dataTags, 'id') : [0];
                    $ids = array_merge($ids, $dataTagsId);
                }
            }

            //* MAP DATA_TAGS TO MAIN DATA
            $postTag = Post::whereIntegerInRaw('id', $ids)->where('id','!=', $id)->limit(3)->get();
            $section = $this->getSectionInDepartment();

            $postSection = Post::whereIntegerInRaw('position_id', $section)->limit(3)->get();
                    
            $data->RelateTags = $postTag;
            $data->RelateSection = $postSection;

        }
        return $data;
    }

    public function listCourseRecommend($id) {
        return Post::whereIntegerInRaw('id', $id)->orderBy('post_sort','ASC')
                ->where('post_status', '1')
                ->with('position')
                ->withCount('visitlog')
                ->withCount(['like' => function($q) {
                    $q->where('type', 'like');
                }])
                ->with('userInfo')    
                ->orderBy('id','DESC')
                ->limit(3)
                ->get();
    }

    public function listPostFNLimit($id, $limit) {
        return Post::whereIntegerInRaw('id', $id)
                ->where('post_status', '1')
                ->with('position')
                ->withCount('visitlog')
                ->withCount(['like' => function($q) {
                    $q->where('type', 'like');
                }])
                ->with('userInfo')
                ->orderBy('post_sort','ASC')
                ->orderBy('id','DESC')
                ->limit($limit)
                ->get();
    }

    public function listMyKnowledge($id, $limit) {
        $section = $this->getSectionInDepartment();
        
        return Post::whereIntegerInRaw('id', $id)
                ->whereIntegerInRaw('position_id', $section)
                ->where('post_status', '1')
                ->with('position')
                ->withCount('visitlog')
                // ->withCount(['like' => function($q) {
                //     $q->where('type', 'like');
                // }])
                ->with('userInfo')
                ->orderBy('post_sort','ASC')
                ->orderBy('id','DESC')
                ->limit($limit)
                ->get();
    }

    public function listArray($id) {
        return Post::whereIntegerInRaw('id', $id)
                ->with('position')
                ->withCount('visitlog')
                ->withCount(['like' => function($q) {
                    $q->where('type', 'like');
                }])
                ->with('userInfo')
                ->orderBy('post_sort','ASC')
                ->orderBy('id','DESC')
                ->get()->toArray();
    }

    public function listArraySearch($id, $keyword) {
        return Post::where(function($q) use($id, $keyword) {
                    $q->where("post_name", "LIKE", "%{$keyword}%");
                    $q->whereIntegerInRaw("id", $id);
                })
                ->orWhere(function($q) use($id, $keyword) {
                    $q->where("post_title", "LIKE", "%{$keyword}%");
                    $q->whereIntegerInRaw("id", $id);
                })
                ->where('post_status', '1')
                ->with('position')
                ->withCount('visitlog')
                ->withCount(['like' => function($q) {
                    $q->where('type', 'like');
                }])
                ->with('userInfo')
                ->orderBy('post_sort','ASC')
                ->orderBy('id','DESC')
                ->get()->toArray();
    }

    public function create($data) {
        return Post::insert($data);
    }

    public function createGetId($data) {
        return Post::insertGetId($data);
    }

    public function searchTags($tag) {
        return  Post::where('post_tags', 'LIKE', "%{$tag}%")
        ->where('post_status', '1')
                    ->with('relate', function($q) {
                        $q->with('productInfo');
                        // $q->where('type_tb', 'posts');
                        // $q->where('type', 'posts_relate');
                        $q->orderBy('sort_orders', 'asc');
                        $q->limit(3);
                        $q->get();
                    })
                    ->with('position')
                    ->with('visitlog')
                    ->with('userInfo')
                    ->orderBy('post_priority', 'DESC')
                    ->orderBy('post_sort','ASC')
                    ->orderBy('id','DESC')
                    ->get();
    }


    public function searchResult($keyword, $position = '') {
        if($position != '') {
            return Post::where(function($q) use($keyword, $position) {
                $q->where("post_name", "LIKE", "%{$keyword}%");
                $q->where('position_id', $position);
            })
            ->orWhere(function($q) use($keyword, $position) {
                $q->where("post_title", "LIKE", "%{$keyword}%");
                $q->where('position_id', $position);
            })
            ->where('post_status', '1')
            ->orderBy('id','DESC')
            ->get();
        }


        return Post::where(function($q) use($keyword) {
            $q->where("post_name", "LIKE", "%{$keyword}%");
        })
        ->orWhere(function($q) use($keyword) {
            $q->where("post_title", "LIKE", "%{$keyword}%");
        })
        ->orderBy('id','DESC')
        ->get();
    }

    public function searchTagResult($keywordTag, $position = "") {
        if($position != "") {
            return Post::where(function($q) use($keywordTag, $position) {
                $q->where("post_tags", "LIKE", "%{$keywordTag}%");
                $q->where('position_id', $position);
            })
            ->where('post_status', '1')
            ->orderBy('id','DESC')
            ->get();
        }

        return Post::where(function($q) use($keywordTag) {
            $q->where("post_tags", "LIKE", "%{$keywordTag}%");
        })
        ->orderBy('id','DESC')
        ->get();

    }


    public function getApiPostInAllId($ids) {
        return Post::select('id', 'post_title', 'post_excerpt', 'post_content', 'post_status', 'post_date', 'post_tags')
                ->whereIntegerInRaw('id', $ids)
                ->where('post_status', '1')
                ->limit(20)
                ->orderBy('id','DESC')
                ->get();
    }

    public function getApiPostDetail($id) {
        return Post::select('id', 'post_title', 'post_excerpt', 'post_content', 'post_status', 'post_date', 'post_tags', 'created_by', 'post_priority', 'position_id')
                ->where('id', $id)
                ->where('post_status', '1')
                ->get();
    }


    public function listCalendarEvent($id, $date) {
        return Post::select('id','post_name','post_title','start_date','end_date')
                ->whereIntegerInRaw('id', $id)
                ->whereDate('start_date', $date)
                ->where('post_status', '1')
                ->orderBy('post_sort','ASC')
                ->orderBy('id','DESC')
                ->get();
    }


    public function listFAQ($id) {
        return  Post::whereIntegerInRaw('id', $id)
        ->where('post_status', '1')
                ->orderBy('post_sort','ASC')
                ->orderBy('id','DESC')
                ->get();
    }

    public function listFAQSearch($id, $keyword, $category = "") {
        
        if($category != "") {
           
            return Post::where(function($q) use($id, $keyword, $category) {
                $q->where("post_content", "LIKE", "%{$keyword}%");
                $q->where('position_id', $category);
                $q->whereIntegerInRaw("id", $id);
            })
            ->orWhere(function($q) use($id, $keyword, $category) {
                $q->where("post_title", "LIKE", "%{$keyword}%");
                $q->where('position_id', $category);
                $q->whereIntegerInRaw("id", $id);
            })
            ->where('post_status', '1')
            ->orderBy('post_sort','ASC')
            ->orderBy('id','DESC')
            ->get();
        }

        return Post::where(function($q) use($id, $keyword) {
            $q->where("post_content", "LIKE", "%{$keyword}%");
            $q->whereIntegerInRaw("id", $id);
        })
        ->orWhere(function($q) use($id, $keyword) {
            $q->where("post_title", "LIKE", "%{$keyword}%");
            $q->whereIntegerInRaw("id", $id);
        })
        ->where('post_status', '1')
        ->orderBy('post_sort','ASC')
        ->orderBy('id','DESC')
        ->get();
    }

    function getSectionInDepartment() {
        $user = \Auth::guard('backpack')->user();
        $configSection = CoreConfig::where('id', $user->position_id)->first();
        $sectionInDepartment = CoreConfig::where('parent_id', $configSection->parent_id)->get()->toArray();
        $sectionInDepartmentId = (!empty($sectionInDepartment)) ? array_column($sectionInDepartment, 'id') : [0];
        return $sectionInDepartmentId;
    }

    public function findMail($id) {
        return Post::find($id);
    }


    
    
}