<?php 

namespace App\Repository;

use App\Models\CoreConfig;

class CoreConfigRepository
{
    public function listOrganization($id) {
        return CoreConfig::where('group','office')
                ->where('category', 'department')
                ->where('parent_id', $id)
                ->with('section', function($q) {
                    $q->where('status', '1');
                    $q->where('category', 'section');
                })
                ->where('status', '1')
                ->get();
    }

    public function list($id) {
        return CoreConfig::find($id);
    }

    public function listGroupAndCategory($group, $category) {
        return CoreConfig::where('group', $group)
                ->where('category', $category)
                ->get();
    }

    public function listPhoneDirectory($request) {

        if($request->search) {
            $search = $request->search;
            if($request->category && $request->category != '') {
                $data = CoreConfig::where('group', 'phone_directory')
                        ->where('value', 'LIKE', "%{$search}%")
                        ->where('category', $request->category)
                        ->with('phoneDerectorySection')
                        ->orderBy('level','ASC')
                        ->get()->toArray();
            }else {
                $data = CoreConfig::where('group', 'phone_directory')->where('value', 'LIKE', "%{$search}%")->where('category','!=', '')->with('phoneDerectorySection')->orderBy('level','ASC')->get()->toArray();
            }
           
        }else {
            $data = CoreConfig::where('group', 'phone_directory')
                        ->where('category','!=', '')
                        ->with('phoneDerectorySection')->orderBy('level','ASC')->get()->toArray();
            
        }
        $collectData = collect($data);

        return $collectData->groupBy('category');
    }

    public function listPageTitle($group, $category, $value) {
        return CoreConfig::where('group', $group)
                ->where('category', $category)
                ->where('value', $value)
                ->first();
    }


    public function listGroupAndCategoryHomePage($group, $category, $sortOrder = false) {


        if($sortOrder) {

            return CoreConfig::where('group', $group)
                    ->where('category', $category)
                    ->where('status', '1')
                    ->orderBy('scope', 'ASC')
                    ->get();
        }

        return CoreConfig::where('group', $group)
                ->where('category', $category)
                ->where('status', '1')
                ->get();
    }

    public function getTopMenu() {
        return CoreConfig::where('group', 'mainmenu')
                ->with(['subMenu' => function($q) {
                    $q->where('status', '1');
                    $q->orderBy('scope', 'ASC');
                    $q->with(['subMenu' => function($q2) {
                        $q2->where('status', '1');
                        $q2->orderBy('scope', 'ASC');
                    }]);
                }])
                ->where('status', '1')
                ->where('class', NULL)
                ->where('category', 'top_menu')
                ->orderBy('scope', 'ASC')
                ->get();
    }

    public function getFooter() {
        $data['footerTitle'] = CoreConfig::where('group','footpage')
                                ->where('category','title')
                                ->where('status','1')
                                ->first();

        $data['footerAddress'] = CoreConfig::where('group','footpage')
                                    ->where('category','address')
                                    ->where('status','1')
                                    ->first();

        $data['footerCopyRight'] = CoreConfig::where('group','footpage')
                                    ->where('category','copyright')
                                    ->where('status','1')
                                    ->first(); 
        
        return $data;
    }

    public function getDepartment($sectionId) {
        return CoreConfig::where('group', 'office')
                ->where('category', 'department')
                ->where('id', $sectionId)
                ->first();
    }

    public function getSectionInDepartmentId($departmentId) {
        return CoreConfig::where('parent_id', $departmentId)
                ->where('group','office')
                ->where('category','section')
                ->get();
    }
}