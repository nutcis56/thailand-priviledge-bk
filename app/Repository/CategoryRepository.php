<?php 

namespace App\Repository;

use App\Models\Category;

class CategoryRepository
{
    public function findName($name) {
        return Category::where('name->th_TH', $name)->with('children')->first();
    }

    public function findType($name) {
        return Category::where('type', $name)->with('children')->get();
    }

    public function find($id) {
        return Category::find($id);
    }

    public function listType($name) {
        return Category::where('type', $name)->get();
    }
}