<?php

namespace App\Models;

class ConfigIconLink extends CoreConfig
{

    public static function boot()
    {
        parent::boot();

        $group = 'homepage';
        $category = 'member_link';
        
        static::addGlobalScope('group', function (\Illuminate\Database\Eloquent\Builder $builder) use ($group) {
            $builder->where('group', $group);
        });

        static::addGlobalScope('category', function (\Illuminate\Database\Eloquent\Builder $builder) use ($category) {
            $builder->where('category', $category);
        });

        self::saving(function($model) use ($group, $category)  {
            $model->group = $group;
            $model->category = $category;
        });
    }

}
