<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class ConfigThemeColor extends Model
{
    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatable = ['name', 'detail'];
    protected $table = 'core_configs';
    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('group', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('group', 'template');
        });

        static::addGlobalScope('category', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('category', 'website');
        });

        self::saving(function($model)  {
            $model->name = 'สีธีมหน้าเว็บไซต์';
            $model->group = 'template';
            $model->category = 'website';
            // $model->scope = 'post';
            $model->status = '1';
        });
    }
}
