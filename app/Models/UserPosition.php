<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class UserPosition extends Model
{
    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatable = ['name', 'detail'];
    protected $table = 'core_configs';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * Boot function
     */
    public static function boot()
    {
        parent::boot();

        $group = 'office';
        $class = 'user_position';

        static::addGlobalScope('group', function (\Illuminate\Database\Eloquent\Builder $builder) use ($group) {
            $builder->where('group', $group);
        });

        static::addGlobalScope('class', function (\Illuminate\Database\Eloquent\Builder $builder) use ($class) {
            $builder->where('class', $class);
        }); 

        self::saving(function($model) use ($group, $class) {
            $model->group = $group;
            $model->class = $class;
            $model->code = "{$class}-{$model->name}";

            if ($model->depth == 1) {
                $model->category = 'main';
            } else if ($model->depth == 2) {
                $model->category = 'department';
            } else if ($model->depth == 3) {
                $model->category = 'section';
            }
        });
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */


    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function parent() {
        return $this->belongsTo('App\Models\UserPosition', 'parent_id');
    }

    public function children() {
        return $this->hasMany('App\Models\UserPosition', 'parent_id');
    }

    public function depth2() {
        return $this->belongsTo('App\Models\UserPosition', 'parent_id')->where('depth', 2);
    }

    public function depth3() {
        return $this->hasMany('App\Models\UserPosition', 'parent_id')->where('depth', 3);
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
