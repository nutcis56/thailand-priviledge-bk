<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Backpack\CRUD\app\Models\Traits\CrudTrait; // <------------------------------- this one
use Spatie\Permission\Traits\HasRoles;// <---------------------- and this one

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;
    use CrudTrait; // <----- this
    use HasRoles; // <------ and this

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'session',
        'session_timeout'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        self::deleted(function($model) {
            \App\Models\UserInfo::where('user_id', $model->id)->delete();
        });
    }

    public function position() {
        return $this->belongsTo('App\Models\UserPosition', 'position_id');
    }

    public function userInfo() {
        return $this->hasOne('\App\Models\UserInfo', 'user_id');
    }

    /**
     * check this user are logged by another session and still not timeout
     * 
     * @return  Boolean
     */
    public function isLoggedInAnotherDevice() {
        $now = \Carbon\Carbon::now();
        if ($this->session && $this->session_timeout) {
            if (!$this->isSessionValid()) {
                $session_timeout = \Carbon\Carbon::parse($this->session_timeout);
                if ($now < $session_timeout) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Clear session time out
     * 
     */
    public function clearSessionTimeout() {
        $now = \Carbon\Carbon::now();
        $session_timeout = \Carbon\Carbon::parse($this->session_timeout);
        if ($now >= $session_timeout) {
            $this->session = null;
            $this->session_timeout = null;
            $this->save();
        }
    }

    /**
     * Chcek session matched with current session or not?
     * 
     * @return  Boolean
     */
    public function isSessionValid() {
        $session_id = request()->session()->getId();
        if ($session_id == $this->session || $this->session == null) {
            return true;
        }
        return false;
    }

    /**
     * Add new logged device by add new seesion to user table and set timeout
     * 
     * 
     */
    public function addLoggedInDevice() {
        $this->inCreaseSessionTimeout();
    }

    /**
     * +10min session timeout
     * 
     */
    public function inCreaseSessionTimeout() {
        $session_id = request()->session()->getId();
        $this->session = $session_id;
        $this->session_timeout = $this->generateNextSessionTimeoutDateTime();
        $this->save();
    }

    /**
     * generate session timeout +10 min.
     * 
     *  
     */
    private function generateNextSessionTimeoutDateTime() {
        return \Carbon\Carbon::now()->addMinutes(10);
    }

    /**
     * Clear Login session + session timeout
     * 
     * 
     */
    public function clearSession() {
        $this->session = null;
        $this->session_timeout = null;
        $this->save();
    }

}
