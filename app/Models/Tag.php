<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatable = ['name', 'detail'];
    protected $table = 'core_configs';
    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('group', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('group', 'tag');
        });

        static::addGlobalScope('category', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('category', 'tag');
        });

        static::addGlobalScope('scope', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('scope', 'post');
        });

        self::saving(function($model)  {
            $model->code = 'tag-'.$model->name;
            $model->group = 'tag';
            $model->category = 'tag';
            $model->scope = 'post';
            $model->status = '1';
        });
    }
}
