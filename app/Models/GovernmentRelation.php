<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class GovernmentRelation extends \App\Models\Post
{
    public static function boot()
    {
        parent::boot();

        $cat_name = 'Government Relation';
        $cat = \App\Models\Category::where('name', $cat_name)->first();
        if (!$cat) {
            $cat_id = \App\Models\Category::create([
                'name' => $cat_name,
                'type' => 'member_services',
                'status' => '1'
            ]);
        } else {
            $cat_id = $cat->id;
        }
        
        $post_ids = \App\Models\PostCate::where('cate_id', $cat_id)->pluck('post_id')->toArray();

        static::addGlobalScope('id', function (\Illuminate\Database\Eloquent\Builder $builder) use ($post_ids) {
            $builder->whereIn('id', $post_ids);
        }); 

        self::saved(function($model) use ($cat_id) {
            $exists = \App\Models\PostCate::where('post_id', $model->id)->where('cate_id', $cat_id)->get();
            if ($exists->count() == 0) {
                $model->category()->attach($cat_id);
            }
        });

        self::deleted(function($model) {
            \App\Models\PostCate::where('post_id', $model->id)->delete();
        });
    }
}