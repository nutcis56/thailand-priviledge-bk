<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class CoreConfig extends Model
{
    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatable = ['name', 'detail'];
    protected $table = 'core_configs';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    public function section()
    {
        return $this->hasMany('App\Models\CoreConfig', 'parent_id', 'id');
    }

    public function phoneDerectorySection()
    {
        return $this->hasOne('App\Models\CoreConfig', 'id', 'category');
    }


    public function phoneDerectorySection2()
    {
        return $this->belongsTo('App\Models\CoreConfig', 'category', 'id');
    }

    public function subMenu()
    {
        return $this->hasMany('App\Models\CoreConfig', 'class', 'code');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
   

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public static function boot()
    {
        parent::boot();
        // $routeName = \Request::route()->getName();
       
        if(\Request::route()->getPrefix() != 'api') {
            $user = \Auth::guard('backpack')->user();
            if ($user) {
                if(\Request::route()->getPrefix() == 'admin') {
                    static::addGlobalScope('group', function (\Illuminate\Database\Eloquent\Builder $builder){
                        $builder->where('group', '!=', 'phone_directory');
                    }); 
                }
            }
        }
    }
}
