<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RelateData extends Model
{
    use HasFactory;


    protected $table = 'relate_datas';

    

    public function productInfo() {
        return $this->hasOne('\App\Models\Post', 'id', 'post_id');
    }

    public function posts() {
        return $this->hasOne('\App\Models\Post', 'id', 'post_id');
    }
}
