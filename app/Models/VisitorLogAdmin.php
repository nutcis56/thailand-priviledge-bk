<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class VisitorLogAdmin extends Model
{
    use CrudTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'visitor_logs';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * Boot function
     */
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('scope', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('scope', 'admin');
        }); 
    }

    public function visitor() {
        return $this->belongsTo('App\Models\User', 'visitor_id');
    }

}
