<?php

namespace App\Models;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserBookStack extends Model
{
    use HasFactory;
    use HasSlug;

    protected $connection = 'mysql_book_stack';
    protected $table = 'users';
    protected $fillable = [
      'name',
      'email',
      'password',
      'external_auth_id',
      'slug',
    ];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }
}
