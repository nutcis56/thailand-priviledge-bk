<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class CourseFile extends Model
{
    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    public $translatable = ['name', 'detail', 'excerpt'];
    protected $table = 'documents';
    // protected $primaryKey = 'id';
    public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    public static function boot()
    {
        parent::boot();

        $doc_type = "course";
        $group = "course_attach_file";
    

        static::addGlobalScope('doc_type', function (\Illuminate\Database\Eloquent\Builder $builder) use ($doc_type) {
            $builder->where('doc_type', $doc_type);
        }); 

        static::addGlobalScope('group', function (\Illuminate\Database\Eloquent\Builder $builder) use ($group) {
            $builder->where('group', $group);
        }); 

        if (isset($_REQUEST['doc_tb_id'])) {
            $doc_tb_id = $_REQUEST['doc_tb_id'];
            static::addGlobalScope('doc_tb_id', function (\Illuminate\Database\Eloquent\Builder $builder) use ($doc_tb_id) {
                $builder->where('doc_tb_id', $doc_tb_id);
            });
        }

        self::saving(function($model) use ($doc_type, $group) {
            $model->doc_type = $doc_type;
            $model->group = $group;
        });
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
