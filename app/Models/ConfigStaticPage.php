<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class ConfigStaticPage extends Model
{
    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatable = ['name', 'detail'];
    protected $table = 'core_configs';
    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('group', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('group', 'static-page');
        });

        static::addGlobalScope('category', function (\Illuminate\Database\Eloquent\Builder $builder) {
            // $builder->where('category', 'about-us');
        });

        self::saving(function($model)  {
            $model->group = 'static-page';
            // $model->category = 'about-us';
            // $model->scope = 'post';
            $model->status = '1';
        });
    }
}
