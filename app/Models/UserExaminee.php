<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserExaminee extends Model
{
    use HasFactory;

    protected $connection = 'mysql_examinee';
    protected $table = 'users';
    protected $fillable = [
      'name',
      'email',
      'password',
    ];
}
