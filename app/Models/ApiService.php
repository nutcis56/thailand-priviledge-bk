<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class ApiService extends Model
{

    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;
    
    public $translatable = ['post_content'];
    protected $table = 'posts';
    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();
       
        static::addGlobalScope('posts', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('post_name', 'api-service');
        }); 
    }
}
