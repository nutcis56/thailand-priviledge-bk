<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;

class ConfigMenu extends CoreConfig
{

    public static function boot()
    {
        parent::boot();

        $group = 'mainmenu';
        
        static::addGlobalScope('group', function (\Illuminate\Database\Eloquent\Builder $builder) use ($group) {
            $builder->where('group', $group);
        });

        self::saving(function($model) use ($group)  {
            $model->group = $group;
            $model->level = 0;
            
            if ($model->class) {
                $parent = \App\Models\ConfigMenu::where('code',$model->class)->first();
                if ($parent) {
                    $parent_level = intval($parent->level);
                    $model->level = $parent_level + 1;
                }
            }

            if(isset($_REQUEST['class']) && $_REQUEST['class'] != "") {
                $parent = \App\Models\ConfigMenu::where('code',$model->class)->first();
                $model->class = $parent->code;
            }
        });

        self::saved(function($model)  {
            // DB::select();
            DB::select(
                DB::raw("
                            UPDATE
                                core_configs
                            SET
                                code = {$model->id}
                            WHERE
                                id = {$model->id}
                        ")
            );
        });
    }

    public function parent() {
        return $this->belongsTo('App\Models\ConfigMenu', 'class');
    }

    public function children() {
        return $this->hasMany('App\Models\ConfigMenu', 'class');
    }

    public static function getHTMLMenu($menus, $parent_id = null, $level = 0) {

        $route = \Route::current();
        // $name = \Route::currentRouteName();
        $action = \Route::currentRouteAction();

        $current_category_slug = '';

        // $route_index = $menu->slug.'.lists-by-category-slug.index';

        // if ($name == $route_index) {
        //     $parameters = $route->parameters();
        //     if (array_key_exists('category_slug', $parameters)) {
        //         $current_category_slug = $parameters['category_slug'];
        //     }
        // }

        $ret = '<ul>';
        foreach ($menus as $index => $menu) {
            if ($menu['class'] == $parent_id) {
                if ($menu->children->count()) {
                    $ret .= '<li class="sbhassub level-'.$level.'"><a href="#">' . $menu['name'] . '</a>';
                } else {
                    $active =  $current_category_slug == $menu->link ? 'active' : '';
                    $url = $menu->link;
                    $ret .= "<li class='{$active} level-{$level}'><a href='{$url}'>" . $menu['name'] . "</a>";
                }
                
                $sub = \App\Models\ConfigMenu::getHTMLMenu($menus, $menu['id'], $level+1);
                if($sub != '<ul></ul>')
                    $ret .= $sub;
                $ret .= '</li>';
            }
        }

        return $ret . '</ul>';
    }

}
