<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostDashBoard extends Model
{
    use HasFactory;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatable = ['post_title', 'post_content', 'post_excerpt', 'post_tags'];
    protected $table = 'posts';
}
