<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class PhoneDirectory extends Model
{
    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatable = ['name', 'detail'];
    protected $table = 'core_configs';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * Boot function
     */
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('core_config', function (\Illuminate\Database\Eloquent\Builder $builder) {
            $builder->where('group','phone_directory');
        }); 

        static::creating(function ($coreConfig) {
            $model = new CoreConfig();
            $lastedData = $model->latest('id')->first();
            $lastedIdPush = $lastedData->id + 1;
            $coreConfig->code = "phone_directory_{$lastedIdPush}";
            $coreConfig->name = $_REQUEST['name'];
            $setValue = [
                'name' => $_REQUEST['name'],
                'mobile' => $_REQUEST['mobile'],
                'phone1' => $_REQUEST['phone1'],
                'phone2' => $_REQUEST['phone2'],
                'email' => $_REQUEST['email'],
            ];
            $coreConfig->value = \json_encode($setValue, JSON_UNESCAPED_UNICODE);
            $coreConfig->detail = "สมุดโทรศัพท์ภายในสำนักงาน";
            $coreConfig->class = $_REQUEST['department'];
            $coreConfig->level = $_REQUEST['sort_order'];
            $coreConfig->group ="phone_directory";
            $coreConfig->category = $_REQUEST['section'];
            $coreConfig->status = "1";
        });
       
        static::saving(function($coreConfig){
            $coreConfig->name = $_REQUEST['name'];
            $setValue = [
                'name' => $_REQUEST['name'],
                'mobile' => $_REQUEST['mobile'],
                'phone1' => $_REQUEST['phone1'],
                'phone2' => $_REQUEST['phone2'],
                'email' => $_REQUEST['email'],
            ];
            
            $coreConfig->value = \json_encode($setValue, JSON_UNESCAPED_UNICODE);
            $coreConfig->class = $_REQUEST['department'];
            $coreConfig->level = $_REQUEST['sort_order'];
            $coreConfig->category = $_REQUEST['section'];
        });
       
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function department()
    {
        return $this->belongsTo('App\Models\CoreConfig', 'class', 'id');
    }

    public function section()
    {
        return $this->belongsTo('App\Models\CoreConfig', 'category', 'id');
    }
  

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
