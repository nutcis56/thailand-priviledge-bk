<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class StaticAboutUsPage extends Model
{
    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatable = ['post_title', 'post_content'];
    protected $table = 'posts';
    protected $guarded = ['id'];

    public static function boot()
    {
        parent::boot();

        $type = 'aboutus';
        $cat_ids = \App\Models\Category::where('type', $type)->pluck('id')->toArray();
        if (!$cat_ids) {
            $cat_ids = [];
        }
        
        $post_ids = \App\Models\PostCate::whereIn('cate_id', $cat_ids)->pluck('post_id')->toArray();

        static::addGlobalScope('id', function (\Illuminate\Database\Eloquent\Builder $builder) use ($post_ids) {
            $builder->whereIn('id', $post_ids);
        });

        self::creating(function($model) {
            $user = \Auth::guard('backpack')->user();
            $model->created_by = $user->id;
        });

        self::saving(function($model) {
            $user = \Auth::guard('backpack')->user();
            $model->updated_by = $user->id;
        });
    }
}
