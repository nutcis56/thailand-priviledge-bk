<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class Course extends \App\Models\Post
{
    public static function boot()
    {
        parent::boot();

        $cat_id = 11;
        $post_ids = \App\Models\PostCate::where('cate_id', $cat_id)->pluck('post_id')->toArray();

        static::addGlobalScope('id', function (\Illuminate\Database\Eloquent\Builder $builder) use ($post_ids) {
            $builder->whereIn('id', $post_ids);
        }); 


        self::saving(function($model) {
            
            $setAttribute = [
                'name' => (isset($_REQUEST['post_lecturer']) ? $_REQUEST['post_lecturer'] : ''),
                'time' => (isset($_REQUEST['post_lecturer_time']) ? $_REQUEST['post_lecturer_time'] : ''),
                'file' => (isset($_REQUEST['post_lecturer_file']) ? $_REQUEST['post_lecturer_file'] : ''),
                'pre_test' => (isset($_REQUEST['pre_test']) ? $_REQUEST['pre_test'] : ''),
                'post_test' => (isset($_REQUEST['post_test']) ? $_REQUEST['post_test'] : ''),
            ];
            $jsonData = json_encode($setAttribute, JSON_UNESCAPED_UNICODE);
            $model->post_attribute = $jsonData;
        });


        self::updating(function($model) {
            
            $setAttribute = [
                'name' => (isset($_REQUEST['post_lecturer']) ? $_REQUEST['post_lecturer'] : ''),
                'time' => (isset($_REQUEST['post_lecturer_time']) ? $_REQUEST['post_lecturer_time'] : ''),
                'file' => (isset($_REQUEST['post_lecturer_file']) ? $_REQUEST['post_lecturer_file'] : ''),
                'pre_test' => (isset($_REQUEST['pre_test']) ? $_REQUEST['pre_test'] : ''),
                'post_test' => (isset($_REQUEST['post_test']) ? $_REQUEST['post_test'] : ''),
            ];
            $jsonData = json_encode($setAttribute, JSON_UNESCAPED_UNICODE);
            $model->post_attribute = $jsonData;
        });

        self::saved(function($model) use ($cat_id) {
            
            $exists = \App\Models\PostCate::where('post_id', $model->id)->where('cate_id', $cat_id)->get();
            if ($exists->count() == 0) {
                $model->category()->attach($cat_id);
            }
        });

        self::deleted(function($model) {
            \App\Models\PostCate::where('post_id', $model->id)->delete();
        });
    }

    /**
     * Get course files
     * 
     */
    public function courseFiles() {
        return $this->hasMany('\App\Models\CourseFile', 'doc_tb_id', 'id');
    }

    /**
     * Get course lessons
     * 
     */
    public function courseLessons() {
        return $this->hasMany('\App\Models\CourseLesson', 'doc_tb_id', 'id');
    }
}
