<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class OChartMain extends Model
{
    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatable = ['name', 'detail'];
    protected $table = 'core_configs';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /**
     * Boot function
     */
    public static function boot()
    {
        parent::boot();

        $group = 'office';
        $category = 'main';
        $class = 'user_position';

        static::addGlobalScope('group', function (\Illuminate\Database\Eloquent\Builder $builder) use ($group) {
            $builder->where('group', $group);
        }); 

        static::addGlobalScope('category', function (\Illuminate\Database\Eloquent\Builder $builder) use ($category) {
            $builder->where('category', $category);
        }); 

        static::addGlobalScope('class', function (\Illuminate\Database\Eloquent\Builder $builder) use ($class) {
            $builder->where('class', $class);
        }); 

        self::saving(function($model) use ($group, $class, $category) {
            $model->group = $group;
            $model->category = $category;
            $model->class = $class;
            $model->code = "{$class}-{$model->name}";
            $model->status = "1";
        });
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
