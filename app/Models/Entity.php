<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

abstract class Entity extends Model
{
    public static function boot()
    {
        parent::boot();

        self::created(function ($model) {
            $Visitor = new \App\Helpers\Visitor(request(), config('visitor'));
            $Visitor->visitAdmin($model, 'Created');
        });

        self::saved(function($model) {
            $Visitor = new \App\Helpers\Visitor(request(), config('visitor'));
            $Visitor->visitAdmin($model, 'Updated');
        });

        self::deleted(function($model) {
            $Visitor = new \App\Helpers\Visitor(request(), config('visitor'));
            $Visitor->visitAdmin($model, 'Deleted');
        });
    }
}   
