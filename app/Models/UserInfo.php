<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserInfo extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'description',
        'physical_delivery_office_name',
        'telephone_number',
        'department',
        'company',
        'sam_accountname',
    ];
}
