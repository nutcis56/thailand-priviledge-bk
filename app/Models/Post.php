<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use App\Models\category;
class Post extends \App\Models\Entity
{
    use CrudTrait;
    use \Backpack\CRUD\app\Models\Traits\SpatieTranslatable\HasTranslations;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */
    public $translatable = ['post_title', 'post_content', 'post_excerpt', 'post_tags'];
    protected $table = 'posts';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = [
        'files'     =>  'array',
        'post_tags'     =>  'array'
    ];

    public static function boot()
    {
        parent::boot();
        // $routeName = \Request::route()->getName();
       
        if(\Request::route()->getPrefix() != 'api') {
            $user = \Auth::guard('backpack')->user();
            if ($user && !$user->hasRole('Administrator')) {
                $position_id = $user->position_id;
                if(\Request::route()->getPrefix() == 'admin') {
                    static::addGlobalScope('position_id', function (\Illuminate\Database\Eloquent\Builder $builder) use ($position_id) {
                        $builder->where('position_id', $position_id);
                    }); 
                }
            }
        }
        

        self::creating(function($model) {
            $user = \Auth::guard('backpack')->user();
            $model->created_by = $user->id;
        });

        self::saving(function($model) {
            $user = \Auth::guard('backpack')->user();
            $model->updated_by = $user->id;
        });

        self::saved(function($model) {
            if (isset($_REQUEST['releated_with_ids'])) {
                $releated_with_ids = $_REQUEST['releated_with_ids'];
                $releated_with_ids_save_data = [

                ];
                $model->relatePosts()->syncWithPivotValues($releated_with_ids, [
                    'type' => get_class($model)
                ]);
            }
        });

        self::deleted(function($model) {

        });
    }

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function category() {
        return $this->belongsToMany('\App\Models\Category', 'post_cates', 'post_id', 'cate_id');
    }

    public function categoryBlog() {
        return $this->belongsToMany('\App\Models\CategoryBlog', 'post_cates', 'post_id', 'cate_id');
    }

    public function categoryKnowLedge() {
        return $this->belongsToMany('\App\Models\Category', 'post_cates', 'post_id', 'cate_id')->wherePivot("cate_id", 1);
    }

    public function relate() {
        return $this->hasMany('\App\Models\RelateData', 'type_id', 'id');
    }

    public function relatePosts() {
        return $this->belongsToMany('\App\Models\Post', 'relate_datas', 'type_id', 'post_id')->where('type', get_class($this))->withTimestamps();
    }

    public function position() {
        return $this->belongsTo('\App\Models\CoreConfig', 'position_id', 'id');
    }

    public function userInfo() {
        return $this->belongsTo('\App\Models\User', 'created_by', 'id');
    }

    public function visitlog() {
        return $this->hasMany('\App\Models\VisitorLog', 'visitable_id', 'id')->where('method', 'GET');
    }

    public function like() {
        return $this->hasMany('\App\Models\WishList', 'post_id', 'id');
    }

    // public function visitlogCount() {
    //     return $this->hasMany('\App\Models\VisitorLog', 'visitable_id', 'id');
    // }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
