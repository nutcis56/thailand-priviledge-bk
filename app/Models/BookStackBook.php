<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

class BookStackBook extends Model
{
    protected $connection = 'mysql_book_stack';
    protected $table = 'books';
    protected $fillable = [
        'name',
        'slug',
        'description',
        'created_by',
        'updated_by',
        'image_id',
        'owned_by'
    ];
}
