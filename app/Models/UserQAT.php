<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserQAT extends Model
{
    use HasFactory;

    protected $connection = 'mysql_qat';
    protected $table = 'sq_user';
    protected $fillable = [
      'username',
      'name',
      'email',
      'password',
      'full_name',
      'account_type_id',
      'group_ids',
      'user_token'
    ];
    public $timestamps = false;
}
