<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Hash;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Http;

class SyncUserPydioOtherServer extends SyncUserAbstract
{
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($credentials, $user)
    {
        $active = config('app.ldap_sync_to_third_party');
        if (!$active) {
            return false;
        }

        $response = Http::post(config('app.sync_to_pydio_km_url') . '/api/sync-pydio-user-to-another-server/send-data', [
            'email' => $credentials['email'],
            'password' => $credentials['password'],
        ]);
    }

}
