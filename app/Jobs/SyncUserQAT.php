<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Hash;

class SyncUserQAT extends SyncUserAbstract
{

    public $user_model = '\App\Models\UserQAT';

    /**
     * Get Password To save Data
     * 
     */
    public function getPassword() {
        return md5($this->password);
    }

    /**
     * Prepare Data to save
     * 
     */
    public function prepareDataToSave() {
        $email_e = explode('@', $this->email);
        $temp = [
            'username' => $email_e[0],
            'password' => $this->getPassword(),
            'email' => $this->email,
            'full_name' => $this->user->name,
            'account_type_id' => 1, // 1 for admin, 2 for user
            'group_ids' => 1,
            'user_token' => ''
        ];
        $this->data = $temp;
    }

}
