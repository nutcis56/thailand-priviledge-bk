<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Hash;

class SyncUserBookStack extends SyncUserAbstract
{

    public $user_model = '\App\Models\UserBookStack';

    /**
     * Get Password To save Data
     * 
     */
    public function getPassword() {
        return Hash::make($this->password);
    }

    /**
     * Prepare Data to save
     * 
     */
    public function prepareDataToSave() {
        $temp = [
            'name' => $this->user->name,
            'email' => $this->email,
            'password' => $this->getPassword(),
            'email_confirmed' => 1,
            'external_auth_id' => '',
        ];
        $this->data = $temp;
    }

}
