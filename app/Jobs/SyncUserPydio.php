<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Hash;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class SyncUserPydio extends SyncUserAbstract
{

    public $user_model = false;
    public $cells_path = 'C:/Users/Administrator/go/src/github.com/pydio/cells/cells.exe';

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $active = config('app.ldap_sync_to_third_party');
        if (!$active) {
            return false;
        }
        
        // $this->cells_path = config('app.pydio_path');
        $this->cells_path = explode(" ", config('app.pydio_path'));
        
        $exists = $this->getExists($this->email);
        
        if ($exists) {
            $this->updateUser(false, $this->data);
        } else {
            $this->newUser(false, $this->data);
        }
    }

    public function newUser($model, $data) {
        $process = new Process(array_merge($this->cells_path, ["admin", "user", "create", "--username={$data['email']}", "--password={$data['password']}"]));
        $process->run();
        
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $output = $process->getOutput();
        if ($output == "") {
            return true;
        }
        return false;
    }

    public function updateUser($model, $data) {
        $process = new Process(array_merge($this->cells_path, ["admin", "user", "set-pwd" , "--username={$data['email']}", "--password={$data['password']}"]));
        $process->run();
        
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $output = $process->getOutput();

        if (strpos($output, 'successfully updated')) {
            return true;
        }
        return false;
    }

    public function getExists($email) {
        $process = new Process(array_merge($this->cells_path, ["admin", "user", "search"]));
        $process->run();
        
        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $output = $process->getOutput();

        if (strpos($output, $email)) {
            return true;
        }

        return false;
    }

    /**
     * Get Password To save Data
     * 
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Prepare Data to save
     * 
     */
    public function prepareDataToSave() {
        $email_e = explode('@', $this->email);
        $temp = [
            'password' => $this->getPassword(),
            'email' => $this->email,
        ];
        $this->data = $temp;
    }

}
