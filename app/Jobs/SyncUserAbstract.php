<?php
namespace App\Jobs;
    
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Hash;

abstract class SyncUserAbstract implements ShouldQueue {

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user_model = '\App\Models\UserBookStack';
    public $user = false;
    protected $email = false;
    protected $password = false;
    protected $data = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($credentials, $user)
    {
        $active = config('app.ldap_sync_to_third_party');
        if (!$active) {
            return false;
        }
        $this->user = $user;
        $this->email = $credentials['email'];
        $this->password = $credentials['password'];
        $this->prepareDataToSave();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $active = config('app.ldap_sync_to_third_party');
        if (!$active) {
            return false;
        }
        
        $user = $this->getExists($this->email);
        
        if ($user) {
            $this->updateUser($user, $this->data);
        } else {
            $this->newUser($this->user_model, $this->data);
        }
    }

    /**
     * Get Password To save Data
     * 
     */
    public function getPassword() {
        return Hash::make($this->password);
    }

    /**
     * Get User if exists
     * 
     * @param   string  $email
     * @return  UserModel
     * 
     */
    public function getExists($email) {
        return app($this->user_model)::where('email', $email)->first();
    }

    /**
     * New User Function
     * 
     * @param   UserModel
     * 
     */
    public function newUser($model, $data) {
        app($model)::create($data);
    }

    /**
     * Update function
     * 
     * @param   UserModel
     * 
     */
    public function updateUser($model, $data) {
        $model->update($data);
    }

    /**
     * Prepare Data to save
     * 
     */
    public function prepareDataToSave() {
        $temp = [
            'name' => $this->user->name,
            'email' => $this->email,
            'password' => $this->getPassword(),
            'email_confirmed' => 1,
            'external_auth_id' => '',
        ];
        $this->data = $temp;
    }
    
}